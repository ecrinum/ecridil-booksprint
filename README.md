![CC0](https://licensebuttons.net/p/zero/1.0/88x31.png)

## Booksprint Ecridil2018

Bienvenue sur le répertoire du booksprint du colloque [Ecridil](http://ecridil.ex-situ.info/) qui s'est tenu à Montréal en mai 2018.  

Le livre issu de ce booksprint propose un index des notions-clés et des idées majeures qui ont traversé le colloque. Cet index est ouvert : nous vous invitons à enrichir et à discuter (en ligne) la «&nbsp;version 0&nbsp;» que nous proposons ici. Libre à vous de sélectionner, de ré-agencer, de discuter et d'augmenter à votre guise les différentes entrées de l'index afin de construire votre propre anthologie (imprimable). La branche `version1` a d'ores et déjà été ouverte pour vous permettre de construire une «&nbsp;version 1&nbsp;» - et nous espérons qu'il y en aura d'autres.

Pour en savoir plus sur le projet et sur le dispositif d'écriture, rendez-vous sur le site du colloque [ecridil.ex-situ.info/booksprint](http://ecridil.ex-situ.info/booksprint). En quelques lignes :

> Le livre que nous souhaitons éditer ne constituera pas des « actes » à proprement parler et ne redoublera pas non plus l'ensemble des archives (visuelles, sonores, textuelles) produites pendant le colloque. Il s'agira plutôt d'ouvrir nos problématiques, nos réflexions, nos corpus à un public élargi. Nous faisons donc l'hypothèse de produire un index du colloque, qui permettra de rendre compte des concepts et notions les plus importantes, tout en faisant dialoguer les différentes interventions.
>
> Outre l'objet livre qui en sera le résultat concret, ce booksprint est aussi une expérimentation questionnant le sujet du colloque. Nous adopterons ainsi des pratiques éditoriales tournées vers une édition collaborative, continue, susceptible de maintenir un dialogue permanent entre forme et contenu. Or les structures formelles de l'édition et de l'éditorialisation sont en premier lieu de nature scripturale. L'enjeu du dispositif de production réside alors dans l'articulation de ces deux écritures : la structure-code et le discours.



**Process**

![Process Booksprint Ecridil](img/ecridil-booksprint-process.png)

**Liste des outils utilisés :**

- framapad
- markdown
- pandoc
- html5
- css-print
- PagedMedia, une initiative opensource de Pagedmedia.org (nous en avons été les heureux alpha-testeurs  grâce à Julie Blanc, co-conceptrice et développeuse de l'outil et participante à ÉCRIDIL).

## Outputs

Le livre et ses sources sont disponibles sous licence [CC0 Public Domain](https://creativecommons.org/publicdomain/zero/1.0/deed.fr).

- [PDF en local](outputs/version0-ECRIDIL2018.pdf)
- [PDF sur Internet Archive](https://archive.org/download/version0-ECRIDIL2018/version0-ECRIDIL2018.pdf)
- [Notice sur OpenLibrary.org](https://openlibrary.org/books/OL26501805M/Version_0._Notes_sur_le_livre_num%C3%A9rique)
- [Version imprimée sur Lulu.com](http://www.lulu.com/product/23796442) (impression à la demande)


## Crédits

**Éditeurs**

René Audet, Julie Blanc, Renée Bourassa,
Louis-Olivier Brassard, Joana Casenave, Jeanne Hourez,
Ximena Miranda, Margot Mellet, Servanne Monjour,
Marie-Odile Paquin, Lilie Pons, Nicolas Sauret,
Jean-Louis Soubret, Emin Youssef.

**Participants à l'atelier d'idéation**

Emmanuel Château-Dutier, Oriane Deseilligny,
Antoine Fauchié, Jean-Michel Gascuel, Clémence
Jacquot, Arnaud Laborderie, Arthur Perret, Françoise
Paquienséguy, Dominique Raymond, Florence Rio,
Emmanuel Souchier, Elsa Tadier.

**Couverture**

Jean-Louis Soubret, Ximena Miranda

**Design graphique**

Julie Blanc, avec l’aide de Louis-Olivier Brassard
Ce livre a été conçu sur un navigateur web par CSS
et le script pagedjs (paged-media.org)

**Typographie**: IBM Plex
**Impression**: lulu.com
