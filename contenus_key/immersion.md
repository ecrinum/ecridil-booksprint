## Immersion

### Epron_diapo11 #immersion

### Laborderie_PDF&nbsp;:

Expérimentation en milieu scolaire, mise en œuvre de recherches sur le
web, l’entrée privilégiée dans le texte se fait par mode sonore.
Certains élèves ont véritablement surfé sur l’appli comme sur un site
web.

### Paquienséguy_phototwitter

Image de la période de questions via twitter
(https://twitter.com/hashtag/ecridil2018?f=tweets&vertical=default&src=hash)
avec Norwell et Paquienséguy. Peut-être accompagné du texte issu du pad
synthèse: «*Le format du CD-ROM culturel réinvesti permettrait,
peut-être plus que les autres, de créer au profit de l’artiste.*»
