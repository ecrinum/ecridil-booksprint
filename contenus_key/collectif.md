# Collectif {.chapter}
[Collectif]{.shorter}


## frag1 {.fragment .break-before-right}

### Epron_tweet {.main .main-twitter-tweet}
<blockquote class="twitter-tweet" data-cards="hidden" data-lang="fr"><p lang="fr" dir="ltr"><a href="https://twitter.com/hashtag/ECRIDIL2018?src=hash&amp;ref_src=twsrc%5Etfw">#ECRIDIL2018</a> Benoît Epron et Catherine Muller, l’abécédaire des <a href="https://twitter.com/hashtag/mondes?src=hash&amp;ref_src=twsrc%5Etfw">#mondes</a> lettrés, un outil d’<a href="https://twitter.com/hashtag/%C3%A9criture?src=hash&amp;ref_src=twsrc%5Etfw">#écriture</a> collaborative <a href="https://twitter.com/hashtag/savante?src=hash&amp;ref_src=twsrc%5Etfw">#savante</a><a href="https://twitter.com/hashtag/design?src=hash&amp;ref_src=twsrc%5Etfw">#design</a> <a href="https://twitter.com/hashtag/edition?src=hash&amp;ref_src=twsrc%5Etfw">#edition</a> <a href="https://twitter.com/hashtag/numerique?src=hash&amp;ref_src=twsrc%5Etfw">#numerique</a> <a href="https://t.co/650ab2TNLE">https://t.co/650ab2TNLE</a> <a href="https://t.co/Sh7HD1I96A">pic.twitter.com/Sh7HD1I96A</a></p>&mdash; Écritures numériques (@ENumeriques) <a href="https://twitter.com/ENumeriques/status/991042097531838464?ref_src=twsrc%5Etfw">30 avril 2018</a></blockquote>

### Epron_fiche {.glose .glose-texte}

À travers la création de nouvelles plateformes qui instituent des conditions d’écriture nouvelles, le numérique permet de bénéficier de la possibilité de l’écriture collaborative, de faire des liens entre les différentes entrées et d’insérer des hyperliens.

[écriturecollaborative]{.keyword}

## Frag2{.fragment}


### Paquienséguy_fiche {.main .main-texte}

Au sein des différentes catégories de _e-albums_ créés dans une logique d’accompagnement des grandes expositions, le format du CD-ROM culturel réinvesti permettrait, peut-être plus que les expositions à domicile et les mini-catalogues, d’innover au profit de l’artiste en misant sur les possibilités du numérique (l’affichage, la navigation, l’autonomie...). Françoise Paquienséguy pense ces _e-albums_ dans la continuité des CD culturels des années 1990&nbsp;: un passage de «&nbsp;l’hypermédia&nbsp;» au «&nbsp;transmédia&nbsp;».

### Paquienséguy_photo {.glose .glose-photo .polaroid}

![Françoise Paquienséguy](img/photos/paquienseguy2.jpg)


## Frag3 {.fragment}

### Git {.main}

#### slide {.main-slide}

![Systèmes de branches @SensPublic](img/slides/branches.jpg)

#### tweetSauret {.main-twitter-tweet}

<blockquote class="twitter-tweet" data-cards="hidden" data-lang="fr"><p lang="fr" dir="ltr">C’est aussi l’exemple de la chaine de <a href="https://twitter.com/SensPublic?ref_src=twsrc%5Etfw">@SensPublic</a> sur framagit/gitlab encore en bêta mais déjà opérationnelle (100 articles publiés). Ca mériterait un article collectif <a href="https://twitter.com/ENumeriques?ref_src=twsrc%5Etfw">@ENumeriques</a> <a href="https://twitter.com/servanne_m?ref_src=twsrc%5Etfw">@servanne_m</a> <a href="https://twitter.com/monterosato?ref_src=twsrc%5Etfw">@monterosato</a> <a href="https://twitter.com/hashtag/ecridil2018?src=hash&amp;ref_src=twsrc%5Etfw">#ecridil2018</a> <a href="https://t.co/DFNpW4U9vO">pic.twitter.com/DFNpW4U9vO</a></p>&mdash; nicolasauret (@nicolasauret) <a href="https://twitter.com/nicolasauret/status/991318185721909248?ref_src=twsrc%5Etfw">1 mai 2018</a></blockquote>

### Fauchié_slide {.glose}

#### citation {.glose-citation}

> Git est un système conceptuel, avec des interfaces compréhensibles par le commun des mortels qui permettent son utilisation.

[Antoine Fauchié]{.source}

#### citation {.glose-texte}

Pour Antoine Fauchié, l’utilisation de la plateforme _git_, utilisée comme outil d’une chaîne éditoriale, permet non seulement une visualisation globale de projet, mais institue aussi une porosité entre les étapes, les métiers. On intègre les textes à une textualité numérique englobante tout en faisant de l’écriture éditoriale une écriture nativement numérique.

## Frag4 {.fragment}

### Marcoux_communication {.main .main-citation}

«&nbsp;Fabrique d’autonomie collective&nbsp;», le Bâtiment 7 (B7) est une zone d’expérience et d’innovation sociale où la production de livre génère un revenu et, simultanément, sert d’outil à la cohésion communautaire. L’usage de l’outil _Booktype_ offre un moyen de se réapproprier l’espace du document à travers&nbsp;:

- l’écriture de guides pratiques pour les utilisateurs des équipements collectifs
- l’écriture de l’histoire de ces projets, composée de récits de vie, de témoignages et d’expériences
- le partage des apprentissages tirés de ces expériences, sous la forme d’essais, d’analyses critiques, d’études sur les conditions de succès

[Fabrice Marcoux]{.source}

### Marcoux_photo {.glose .glose-slide}

![Bâtiment 7](img/slides/Marcoux_visuelp.3_collectif.jpg)


## Frag5 {.fragment}


### tweet {.main-twitter-tweet}

<blockquote class="twitter-tweet" data-lang="fr"><p lang="fr" dir="ltr"><a href="https://twitter.com/hashtag/ECRIDIL2018?src=hash&amp;ref_src=twsrc%5Etfw">#ECRIDIL2018</a> Poreuse&nbsp;: on aimerait proposer la notion d’&quot;autorité redistribuée&quot;. L’espace éditorial se distribue au travers de différents actants. On peut faire l’hypothèse d’une autorité distribuée, dans un processus constant de croissance et de développement</p>&mdash; Tom Lebrun (@tolebrun) <a href="https://twitter.com/tolebrun/status/990948878227779584?ref_src=twsrc%5Etfw">30 avril 2018</a></blockquote>


### glose-vide {.glose}

## fragment5 {.fragment}

### slide {.main .main-slide}

![Poster de Martine Clouzot (extrait)&nbsp;: _The Living Book_](img/slides/clouzot.png)


### g {.glose .glose-texte}

Les _Living Books about History_ œuvrent en faveur d’une nouvelle sociabilité numérique et
historique entre les chercheurs, l’éditeur, les designers et les conservateurs. Ils permettent&nbsp;:

- une écriture augmentée
- une traduction collaborative
- une gestion collective des images fondée sur un modèle privilégiant l’_open access_ (accessibilité des images, formulaire de contribution), tout en proposant des parcours de lecture à la carte, à destination de tout public et à valeur pédagogique
