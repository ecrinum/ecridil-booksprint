# Jeu {.chapter}
[Jeu]{.shorter}

## frag1 {.fragment .break-before-right}

### Develotte_PP {.main .main-photo}

![Présentation de Mabrouka El Hachani et Christine Develotte](img/slides/Devlotte_visuelp.15_jeu.png)

### Develotte_pad {.glose .glose-texte}

La littérature jeunesse s’intéresse de plus en plus au potentiel interactif des dispositifs numériques. Dans leur analyse de _Dessine-moi le vent_ et _La grande histoire du petit trait_, Mabrouka El Hachani et Christine Develotte expliquent combien la dimension ludique des livres-applications peut renforcer l’implication de l’enfant dans l’histoire.

[universnarratif]{.keyword} [multimodalité]{.keyword} [rapportscorpsécran]{.keyword} [interagir]{.keyword} [implicationdanslhistoire]{.keyword} [explorer]{.keyword}  

## frag2 {.fragment}

### Tréhondart-fiche {.main .main-citation}

> Les lecteurs résistent quand même au spectaculaire. Il y a parfois des outils qui déçoivent l’attente du lecteur.

[Nolwenn Tréhondart]{.source}

### Haute {.glose .glose-texte}

Cette « ludification » touche même l’édition savante. Comme le note Lucile Haute, on ajoute aujourd’hui de plus en plus de médias «&nbsp;inhabituels&nbsp;» pour encourager une autre médiation des articles ou des contenus savants.


## frag3 {.fragment}

### Anthologie_palatine [SLIDES] {.main .main-photo}

![Lycéens au travail sur la plateforme de l’_Anthologie Palatine_](img/slides/Anthologie_school.jpeg)

### Anthologie_palatine_PAD_COLLECTIF {.glose .glose-texte}

Pour élaborer *L’Anthologie Palatine*, Marcello Vitali-Rosati et Enrico Agostini-Marchese entreprennent une collaboration avec des étudiants en traduction issus de deux lycées en Italie. Écrite à l’origine en grec ancien, *L’Anthologie Palatine* est transcrite puis traduite en italien. La langue initiale finit par se transformer en traduction, alors que l’italien devient la langue originale. Certains élèves se sont appropriés les textes pour en faire une version nouvelle, complètement «&nbsp;pop&nbsp;».   

## frag4 {.fragment}

### Rio_Slide {.main .main-slide}

![Florence Rio](img/slides/Rio_visuelp1gesteEcriture.png)

### g {.glose}

#### g {.glose-texte}
*Adrénalivre* permet la création de livres interactifs numériques où la fiction est inspirée des livres dont vous êtes le héros.

[storytellinginteractif]{.keyword}

#### g {.glose-texte}

Condition du récit enchâssé&nbsp;: une arborescence logique programmatique qui se rajoute à la logique créative de la littérature.

## f {.fragment}

### Meynard_Greslou_PDF {.main .main-texte}

Les chercheurs du projet des manuscrits Stendhal ont créé un vaste chantier destiné à générer une œuvre multisupports et multiformes. Il est ainsi possible de jouer avec différentes formes éditoriales, selon les publics&nbsp;: ajout de compléments pédagogiques, multimédias, adaptation aux différents supports...

### {.glose .glose-slide}

![Le chantier de recherche des manuscrits Stendhal - Présentation de Cécile Meynard et Elisabeth Greslou](img/slides/grelousMeynardJeu.png)
