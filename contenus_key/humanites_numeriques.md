# Humanités numériques {.chapter}
[Humanités numériques]{.shorter}

## frag1 {.fragment .break-before-right}

### Perret_fiche #hybridité&nbsp;: {.main .main-citation}

> On se trouve face à des perspectives intéressantes en terme d’épistémologie mais il faut mener un travail hybride. Il est alors nécessaire de monter en compétences à la fois en terme de savoir-faire que d’érudition, en se situant dans une continuité humaniste.

[Arthur Perret]{.source}

[hybridité]{.keyword}

## Frag2 {.fragment}

### m {.main .main-citation}

> On peut résumer la culture numérique comme une prise de conscience de notre rôle dans la société.

[Fabrice Marcoux]{.source}

### g fauchie_comm. {.glose .glose-texte}

«&nbsp;Prise de conscience&nbsp;», certes. Outre cela, le numérique _impose_ et _force_ l’ensemble de la société à prendre parti et à endosser une certaine responsabilité quant à la conceptualisation, la configuration et la modélisation de cet espace en pleine expansion. Antoine Fauchié nous le rappelle d’ailleurs&nbsp;:

> «&nbsp;*Pour citer Gilbert Simondon, philosophe de la technique bien connu, l’homme peut et doit s’associer aux machines, aux systèmes informatiques dans notre cas, et non plus être un opérateur ou un surveillant.*&nbsp;»

L’espace numérique n’est pas une sphère, mais une constituante de notre monde. Du fait de sa structure même, il s’impose à tous et se fait facteur d’échanges, de [porosité]{.keyword}. C’est en ce sens d’ailleurs qu’Arnaud Laborderie a souligné les principes de _morcellement_ et de _fragmentation_ grâce auxquels l’objet numérique échappe à toute [clôture]{.keyword}.

## Frag3 {.fragment}

### fauchie_comm. {.main .main-citation}

> L’homme est *parmi* les machines qui opèrent avec lui.

[Gilbert Simondon, _Du mode d’existence des objets techniques_, cité par Antoine Fauchié]{.source}


### g fauchie_comm. {.glose .glose-citation}

> Entretenir un rapport créatif aux machines n’est pas évident d’autant
> que ce dernier est parfois, voire souvent, empêché par les machines
> elles-mêmes et par la structure productive dans laquelle elles
> s’inscrivent.

[Sophie Fétro, «&nbsp;Œuvrer avec les machines numériques&nbsp;», *Back Office*, cité par Antoine Fauchié]{.source}

## Frag4 {.fragment}

### m {.main .main-slide}

![Présentation d’Anthony Masure](img/slides/Masure_visuelp.12_HN.png)

### g {.glose .glose-texte}

Emmanuël Souchier soulève l’importance de dissocier _écriture_ et _livre_ qui, aujourd’hui, sont culturellement compris comme un seul et même objet, un codex. L’écriture doit s’affranchir de son médium, dont elle n’a pas toujours été dépendante. Dire que le numérique nous permet de repenser de manière réflexive les usages et les discours, c’est de la «&nbsp;tarte à la crème&nbsp;». Il faut plutôt comprendre le numérique comme une _injonction_ à remplacer le livre dans son espace&nbsp;: celui de l’histoire et de la culture. L’écriture et le livre sont avant tout _communication_, _réponses_ à des besoins anthropologiques. Cependant, leurs usages contemporains sont davantage dictés par une logique marchande. L’industrie du numérique ne pense ni l’écriture, ni le livre, ni même l’information véhiculée, mais l’usager conçu comme consommateur. Le _design_, pour Elsa Tadier, est ce qui réinscrit la dimension humaine dans les dispositifs d’écriture et de lecture, ainsi que dans nos propres outils et machines. C’est dans ce même ordre d’idées que Jean-Louis Soubret affirme que «&nbsp;*les humanités numériques ne sont pas si éloignées du design.*&nbsp;»

## Frag5 {.fragment}

### m {.main}

#### q {.main-citation}

> Selon vous, quels sont les enjeux les plus urgents pour repenser les designs de publication&nbsp;?

#### t {.main-photo .polaroid}

![Table ronde&nbsp;: les enjeux de l’édition savante (avec Julie Blanc, Lucile Haute, Anthony Masure, Servanne Monjour, Nicolas Sauret)](img/photos/haute-table-ronde.jpg)


### g {.glose .glose-texte .break-before}

Accessibilité, pérennité et transmission sont les enjeux premiers de la publication scientifique, de l’existence même du savoir. Si le WWW a d’abord été conçu pour répondre à des problématiques de chercheurs (Berners-Lee), il faut aujourd’hui le refonder comme [espacepublic]{.keyword}. Le Web d’aujourd’hui est à la dérive, emporté par le capitalisme. Il est devenu «&nbsp;web des plateformes, c’est-à-dire web de prolétarisation&nbsp;», celui aussi de la «&nbsp;captation des savoirs à des fins marchandes&nbsp;». Ce point de vue critique fait écho aux positions de Lucile Haute, qui pointe les limites des solutions «&nbsp;clé en main&nbsp;» proposées par certains éditeurs et diffuseurs dans le monde du libre accès. Trouver une identité éditoriale est aussi une question de design.

## Frag6 {.fragment}


### m {.main .main-citation}

> Les chercheurs sont peu lus, voire même jamais lus. Certains ont trouvé refuge dans l’autopublication, souvent au sein de blogues. Mais plus généralement, les chercheurs sont enfermés par la forme, ou le format, de leurs publications.

[Anthony Masure]{.source}

### g {.glose .glose-texte}

_Design_ et _humanités numériques_ vont de pair. Le design ne doit pas être contemplatif, mais innovant&nbsp;: alliant contenu et forme. Dans le champ des Humanités numériques, il devient essentiel de mieux prendre en compte le design. Celui-ci permet en effet de créer «&nbsp;*un espace de dialogue capable d’assumer la forme de ce qu’il discute*&nbsp;», pour reprendre les mots de Jean-Louis Soubret. Cette logique est aussi, en quelque sorte, celle de Florence Rio qui pense la mise en forme &mdash;&nbsp;ou mise en scène&nbsp;&mdash; des contenus de manière à produire autre chose qu’une littérature typique de la forme livresque.


## Frag7 {.fragment}

### m {.main .main-slide}

![_Bug Magazine_, un projet de Nicolas Tilly](img/slides/bugMagazine.png)

### glose {.glose .glose-texte}

Encourager le développement des Humanités numériques, c’est aussi inventer de nouveaux médias, comme _Bug Magazine_, présenté par Nicolas Tilly. La philosophie du journal&nbsp;: «&nbsp;embrasser le numérique non plus comme cette chose vague (support, système, outil, matérialité&nbsp;?), mais une culture.&nbsp;»

## Frag8 enjeux de publication (suite) {.fragment}

### m {.main .main-citation}

> La recherche commerciale est plus rapide, performante, efficace que celle des universitaires&nbsp;: elle a un but et les gens y travaillent activement... Il y a une recherche, mais l’orientation est commerciale &mdash;&nbsp;on cherche à répondre aux besoins des consommateurs, à leurs désirs.

[Jean-Michel Gascuel]{.source}

### m {.glose .glose-texte}

En dehors des _humanités numériques_ des recherches d’innovations bien différentes s’opèrent. La logique est alors plus industrielle ou commerciale. Elle est issue de cet «&nbsp;autre _web_&nbsp;» dont ont parlé plusieurs intervenants durant le colloque &mdash;&nbsp;celui qui, en fait, est hégémonique. Jean-Michel Gascuel, cofondateur de _Googtime_, travaille avec des éditeurs, des entreprises et diverses équipes qui cherchent l’**objet-livre** de demain. C’est-à-dire un objet plus qu’une écriture ou un savoir &mdash;&nbsp;mais, somme toute, un objet ancré dans la culture. «&nbsp;*On travaille sur des problématiques d’éditeurs &mdash;&nbsp;financières et non celles de
chercheurs.*&nbsp;», précise-t-il à plusieurs reprises.

## frag9 {.fragment}

### Lacelle_visuel[texte issu du pwp, p. 23-28] {.main .main-slide}

![Présentation de Nathalie Lacelle, Marie-Christine Beaudry, Prune Lieutier](img/slides/Lacelle.png)


### Lacelle_fiche {.glose .glose-texte}

Nathalie Lacelle, Marie-Christine Beaudry et Prune Lieutier soulignent le manque de «&nbsp;traducteurs&nbsp;» entre les maillons, les mondes de la pratique et de la recherche.
