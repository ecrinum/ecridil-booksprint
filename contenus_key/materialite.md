# Matérialité {.chapter}
[Matérialité]{.shorter}

## frag1 {.fragment .break-before-right}

### Jahjah_visuel {.main .main-citation}

> Le chimiste pense la matière comme un invariant atomique ou une structure moléculaire. [...] Pour l’alchimiste au contraire, le matériau est connu non pas par ce qu’il est mais par ce qu’il fait, en particulier lorsqu’il est mélangé avec d’autres matériaux et utilisé d’une manière particulière ou placé dans une situation particulière.

[Marc Jahjah et Clémence Jacquot]{.source}

### biblio {.glose .glose-biblio}

[Tim Ingold]{.biblio-auteur},
[Faire&nbsp;: Anthropologie, Archéologie, Art et Architecture]{.biblio-titre},
[2017, Bellevaux&nbsp;: Édition Dehors]{.biblio-autre}

## Frag2 {.fragment}

### Jahjah_tweet {.main .main-twitter-tweet}

<blockquote class="twitter-tweet" data-lang="fr"><p lang="fr" dir="ltr">Un auteur support. <a href="https://t.co/m9nu1TtxX0">https://t.co/m9nu1TtxX0</a></p>&mdash; monterosato (@monterosato) <a href="https://twitter.com/monterosato/status/990976290474942464?ref_src=twsrc%5Etfw">30 avril 2018</a></blockquote>

<blockquote class="twitter-tweet" data-lang="fr"><p lang="fr" dir="ltr"><a href="https://twitter.com/marc_jahjah?ref_src=twsrc%5Etfw">@marc_jahjah</a> <a href="https://twitter.com/monterosato?ref_src=twsrc%5Etfw">@monterosato</a> voire même
&nbsp;: l’auteur en tant que tel comme nouvelle matérialité relais&nbsp;? (je plaisante pas tant que ça)</p>&mdash; françois bon (@fbon) <a href="https://twitter.com/fbon/status/990974940575948800?ref_src=twsrc%5Etfw">30 avril 2018</a></blockquote>

### Jahjah_tweet {.glose .glose-twitter-tweet}

<blockquote class="twitter-tweet" data-conversation="none" data-lang="fr"><p lang="fr" dir="ltr">Oui je suis d&#39;accord&nbsp;: mais un pivot ou pris dans ce flux de forces et de matériaux&nbsp;? Comme l&#39;alchimiste qui unit ses forces aux leurs, rentre en correspondance avec elles pour se construire un petit coin, un monde, un site.</p>&mdash; Marc Jahjah (@marc_jahjah) <a href="https://twitter.com/marc_jahjah/status/990981344586067968?ref_src=twsrc%5Etfw">30 avril 2018</a></blockquote>

## Frag3 {.fragment}

### Tadier_photo {.main .main-texte}

Comme lecteurs et lectrices, on appréhende physiquement la matière en tournant les pages &mdash;&nbsp;par exemple, on interrompt la lecture pour jauger le volume, percevoir l’objet. Le design se dissocie de l’économie de marché&nbsp;: la corporéité est un supplément d’âme, il doit dépasser ses conditions actuelles d’exercice pour interroger la matière d’un point de vue phénoménologique. Ce dépassement est nécessaire, selon Elsa Tadier, afin de réorganiser notre rapport aux contenus, de saisir les implications matérielles des objets que l’on utilise. Il faut penser les dispositifs numériques d’un point de vue anthropologique et non consumériste.

### Tadier_fiche {.glose .glose-photo .polaroid}

![Elsa Tadier](img/photos/tadier.jpg)

## Frag4 {.fragment}

### Drochon_visuel {.main .main-citation}

> Alors que reste-il au livre&nbsp;? du contenu&nbsp;? des mots&nbsp;? des formes&nbsp;? du papier&nbsp;?

> La réponse se trouve peut être dans ce *Livre Illisible* de Bruno Munari. Le designer y propose une pure expérience du toucher et des yeux&nbsp;: tourner des pages colorées en touchant du papier. L’expérience faite du livre reste la seule définition possible de celui-ci.

[Julien Drochon]{.source}

### Tadier_tweet {.glose .glose-twitter-tweet}

<blockquote class="twitter-tweet" data-lang="fr"><p lang="fr" dir="ltr">&quot;Avec les livres numériques, le texte et son support ne vieillissent plus ensemble&quot; Elsa Tadier <a href="https://twitter.com/hashtag/Ecridil2018?src=hash&amp;ref_src=twsrc%5Etfw">#Ecridil2018</a></p>&mdash; Julie (Blanc) McFly (@Julie_McFly) <a href="https://twitter.com/Julie_McFly/status/991346420467863552?ref_src=twsrc%5Etfw">1&nbsp;mai&nbsp;2018</a></blockquote>



## frag6

### main {.main .main-twitter-tweet}

<blockquote class="twitter-tweet" data-lang="fr"><p lang="fr" dir="ltr">« Objet à lire », un terme intéressant à retenir. <a href="https://twitter.com/hashtag/ECRIDIL2018?src=hash&amp;ref_src=twsrc%5Etfw">#ECRIDIL2018</a></p>&mdash; Ncls_Tll ✏️📲 (@nicolastilly) <a href="https://twitter.com/nicolastilly/status/990965144225243136?ref_src=twsrc%5Etfw">30 avril 2018</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>


### g {.glose}

### Tadier {.main .main-citation}

> Le livre est creusé par des traces de frottement, de grattement et la peinture effacée sous les baisers dévorateurs.

[Elsa Tadier (à propos du Psautier et Rosaire de la Vierge)]{.source}

### g {.glose .glose-slide}

![Présentation d’Elsa Tadier](img/slides/extraitTadier.png)
