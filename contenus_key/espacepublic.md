# Espace (public) {.chapter}
[Espace (public)]{.shorter}


## frag1 {.fragment .break-before-right}

### Charbonneau_tweet {.main .main-twitter-tweet}

<blockquote class="twitter-tweet" data-lang="fr"><p lang="fr" dir="ltr"><a href="https://twitter.com/hashtag/ECRIDIL2018?src=hash&amp;ref_src=twsrc%5Etfw">#ECRIDIL2018</a> Charbonneau &quot;Réfléchissez sur quelle plateforme vous rendez disponibles vos documents numériques, car certaines interdisent leur acquisition par les bibliothèques. Or un livre numérique, pour moi, c&#39;est un livre qui peut être acquis par une bibliothèque avant tout&quot;</p>&mdash; Tom Lebrun (@tolebrun) <a href="https://twitter.com/tolebrun/status/991315689549062144?ref_src=twsrc%5Etfw">1 mai 2018</a></blockquote>

[plateforme]{.keyword}

### TableRonde_PublierLaRecherche_notecoll. {.glose .glose-citation}

> Tout le budget d’acquisition des bibliothèques passe dans l’abonnement aux revues. La bibliothèque devient noyautée par des forces extérieures. Dans certaines universités, il n’est plus possible d’acheter de livres.

[Discussion (animée) dans la salle, pendant la table ronde «&nbsp;Publier la recherche&nbsp;»]{.source}

## Fragment2 {.fragment}

### Masure_tweet {.main .main-twitter-tweet}

<blockquote class="twitter-tweet" data-cards="hidden" data-lang="fr"><p lang="fr" dir="ltr"><a href="https://twitter.com/hashtag/ECRIDIL2018?src=hash&amp;ref_src=twsrc%5Etfw">#ECRIDIL2018</a> <a href="https://twitter.com/AnthonyMasure?ref_src=twsrc%5Etfw">@AnthonyMasure</a>, refaire du Web un espace public, évitant la privatisation des plateformes <a href="https://t.co/r0tNDe5h4X">https://t.co/r0tNDe5h4X</a> <a href="https://twitter.com/hashtag/numerique?src=hash&amp;ref_src=twsrc%5Etfw">#numerique</a> <a href="https://twitter.com/hashtag/edition?src=hash&amp;ref_src=twsrc%5Etfw">#edition</a> <a href="https://twitter.com/hashtag/Web?src=hash&amp;ref_src=twsrc%5Etfw">#Web</a> <a href="https://twitter.com/hashtag/livre?src=hash&amp;ref_src=twsrc%5Etfw">#livre</a> <a href="https://twitter.com/hashtag/design?src=hash&amp;ref_src=twsrc%5Etfw">#design</a> <a href="https://t.co/5bfyVS960D">pic.twitter.com/5bfyVS960D</a></p>&mdash; Écritures numériques (@ENumeriques) <a href="https://twitter.com/ENumeriques/status/991017656420618243?ref_src=twsrc%5Etfw">30 avril 2018</a></blockquote>

### g {.glose}

## Frag3 {.fragment}

### TableRonde_PublierLaRecherche_question {.main .main-texte}

> Comment peut-on développer l’_open access_, par quels moyens concrets&nbsp;?  

Pour Nicolas Sauret, les institutions doivent être prescriptives dans les pratiques favorisant l’_open access_. Les revues devraient adopter des licences et des _labels_ assurant le développement de l’accès ouvert.

Mais comme le souligne Julie Blanc, parler d’_open access_ au niveau de la diffusion implique également une réflexion sur les outils (_open source_, _open tech_, etc.). L’obsolescence des documents liée à certains logiciels propriétaires en est le meilleur exemple.

[pérennité]{.keyword} [openaccess]{.keyword}

### glose {.glose}


## Frag3 {.fragment}

### Table ronde PublierLaRecherche_notecoll. {.main .main-citation}

> Publish or perish.  
> Demo or die.  
> Get visible or vanish.  
> Deploy or die.

[Discussion avec Lucile Haute]{.source}

### Table ronde PublierLaRecherche_notecoll. {.glose .glose-texte}

Les injonctions faites aux chercheurs évoluent. En arrière-plan se jouent des enjeux de brevets, de prototypages, une exigence marchande d’accessibilité pour le grand public et de réception des pairs.


## Frag4 {.fragment}

### Vitali-Rosati_Tréhondart_fiche {.main .main-citation}


> Au sujet des formats sur iPad et Android, il semble que des acteurs institutionnels ne se posent pas la question des alternatives &mdash;&nbsp;car on peut éviter les plateformes commerciales. Les pratiques qui se développent sont complètement véhiculées par deux entreprises.

> Est-ce par ignorance et incompétence ou par choix de la part de Réunion des Musées Nationaux&nbsp;? C’est un système de production&nbsp;: les musées publics deviennent des propriétés intellectuelles de Apple ou Google. Vos analyses ont-elles permis de rendre compte de ces choix&nbsp;?

[Marcello Vitali-Rosati à Nolwenn Tréhondart]{.source}

[public-privé]{.keyword} [littératie]{.keyword}

### g {.glose .glose-photo .polaroid .break-before}

![Marcello Vitali-Rosati](img/photos/vitali-rosati.jpg)

## f {.fragment}

### Charbonneau_tweet {.main .main-twitter-tweet}

<blockquote class="twitter-tweet" data-lang="fr"><p lang="fr" dir="ltr">(et donc si <a href="https://twitter.com/GrandPalaisRmn?ref_src=twsrc%5Etfw">@GrandPalaisRmn</a> ne parvient pas à rendre _rentables_ ses éditions numériques diffusées/distribuées sur l&#39;App Store, c&#39;est peut-être parce que cette dimension a été oubliée, les bibliothèques ne pouvant acquérir ces œuvres <a href="https://twitter.com/hashtag/ecridil2018?src=hash&amp;ref_src=twsrc%5Etfw">#ecridil2018</a>)</p>&mdash; antoinefauchié (@antoinentl) <a href="https://twitter.com/antoinentl/status/991317262920507397?ref_src=twsrc%5Etfw">1 mai 2018</a></blockquote>

### g {.glose .glose-twitter-tweet}

<blockquote class="twitter-tweet" data-lang="fr"><p lang="fr" dir="ltr">les plateformes *disent* quel est le droit associé à un objet numérique –&nbsp;par exemple un livre&nbsp;–, il faut prendre garde à ce que permet la plateforme de distribution/diffusion de cet objet <a href="https://twitter.com/hashtag/ecridil2018?src=hash&amp;ref_src=twsrc%5Etfw">#ecridil2018</a></p>&mdash; antoinefauchié (@antoinentl) <a href="https://twitter.com/antoinentl/status/991316006395154434?ref_src=twsrc%5Etfw">1 mai 2018</a></blockquote>


## f4 {.fragment}

### slide {.main .main-slide}

![_L’Abécédaire des mondes lettrés_ de l’Enssib](img/slides/abecedaire-mondes-lettres.jpeg)

### Epron_fiche {.glose .glose-citation}

> Nous nous trouvons devant un univers visuel sphérique pour renvoyer à la vision humaniste d’un nouveau monde. Des mots flottent dans la voûte céleste et les données sont visualisées en 3D.

[Benoît Epron]{.source}

[univers]{.keyword}

## Frag5 {.fragment}

### Charbonneau_tweet {.main .main-twitter-tweet}

<blockquote class="twitter-tweet" data-lang="fr"><p lang="fr" dir="ltr"><a href="https://twitter.com/hashtag/ECRIDIL2018?src=hash&amp;ref_src=twsrc%5Etfw">#ECRIDIL2018</a> Charbonneau: &quot;Il faut conceptualiser le droit d&#39;auteur en contexte numérique comme un chantier. Le droit d&#39;auteur n&#39;est pas un problème juridique, mais un problème de gouvernance&quot; <a href="https://twitter.com/em_guiraud?ref_src=twsrc%5Etfw">@em_guiraud</a></p>&mdash; Tom Lebrun (@tolebrun) <a href="https://twitter.com/tolebrun/status/991312431422033920?ref_src=twsrc%5Etfw">1 mai 2018</a></blockquote>

### Charbonneau_fiche{.glose .glose-texte}

Sans plateforme, nous dit Olivier Charbonneau, il n’y a pas de diffusion et, conséquemment, le livre n’existe tout simplement pas. Parmi les enjeux que cela soulève, il faut penser à ajouter une strate de métadonnées aux catalogues des bibliothèques. Comment les agissements des communautés s’insèrent-ils dans la documentation numérique&nbsp;? Comment bâtir un marché sur des bases aussi mouvantes que le sont celles du numérique&nbsp;? C’est aux instances de gouvernance que revient la tâche de réfléchir la place du numérique dans l’espace commun, public, mais également celle de ne pas laisser des intérêts privés, préconisant la rentabilité (GAFAM, particulièrement), dicter les paramètres qui délimitent et définissent l’espace numérique.

## f6 {.fragment}

### Charbonneau_tweet {.main .main-twitter-tweet}

<blockquote class="twitter-tweet" data-lang="fr"><p lang="fr" dir="ltr"><a href="https://twitter.com/hashtag/ECRIDIL2018?src=hash&amp;ref_src=twsrc%5Etfw">#ECRIDIL2018</a> Charbonneau &quot;Nous devons réfléchir au design institutionnel. Le libre accès doit être une hypothèse de travail dès le départ. Cela introduit le concept de gratuité comme étant la plus-value numérique par excellence&quot;</p>&mdash; Tom Lebrun (@tolebrun) <a href="https://twitter.com/tolebrun/status/991316289007312896?ref_src=twsrc%5Etfw">1 mai 2018</a></blockquote>

### g {.glose}
