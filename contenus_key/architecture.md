# Architecture {.chapter}
[Architecture]{.shorter}

## frag1 {.fragment .break-before-right}

### m {.main .main-slide}

![_L’abécédaire des mondes lettrés_ de l’Enssib](img/slides/abecedaire-mondes-lettres2.jpg)

## frag2 {.fragment}

### m {.main .main-texte}

Le web et le livre sont d’abord des architectures de lecture. L’ePub est un mini-site web encapsulé. Il fonctionne avec les technologies web standard (HTML, CSS, JS), mais il a pris du retard sur le web. Pour Anthony Masure, il s’agit donc d’un format «&nbsp;pauvre&nbsp;», limité. Comme le souligne Julie Blanc, le logiciel a un impact sur la forme. Le web offre à travers ses standards un environnement libre et ouvert qui permet une émancipation par rapport aux outils propriétaires d’édition et de publication.


### g {.glose .glose-texte }

La revue &mdash;&nbsp;numérique&nbsp;&mdash; imaginée dans le cadre de la Chaire _Arts et sciences_, et présentée par Lucile Haute et Julie Blanc, est fondée sur les principes et les défis suivants&nbsp;:

- multimodale
- modulable
- _openscience_ / _opentech_
- multi-support (en incluant même les _smartwatches_)
- paramétrique (déclinaison des contenus en fonction des environnements de lecture)

L’outil numérique _Pagedmedia_ tente de combler un vide dans le domaine des chaînes de publication numérique par la création d’un «&nbsp;_single source publishing_&nbsp;», c’est-à-dire un _workflow_ basé sur le balisage du contenu avec visualisation distincte. Le caractère ouvert de cet outil permet d’échapper à la rigidité des gabarits pour produire des CSS print &mdash;&nbsp;ouvrages nativement numériques pouvant être adaptés au format papier. La flexibilité offerte par l’environnement numérique rejaillit sur le geste même de la composition éditoriale.


## frag3 {.fragment}

### Rio_Pad {.main .main-texte}

Selon Florence Rio, le logiciel est à considérer comme un _architexte_ qui commande la structure même du texte. L’écriture, du fait de ce système, est elle-même reconfigurée en tant que pratique sociale. Il s’agit dorénavant de comprendre le fonctionnement d’une écriture interactive, du contrôle qu’exerce une interface sur le texte écrit. L’écriture devient donc négociation entre écrivant et architexte.


### glose {.glose .glose-twitter-tweet}

<blockquote class="twitter-tweet" data-lang="fr"><p lang="fr" dir="ltr">le balisage est du texte qui décrit du texte&nbsp;: *architexte* (toujours <a href="https://twitter.com/arthurperret?ref_src=twsrc%5Etfw">@arthurperret</a>) <a href="https://twitter.com/hashtag/ecridil2018?src=hash&amp;ref_src=twsrc%5Etfw">#ecridil2018</a></p>&mdash; antoinefauchié (@antoinentl) <a href="https://twitter.com/antoinentl/status/991340727522250752?ref_src=twsrc%5Etfw">1 mai 2018</a></blockquote>

## fragment4 {.fragment}

### slide {.main .main-slide}

![Présentation d’Enrico Agostini-Marchese et Marcello Vitali-Rosati](img/slides/anthologie.jpg)


### potentialités {.glose .glose-texte}

Afin de donner à l’_Anthologie palatine_ une grande malléabilité et un large espace d’action et d’interaction sémiotique capables de restituer la nature protéiforme de cet ensemble de textes, le principal défi technique a été la création d’une plateforme adaptée. Les textes gagnent alors, au sein de la plateforme, de multiples possibilités d’usage et de dissémination.

## Frag7 {.fragment}

### g {.main .main-texte}

Arnaud Laborderie donne raison à François Bon&nbsp;: la nouvelle forme du livre, c’est le site. Mais comment différencier désormais le livre numérique du site web&nbsp;? ...

### g {.glose}
