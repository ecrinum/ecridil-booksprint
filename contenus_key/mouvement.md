# Mouvement {.chapter}
[Mouvement]{.shorter}

## frag1 {.fragment .break-before-right}

### Perret_diapo4 #écriture_en_mouvement + @Perret_fiche {.main .main-citation}

> Définitive, l’écriture gagne une valeur d’interaction très forte
avec le numérique. Ses paramétrages font partie du contrat de lecture
participant de l’expérience de l’utilisateur.

[Arthur Perret]{.source}

### g {.glose}

## frag {.fragment}

### Meynard/Greslou_PDF&nbsp;: {.main .main-slide}

![Présentation des Manuscrits Stendhal par Cécile Meynard et Elisabeth Greslou](img/slides/stendhal.png)

### g {.glose .glose-texte}

L’édition des manuscrits Stendhal a d’abord été pensée selon une perspective de mouvement, dans un souci de complémentarité entre les versions selon les supports/plateformes. Elle s’éloigne du modèle éditorial figé avec un apparat critique développé, conforme aux normes de l’édition scientifique.


## frag {.fragment}

### Antoine  {.main .main-twitter-tweet}

<blockquote class="twitter-tweet" data-lang="fr"><p lang="fr" dir="ltr">je suis fan de cette définition du livre numérique de <a href="https://twitter.com/culturelibre?ref_src=twsrc%5Etfw">@culturelibre</a>&nbsp;: un livre numérique est un livre qui peut être acquis par une bibliothèque (et c&#39;est tout). si la plateforme qui permettrait cet accès n&#39;existe pas: créez-la! <a href="https://twitter.com/hashtag/ecridil2018?src=hash&amp;ref_src=twsrc%5Etfw">#ecridil2018</a></p>&mdash; antoinefauchié (@antoinentl) <a href="https://twitter.com/antoinentl/status/991317685161152512?ref_src=twsrc%5Etfw">1 mai 2018</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

### g {.glose}

## frag {.fragment}

### Agostino_fiche&nbsp;: ouverture aux changements au fil du temps {.main .main-citation}

>L’Anthologie n’est pas un texte, ni un objet clos. Ses racines
sont obscures&nbsp;: on peut parler du _Codex Palatinus 23_ (Anthologia
Palatina) ou encore de l’_Anthologie Grecque (Anthologia Graeca)_.
Plus largement, on parle de près de 2 600 ans d’épigrammes, d’inscriptions
et de poèmes.

[Enrico Agostini-Marchese et Marcello Vitali-Rosati]{.source}

### Agostino_fiche&nbsp;: ouverture aux changements au fil du temps {.glose .glose-texte}
Le projet mené par la CRC sur les écritures numériques réinvestit les données sur toutes sortes de plateformes, dont un
_bot_ sur Twitter, pour faire circuler le texte et son sens. On ouvre donc le projet à tous &mdash;&nbsp;des lycéens aux philologues
universitaires&nbsp;&mdash; pour réintégrer le corpus à la circulation dans
l’imaginaire, y compris l’imaginaire contemporain. Une API spécifique à l’Anthologie a été créée, permettant de remplir les
visées du projet qui restent les plus ouvertes possibles. En optant pour le format JSON, le projet s’ouvre aux usages multiples &mdash;&nbsp;tant savants qu’amateurs ou ludiques.

## frag {.fragment}

### Drochon_citationdiapo8 #écriture_en_mouvement&nbsp;:  {.main .main-citation}

> Envisager la transcription orale d’un point de vue typographique
nécessite obligatoirement de la réaliser en mouvement. Le collectif _The Moving poster_ explore les possibilités de la
mobilité de l’écrit, passant du GIF aux formes imprimées.

[Julien Drochon]{.source}

### g {.glose .glose-slide}

![Le cas de _The Moving Poster_ présenté par Julien Drochon](img/slides/drochonSlideMovongPoster.png)


## frag {.fragment}

### Table ronde (C) {.main .main-texte}

L’échange entre l’écriture numérique et papier est de plus
en plus questionné par la revue *bleuOrange* (présentée par Ariane Savoie) dans un contexte d’obsolescence des technologies. La revue est toujours confrontée à différents
langages en mutation. *bleuOrange* est en train de revoir sa mission pour réétudier la question de l’adaptation, notamment la traduction du français vers l’anglais, à rebours de la pratique actuelle qui consiste à traduire des textes de l’anglais vers le français.

### g {.glose}

## frag {.fragment}

### Souchier-fiche&nbsp;: {.main .main-texte}
Pour Emmanuël Souchier, ce n’est pas le livre qui est transformé par le numérique, mais la
relation intime de l’écriture à ses supports et à ses usages.
Nous sommes aujourd’hui en face de formes hybrides et passagères. Le
livre, comme matrice culturelle, ne trouvera dans le numérique que le
prolongement de ce qu’il a déjà envisagé dans sa forme même.

Pour penser le livre numérique, il importe de ne pas rester prisonniers des formes instituantes. Il faut s’en extraire car ces cadres agissent comme théâtre de mémoire, à la fois support matériel et technologie de l’intellect, ils assument corporellement la pérennité et la matérialité de l’écriture.

### glose {.glose}

#### Souchier-fiche {.glose-texte}
Jack Goody présente le livre (codex) comme un petit véhicule plastique.  Le livre offre à l’écriture une portabililté en l’assignant à une forme, à un support et à des formats. Le livre répond aux nécessités anthropologiques de l’écriture, il ouvre la notion d’écriture au sens.

#### bilio {.glose-biblio}

[Jack Goody]{.biblio-auteur},
[The Domestication of the Savage Mind]{.biblio-titre},
[1977, Cambridge: Cambridge University Press]{.biblio-autre}


<!--déplacer vers pluridisciplinarite ?
## frag {.fragment}


###  @Masure-fiche&nbsp;:

Modalités du design en jeu ici&nbsp;:

Design éditorial&nbsp;: hiérarchie + structuration des contenus.
Design graphique&nbsp;: le langage visuel comme organisation sémantique.
Design d’interfaces&nbsp;: expertise de lecture à l’écran; sensibilité.

Quelles sont les nouvelles possibilités permises par ces nouvelles
techniques? (Parallèle&nbsp;: nouvelles techniques, plus spécifiquement
nouvelles techniques d’écriture.)

Les types d’écrits web portent généralement sur eux-mêmes (i.e. web,
design). Le web et le design écrivent justement sur eux-mêmes avec ces
technologies.

Les chercheurs sont souvent en manque de littératie numérique. Pourtant,
le web est né dans un contexte de recherche, (co-)inventé par (entre
autres) le physicien-chercheur Tim Berners-Lee alors qu’il travaillait
au CERN. -->

<!-- déplacer vers Forme ?
## frag {.fragment}

### Table ronde (C)

Quelques éditeurs de livres d’art ont essayé de tirer partie des nouveaux outils et ont développé leurs
collections en epub. Pour un éditeur de livre d’art, publier aujourd’hui du livre
numérique renvoie à l’idée de stratégie de distinction par l’innovation. Il s’agit de montrer qu’ils restent
branchés, modernes et tirent partie des possibilités de l’écran en intégrant des vidéos, des
éléments interactifs.


-->
