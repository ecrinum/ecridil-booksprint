# Parcours {.chapter}
[Parcours]{.shorter}

## frag1 {.fragment .break-before-right}

### Tréhondart-fiche {.main .main-citation}

> La lecture numérique doit pouvoir donner la possibilité de faire son choix librement.

[Nolwenn Tréhondart]{.source}

[lecturenumérique]{.keyword}

### g {.glose}

## frag2 {.fragment}

### Drochon_Slide 6 {.main .main-slide}

![Présentation de Julien Drochon](img/slides/Drochon_visuelp.6_fragment.png)


### g {.glose .glose-citation}

> Faire l’expérience d’une lecture numérique c’est avant tout faire l’expérience d’une dissociation.

[Julien Drochon]{.source}

[expérience]{.keyword}

## frag3 {.fragment}

### Epron_slide {.main .main-slide}

![Présentation de Benoît Epron](img/slides/Epron_visuelp.11_immersion.png)

[immersion]{.keyword}

### glose epron {.glose .glose-texte}

_L’Abécédaire des mondes lettrés_ met en place une potentialité de parcours de lecture non contraints&nbsp;: l’organisation des termes offre des potentialités libres &mdash;&nbsp;bien que prévues par les liens proposés entre les différents termes. En revanche, le papier fait directement référence à une matéralité qui contraint le parcours de lecture.

[parcoursdelecture]{.keyword} [expérience]{.keyword}

## frag4 {.fragment}

### m {.main .main-slide}

![Le projet d’édition numérique enrichie de _Candide_, présenté par Arnaud Laborderie](img/slides/candide.png)

### Laborderie_fiche {.glose .glose-texte}

Arnaud Laborderie évoque les expérimentations en milieu scolaire et la mise en œuvre de recherches sur le web&nbsp;: pour les élèves, l’entrée privilégiée dans le texte se fait par mode sonore&nbsp;: l’écrit se trouve oralisé. Surtout, certains élèves ont véritablement surfé sur les œuvres littéraires, comme sur un site web.

[expérience]{.keyword} [immersion]{.keyword} [parcoursdelecture]{.keyword}

## frag5 {.fragment}

### Paquienséguy_photo {.main .main-photo .polaroid}

![Françoise Paquienséguy](img/photos/paquienseguy2.jpg)

### g {.glose .glose-citation}

> Le format du CD-ROM culturel réinvesti permettrait, peut-être plus que les autres, de créer au profit de l’artiste.

[Françoise Paquienséguy]{.source}

## frag6 {.fragment}

### Jahjah_fiche {.main .main-citation}

> À propos de l’énonciation éditoriale selon Souchier et Jeanneret&nbsp;: chaque métier de la fabrication du livre a laissé une trace de son passage sur le livre.

[Marc Jahjah]{.source}

### glose {.glose .glose-texte}

La matérialité éditoriale est aussi retravaillée par les lecteurs. Par leur interprétation, ceux-ci deviennent des actants. C’est tout le principe d’une _autorité distribuée_ à travers la généalogie des signes et des ordres négociés, comme le soulignent Marc Jahjah et Clémence Jacquot.

[linéarité]{.keyword} [déplacementdulecteur]{.keyword} [labyrinthe]{.keyword} [fildAriane]{.keyword} [errance]{.keyword}
