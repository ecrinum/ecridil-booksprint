# Geste d’écriture {.chapter}
[Geste d’écriture]{.shorter}

## frag1 {.fragment .break-before-right}

### Souchier-fiche {.main .main-citation}

> Design et affordance, sont-ils des termes si contemporains&nbsp;?

[Emmanuël Souchier]{.source}

### Souchier-fiche g {.glose .glose-texte}

Première technologie de l’intellect, l’écriture a permis à l’Humain de s’émanciper. Elle marque la sortie du contexte et de la situation de l’oralité, et répond au besoin communicationnel de l’être humain. Pour Emmanuël Souchier, l’écriture porte la présence du geste, de la main, ainsi que la trace des voix et des corps. Elle modèle aussi notre manière de voir le monde&nbsp;: d’ailleurs, l’écriture nous a tellement enveloppés que nous n’arrivons plus à en sortir. Aussi faut-il faire l’effort de distinguer le livre de l’écriture, sa matérialité propre&nbsp;: «&nbsp;l’image du texte&nbsp;».

## frag {.fragment}

### Jahjah_PAD {.main .main-texte}

Dans _Poreuse_, de Juliette Mézenc, analysé par Marc Jahjah et Clémence Jacquot, la matérialité éditoriale est retravaillée par les lecteurs &mdash;&nbsp;ou plutôt les _actants_ du texte&nbsp;&mdash; pour opérer le renversement des connotations associées aux gestes de lecture dans l’espace numérique. Tandis que le geste continu de défilement des pages fait naître un sentiment de désorientation dans un labyrinthe narratif, la lecture hypertextuelle permet de suivre le fil d’Ariane entre les fragments du récit.

### g {.glose .glose-slide}

![*Poreuse*, Juliette Mézenc](img/photos/poreuse.jpg)

## frag {.fragment}

### Rio_PAD {.main .main-slide}

![Présentation de Florence Rio, autour du projet Adrénalivre](img/slides/rioMvmt1.png)

### g {.glose .glose-texte}

La production éditoriale peut être une forme d’accompagnement de l’écriture. Dans la tradition des livres dont vous êtes le héros, le projet en recherche et développement mené par l’équipe de Florence Rio, en collaboration avec l’entreprise Adrénalivre, propose de créer des livres numériques interactifs et notamment d’accompagner les auteurs dans leur processus d’écriture.


## frag {.fragment}

### Fauchié_communication {.main .main-citation}

> Versionner du code c’est versionner du texte.

[Antoine Fauchié]{.source}

### g {.glose}

## frag {.fragment}

### Meynard/Greslou_PDF Synthèse {.main .main-slide}

![Présentation de Cécile Meynard et Elisabeth Greslou](img/slides/Meynard_visuelpGesteEcriture.png)

### g {.glose .glose-texte}

L’idéal, afin de toucher tous les publics, serait de permettre au lecteur de trouver la plateforme de lecture qui lui est la plus adaptée. Ainsi, l’édition ne doit pas être figée dans un seul support&nbsp;: il s’agit de proposer une pluralité de formes éditoriales. Sur le site des *Manuscrits Stendhal*, l’utilisation du mot «&nbsp;publication&nbsp;» renvoie à l’idée de «&nbsp;rendre publics&nbsp;» les manuscrits mais aussi le travail des chercheurs.
