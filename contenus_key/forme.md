# Forme {.chapter}
[Forme]{.shorter}

## frag1 {.fragment .break-before-right}

### Raymond_photo {.main .main-citation}

> Il faut considérer la contrainte comme une structure portant en germe une potentialité en attente de réalisation.

[Dominique Raymond]{.source}

### Raymond_fiche {.glose .glose-slide}

![Dominique Raymond](img/slides/raymond2.png)

## Frag2 {.fragment}

### Drochon_visuel {.main .main-citation}

> Le livre, si on le définit du côté de ses producteurs, est comme la réalisation d’un artefact édité (agglomération, discussion, sélection, vérification et diffusion) et peut potentiellement être protéiforme. Le livre peut être envisagé comme un podcast, comme un livre audio, comme la convergence des médiums.

[Julien Drochon]{.source}

[protéiforme]{.keyword}

### Tadier_noutecoll {.glose .glose-texte}

Pour Elsa Tadier, il existe une intimité entre fond et forme. Cette proximité est liée au rôle des corps, du designer mais également à celui du lecteur.


## Frag3 {.fragment}

### Souchier_fiche {.main .main-texte}
Selon Emmanuël Souchier, pour replacer l’écriture dans son espace propre (et donc sous toutes ses formes), il faut revenir à quatre schèmes fondamentaux&nbsp;: la mémorisation, la circulation, la narrativisation et la visualisation. Mais concrètement, il y a une double exigence, une tension entre l’économie capitaliste du flux (d’informations) et la question des formats qui nécessitent une réification.

### g {.glose}

## Frag4  {.fragment}

### Soubret_notecoll {.main .main-texte}

L’art existe pour être contemplé, tandis que le design a une fonctionnalité esthétique. Il peut améliorer le processus de gestion. Le design thinking, auquel Jean-Louis Soubret consacre sa thèse, est une exploitation du design, un mal nécessaire pour le monde de l’édition, qui pourrait rendre le design compréhensible aux non-designers (les gens d’affaires, les littéraires, les investisseurs, et tous les autres acteurs du livre...).

### Lacelle_visuel {.glose .glose-citation}
> Des banques de livres numériques en accès libre et gratuit pour les enfants se multiplient depuis le début du millénaire. Cependant, la production de ces livres numériques pour enfants reste encore très marginale dans le milieu québécois, parce que mal connue ou encore mal adaptée à toutes les possibilités du numérique et aux caractéristiques du lectorat.

[Nathalie Lacelle, Marie-Christine Beaudry et Prune Lieutier]{.source}


## Frag5  {.fragment}

### Masure_slide {.main .main-citation}
> Un livre Web (web book) est un livre consultable dans un navigateur&nbsp;: une collection de documents organisés, structurés en parties (comme des chapitres), avec des outils de navigation (comme une table des matières), lisibles sur le web et accessibles par des hyperliens. Il existe différentes modalités technologiques&nbsp;: mobile (responsive), consultable hors connexion, Progressive Web Application (pwa), etc.

[Anthony Masure]{.source}

### biblio {.glose .glose-biblio}

[Antoine Fauchié]{.biblio-auteur},
[Le livre web]{.biblio-titre},
[ÉCRIDIL 2016]{.biblio-autre}


## Frag6  {.fragment}

### Soubret_photo {.main .main-citation}

> Le livre est historiquement la forme de consécration des disciplines en tension entre les mondes intellectuels et académiques et procède d’un projet, forme privilégiée du design. Je précise donc par là le genre que j’aborde&nbsp;: le transfert qui n’est pas nécessairement une vulgarisation mais gagne à une formalisation abordable pouvant aller jusqu’à un certain niveau de modélisation des arguments (quitte à les discuter de manière réflexive et subtile, à ne pas en ignorer la complexité) et de visualisation versus lecture (métaphores, illustrations, images du texte...).

[Jean-Louis Soubret]{.source}

### Soubret_doc {.glose .glose-photo .polaroid}

![Jean-Louis Soubret](img/photos/soubret.jpg)


## Frag7  {.fragment}

### Haute_notecoll {.main .main-citation}

> Les chercheurs sont évalués sur les articles et les publications. Si on veut innover, il faudrait que ces formes puissent être reconnues comme résultats de recherche. Il faut des contextes éditoriaux nouveaux, qui évaluent pareillement, mais s’ouvrent à inventer ou encore à s’accorder à celles existantes (ex&nbsp;: revue bleuOrange).

[Lucile Haute]{.source}

### Masure_fiche {.glose .glose-texte}

Pour Anthony Masure, donner une forme aux savoirs, c’est les transformer. Le design les fait alors vraiment exister dans le monde.


## Frag8 {.fragment}

### Epron_photo {.main .main-photo .polaroid}

![Benoît Epron](img/photos/epron.jpg)

### Epron_fiche {.glose .glose-citation}

> Le livre imprimé devient un réel projet de design éditorial qui répond à un besoin d’outil d’écriture savante.

[Benoît Epron]{.source}

[design]{.keyword}

## Frag9  {.fragment}

### Raymond_visuel {.main .main-slide}

![Présentation Dominique Raymond](img/slides/raymond1.png)


### commentaire raymond {.glose}

#### biblio {.glose-biblio}

[Nicolas Dickner, Dominique Fortier]{.biblio-auteur},
[Révolutions]{.biblio-titre},
[2014, Québec&nbsp;: Éditions Alto]{.biblio-autre}

#### mots-clés {.glose-texte}

[fragment]{.keyword} [miseenscène]{.keyword}


<!--
## Frag6 {.fragment}

### Meynard/Greslou_fiche {.main .main-texte}

Au sein de projet des Manuscrits Stendhal, Élisabeth Greslou et Cécile Meynard travaillent dans l’optique d’une œuvre multisupports à l’édition multiforme. Selon les publics, il faut rajouts des compléments pédagogiques, ou encore des complément multimédias&nbsp;: il y a un souci de complémentarité entre les versions et les plateformes. Leur projet se veut à l’origine être un chantier de recherche collectif qui réunit des chercheurs et des chercheuses de différements domaines - des «&nbsp;bricoleurs&nbsp;» et «&nbsp;bricoleuses&nbsp;». C’est également l’un des premiers projets en humanités numériques&nbsp;: commencé en 2006, le projet est encore aujourd’hui un lieu de recherches et d’expérimentations. C’est un travail en mouvement, non une édition figée, avec un appareil critique développé et répondant aux normes de l’édition scientifique.

-->
