# Pluridisciplinaire {.chapter}
[Pluridisciplinaire]{.shorter}

## frag1 {.fragment .break-before-right}

### Perret_fiche #hybridité&nbsp;: {.main .main-citation}

> On se trouve face à des perspectives intéressantes en terme d’épistémologie mais il faut mener un travail hybride. Il est alors nécessaire de monter en compétences à la fois en terme de savoir-faire que d’érudition se situant alors dans une continuité humaniste.

[Arthur Perret]{.source}

[hybridité]{.keyword}

## frag2 {.fragment}

### Paquienséguy_visuelp.1 {.main .main-citation}

> Les e-albums reflètent le glissement du « multimédia » au « transmédia » de Jenkins et son « univers narratif original »
. Ils deviennent un complément aux grandes expositions entre le catalogue d’exposition, la conférence introductive et la trace

[Françoise Paquienséguy]{.source}

### biblio {.glose .glose-biblio}

[Henry Jenkins]{.biblio-auteur},
[Convergence Culture: Where Old and New Media Collide]{.biblio-titre},
[2006, New York: New York University Press.]{.biblio-autre}

## frag3 {.fragment}

### Tréhondart-fiche + Tréhondart_photo {.main .main-photo .polaroid}

![Nolwenn Tréhondart](img/photos/trehondart.jpg)

### g {.glose .glose-texte}

Nolwenn Tréhondar présente les résultats d’un projet de recherche qui vise à dresser un bilan du livre numérique en France. C’est un ouvrage collectif réalisé dans une démarche pluridisplinaire.


## frag4 {.fragment}

### Gascuel_fiche&nbsp;: TRANSMEDIA ==> AU-DELÀ DES FRONTIÈRES, OUVERTURE DU SPECTRE-LIVRE {.main .main-texte}

L’objet transmédia est quelque chose qui dépasse largement le spectre du livre traditionnel, papier. Il est un objet connecté au monde, aux objets et est plus un outil qu’un objet fermé. « Trans- », il dépasse les notions de clôtures traditionnelles. On trouve, parmi les prototypes de ces objets, des étiquettes de vin où sont reliées des recettes qui s’y accordent, _Don Camillo_, bouquin par lequel on accède à un film, récit pour enfants (_Petit ours brun_) qui se relie à des épisodes de séries jeunesse. Le livre transmédia, dans ces exemples, est une clé d’accès à un contenu numérique. C’est un gadget.

### g {.glose .glose-slide}

![Slide Jean-Michel Gascuel](img/gascuel-slide01.png)

## frag5 {.fragment}

### Lacelle_visuel[texte issu du pwp, p. 23-28] {.main .main-slide}

![Présentation de Nathalie Lacelle, Marie-Christine Beaudry, Prune Lieutier](img/slides/Lacelle.png)

### Lacelle_fiche {.glose .glose-text}

Nathalie Lacelle, Marie-Christine Beaudry, Prune Lieutier soulignent le manque de «&nbsp;traducteurs&nbsp;» entre les maillons, les mondes de la pratique et de la recherche.
