# Clôture {.chapter}
[Clôture]{.shorter}

## frag {.fragment .break-before-right}

### Table-Ronde {.main .main-citation}

> Quand les formes de langage s’évaporeront, tout redeviendra littérature.

[Nolwenn Tréhondart]{.source}

### g {.glose .glose-texte}

Comme l’appelle de ses vœux Nolwenn Tréhondart, espérons qu’à un moment la _littérature numérique_ deviendra tout simplement de la _littérature_. Aux yeux de nombreux éditeurs, le livre numérique est encore marqué par la clôture qui l’a historiquement caractérisé. Il faudrait former un espace protégé dans l’espace des lectures industrielles.

## frag {.fragment}

### Laborderie_PP {.main .main-slide}

![Présentation d’Arnaud Laborderie](img/slides/Laborderie_visuelp.4_cloture.jpg)

### Laborderie-fiche {.glose .glose-texte}

La question de la clôture est-elle seulement valable pour le livre objet&nbsp;? Comment s’insère-t-elle au sein du livre numérique&nbsp;?


## frag {.fragment}

### Raymond_visuelp.6 {.main .main-slide}

![Présentation de Dominique Raymond](img/slides/Raymond_visuel.jpg)

### glose {.glose}

#### g {.glose-citation}

> Le livre est un réseau.

[Arnaud Laborderie]{.source}

#### g {.glose-citation}

> Le livre est un nœud dans un réseau.

[Michel Foucault]{.source}

## frag {.fragment}

### Tolebrun_tweet {.main .main-twitter-tweet}
<blockquote class="twitter-tweet" data-lang="fr"><p lang="fr" dir="ltr"><a href="https://twitter.com/hashtag/ECRIDIL2018?src=hash&amp;ref_src=twsrc%5Etfw">#ECRIDIL2018</a> «&nbsp;Le livre se définit normalement par sa clôture, c&#39;est un espace fermé. Il y a un oxymore à parler de livre “numérique”, car un livre numérique est par définition ouvert, car connecté.&nbsp;»</p>&mdash; Tom Lebrun (@tolebrun) <a href="https://twitter.com/tolebrun/status/991318464513048576?ref_src=twsrc%5Etfw">1 mai 2018</a></blockquote>

### Laborderie_notecoll. {.glose .glose-texte}

Arnaud Laborderie propose de penser les nouvelles formes du livre dans l’espace numérique avec les métaphores de la cellule et du jardin &mdash;&nbsp;des espaces clos, mais en expansion. Ces images illustrent le prolongement du livre vers un espace extérieur qu’il déborde et s’approprie. La clôture définit un espace que le lecteur peut arpenter.
