# Littératie {.chapter }

### Drochon_tweet #approche

[[https://twitter.com/tolebrun/status/990955369940897792]{.underline}](https://twitter.com/tolebrun/status/990955369940897792)

"[[#ECRIDIL2018]{.underline}](https://twitter.com/hashtag/ECRIDIL2018?src=hash)
Julien Drochon anime avec
[[@AnthonyMasure]{.underline}](https://twitter.com/AnthonyMasure) un
programme de recherche (Vox Machine) qui cherche à relier
recherche-création avec une approche d’enseignement".


###  @Masure-fiche&nbsp;:

Quels rôles peuvent jouer les designers dans la transmission des savoirs
à un public élargi?
Rendre le livre attrayant à tous; sensibiliser le milieu de la recherche
à ces problématiques.
