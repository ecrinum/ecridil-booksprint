# Transferts {.chapter}
[Transferts]{.shorter}

## frag1 {.fragment .break-before-right}

### Deseilligny_pad {.main .main-citation}

> Le livre fabrique un ethos de marque culturelle.  

[Oriane Deseilligny]{.source}

### g {.glose .glose-texte}

En dehors de l’industrie éditoriale, les marques (de vêtements, d’accessoires...) qui utilisent le livre dans leurs publicités aspirent à s’approprier ses connotations symboliques et affectives, notamment avec la rhétorique visuelle ou picturale et les codes de la nature morte, du _punctum_.

## frag2 {.fragment}

### Deseilligny_PP {.main .main-slide}

![Oriane Deseilligny](img/slides/Deseilligny_visuelp.13_miseenscene.png)

### Deseilligny_pad {.glose .glose-texte}

Oriane Deseilligny propose une étude du livre imprimé en régime numérique, à travers une analyse des processus de communications et de mise en scène du livre en tant qu’objet imprimé. Le livre, en ce sens, devient le sujet principal d’images et d’approches en communication en tant qu’il est porteur d’une signification. On reconnaît ainsi l’héritage d’une culture visuelle avec des formes modernes qui s’apparentent à des cabinets de curiosités, tout particulièrement chez les blogueurs.

[cabinetsdecuriosités]{.keyword}

## frag3 {.fragment}

### Table Ronde {C}-PAD {.main .main-photo}

![Ariane Savoie (gauche) Nolwenn Tréhondart (centre) et Lucile Haute (droite) participent à la table ronde _Arts, littérature et formes numériques du livre_](img/photos/c.jpg)

### g {.glose}

#### g {.glose-texte}

Conclusion des échanges sur le livre d’art numérique&nbsp;: il faut travailler collectivement pour mieux appréhender l’objet-création.

#### g {.glose-texte}

Parmi les projets éditoriaux numériques qui font surface, il s’agit pour la plupart d’expérimentations sans lendemain, car les artistes sont confrontés à des contextes industriels contraignants. Les acteurs du marché du livre ont du mal à dépasser le statut d’objet livre dans ses dimensions culturelles et esthétiques. Les éditeurs ont alors eux-mêmes des difficultés à passer au beau livre numérique. Quelques éditeurs de livres d’art ont essayé de développer leurs collections en epub - mais cela reste anecdotique. Un éditeur de livre d’art qui fait aujourd’hui des livres numériques démontre une idée de stratégie de distinction par l’innovation.

## frag4 {.fragment}

### Table Ronde {C}-PAD {.main .main-citation}

>La question de la traduction nous ramène à la question de l’écriture&nbsp;: comment écrire une œuvre hypermédiatique sur le plan autant littéraire que programmatif ?

[Ariane Savoie]{.source}

### g {.glose .glose-texte}

L’échange entre l’écriture numérique et papier est questionnée de plus en plus par la revue _bleuOrange_, confrontée à l’obsolescence des technologies et des différents langages.

## frag6 {.fragment}

### Gascuel_fiche&nbsp;: TRANSMEDIA ==> AU-DELÀ DES FRONTIÈRES, OUVERTURE DU SPECTRE-LIVRE {.main .main-texte}

L’objet transmédia est quelque chose qui dépasse largement le spectre du livre traditionnel, papier. Il est un objet connecté au monde et aux autres objets. Il est bien plus qu’un outil ou qu’un objet fermé. «&nbsp;Trans-&nbsp;», il dépasse les notions de clôtures traditionnelles. On trouve, parmi les prototypes de ces objets, des étiquettes de vin où sont reliées des recettes qui s’y accordent&nbsp;; _Don Camillo_, un livre par lequel on accède à un film&nbsp;; un récit pour enfants (_Petit ours brun_) qui se relie à des épisodes de séries jeunesse... Le livre transmédia, dans ces exemples, est une clé d’accès à un contenu numérique.

### g {.glose .glose-slide}

![Présentation de Jean-Michel Gascuel](img/gascuel-slide01.png)


## frag7 {.fragment}

### m {.main .main-slide}

![Présentation d’Elsa Tadier](img/slides/Tadier_visuelp.6_miseenscene.png)

![Présentation d’Oriane Deseilligny](img/slides/Deseilligny_visuelp.2_miseenscene.png)

### CarnetDLIS {.glose .glose-twitter-tweet}

<blockquote class="twitter-tweet" data-lang="fr"><p lang="fr" dir="ltr"><a href="https://twitter.com/hashtag/ecridil2018?src=hash&amp;ref_src=twsrc%5Etfw">#ecridil2018</a> Magnifique présentation d&#39;Elsa Tadier qui rappelle étrangement celle d&#39;<a href="https://twitter.com/ODeseilligny?ref_src=twsrc%5Etfw">@ODeseilligny</a> hier matin (l&#39;image du livre, de la peinture Renaissance aux blogues de lecteurs)</p>&mdash; Servanne M. (@servanne_m) <a href="https://twitter.com/servanne_m/status/991345477261111296?ref_src=twsrc%5Etfw">1 mai 2018</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
