## Fragment

### Drochon_captured’écrandeladiapo6 #fragment

### Tréhondart-fiche (mot-clé #fragmentation):

«&nbsp;Il y a un risque de fragmentation du contenu dans le livre d’art
numérique&nbsp;».

###  @Laborderie PAD&nbsp;:

Principe de morcellement et de fragment&nbsp;: volonté d’échapper à un mode
clôt.

#cellule/#fragment

### Charbonneau:

Critique ces métaphores. enclosures au R-U, émergence de la propriété
individuelle [?]
Mouvement des enclosures au Royaume Uni
Dans la cellule, il y a aussi l’infection
La cellule n’est pertinente que s’il y a une masse critique (plusieurs
cellules pour faire un corps)
Masse critique&nbsp;: plusieurs cellules pour faire corps
Idée de la rhétorique des clotures qui va profondément vers le droit
propriétaires.

Membrane cellulaire plutôt que cloture. Lieu de circulation ente mondes
intérieur et extérieur
On a besoin de points dans le web où il y a une sorte de condensation du
savoir, de l’imaginaire mais aussi un mouvement centrifuge qui va vers
l’extérieur.

### Jahjah PAD&nbsp;:

Gestes (configurations)&nbsp;: continu (tourner la page qui crée de la
désorientation), discontinu (hyperlien qui crée le fil d’ariane)&nbsp;:
fragmentation

###  @Rio_PAD&nbsp;:

Mise en scène&nbsp;: Format court et sériel, saisons et épisodes -> autre
univers que la littérature.
