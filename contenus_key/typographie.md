# Typographie {.chapter}
[Typographie]{.shorter}

## frag1 {.fragment .break-before-right}

### Perret_fiche + Perret_photo {.main .main-photo .polaroid}

![Arthur Perret](img/photos/perret2.jpg)

### g {.glose .glose-texte}

Arthur Perret part du postulat que la typographie est tout ce qu’il reste du papier et que c’est un point de passage entre l’imprimé et l’écran.  

## frag {.fragment}

### Tadier_Pad {.main .main-texte}

Dans l’histoire du livre, les choix typographiques ont parfois mis en place un jeu de connotations déterminant la valeur du livre ou celle du texte. Qu’en est-il aujourd’hui de cette charge symbolique&nbsp;? Comme l’indique Elsa Tadier, la perspective sémiotique peut nous apporter une aide précieuse, parce qu’elle va interroger les valeurs connotées par la matérialité du livre. Que ce soit face au livre ou, même, face à la tablette, il devient important de s’interroger sur le rapport entre une valeur «&nbsp;perçue&nbsp;» et la valeur économique.

### g. {.glose .glose-biblio}

[Gérard Blanchard]{.biblio-auteur}, [Sémiotique de la typographie]{.biblio-titre}[, Les éditions Riguil Internationales, Québec, 1982]{.biblio-autre}

## frag2 {.fragment}

### Perret_diapo10 {.main .main-slide}

![Présentation d’Arthur Perret](img/slides/Peret_visuelp.10_architexte.png)

### g {.glose}

## frag {.fragment}

### masure {.main .main-texte}

Anthony Masure souligne le problème d’une gestion typographique parfois «&nbsp;hasardeuse&nbsp;» dans les publications numériques, réalisée sans prendre en compte les pratiques et les codes pourtant bien établis dans les techniques de mise en page (la typographie s’est en effet formée et perfectionnée sur plusieurs siècles). Les qualités graphiques des objets de lecture reçoivent généralement peu d’attention sur le plan de la forme et prennent peu en compte le retour des lecteurs (pour qui on écrit pourtant&nbsp;!). Le design d’interface du livre web est donc de qualité très variable. Une question essentielle se pose alors&nbsp;: quel rôle peuvent jouer les designers dans la transmission des savoirs à un public élargi&nbsp;?

### Perret_tweet {.glose .glose-twitter-tweet}

<blockquote class="twitter-tweet" data-lang="fr"><p lang="fr" dir="ltr">Une pointe de typographie à <a href="https://twitter.com/hashtag/Ecridil2018?src=hash&amp;ref_src=twsrc%5Etfw">#Ecridil2018</a> par <a href="https://twitter.com/arthurperret?ref_src=twsrc%5Etfw">@arthurperret</a>, featuring <a href="https://twitter.com/hashtag/TypoFaune?src=hash&amp;ref_src=twsrc%5Etfw">#TypoFaune</a> <a href="https://twitter.com/CNAPfr?ref_src=twsrc%5Etfw">@CNAPfr</a> <a href="https://twitter.com/alicesavoie?ref_src=twsrc%5Etfw">@alicesavoie</a> ![pic.twitter.com/IdZVqD8YTw](img/photos/perretTwitter.jpg)</p>&mdash; Anthony Masure (@AnthonyMasure) <a href="https://twitter.com/AnthonyMasure/status/991338605451862016?ref_src=twsrc%5Etfw">1 mai 2018</a></blockquote>
