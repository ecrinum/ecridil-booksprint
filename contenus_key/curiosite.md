# Curiosité {.chapter}

## frag1 {.fragment .break-before-right}

###  @Gascuel_fiche {.main .main-texte}

Travaille aux éditions _Googtime_. Le livre se trouve dans une période
charnière, au carrefour de deux moments&nbsp;: celui du papier et celui
d’une transmédialité&nbsp;: un nouvel objet, connecté -- sans application
qui font l’intermédiaire entre les objets connectés.

### Tréhondart-fiche {.glose .glose-texte}

(mot-clé #vision_augmentée)&nbsp;: "Il permet une vision augmentée de
l’art grâce à des images en haute résolution ainsi que la possibilité
de zoomer sur les tableaux."
