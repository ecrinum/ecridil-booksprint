# Sensorialité {.chapter}
[Sensorialité]{.shorter}

## frag1 {.fragment .break-before-right}

### t {.main .main-twitter-tweet}

<blockquote class=“twitter-tweet” data-lang=“fr”><p lang=“fr” dir=“ltr”><a href=“https://twitter.com/hashtag/ECRIDIL2018?src=hash&amp;ref_src=twsrc%5Etfw“>#ECRIDIL2018</a> Poreuse&nbsp;: sur la question des configurations, Poreuse est vu comme un espace de désorientation. Tous se perdent dans la configuration proposée. La cause&nbsp;? Un tressage polyphonique, mais aussi un horizon d&#39;attente pour une lecture discontinue au travers de l&#39;hyperlien</p>&mdash; Tom Lebrun (@tolebrun) <a href=“https://twitter.com/tolebrun/status/990947614777597952?ref_src=twsrc%5Etfw”>30 avril 2018</a></blockquote>

### g {.glose}

## frag {.fragment}

### Laborderie_PDF {.main .main-texte}

Arnaud Laborderie considère que l’édition numérique enrichie de _Candide_ vient modéliser et proposer au lecteur différents usages&nbsp;: lire, explorer, éditorialiser.

### g {.glose}

## frag {.fragment}

### g {.main .main-citation}

> Vox Machines vise notamment à expérimenter le rapprochement des
interactions vocales et de l’écrit par la production de dispositifs
sensibles multi-utilisateurs hybridant le visuel et le vocal.

[Julien Drochon]{.source}

### Drochon_diapo2 {.glose .glose-slide}

![Présentation de Julien Drochon, Vox Machines](img/slides/Drochon_visuelp.2_Sensorialite.png)

## frag {.fragment}

### Paquienséguy_visuelp.2 {.main .main-texte}

Françoise Paquienséguy se demande si le numérique ne génèrerait pas de nouvelles pratiques&nbsp;: «&nbsp;revoir, voir pendant, voir plusieurs choses en même temps, dont
l’appropriation est quasi physique&nbsp;: toucher, emporter avec soi, zoomer,
écouter...&nbsp;»

Avec ces pratiques de lecture, Nolwenn Tréhondart considère que le livre numérique renoue en fait avec une certaine sensualité livresque (mouvements, animations etc...).

### g {.glose}

## frag {.fragment}

### Tadier_Slide_2 {.main .main-slide}

![Présentation d’Elsa Tadier](img/slides/Tadier_visuelp.2_sensorialite.png)

### g {.glose .glose-texte}

Pour Elsa Tadier, il existe une intimité entre fond et forme. Cette
proximité est liée au rôle des corps, au rôle du designer mais également
au rôle du lecteur. Il est donc nécessaire d’insister sur la place du corps en contexte
numérique.


## frag {.fragment}

###  carnetdlis {.main .main-twitter-tweet}

<blockquote class="twitter-tweet" data-lang="fr"><p lang="fr" dir="ltr"><a href="https://twitter.com/hashtag/ecridil2018?src=hash&amp;ref_src=twsrc%5Etfw">#ecridil2018</a> Elsa Tadier&nbsp;: le livre suscite le corps à corps. Le corps comme partie intégrante du livre ?</p>&mdash; DLIS (@carnetdlis) <a href="https://twitter.com/carnetdlis/status/991344996589678598?ref_src=twsrc%5Etfw">1 mai 2018</a></blockquote>

### g {.glose}

## frag {.fragment}

###  Masure_fiche {.main .main-citation}

Les interfaces ont pour but de rendre les contenus sensibles.

[Anthony Masure]{.source}

### g {.glose}

## frag {.fragment}

### Haute_fiche {.main .main-citation}

Quelle serait la forme de publication pour une recherche en art et création (et non pas seulement sur l’art et la création)?

[Lucile Haute et Julie Blanc]{.source}

### g {.glose .glose-texte}

La publication scientifique ne tient pas assez en compte les aspects
graphiques et sensibles. Elle présuppose une idéalité du sens face à la forme et rencontre également des problèmes économiques notamment sur le maintient des revues. Les propositions «&nbsp;clé en main&nbsp;» ont entraîné un formatage technique des revues publiées dans des types hypernormés &mdash;&nbsp;ce qui facilite certes la production et la consultation, mais aux dépens du design. À l’heure actuelle, une même maquette peut être appliquée pour des propositions très différentes, comme si une forme pouvait se prêter à de multiples contenus.

## frag {.fragment}

### Tadier_Slide_4 {.main .main-slide}

![Psautier et Rosaire de la Vierge](img/slides/Tadier_visuelp.4_sensorialite.png)

### g {.glose .glose-texte}

La question des rapports entre corps et design
n’est pas neuve. Elsa Tadier donne l’exemple d’Hegerton (1821), un livre de dévotion anglais
du XVe siècle qui reprend esthétiquement le mythe de l’eucharistie en représentant un livre ensanglanté.


## frag {.fragment}

### main {.main .main-citation}

>Sur Opuscules, nous avons récolté toutes les voix des écrivains et écrivaines. Cela nous rapproche des pratiques des auteurs - c’est une chose de lire un texte à l’écran, c’est une autre chose d’avoir le souffle du poète, le rythme, de sentir les enjambements... Cette mise en tension entre le texte et la voix de l’auteur permet de s’approprier un peu mieux le texte.

[Benoît Bordeleau]{.source}

### glose {.glose .glose-slide}

![Le projet Opuscules (dirigé par Bertrand Gervais), publie désormais des extraits de textes lus par les auteurs.](img/slides/opuscules.png)


## frag {.fragment}

### GASCUEL_FICHE {.main .main-citation}

> Le livre connecté est un objet &mdash;&nbsp;sa numéricité, son code n’est pas
l’intérêt&nbsp;: l’expérience de lecture est sensorielle&nbsp;; sens et
sensorialité sont consubstantielles.

[Jean-Michel Gascuel]{.source}

### g {.glose}

## frag {.fragment}

### Souchier-fiche {.main .main-texte}

Pour Emmanuël Souchier, le livre porte la présence du geste, de la main ainsi qu’une trace des voix et des corps. Il modèle la manière de voir le monde. L’émancipation du corps, de l’espace et du temps est la première technonologie de l’intellect de l’humanité.

### g {.glose}
