# Sémiotique {.chapter}
[Sémiotique]{.shorter}

## frag1 {.fragment .break-before-right}

### Tréhondart-fiche (mot-clé #sémiotique_sociale) {.main .main-citation}

>La sémiotique sociale est une méthodologie en construction.

[Nolwenn Tréhondart]{.source}


## frag {.fragment}

### Perret_slide {.main .main-slide}

![Présentation d’Arthur Perret, une sémiotique des fluides](img/slides/Perret_visuelp.12_Semiotique.png)

###  Tadier_Pad {.glose .glose-texte}

Elsa Tadier et Emmanuël Souchier soulèvent l’idée d’une rupture sémiotique.
Avec les tablettes, la perception même du volume est en effet à requestionner.


## frag {.fragment}

###  Palatine_PAD&nbsp;: sur la circulation du sens {.main .main-texte}

Le projet d’édition de l’Anthologie Palatine mené par l’équipe de la CRC sur les écritures numériques est réalisé en collaboration avec deux lycées italiens qui travaillent à l’édition des textes sous la supervision de professeurs de grec ou de latin. Si les contributeurs au projet de transcription et de traduction peuvent travailler autour de l’imaginaire anthologique, c’est d’abord grâce à la structure de la plateforme, qui permet d’ajouter des contenus additionnels comme des liens vers youtube, ou encore des objets iconographiques (notamment des images de vases grecs, etc.). L’édition ne cherche donc pas à établir la vérité du «&nbsp;texte&nbsp;», mais elle travaille autour de la notion d’imaginaire collectif et populaire. Une API a été conçue pour proposer une structuration ouverte des données. N’importe qui peut donc participer, proposer des versions, ce qui génère un phénomène de résonnance entre les textes plutôt que la simple traduction d’un original.

## frag {.fragment}

### tweet {.main .main-twitter-tweet}

<blockquote class="twitter-tweet" data-lang="fr"><p lang="fr" dir="ltr"><a href="https://twitter.com/hashtag/ecridil18?src=hash&amp;ref_src=twsrc%5Etfw">#ecridil18</a> Le livre _Poreuse_ met à l&#39;épreuve les concepts de l&#39;édition traditionnelle; l&#39;auteure J.&nbsp;Mézenc parle d&#39;œuvre hybride, où se croisent et se mélangent les voix de 3 personnages. Polyphonie, mais aussi matérialités mélangées. Un paratexte qui construit une errance</p>&mdash; Tom Lebrun (@tolebrun) <a href="https://twitter.com/tolebrun/status/990945492958838784?ref_src=twsrc%5Etfw">30 avril 2018</a></blockquote>

### tweet {.glose .glose-twitter-tweet}

<blockquote class="twitter-tweet" data-lang="fr"><p lang="fr" dir="ltr">... Avec des termes de labyrinthe, de fil d&#39;ariane, etc. L&#39;espace éditorial, supports comme gestes, s&#39;organisent d&#39;une manière spécifique ==&gt; l&#39;enjeu de la présentation est de comprendre comment</p>&mdash; Tom Lebrun (@tolebrun) <a href="https://twitter.com/tolebrun/status/990945715408031745?ref_src=twsrc%5Etfw">30 avril 2018</a></blockquote>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>


## frag {.fragment}

### Deseilligny_slide {.main .main-slide}

![Présentation d’Oriane Deseilligny](img/slides/Deseilligny_visuelp.10_Semiotique.png)

### g {.glose .glose-texte}

Dans ses mises en scènes sur le web (blogues de lecteurs, sites d’éditeurs ou sites de marques qui n’ont rien à voir avec le monde de l’édition), le livre est scénographié en fonction de ses signifiants et de ses connotations positives. Certains motifs récurrents - les fleurs, les tasses à café, le fauteuil... renvoient au moment de la lecture, conçu comme un moment de divertissement. En revanche, on peut rester perplexes devant l’association entre le livre et les chaussures à talon !
