#Fiche synthèse


###Hypothèse de travail de l'intervenant


La communication proposer une étude du livre imprimé en régime numérique, à travers une analyse des processus de communications et de mise en scènes du livre en tant qu'objet imprimé en contexte numérique. 

Il s'agit de démontrer que le livre imprimé devient, en contexte numérique, une élément de design visuel servant un objectif commercial. Le devient le sujet principal d'images et d'approches en communication en tant qu'il est porteur d'une signification 

Le livre devient un simple "adjuvant". Décontextualisation du livre par son appropriation / recontextualisation dans l'espace numérique = tt cela exprime l'attachement au livre imprimé (ses symboles)

De plus, cette imagerie est aussi utilisée par les auteurs pour éduquer le lecteur (notamment littérature jeunesse). Explication de la chaine éditoriale et la fabrique du livre. Les différents mondes du livre cherchent à représenter leur médiation qui est mise à l'épreuve par le numérique. 


###Positionnement théorique : auteur.s, courant.s de pensée, théorie.s, 

Info-com - inscription dans les travaux sur l'énonciation éditoriale (Souchier/Jeanneret)
Sur les réseaux sociaux, le livre comme objet matériel existe aussi via l'énonciation éditoriale des différents acteurs qui la composent. Les éditeurs mettent l'accent sur cette énonciation éditoriale. 
Approche qui prend à rebours le sujet du colloque, pour montrer la permanence d'un jeu avec le volume et la matérialité de l'imprimé présents à l'écran.  


###Idées principales défendues

## Livre comme support d'accessoire de mode 
* Fonction présentoir du livre - dimension symbolique effacée.
* Le livre porteur de signifiants connotés positivement.
* La marque qui utilise le livre dans ses publicités aspire ces connotation symboliques et affectives. 
* Le livre fabrique un ethos de marque culturelle.


## Livre comme affirmation d'une prétention littéraire  
* Le livre suggère un espace habité et connote la pratique de la lecture (style de vie et pratique investie, réelle) cf Barthes. 
* Cette accessoirisation propre à Instagram ("instagrama"= pays fantastique d'Instagram)
* Le design de ces livres comprend des récurrences  : les livres anciens, la collection Blanche, mais aussi les "beaux-livres" propices à une lecture cursive, rapide. 


## Livre comme élément promotionnel (maison édition ou autre)
* Effet seuil du livre, qui vient activer un imaginaire dans le cadre d'une approche/stratégie marketing.
* Hédonisme du bien-être (divertissement et loisir, dont le livre et son titre sont un ressort) 
* Construction/jeu d'écart par rapport aux connotations de certaines marques - marketing de marque (Ipad avec Hamlet) "utilisez cet ancien bout de papier en déco d'intérieur". 


## Le livre scénographié (composition visuelle très présente dans les blogues )
* Le livre comme posture / activité (mannequins représentés en train de livre)
* Dans les visuels composés autour des ouvrages par les maisons d'édition ou encore les blagueurs, le livre est l'objet d'un design visuel un objet de curiosité. La xcénographie du livre et du lire vient créer un horizon d'attente. 
- Héritage d'une culture visuelle = formes modernes des cabinets de curiosité (surtout chez les blogueurs), jeu avec l'écclectisme des motifs et objets convoqués. Ces motifs récurrents peuvent être systématisés : fleurs, tasses à café... description du moment de la lecture. Ces visuels accompagnent une chronique ou une critique du livre. 
- rhétorique visuelle ou picturale - codes de la nature morte / punctum.
- poétique de l'immersion (Saemmer - plonger le lecteur dans un univers)


###Méthodologie de travail

Il s'agit principalement d'analyser la manière dont sont énoncés et représentés les livres sur les réseaux sociaux (braconnage sur les blogues, réseaux sociaux de l'image comme Instagram)

Analyse d'une anthologie visuelle de la lecture des blogueurs. 

###Corpus / objets et exemples présentés 

- blogues de lecteurs/lectrices
- Instagram 
- sites d'éditeurs 
- journaux numériques 

###Défis techniques
*Non pertinent *

###Conversations sur le pad collectif


###Références citées : publication scientifique mais aussi pages web
*Alexandra Saemmer *
Emmanuël Souchier 
Yves Jeanneret 

###Vos remarques personnelles

Démontre un phénomène de remédiation et rétromédiation du livre imprimé, dont les connotations deviennent plus fortes (ou autres) en contexte numérique. L'imprimer gagne en connotations (qualitatif, littéraire).


###Questions /remarques dans la salle 

Question : (au sujet de « Représentions, mise en scène, autorité... ») représentation de mise en scène, cette visibilité des ouvrages dans d'autres contextes, notamment politique (exemple avec Macron et nouvelle photo présidentielle qui fait apparaître des livres). 

   * Comment les professionnels (libraires, éditeurs) du livre mettent en scène les objets-livres, comment l'iconographie du livre est abordée aujourd'hui, quelque chose de nouveau est-il en train de se passer ? 
   * Sur un site web de libraire ou d'éditeur, comment l'iconographie du livre est-elle abordée ?
   * 
-> c'est davantage la librairie qui est représentée comme espace de sociabilité, de dédicace, d'échanges. Avec les coups de coeur papier, etc. L'espace de la librairie est plutôt dédié à ?? et site web comme espace de représentation ??


Renée Bourassa : Le livre abordée en tant qu'objet de design amène une valeur marketing  assez forte. Pas tant son contenu que son image.

