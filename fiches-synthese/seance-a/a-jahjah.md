#
#Fiche synthèse

Titre : Interroger la matérialité numérique d'une oeuvre hybride : énonciation éditoriale de Poreuse


###Hypothèse de travail de l'intervenant
*5 lignes max*

comment la dynamique de travail entre acteurs participe de la fabrication des signes

renégociation constante des rôles entre les 3 actrices :

   *     - articulation d'une narration linéaire vs tressage des voix
les acteurs argumentent entre eux sur la receptivité du texte possibles : intentionnels, organisationnels, interprétatifs, techniques, structurels

Gestes (configurations) : continu (tourner la page qui crée de la désorientation), discontinu (hyperlien qui crée le fil d'ariane) : fragmentation 

Recepteurs : 

   *     - code sémiotique : couleurs, glyphes (en-tete de chaque fragment) pour identifier un personnage,           - notice (le fil d'ariane, le labyrinthe, la linéarité)
          

###Positionnement théorique : auteur.s, courant.s de pensée, théorie.s, 
*5 lignes max*

énonciation éditoriale (Souchier et Jeanneret) : chaque métier de la fabrication du livre a laissé une trace de son passage sur le livre.

###Idées principales défendues
*5 lignes max par idée défendue*

La matérialité : linéarité, déplacement du lecteur, 
- labyrinthe, file d'ariane, errance : enigme à résoudre par l'expérience de lecture

renégociation constante entre les acteurs travaillant la tension entre une linéarité-labyrinthe et une discontinuité-fil d'Ariane.

matérialité éditoriale est retravaillé par les lecteurs, leur interprétation, ils deviennent des actants.

idée d'une underlineAutorité distribuéeunderline à travers la généalogie des signes et ordres négociés
distribution de la cognition dans la matérialité de leur organisation (déploiement des outils)
arguments anticipés dans la fabrication de l'espace éditorial

###Méthodologie de travail
*5 lignes max* 

L'énigme éditoriale Poreuse autour de 4 axes :
   1. Acteurs : 

       * sur la base d'entretiens, corpus de 40 mails de travail : Juliette Mézenc, Roxane Lecomte (conceptrice, fabrique du texte num), Chrstine Jeanney (préparatrice éditoriale)    2. Occurrences:
    diff. déclinaison d'un meme type (epub, imprimé)
    3. Configuration :  problématique de parcours (continu et discontinu)
    4. Récepteurs : mode d'interprétation de la lecture : gestes

###Corpus / objets et exemples présentés 
*5 lignes max*

Poreuse, Publie.net, publié en format numérique 2012, maj en 2017 : oeuvre hybride

   * 3 personnages se croisent, rapport à l'altérité (fragments, tressage des voix)
###Défis techniques
*5 lignes max*

--

###Conversations sur le pad collectif


###Références citées : publication scientifique mais aussi pages web
*présentées en liste*

L'énonciation éditoriale (Souchier et Jeanneret)

###Vos remarques personnelles
*5 lignes max*

###Questions /remarques dans la salle 

Jahjah : le code éditorial et la notice cherche à liimiter les interprétations infinies de l'espace éditorial
espace editorial : superposition de plans : économiques, structurels, matériel, l'espace éditorial est le compromis entre les diff. plans : d'où l'idée d'autorité distribuée

MVR : l'énonciation est multiple aussi dans le livre papier, l'autorité y était aussi très distribuée.

Souchier :  hypothèse sur l'autorité : liée à la cohérence en tant que dispositif matériel qui assure l'autorité de tous les acteurs de l'énonciation éditoriale.
    sur le numérique : c'est le fragment : des supports, des pratiques. quelle autorité pour ces fragments.

Jahjah: la stratification permet de retravailler cette notion de mélange : qui a fait quoi, écoulement des actions de chacun dans les actions des autres.
