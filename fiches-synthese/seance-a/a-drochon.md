#Fiche synthèse : J. Drochon

##Principes 

   * Une prise de note est effectuée de manière collaborative pendant le colloque. 
   * La fiche synthèse est produite pendant et après la communication et peut s'appuyer sur la prise de note collaborative.
   * Les synthèses doivent être relativement bien rédigées (on n'est plus dans la simple prise de note)
   * L'objectif des synthèses sera de produire une grille de lecture de l'ensemble du colloque. Cette grille de lecture servira la seconde phase d'édition.
   * Tous les champs ne doivent pas nécessairement être remplis - parfois ils ne seront pas pertinents - c'est plutôt à titre indicatif
##Vos prises de notes


##Fiche synthèse


###Hypothèse de travail de l'intervenant

Plasticités plurielles des lectures numériques 

N'utilise pas le mot livre car ce terme cristallise des représentations qu'il va essayer de déjouer

###Positionnement théorique : auteur.s, courant.s de pensée, théorie.s, 


###Idées principales défendues

La lecture sur support numérique implique un processus de dissociation chez le lecteur.

2 formes du lecteurs :
    - lecteur-appareil : appareillage par lequel il faut passer pour accéder à la lecture du texte
    - lecteur humain
    
Lecture numérique de discrétisation (pendant de la question de la numérisation)
    
Ce que le livre peut proposer : une expérience spécifique de consultation du contenu.
Aujourd'hui, consultation protéiforme : ce qui permet d'imaginer la forme d'un livre, c'est le choix du lecteur de participer à un type d'expérience esthétique.

Avec la multiplication de ces lecteurs-appareils, que reste-t-il de la lecture du monde ?

###Méthodologie de travail

Méthodologie qui combine expérimentation et observation :

**1. Expérimentation sur 2 projets auxquels il participe : **
    
 - **Vox machines** (sous la responsabilité d'Anthony Masure), programme de recherche-création
Confronte les approches de la performativité vocale + de la forme typographique
S'est posé la question de comment faire se rencontrer l'affiche et l'assistant conversationnel ?
-> penser un dispositif qui mettrait en relation des usagers et les dispositifs de reconnaissance vocale sur mobile.

Proposer différentes formes et différents signes sur l'espace public de l'affiche. Mixer la question de la proximité et de la mise au public de cette privacité.

Différents contenus (textes, voix, iconiques) qui permettent de retrouver un espace de partage commun sur l'affiche

**- A caractère secret. **
Se poser la question de quelle place on laisse aux utilisateurs pour exister dans l'espace possible de création sur téléphone mobile ?
Communication d'un secret qui permet l'affichage de caractères qui crée un autre secret.

**2. Observation de plusieurs projets : **
 - The moving poster : relations entre expressivité typographique et format poster
 - festival de Breda : atelier de fabrication de livres
  - exposition Biennale Venise : concept de Stay Radical

###Corpus / objets et exemples présentés 
*5 lignes max*

**The moving poster : **

**festival de Breda : **Atelier de fabrication de livres (studio Indiamen), constituer un livre à partir de formes fragmentaires préexistantes.
Visiteurs de l'exposition qui pouvaient être mis à niveau - faire exister une édition dans le cadre de cet atelier.
Les principes de fabrication et de constitution des contenus et des formes s'adressent à différents types de public et sont réalises au même endroit

**Concept de Stay Radical :** espace d'expérience de la lecture et de consultation des livres numériques (dispositi présenté à la Biennale de Venise). Rouleaux qui déterminent la disposition de l'espace

###Défis techniques
*5 lignes max*



###Conversations sur le pad collectif


###Références citées : publication scientifique mais aussi pages web
*présentées en liste*

###Vos remarques personnelles
*5 lignes max*

###Questions /remarques dans la salle 

- question 1 : qui, quoi, réponse

- question 2 : 

