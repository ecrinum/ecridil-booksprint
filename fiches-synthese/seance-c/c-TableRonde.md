#*Table ronde {C} « Arts, littérature et formes numériques du livre »*




##Vos prises de notes

CF. plus bas


##Fiche synthèse


###Hypothèse de travail de l'intervenant

Question du 'beau livre", comment lui rendre justice en numérique. Commen tenvisager l'art ce qui a trait à l'art dans le numérique (notamment ce qui touche aux musées et aux catalogues).
Question de la traduction/adaptation/interprétation : Lléchange entre l'écriture numérique et papier est questionnée de plus en plus par Bleu Orange car on est confronté à l'obsolescence des technologies. La revue est toujours confrontée à ces différents langages. En train de revoir la mission pour revoir la question de traduction. 

###Positionnement théorique : auteur.s, courant.s de pensée, théorie.s, 


###Idées principales défendues
Volonté de développer des beaux livres numériques, vendus très chers car le livre fait la personne que l'on est. 
Question du code légal en France. 
Frein par les éditeurs pour qui le livre reste attaché à l'objet livre, contraintes dans la production et la diffusion. 
Essayer de tirer partie des novueaux outils offerts. 

###Méthodologie de travail
Projets éditoriaux numériques : il s'agit surtout d'expérimentation, confrontés à des contextes industriels contraignants. 

###Corpus / objets et exemples présentés + références

Hopper
Revue BleuOrange
e-album d'expo
"Testament de vie"
Hexes
Mégawatt - Watt de Becket

###Défis techniques

Poids des fichiers (e-album d'expo plus léger que catalogue d'expo, plus facile et moins couteux)
Défis de traduction et adaptation dans Megawatt car langage complexe, associé à un code hexadécimal qui renvoie aussi à des couleurs 

###Conversations sur le pad collectif

Plusieurs projets éditoriaux numériques : certains sont des livres, certains appartiennent au champ de la litt numérique. 
Quels critères pour le livre numérique ?  Souvent, ces objets sont d'ordre expérimentaux, confrontés à des contextes industriels contraignants 
Cadre légal de l'existence de ces objets : en France, loi de 2011, qui donne une définition de ce que c'est un livre numérique. 
Tout ce qui est de l'ordre du design d'intéraction, de l'ergonomie, toutes les possibilités du numériques sont d'ordre accessoire. Se pose la question du beau livre numérique : quand passe-t-on d'une qualification à une autre ? 

Qualité des interactions pour changer de qualification.  
Éditeurs traditionnels en France : en les interrogeant, Sylvie ?? a noté qu'il y toujours ce statut d'objet livre qui demeure attaché à l'objet livre.  
quels sont les freins ?  statut d'objet livre dans ses dimensions culturelles et esthétiques qui font que ces éditeurs ont du mal à passer au beau livre numérique  

Quelques éditeurs de livre d'art qui ont essayé de développer leurs collections en  epub. Un éditeur de livre d'art qui fait auj du livre numérique : idée de stratégie de disinction par l'innovation que mettent en oeuvre ces éditeurs à l'heure actuelle. Montrer qu'on reste branché et qu'on est moderne. Par ex. Diane de Sellier qui vend des livres d'art à plus de 1 000 euros. 

E-album de la RMN : plus de 30 à l'heure actuelle : offre assez large, 1er éditeur de live numérique français.    

Pourquoi e-album d'expo et pas catalogue d'expo ? Déjà cest moins onereux, ensuite plus rapide à fabriquer et à charger, raison principale c'est la plateforme, qui applatit la forme, on ne peut pas représenter sur l'appstore la variété des prix et des contenus (vs comparer les livres dans une librairie)  

2 points de vue à partir d'un exmple :     
- histoire de l'art     
- histoire de la contitution du Musée de Budapest  

2 livres dans un livre : jouer avec la tablette elle-même, qu'on peut incliner dans un sens ou dans l'autre.  

Subvention du CNL de 50 000 euros pour l'exception Hooper. Collection de l'ENSAD avec un catalogue par articte de 50 mots : motion design (mouvement, sensualité..) Motion design : travailler le mouvement (entrée dans les mots) 
Très application, très peu epub : plus léger, plus rapide à télécharger  
Plutôt application que Epub. RMN très application et très peu epub Centre Pompidou a fait développer un CMS, stratégie différente qui leur a valu de ne rencontrer aucun public ; chiffres de ventes trops faibles, ils ont dû arrêter. Remarque : le cms est censé réduire le ? La RMN ne sont pas sur de pouvoir continuer à travailler avec Aquafadas  («Aquafadas est un éditeur de logiciels, spécialisé dans les solutions mobiles pour les entreprises et dans la publication numérique. Basée à Montpellier dans le sud de la France, Aquafadas est une filiale majeure au sein du groupe japonais Rakuten» tiré de wikipédia) 

Développent maintenant des liens pour l'élaboration d'epubs — pérennité?  Livre augmenté : jeu vidéo-livre. Application pour Iphone/Ipod et on va jouer. Le support imprimé est le chemin d'entré pour avoir accès aux contenus qu'il y a dans l'appli. Mais question de la pérénité.   Problème de sites internet qui ne sont plus maintenus pour des expériences en réalité augmentée (par ex. zombies en QR code)  Application de réalité augmentée à partir du support imprimé (app pèse 1G). Si on a le support imprimé, on a déjà beaucoup de contenus.  

Epub : format qui a tendance à remplacer le PDF pour l'édition numérique, en particulier textuelle (roman), format lisible sur différents supports (tablette, liseuse). L'epub n'est pas tant la création des contenus que la création des logiciels d'interprétation de lecture de ces contenus. Les lecteurs actuels n'ont pas assez de puissance pour des interpréteurs de contenus epub qui soient performants en termes de motion design.  

Tirer partie des possibilités de l'écran en intégrant des vidéos, des éléments interactifs. Fréquence(s) de Célia Beaudard ?? : oeuvre qui est du texte, images, son, parfois ensemble, mélanger des médiums qui normalement se mélangent assez mal. Permet de glisser vers des enjeux qui sont plus du côté de "comment est-ce qu'on écrit pour ce genre de supports-là ?"  Comment écrit-on pour nos différents supports ?  

Bleu Orange : Revue de litt hypermédiatique en 2008. Conçue pour promouvoir la litt hypermédiatique francophone et soutenir les artistes franco. Plateforme de diffusion pour oeuvres en français. Soutenir les artistes 
Question de diffusion des oeuvres en français a amené ;a revue à penser a question de traduction et s'approprier certaines oeuvres en anglais canoniques — communautés minoritaires. Associé avec le département de traduction de Concordia. Les étudiants sont appelés à traduire des oeuvres. Conçue comme une revue, qui rassemble plusieurs oeuvres sous un même couvert, oeuvres qui prennent des formes de plus en plus différentes, qu'on essaye de rassembler sur une même plateforme. 
Question de la traduction ramène la question de l'écriture, comment écrire une oeuvre hypermédiatique sur le plan autant littéraire que programmatif ? Plusieurs types d'oeuvres qui existent (récits avec hyperliens et hypertextes où l'oeuvre est construite par passage déjà condensé/circonscrits qui sont reliées par ça à d'autres passages)  

Par ex. Testament de vie. Ce sont des hyperliens, la traduction fonctionne de la même façon que si l'on traduisait un roman.  2e exemple : luckysoap.com de J.R. Carpenter. Le code source révèle l'écriture même de cette oeuvre. Texte existe comme formule à exécuter plutôt que produit fini. (Mais avec les théories de la lecture, le texte est-il vraiment fini? n'est-il pas tout de même une formule à exécuter?) Le texte n'existe pas de manière définie mais comme formule à traduire, code informatique à traduire. 

Sur la question de la traduction de transmission : a dialogue, voir l'article d'Ariane Savoie : \url{http://nt2.uqam.ca/fr/cahiers-virtuels/article/la-litterarite-du-code-informatique} Traduction ou adaptation ? car traduction = interprétation (surtout dans le cas présenté, il a fallu agir sur le texte). Le contenu ne peut pas être traduit tel quel    

3e Hexes (ou Sextes traduit par Ariane Savoie) : \url{https://nickm.com/poems/hexes\_etc.html} 
iContenu accès sur les fonctionnalités du langage numérique définition comme poème? SIgnification à travers des codes extrêmement contingents — code hexadécimal. Renvoie aussi à des couleurs. 
Effet de rituel du défilement des mots, prière (sexte). Éloigne du livre papier car impossible d'exister sur papier.   

Roman Mégawatt de Monfort : programme pattern qui allait exploiter Roman What de Beckett. Création d'un algorythme pour prolonger et étirer les sections du texte original. Souligner la lourdeur dans l'écriture, puis finalement imprimer sur papier pour renvoyer à l'objet livre.  

L'échange entre l'écriture numérique et papier est questionnée de plus en plus par Bleu Orange car on est confronté à l'obsolescence des technologies. La Revue BleuOrange toujours confrontée à ces différents langages. En train de revoir la mission pour revoir la question de traduction. Notamment la traduction du français vers l'anglais, à rebours de la pratique actuelle de celle de la traduction de textes de l'anglais vers le français.   


Question de Gascuel: Q :  qu'est-ce qu'un vrai livre numérique ?  Le vrai numérique, où est-ce que ça commence ? Sachant que le numérique est avant tout  « transmédia » et non multimédia. les exemples présentés montrent cette dynique : le lecteur presque auteur. Le livre hors-papier se cherche, et se cherchera longtemps... Commence à partir du langage transmédial : chaque lecteur est quasiment un auteur dans le livre numérique. Le livre hors papier se cherche et continuera de se chercher pendant longtemps.  
R : Définition légale du livre numérique afin d'appliquer la TVA réduite : livre ou jeu vidéo? Travailler aussi la dimension esthétique au-delà de la dimension juridique. Devenir authentique des techniques : une technique apparaît avant qu'on sache vraiment à quoi elle sert. Travailler collectivement pour mieux appréhender l'objet-création. 
Ariane : « Le livre, est plus un modèle qu'un réel objet » ; 
Nolwenn : « Pour les éditeurs, le livre numérique, c'est la clôture. Un espace protégé dans l'espace des lectures industrielles. » Définir littérature numérique : on espère qu'à un moment ça deviendra juste de la littérature. Pour les éditeurs, le livres numériques, c'est la clôture.  Quand les formes de langage s'évaporeront, tout redeviendra littérature Pour les éditeurs, le livre numérique c'est la clôture, rempart à des questions de lectures superficielles...  

Olivier Charbonneau : quelles dispositions juridiques peut-on anticiper autour d'un livre numérique ??  
Q : Qu'est-ce qu'un beau livre ? Comment animer une communauté artistique ? Surtout en pensant le droit d'auteur.  
R : secteur économique qui cherche à ne pas perdre d'argent (quand c'est gratuit, c'est mieux Android, quand c'est payant, mieux sur AppStore, trop cher de faire les deux. ). Quand on distribue en gratuit, on a beaucoup de lecteurs sous Android, dès que cela devient payant, on perd 90% des lecteurs, ce n'est pas le cas sous iOS. L'imprimeur, il faut le payer, le développement, on peut le faire financer comme des projets de recherche. Les artistes de BleuOrange, ce sont des gens qui font ça probono pour leur propre compte. Certains ont eu subventions mais jamais à des fins commerciales. Pour ce qui est de la traduction, la plupart des artistes sont ravis de se faire traduire. Bleu orange est une initiative para-universitraire, financé par différents groupes de recherche et par le bénévolat. Initiative parauniversitaire, un peu comme recherche-création. Problème du copier/coller qui permet de faire des emprunts sans citer les oeuvres. 




###Questions /remarques dans la salle 

- Question de Gascuel : qu'est-ce qu'un vrai livre numérique ?  Le vrai numérique, où est-ce que ça commence ? Sachant que le numérique est avant tout  « transmédia » et non multimédia. les exemples présentés montrent cette dynique : le lecteur presque auteur. Le livre hors-papier se cherche, et se cherchera longtemps... Commence à partir du langage transmédial : chaque lecteur est quasiment un auteur dans le livre numérique. Le livre hors papier se cherche et continuera de se chercher pendant longtemps.  

R : Définition légale du livre numérique afin d'appliquer la TVA réduite : livre ou jeu vidéo? Travailler aussi la dimension esthétique au-delà de la dimension juridique. Devenir authentique des techniques : une technique apparaît avant qu'on sache vraiment à quoi elle sert. Travailler collectivement pour mieux appréhender l'objet-création. 
**Ariane :** « Le livre, est plus un modèle qu'un réel objet » ; 
**Nolwenn** : « Pour les éditeurs, le livre numérique, c'est la clôture. Un espace protégé dans l'espace des lectures industrielles. » Définir littérature numérique : on espère qu'à un moment ça deviendra juste de la littérature. Pour les éditeurs, le livres numériques, c'est la clôture.  Quand les formes de langage s'évaporeront, tout redeviendra littérature Pour les éditeurs, le livre numérique c'est la clôture, rempart à des questions de lectures superficielles...  


- Question d'Olivier Charbonneau : quelles dispositions juridiques peut-on anticiper autour d'un livre numérique ??  Qu'est-ce qu'un beau livre ? Comment animer une communauté artistique ? Surtout en pensant le droit d'auteur.  

R : secteur économique qui cherche à ne pas perdre d'argent (quand c'est gratuit, c'est mieux Android, quand c'est payant, mieux sur AppStore, trop cher de faire les deux. ). Quand on distribue en gratuit, on a beaucoup de lecteurs sous Android, dès que cela devient payant, on perd 90% des lecteurs, ce n'est pas le cas sous iOS. L'imprimeur, il faut le payer, le développement, on peut le faire financer comme des projets de recherche. Les artistes de BleuOrange, ce sont des gens qui font ça probono pour leur propre compte. Certains ont eu subventions mais jamais à des fins commerciales. Pour ce qui est de la traduction, la plupart des artistes sont ravis de se faire traduire. Bleu orange est une initiative para-universitraire, financé par différents groupes de recherche et par le bénévolat. Initiative parauniversitaire, un peu comme recherche-création. Problème du copier/coller qui permet de faire des emprunts sans citer les oeuvres. 

