#{H} Emmanuël Souchier : Épistémologie du design en contexte numérique


##Hypothèse de travail de l'intervenant

Quel est le rôle du numérique dans l'histoire de l'écriture?


##Positionnement théorique : auteur.s, courant.s de pensée, théorie.s, 

Retenir une leçon de Queneau (Kant?). Lorsque Kant l'évoque en 1796, il est travaillé par des préoccupation analogues liées au **droit** et à la **propriété intellectuelle**. Profiter de l'injonction technologique posée par l'introduction du numérique dans le corps social et en retenir une dimension positive (en-dehors des logiques marchande et juridique).

Goody : le livre (codex) comme petit véhicule plastique.

Importance de la **matérialité sensible** du livre (communication).


##Idées principales défendues

Livre et écriture sont si indissociablement liés dans notre culture/époque; il nous faut les **dissocier,** penser l'écriture sous toutes ses formes; ne pas s'enfermer dans le paradigme du codex. **S'affranchir** de de l'**emprise intellectuelle** du livre.

Replacer l'écriture dans son **espace propre** (et donc sous *toutes ses formes*).

Revenir à **quatre schèmes fondamentaux** :

   * #mémorisation
   * #circulation
   * #narrativisation
   * #visualisation 

##Méthodologie de travail

Réfléchir à l'écriture comme **technique de l'intellect**, comme **vecteur anthropologique** (l'humain qui s'adresse à l'*Autre*).

Approche anthropologique : l'écriture répond au **besoin communicationnel** de l'être humain. L'écriture doit être repensée selon sa **fonction**.

Usages du livre : **matérialité**, **gestuelle** et **lieux communs**. Constat : détournement du culte du livre par intérêts capitalistes.


---


##Notes collectives

Recentré sur deux questions : qu'est.ce qu'un livre et comment est-il appelé à se transformer en contexte numérique?

Qu'est-ce qu'un livre? comment prendre cette question "au sérieux" ?

Retenir une leçon de Queneau  (Kant?) Lorsque Kant l'évoque en 1796, il est travaillé par des préoccupation analogues liées au droit et à la propriété intellectuelle. Profiter de l'injonction technologique posée par l'introduction du numérique dans le corps social et en retenir une dimension positive.

Sortir des logiques marchandes et juridiques : retenir une dimension positive. 
 
dire que le numérique nous permet de repenser de manière reflexive les usages et les discours est une tarte à la crème.
le numérique nous oblige de repenser l'ancien monde : soit.
mais profiter de l'injonction numérique pour replacer le livre dans l'espace qui est le sien : la culture, l'histoire
 
Replacer le livre dans l'Espace qui est le sien : celui de la culture et de l'histoire
 
nécessités anthropologiques
quelle nécessités humaines et communicationnelles, et préoccupations culturelles le livre a prétendu répondre au cours de ces 2 millénaires
 
histoire du codex... (approche très ethnocentriques, d'autres cultures existent)
 
en quoi le numérique  a-t-il nécessairement un rôle à jouer dans cette histoire ?
 
Logique de la question, sous l'emprise intellectuelle de l'histoire du livre
 
Emprise intellectuelle héritée de l'histoire du livre
Représentations des connaissances.
Songer aux modalités de représentation des connaissances : intrasèquement lié à l'histoire du codex.
le plus difficile est de sortir des cadres institutatns, sortir de la pensée du codex 
Cadres instituants, la pensée du codex
 
nous pensons à travers les formes du codex. 
admirable machinerie intellectuelle qu'est le codex
il faut en sortir pour aborder le numérique
comprendre sa spécificité, comprendre le livre pour reformuler son questionnonement. : Ce n'est pas le livre qui est transformé par le numérique, mais la relation intime de l'écriture à ses supports et à ses usages
 
Nous avons aujourd'hui des formes hybrides et passagères mais  : Le livre comme matrice culturelle ne trouvera dans le numérique que le prolongement de ce qu'il a déjà envisagé dans sa forme même : 
    
Ne pas rester prisonniers des formes instituantes , s'en extraire car ces cadres agissent comme... 
 
Théâtre de mémoire, à la fois support matériel et technologie de l'intellect, assume corporellement la pérennité et la matérialité? de l'écriture
technologie de l'intellect au sens de Goody
 
Le livre offre à l'écriture underlineune portabililtéunderline : 
En l'assignant à une forme et des formats, le livre répond aux nécessités anthropologiques de l'écriture.
En assignant le livre à une forme, des supports, un format, le livre répond aux questions de l'écriture
 
Travailler sur une ouverture de la notion d'écriture au sens anthropologique - appel à travaux
 
Porte la présence du geste, de la main. Trace des voix et des corps. Modèle la manière de voir le monde
émancipation de son corps, de l'espace et du temps : c'est la première techno de l'intellect de l'humanité
Sortir de l'échange direct - l'écriture  = LA première technologie de l'intellect de l'humanité. On n'arrive plus à en sortir tellement elle nous a enveloppé
 
L'écriture au sens où nous l'entendons permet à l'Homme de communiquer, de s'émanciper : sortie du contexte et de la situation de l'oralité ==> première technologie de l'intellect. C'est à l'homme, l'autre moi-même qui s'exprime à sa place, elle se déploie dans l'espace. À travers le mythologique, au delà du temps et de l'imagination - vers l'Autre (!). 
 
question :::::::::: 
l'écriture vient d'un besoin de compter. => pas forcément. Ça c'est la théorie de Goody surtout, il me semble. Mais chez AM Christin on trouve une autre façon de concevoir l'écriture (origine de l'écriture dans l'image).  Ca m'intéresse
je voulais faire le lien-tarte à la crème écriture/code ben mange ta tarte et toi, va faire un selfie -miam.
::::::::::
 
Goody : L'homme peut s'adresser à l'AUTRE
Le Codex offre un véhicule petit volumen polymorphe et plastique  
 
 
Attendus anthroplogiques de l'écriture et du codex : 
    effort pour sortir de l'impensé, il nous faut distinguer le livre de l'écriture, sa matérialité propre : "l'image du texte"
 
Livre et écriture sont si indissociablement liés dans notre culture/époque; nous devons les dissocier^^, de reconnaître l'écriture sous *toutes ses formes*; **penser l'écriture sans le livre**.
 
Nécessité communicationnelle vitale pour homo sapiens
nécessité anthropologie de la communication : l'écriture : circulation dans le corps social, toujours réductible au support/dispositif matériel
Numérique au service de la communication?
 
intrusion du numérique dans le registre du livre : chargé d'un fort capital symbolique ancré dans l'histoire et les mentalités
chargé d'un très fort capital symbolique, efficacité symbolique aussi
 
Ce que le numérique touche et vise n'est pa le livre mais l'écriture dont il se nourrit à travaers les dispositifs de textualisation
Ce que le numérique vise : pas le livre en soi, comme véhicule, mais bien l'écriture à travers la textualisation -- processus d’accessibilité.
textualisation pour se rendre accessible aux humain
 
Lectrure : activité de lecture et d'écriture : tech intellectuelle fonctionne à partir de ce deux activités
Dispositifs de technologie de l'intellect qui fonctionnent à partir de la lecture
 
L'irréductibilité du livre - ce qu'il a et que d'autres dispositifs n'ont pas.
Ce qui est irréductible du livre vis-à-vis de tout autre dispositif... ? 
 
 
Les questions portent sur la représentation des connaissances

   * Représnetation des connaissances
   * DiffusionUne double exigence : tension entre l'économie capitaliste du flux (d'information) et la question des formats qui nécessitent une réification.
 
autre hypothèse : le Graal de l''industrie num. : réside dans ce que le codex a mis au point à travers les siècles : des communs que les industriels cherchent à faire émerger grâce aux usages démultipliés par les utilisateurs. : c'est la pratique des usagers qui est visée
Ce ne sont plus le livre ni l'info qui sont visés par l'industrie,  mais la pratique des usagers
 
Image crue de notre société dans laquelle tout peut être marchandisé
 
la relation que l'homme a au support : fonction de communication vitale qui est une des conditions de survie de l'espece humaine. => ENjeu sociétal 
L'industrie numérique a la nécessité de  développer de nouvelles formes de médias et médiations :
    - internet
    - smartphone qui deviennent la zapette de nos sociétés
 
les médias produisent l'équivalent des communs : mais deviennent des savoirs collectifs marchandisés, ce ne sont plus des biens communs.
 
Le livre a pu forger des cadres d'efficacité au terme de x siècles 
 
===
intéressant pour publishing sphere: le livre comme espace collectif et commun est remplacé par l'industrie du numérique qui devient l'espace principal de communication en marchandisant l'espace commun 
==============
 
 ================================
 !!!! Première partie copiée à partir, environ, de la 3000e sauvegarde de l'historique dynamique.  !!!!!!!
 =========================================
Anonymat des artisans, auteurs, etc.
Silex chargé de gestes qui en font l'efficacité
 
 Il y a donc cette interrogation qu'il faut poser d'un point de vue économique, politique et sociétal. Par rapport au livre, on reprend cette histoire longue d'un médias qui au fil des siècle a forgé ses cadres à travers travail, lecture, et je la met en tension avec l'appropriation marchande de la pratique des consommateurs qui permettent des jeux de reconfiguration sémiotique des médias reformais: design, affordance, sont-ils des termes si contemporains? 
 
Média qui a forgé la lecture et les lecteurs sur des siècles
 
Fonction communicationnelle de l'écriture
L'information en soi n'offre aucune prise, aucune réalité en contexte  numérique si elle n'est pas socialisée
 
 
Il faut revenir à la fonction communicationnelle de l’écriture, car l’information en soi n’orffre en traitement numérique aucune réalité si elle n’est pas socialisée par l’écriture
 
 nous sommes passés des termes suivants : citoyens, usagers, consommateurs pour devenir des data face à la consomation du livre
 
Il faut revenir à quatre schemes:
- mémorisation 
- circulation 
- narrativisation 
- visualisation 
permettent la compréhension sensorielle du corps, nous ne sommes que ça
 
4 Points constitutifs de l'écriture et de ses enjeux fondamentaux d'un point de vue communicationnel et anthropologique

