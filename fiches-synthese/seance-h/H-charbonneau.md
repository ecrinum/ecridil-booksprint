#Fiche synthèse : Olivier Charbonneau, « Encoder les communs de la lecture »

##Hypothèse de travail de l'intervenant

L'avènement du livre numérique n'est pas la catastrophe que le milieu du livre -- papier -- pense avoir découvert, trop tard. Le numérique nous oblige cependant à repenser les paramètres qui entoure le droit d'auteur, tant sur les plans juridique qu'institutionnel. Il y a là tout un chantier. 

##Positionnement théorique : auteur.s, courant.s de pensée, théorie.s,

Doctorant en droit, Olivier Charbonneau est militant et chercheur à Concordia. S'efforçant de repenser le statut des communs dans le cadre d'une culture numérique, il propose sur son [blog](culturelibre.ca) de suivre ses réflexions et recherches.

O. Charbonneau plaide pour une approche pragmatique du numérique : quelque chose qu'il nous faut conceptualiser, réagir pour mieux en appréhender les enjeux et, finalement, se les réapproprié.

##Idées principales défendues

- Le droit d'auteur n'est pas un problème juridique, mais de gouvernance. Deux traditions priment lorsqu'il s'agit de concevoir le droit d'auteur : (a) le penser comme un \_bien privé\_ (française) et (b) le penser selon le spectre de l'utilitarisme (\_common law\_ anglo-saxon). Ces deux approches donnent à réfléchir le droit d'auteur de deux manières : comme un droit immuable ou comme le résultat d'un système de relations complexes entre divers intervenants. C'est donc aux institutions de poser leur propre réfléxion quant au droit d'auteur ; le réfléchir oblige une prise de position quant à ces deux modes de pensées pour éventuellement fonder une épistémé commune.

- Le numérique provoque un reparamétrage des relations entre matérialité et droit dans le cadre de l'économie marchande. Le livre papier, du fait de sa matérialité, est liéà ses droits selon des contraintes physiques, p. ex. division matérielle des documents dans une bibliothèque. Le numérique fait plutôt en sorte que droits et documents soient imbriqués par licences. Il faut ainsi redéfinir la chaîne éditoriale du livre, repenser la manière dont on structure les licences.

- Il faut penser de nouvelles plateformes permettant l'accessibilité de documents numériques dans les bibliothèques par exemple. Sans plateforme, il n'y a pas de diffusion et, conséquemment, le livre n'existe tout simplement pas. Parmi les enjeux que cela implique, il faut penser à ajouter une strate de métadonnées aux catalogues des bibliothèques.

- Réfléchir aux nouveaux arrangements, designs et plateformes. Plusieurs questions se posent : comment les agissement des communautés s'insèrent dans la documentation numérique et comment bâtir un marché sur des bases aussi mouvantes que sont celles du numérique. Il faut aussi considérer la gratuité et le libre-accès comme une plus-value des possibilités qu'offre le numérique. Il faut donc considérer cet aspect propre au numérique dans les nouvelles approches institutionnelles à bâtir.

C'est aux instances de gouvernance que revient la tâche de réfléchir la place du numérique dans l'espace commun, public et ne pas laisser des intérêts privés, préconisant la rentabilité (GAFAM, particulièrement) dicter les paramètres qui délimitent et définissent l'espace numérique.



### Méthodologie de travail

Structer les métadonnées existantes, la littératie législative et faire, en quelque sorte, un travail d'éducation à l'endroit des instances de gouvernances et les institutions devant adapter leurs modèles au numérique.


### Questions /remarques dans la salle

