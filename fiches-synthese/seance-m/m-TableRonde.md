#Table ronde {M} « Imaginer des structures d’accompagnement éditorial en contexte numérique » 
#
Participants : Bertrand Gervais, Benoit Bordeleau, Nathalie Lacelle, Prune Lieutier, Annabelle Moreau


##Vos prises de notes
#
Stratégies éditoriales, mise de l'avant de répertoire, aide à la prise de décision. Livre numérique ou livre augmenté.
Opuscules 2 : développement partenariat littérature québécoise mobile. Présenter ce qui se passe à la grandeur du territoire. Evenements, finalistes de prix littéraires, version audio par les écrivains et les écrivaines. Rapprochement de la pratique des auteurs ; rythme, enjambement des poètes. 
Pratiques naissent sur les plateformes, encore en rodage parfois. Spectacles, mises en lecture, mettent à l'essai des textes devant public. Réflexe d'enregistement se crée à l'extérieur du projet. 
Agrégateur regroupe 175 blogues littéraires. présenter ce qui se passe sur la scène littéraire actuelle. Site web et web app. Interface directe, sans passer par itunes. outils numériques qui servent la communauté littéraire. Aider plutôt que compliquer la vie d'une communauté d'emblée plutôt résistante. Drupal. 

Annabelle Moreau : Magazine lettres québécoises, (1976) , porté longtemps par une même équipe. Renouvellement de l'équipe en 2017. Mission de critique littéraire. Exploration et diffusion de la litt qc et franco can. 
Recentrer la stratégie papier, nouvelle maquette. nouveaux collaborateurs. se réinscrire dans le milieu littéraire et répondre aux attentes et aux besoin des gens du milieu. chercheurs, lecteurs, auteurs, éditeurs. 
2015 : états généraux : vouloir un magazine plus attirant . 
à l'avenir, des balados critiques. peu présente dans les médias. produire des débats enflammés et intéressants. 45 min, se laisser le temps de débattre. ça va se retrouver sur opuscules. 

Prune : accompagnement des producteurs, des éditeurs. recommandations aux pouvoirs publics. Mise en place de programmes d'éducation au numérique.

BG : avoir à la table à la fois revue papier litt, qui sent le besoin de l'appel d'air numérique, 
La fabrique du livre répond à des enjeux numériques. mettre en vis a vis des gens préoccupés par l'avenir du livre et ceux qui le renouvellent. Proposer des livres numériques, partager le territoire. 
LQM interface entre différents acteurs, dans un milieu en pleine transformation. 




##Fiche synthèse
#


###Hypothèse de travail des intervenants
#
présentation des enjeux liés à leur pratique + proposition de stratégies 
plusieurs organismes québécois contribuent au développement de littérature numérique

###Positionnement théorique : auteur.s, courant.s de pensée, théorie.s, 
#
*5 lignes max*

###Idées principales défendues

Continuer à faire de la critique littéraire
produire des débats enflammés dans le milieu littéraire
projet pour l'automne prochain
réunion de gens du milieu; invitation de gens de l'extérieur (journalistes, etc)

 
###Méthodologie de travail
#
*5 lignes max* 

###Corpus / objets et exemples présentés 

Opuscule 2 - littérature québécoise mobile : présenter la diversité des productions littéraires au Québec -----» accès, notion importante !
Production Rhizome

magasine «lettres québécoise» (critique littéraire) : envie de se projeter dans l'avenir, alors qu'il était vieux et dépassé, pour tout changer et tout recentré la stratégie papier (marketing) : nouveaux collaborateurs, nouvelles maquettes, afin de s'inscrire dans le milieu littéraire (chez les lecteurs et les chercheurs)
publiés 4 fois par annees

3. le livre blanc
vise à documenter les aspects prod, diff, réception
hackaton de 2 jours 
enjeu de l'accompagnement de l'éditeur


###Défis techniques
#
*5 lignes max*

###Conversations sur le pad collectif

1. Opuscule 2 - littérature québécoise mobile : présenter la diversité des productions littéraires au Québec -----» accès, notion importante !
Production Rhizome

Calendrier d'événements littéraires
Idée : présenter ce qui se passe en dehors du Québec

version audio des textes proposés, ce qui rapproche des pratiques auctoriales (souffles du poète, mieux sentir les enjambements) Mise en tension entre texte et voix de l'auteur

une audiothèque dans le cadre de lancement et d'entrevue(conf, tables rondes)

Présenter pas seulement des auteurs consacrés
présent avec les auteurs, auditeurs

agrégateur de blogs inscrits dans la sphère littéraire du Québec

téléchargement gratuit de l'interface pour le cell ou la tablette : développer des outils numériques qui servent la communauté littéraire (le monde du livre numérique peut se servir d'outils simples = motivation à utiliser les nouveaux dispositifs)

Technologie : dorsale Drupal (CMS), crée des JSONS qui sont repris par Angular JS

2. magasine «lettres québécoise» (critique littéraire) : envie de se projeter dans l'avenir, alors qu'il était vieux et dépassé, pour tout changer et tout recentré la stratégie papier (marketing) : nouveaux collaborateurs, nouvelles maquettes, afin de s'inscrire dans le milieu littéraire (chez les lecteurs et les chercheurs)
publiés 4 fois par annees

Ont procédé à des États généraux : été 2016 (une quinzaine de personnes à qui ils ont demandés quelles sont leurs attentes)

succès de la stratégie : développer d'autres projets ailleurs que dans le papier (càd vers le numérique), quelque chose de propre à eux et absent ailleurs

Continuer à faire de la critique littéraire
produire des débats enflammés dans le milieu littéraire
projet pour l'automne prochain
réunion de gens du milieu; invitation de gens de l'extérieur (journalistes, etc)

3. le livre blanc
vise à documenter les aspects prod, diff, réception
hackaton de 2 jours 
enjeu de l'accompagnement de l'éditeur


outils numériques : alimenter un intérêt pour des formes brèves qui cherchent encore leur place 
faire un portrait de la littérature québécoise actuelle;  essaient de tendre vers l'exhaustivité représentative des pratiques actuelles 

intérêt pour amener les gens à s'interroger sur la littérature (offrir des textes inédits, des extraits)

###Références citées : publication scientifique mais aussi pages web
#
*présentées en liste*

###Vos remarques personnelles
#
*5 lignes max*

###Questions /remarques dans la salle 

Q. Reader's digest, échantillons à lire. Le numérique autorise une forme de modulation, d'échantillons des chapitres. Acheter la suite. Est-ce que cette forme d'échantillon est envisageable pour le numérique? 
R: freemium. Mécanique d'achat. laisser es enfants être hookés : éthique. 
R: lectures lors des lancements. stratégies de vente. alimenter un intérêt pour les formes brèves qui vendent moins. outil intéressant pour ça. Auteurs arrêtent leurs lectures avant la fin.  offrir une diversité de pratiques. piquer la curiosité.
Conserver des traces. 
AM: donner le plus d'informations possibles pour favoriser la réflexion. textes inédits? 

B.Epron question : le numérique autorise une forme de modulation d'échantillons : est-ce utilisé dans la promotion,?
problématique éthique en jeunesse : accès à certains jeux
