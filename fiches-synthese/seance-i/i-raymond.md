#Dominique Raymond, « Potentiel2. Numérique et contrainte pour Savage (*Nathalie*), Dickner et Fortier (*Révolutions*) »

**Vos prises de notes**
Potentiel au carré dans les deux œuvres à l'étude.  Corpus: Savage et Fortier.
La potentialité : ouverture, champ de possible, d'une aprt le potentiel numérique comme outil, plateforme, support. D'autre pas, le potentiel de la contrainte, Oulipo: une écriture potentielle (ex mot livre chez Rochon hier). 
Considérer la contrainte comme potentielle et non entrave.
L'objectif : examiner comme s'articule le double potentiel (au carré) dans deux peuvres québécoise, d'en observer les effets sur la lecture. 
Précision: s,inscrit dans une recherche postdoc sur la litt à contrainte au québec, le numérique comme un des 4 points d'Ancrage (pataphysique, formalisme, etc.)
*Révolution* : ouvrage à 4 main réalisé par Dikcner et Fortier (2014), format epub, pdf papier. Quotidiennement, le site des édition, fait paraitre révolution. La trame de base : un calendrier révolutionnaire française (utilisé de 1793-1806) : on fête les plantes, les outils plutôt que les saints. On peut lier à l'aspect thématique : thème imposé. Contraint implicite (366 jours, 4 main, ce qui double le nombre de commentaire).  Ils programme une application pour recevoir un mot par jour : surprise et quotidienneté. les édition Alto on fait paraitre quotidiennement sur le site révolution pour admettre al routine à la découverte. Les édition font valoir que c'ets pour reproduire els expériences des auteurs (bouuh).
Contrainte et numérique : les allusions métatexte à propos des contrainte, entreprise, numérique. Résultat: le 2/3 des entré concerne les deux domaine, soit la contrainte, soi le numérique. 
Une réflexion classificatrice sur le travail en cours.  «Raifort»: aspic, bhromure. Les contraintes donnent envie d'écrire de la contrainte. Une démarche analytique (aplatissement des hierarchie). Le numérique comme territoire à recenser. 
*Savage*: Un onomagraphie l'histoire d'une nom, compilation de résultat google. La contrainte est indissociable du numérique. Récolter un matière numérique et l'adapter au support textuel. Division en 4 état, le livre se compose de sections (était, est, sera, serait). Des phrases suivies, «Je me souviens» de Perec.  Lecture rapide. 
L'intervention minimale. Raymond investigue et découvre que L,auteur n'a que coupé des début/fin de phrase. L'ordre de succession : moteur de recherche.  Google donne L'ordre de succesion.
Comme pour révolution, quelques énoncés qui lient au numérique (rien sur la contrainte). Quelque méta Natalie.
Le principe de Natalie a eu d'autre vie: (lien sur le pwp). Savage prévoit aussi John Smith (pendant anglo). Chaque relecture de Nathalie donne L'impression de capitaliser : avec Queneau en tête, on se promène entre les phrases (Cent mille milliards de poèmes de Queneau). Une autre Nathalie en tout découpé. Ici, Chaque Nathalie en quatrain forme une histoire (remet en une page les temps et propose une chronologie : *Presque cent mille milliard*). 
Devrait être remis sur plateforme numérique : refermer la boucle étrange. 
Conclusion : si pour Boots est potentiel ce qui est défini, pour Queneau est potentiel ce...
##
**Fiche synthèse Hypothèse de travail de l'intervenant**
*Raymond propose de penser la potentialité numérique comme outil, mais également d'envisager la contrainte comme à la maniètre de la pratique oulipienne — créatrice, non comme entrave.*
###
**Positionnement théorique : auteur.s, courant.s de pensée, théorie.s, **
*Sa recherche s'inscrit dans sa recherche postdoctorale sur la littérature à contraintes au Québec. *
###
**Idées principales défendues**
*Le numérique comme plateforme, outil et forme contraignante génère de la potentialité dans un contexte littéraire, tant dans la création que dans la réception et la remise en circulation des oeuvres.*
###
**Méthodologie de travail**
*Raymond propose une analyse littéraire des oeuvre (Nathalie et Révolution) et des supports et des formes numériques qui les ont influencées.*

###**Corpus**
*Nathalie* de Steve Savage
*Révolution* de Dominique Fortier et Nicolas Dickner.
*Cent mille milliards de poèmes* de Queneau
