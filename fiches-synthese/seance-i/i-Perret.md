#Fiche synthèse : Arthur Perret, *"La typographie du livre numérique, entre fluidité et frictions"*

Il continue la réflexion amorcée par Souchier sur l'écriture tout en plongeant dans la technique.

###Hypothèse de travail de l'intervenant
Il part du postulat que la typographie est tout ce qu'il reste du papier et que c'est un point de passage entre l'imprimé et l'écran. 
Il pose également une question : Quel sens peut-on produire avec un système de signes changeants ? 

###Positionnement théorique : auteur.s, courant.s de pensée, théorie.s, 
Il a parlé des travaux de Roger Laufer qui sont d'une grande actualité. 
L'analyse de la typographie est une activité récente, liée à la sortie progressive de l'imprimé.

###Idées principales défendues
Le passage d'une écriture fixe à une écriture en mouvement.
Arthur Perret évoque le terme de flux (écriture en mouvement) qui cristallise enthousiasme et appréhension. C'est un terme clivant qui suggère des mutations fortes.
En définitive, l'écriture gagne une valeur d'interaction très forte avec le numérique et ses paramétrages font partie du contrat de lecture ainsi que de l'expérience de l'utilisateur. 
Pour lui, on se trouve face à des perspectives intéressantes en terme d'épistémologie mais il faut mener un travail hybride. Il est alors nécessaire de monter en compétences à la fois en terme de savoir-faire que d'érudition se situant alors dans une continuité humaniste. 

###Méthodologie de travail
Arthur Perret se pose deux questions : 
1. Qu'est-ce qu'implique une fluidification du sémiotique ?
La plasticité minimale des caractères (pas malléables, plastiques) fait qu'ils restent figés comme des glaçons dans l'océan de la page. 
Mais les écrans sont devenus mobiles, variables, plastiques et l'Apparition des font variables Open Type ou d'une typographie paramétrique comme metafont permettent le dégel de la typographie.

2. Qu'est-ce qu'on apprend en examinant la manière dont la fluidité se manifeste par des signes ? 
Le flux est en réalité un effet d'optique. Lorsque l'on redimensionne la fenêtre, nous nous trouvons devant un succession d'images fixes du texte. 


###Corpus / objets et exemples présentés 
Article de Roger Laufer

