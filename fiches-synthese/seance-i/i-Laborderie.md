#Arnaud Laborderie, *« Du livre enrichi au livre augmenté : les enjeux d’une clôture numérique »* 

##

##Vos prises de notes

Éditeur à la BNF et enseignant-chercheur. 

Il y a quelques années il a travaillé sur un livre d'un genre nouveau : application Ipad 
Le lecteur lamba pouvait être dérouté par la profusion. Possibilités de paramétrer les fonctions de l'application pour choisir son mode de lecture : lecture paramétrable et personnalisée. 

Expérimentation en milieu scolaire, mise en oeuvre de recherches sur le web, l'entrée privilégiée dans le texte se fait par mode sonore. Certains élèves ont véritablement surfé sur l'appli comme sur un site web. 
Carte comme une table des matières analytiques. 

Entre dans le texte en utilisant des enrichissements. 

Cloture : gageure du livre, articuler la cloture du livre



##Fiche synthèse


###Hypothèse de travail de l'intervenant

La question de clotûre est-elle seulement valable pour le livre objet ? Comment s'insère-t-elle au sein du livre numérique ?
Comment différencier livre numérique de site web ? Peut-on distinguer plusieurs types/formes de clotures ?

###Positionnement théorique : auteur.s, courant.s de pensée, théorie.s 

Foucault : "Le livre est un noeud dans un réseau"

###Idées principales défendues

La cloture du livre ne correspond pas à la cloture du texte. Et celle du texte ne correspond pas à celle de l'oeuvre. C'est l'imprimerie qui clot le texte, avant cela, le texte n'est pas vraiment contenu dans un objet fermé. Cloture numérique : matérielle et symbolique. 
La cloture définit un espace que le lecteur peut arpenter
Dualité dans laquelle se négocient les frontières et les limites du livre numérique. 
La cloture est la condition entre signification et interprétation, voire d'un champ de connaissances
Clôture ne signifie pas fermeture : permet l'enrichissement et l'augmentation

Livre enrichi vs livre augmenté : réflexion qui passe par l'utilisation du design


###Méthodologie de travail


###Corpus / objets et exemples présentés 

\url{http://ecridil.ex-situ.info/corpus-du-colloque/candide-ou-loptimisme}

###Défis techniques


###Conversations sur le pad collectif

Enseignant chercheur et éditeur à la BNF
 
Visuel sur YouTube (voir « corpus »)
 
(changement au programme)
Edition enrichie de Candide, une coédition de BNF avec Orange et Fondation Voltaire. Une recherche sur les mutations du livre, de la lecture. 
 
Recherche théorique sur les implications de la lecture...

Candidde, l'édition enrichie propose 3 entrées possible au lecteur : le livre, le monde, le jardin. Application Ipad. 
Du point de vue de la conception, nous avons voulu modeler des usgaes
codex
carte
arbre
Modéliser des usages et proposer au lecteur : lire, explorer, éditorialiser. 
Ces différents formes se combinent pour proposer une nouvelle app

La carte se présente comme une métaphore du web. Multimodal et multimédias : herméneutique du jardin [?]. Possibilité de paramétrer les fonctions de l'appli pour choisir son mode de lecture. Paramétrable et personnalisé. Les expérimentation montrent un changement de posture, la mise en oeuvre des pratiques de recherche comme sur le web, montrer que l'entrée privilégiée se fait sur un mode sonore.

L'entrée privilégiée se fait par l'audio, le conte lu par Denis Podalydès

Média qui a forgé la lecture et les lecteurs sur des siècles.

Fonction communicationnelle de l'écriture
L'information en soi n'offre aucune prise, aucune réalité en contexte numérique si elle n'est pas socialisée
C'est ce que les enseignants ont constaté en observant leurs élèves


L'app. interroge frontière entre linéarité et cloture
C'est la cloture que je voudrais discuter aujourd'hui

E Souchier  : livre numérique est un oxymore

« Le livre est un objet clos » ... Pas toujours -- il n'est clos que dans le moment et l'espace de l'imprimé qui n'est pas l'unique forme du livre. 

Média qui a forgé la lecture et les lecteurssur des siècles

Fonction communicationnelle de l'écriture
L'information en soi n'offre aucune prise, aucune réalité en contexte  umérique si elle n'est pas socialiséee n'est pas toujours le cas : uniquement dans un certain temps et espace : l'imprim
S'affranchir du modèle de l'imprimé pour penser le livre numérique comme un objet nouveau. 
De quoi parle ''on lorsqu'on parle de cloture ?
Cloture : peut-on distinguer plusieurs types/formes de clotures ? 

La cloture du livre n'implique pas nécessairement la cloture du texte, et la cloture du texte pas forcément celle de l'oeuvre. 

Cloture ?
Du livre, du texte, de l'oeuvre...

La cloture du livre n'est pas cloture du texte et non plus cloture de l'oeuvre. Avec le volumen, pas de cloture, aucune percpetion des limites et des extrêmités... Un flux du texte à l'image de la parole qui le consigne. 

Ni cloture physique, ni cloture symbolique, mais un flux du texte. 


Avec le volumen, c'est un défilement (une performance du texte)
Le codex vient définir des clôtures, une spatialisation de la parole


Distinguer 2 types de codex : 

   * Manuscrit (matériel, intellectuel, spirituel) / - manuscrit [cloture = condition de l'Avènement du texte]
   * Le livre médiéval est un espace ouvert par la glose, etc.
   * Texte qui est toujours inachevé, qui échappe à la cloture matérielle du livre
   * Le texte toujours inachevé (anthologie, corpora)
   * L'oeuvre déborde l'espace du livre et vice-versa... L'imprimerie isole le texte -- organise l'unité discursive et fusionne livre et appareil textuel : une clôture auparavant étrangère au texte. Plusieurs textes cohabitent

C'est l'imprimerie qui clot le texte et le renferme sur lui-même (le débarassant des gloses, images, en fusionnant l'unité textuelle et le livre.). Elle normalise et renferme le texte sur lui-même en lui imposant une fermeture qu'il ne possédait pas. 

Avec des exempliares tous identiques
La cloture du texte impose -t-elle une cloture de l'oeuvre?

La notion de clôture narrative en littérature

   * La norme : la clôture comme fermeture. L'oeuvre répond à une intentionnalité, conclusive et cloturante. Le livre appelle un dénouement. Pour Aristote, la clôture donne unité au récit... Mortimer : « la clôture est un fait de la lecture »
En litt. la norme est la cloture comme fermeture d'abord parce que l'oeuvre répond à une intention de l'auteur qui est conclusive et cloturante. 
Intention / intentionnalité de l'auteur = concluante, dénouement. 
Pour Aristote = cloture donne unité au récit

Le récit appelle un dénouement, la fin d'un récit. La cloture est créatrice d'ordre, de sens. Le sujet peut-il exiter sans la fin ?

La cloture interroge la fin mais aussi les fins. Parler de cloture = parler de sens
Une histoire peut-être exister sans sa fin ? Même si certains textes ouvrent le texte en guise de clôture.

Cette norme de la cloture comme fermeture n'est pas exclusive. 
Mortimer : il y a toujours cloture car c'est un fait de la lecture. 
L'absence de cloture peut être délibérée
Le Nouveau Roman nous a familiarisé avec la non-cloture, on s'est familiarisé avec les non-fins. 
Principe de morcellement et de fragment : volonté d'échapper à un mode clôt. 

La clôture contemporaine (problématique) du point de vue du lecteur

   * 
Aujourd'hui, lecteur tiraillé entre attente et refus de la clôture - désir d'une certaine mise en ordre, mais refus d'une résolution du monde trop prescriptive. 
Le livre, la litt. sont tiraillés entre une volonté d'ouverture et ??

Cloture numérique : matérielle et symbolique. 
- Matériel
- Symbolique

L'objet livre est clos mais pas fermé : une dialectique entre clôture/fermeture matériel/symbolique
Double fonction d'ouverture et de fermeture 
C'est dans cette dialectique que se pose la question du livre de demain 

Quels sont les conditions et les enjeux de la cloture numérique ?

Application Hopper : bon exemple de cloture matériel de l'objet livre numérique et de l'absence d'une cloture symbolique car l'appli propose de multiple parcours sur un arbre arborescent. 
Les livres transmédia où la narration est inépuisable, infinie. Dans ces dialectique, ce négocient les forntière du livre dans l'espace numérique. 

Sans clôture, on est plus dans le livre -- c'est la condition du livre, d'un champ de connaissance et de signification. Il faut que ce soit un ESPACE que le lecteur arpente.

La cloture définit un espace que le lecteur peut arpenter
Dualité dans laquelle se négocient les frontières et les limites du livre numérique. 
La cloture est la condition entre signification et interprétation, voire d'un champ de connaissances. 


Clôture ne signifie pas fermeture : permet l'enrichissement et l'augmentation

remarque : est ce que la clôture n'est pas une question de structure et de format. Dans ce cas, l'éditorialisation et la circulation du "livre" reste permise (et est

**Distinction entre livre enrichi et livre augmenté au regard de cette cloture : **

   * Enrichissement : pratique issue du livre et de l'édition critique  texte, contenu et données. 
   * Avec le numérique, nouvelle dimension : hypertextes, médias, etc. Enrichissement du texte par le lecteur. L'enrichissement clot le texte sur sa propre histoire. 
   * Enrichissement est issu de la  lecture que l'augmentation ouvre vers d'autre pratique. L'augmentation est du coté des usages et des technologies. 
   * L'augmentation ouvre
   * 

   * Augmentation : se situe du côté des usages technologiques 
   * technologie permettant d'augmenter les capacités mentales et psychiques
   * Comprise comme une technologie permettant d'augmenter capacités mentales et pshychiques tels alphabets et algorythmes
   * Augmentation = extension virtuelle du lvire dans le web ou avec appli de réalité virtuelle et augmentée 
   * Doivent se situer en dehors d'une cloture dont la fonction reste de devoir délimiter l'oeuvre
   * Prolongement du livre dans un espace extérieur et avec lequle il s'articule
   * Enrichi à l'intérieur et augmenté à l'extérieur

Conclusion 

Foucault : "Le livre est un noeud dans un réseau"

Le livre est un réseau. Le livre augmenté précise le réseau et propose une interface du noeud, c'est un enjeu de design

interface du noeud, représente l'objet-livre numérique —> une question de Design

Quelques travaux : 

- Au bonheur des dames : reprend cette même idée d'un double livre





###Questions /remarques dans la salle 

*Période de questions pour les 3 communications.*

Q. : MVR Le codex a été un espace public, un espace de production commun.
Cet espace peut devenir un espace marchandisé
 Dans quel mesure la production de cet espace est conditionné par les entreprises dont on parle et à quel point il est possible de penser l'espace de l'expression comme une publishing sphere
R.: ES : en rapport aux communs
Impensé radical des processus d'édition , la mémoire de l'oubli
Mémoire de l'oubli : le fait de prendre un livre sans savoir que c'est un livre (comme commun).
Les livres ne peuvent pas être, conçus pensés en dehors de leur lecteur
Le problème de l'histoire litt., le problème du bibliothécaire c'est de ne pas avoir de lecteur. 
À partir du moment où on appréhende que "ça" est un livre, à aucun moment quand on le prend on va se demander combien de générations, de gestes ont fait que je puisse l'avoir ; ils sont là les communs

Le numérique vient percuter cela
Il faut revenir à ce dont on est héritier : 
Prétention contemporaine redoutable quand on aborde l'affordance que l'on a intégré dans notre pensée infra-ordinaire

Dans le monde universitaire, porosité des lecteurs, des étudiants et des auteurs: les universitaires parlent aux universitaires
Il faut amener ces perspectives là sur ce qu'est le contexte de la bibliothèque. Les éditeurs grand public, les bibliothèque représentent pour eux 15-20% [??] de leur marché

Les éditeurs ne veulent pas vendre leurs livres numériques destinés aux lecteurs non-universitaires aux bibliothèques pour ne pas éroder leur marche
Le problème du livre numérique : comment réfléchir à des contraintes universitaires [??]
Il y a des lecteurs mais ce sont les éditeurs qui ne veulent pas vendre plus de livres numériques. 

AL : Nos usages de lectures sont façonnés par Google, Amazon, par des logiques commerciales. La question des usgaes du ivres, ils sont façonnés par Google, Amazon...
Il faudrait essayer de modeler nos propres outils 
Mettre en forme l'information est un enjeu de design et une responsabilité collective

ES : Idée selon laquelle l'écriture est née en même temps que le cadastrage

L'écriture est née en même temps que le cadastrage à Sumer

Pour sortir de cette dichotomie cloture/ouverture posée entre le codex et internet, on pourrait pas sortir de cette dichotomie fermée en proposant quelque chose qui serait d'une autre nature, qui est ce que les  technologies de l'intellect permettent à l'homme d'être rassuré par rapport au monde
Cadastrer l'espace pour des raisons de droit
Orgnaiser l'espace collectif dans un rapport d'usage
organiser l'espace collectif dans un rapport d'usage, d'usage du territoire — c'est à dire limiter le chaos à quelque chose qui soit raisonnable, vivable pour l'humain

On va limiter le chaos à quelque chose qui soit raisonnable et vivable pour l'humain. Geste toujours en rassurance en tension dynamique, car il ne suffit pas.
Lutter contre le chaos, l'inadmissible

Q de Bertrand Gervais : notion d'écosystème au centre duquel était le livre, maintenant c'est l'écran
Goody raison graphique à Bachimont raison computationnelle
Idée de la cloture s'inscrit dans une raison graphique mais plus du tout du côté d'une raison computationnelle. 
R :    Le droit d'auteur interdit ce que la technologie permet
C'est absurde, on se bat pour des fractions de centimes. 
On perd cette idée d'émergence, de plus pertinent, de plus gros, qui serait le commun. 
Qu'est-ce qu'un livre dans l'espace numérique ? François Bon : la nouvelle forme du livre, c'est le site

Qu'est-ce qui distingue un livre d'un site web ? Pour Laborderie, c'est la cloture. Partage point de vue de François Bon : "la nouvelle forme du livre c’est le site". La notion de cloture est terriblement présente dans l'espace numérique. 

Perspective idélaisée d'un environnement ouvert ; ce n'est as le cas, il faut franchir des péages exorbitants


Q : Renée Bourassa ? : propose de parler de la membrane cellulaire, métaphore organique, lieu de circulation entre monde de l'intérieur et de l'extérieur. Nous amène dans une forme de définition du livre. 

Membrane cellulaire plutôt que cloture. Lieu de circulation ente mondes intérieur et extérieur
On a besoin de points dans le web où il y a une sorte de condensation du savoir, de l'imaginaire mais aussi un mouvement centrifuge qui va vers l'extérieur. 
Besoin aussi d'un mouvement centrifuge

R :
   Lire est quelque chose d'éminemment vivant
    
AL : Métaphore de la cellule et du jardin, espace clos mais en expansion
Espace en expansian dans la cellule, dans le jardin, métaphore assez fructueuse pour penser les nouvelles formes du livre dans l'espace numérique. 

Charbonneau: Critique ces métaphores.  enclosures au R-U, émergence de la propriété individuelle [?]
Mouvement des enclosures au Royaume Uni
Dans la cellule, il y a aussi l'infection
La cellule n'est pertinente que s'il y a une masse critique (plusieurs cellules pour faire un corps)
Masse critique : plusieurs cellules pour faire corps
Idée de la rhétorique des clotures qui va profondément vers le droit propriétaires. 

ES : 
Appréhender l'objet en tant qu'objet. Le livre a un pli
Je ne pense pas que l'écritue augmente, elle permet
Le fait de constater d'un point de vue anthopologique que l'Homme invente le cadastrage et l'écriture, c'ets une vraie question. Elle m'interroge, qu'est ce que veux dire ce geste simultané?
