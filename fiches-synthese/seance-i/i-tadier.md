#Fiche synthèse


###Hypothèse de travail de l'intervenant

- Il est nécessaire d'insister sur la place du corps pour réévaluer le design du livre en contexte numérique. 

###Positionnement théorique : auteur.s, courant.s de pensée, théorie.s, 

- Citation de Merleau Ponty, *L'oeil et l'esprit*
- Boudhi (?), Lauroix Gold, un autre auteur (3 citations à retrouver) 

###Idées principales défendues

- La citation de Merleau Ponty permet d'installer le propos, de par ses nombreuses évocations du corps et de sa réalité physique. 
- Tadier pose la question : pourquoi tient-on tant à l'expression livre numérique ?
- Pour Tadier, il existe une intimité entre fond et forme. Cette proximité est liée au rôle des corps, au rôle du designer mais également au rôle du lecteur.
- En ce sens, le corps participe aux fondements mêmes du livre numérique.
- Il est donc nécessaire d'insister sur la place du corps en contexte numérique (c'est la thèse de Tadier, d'ailleurs dirigée par Emmanüel Souchier)
- Cette place du corps permet de replacer dans le débat les enjeux très anciens d'affordance et d'économie
- La réflexion doit également s'inscrire dans une pensée graphique, dirigée en fonction des corps de métiers. La question à garder en tête est la suivante : "Qu'est-ce que le livre meut en moi et comment est-ce que je meus le livre ?"
- Selon Tadier, il est nécessaire de rappeler que le livre a été construit de façon itérative au travers des siècles, et notamment au travers du corps. Le milieu et le corps s'ajustent, de façon itérative. 
- Thèse de Tadier : une histoire du corps qui participe de la culture du livre. Ce corps confronté au livre est pluriel, normé, mais aussi émotif, en mouvements, sensoriel.
- L'idée que la question du corps est primordiale pour celle du design n'est pas neuve mais se pose autrement avec les dispositifs numériques. Tadier donne l'exemple d'Hegerton (1821), un livre de dévotion anglais du XVe siècle qui reprend esthétiquement le mythe de l'eucharistie ==> tout le livre saigne.
- Plus largement, en tant que lectrice, Tadier rappelle qu'elle appréhende physiquement la matière lorsqu'elle tourne les pages (pratique de la pause pour jauger le volume notamment, afin d'avoir une perception de l'objet). 
- Tadier rappelle aussi la disposition anthropologique du corps communiquant, qui nous renvoie à notre condition d'homme. Les conditions de la relation sont transformées, notamment au travers de ce qu'Emmanuel Souchier nomme la "rupture sémiotique".
- Tadier insiste enfin sur le fait que le design se dissocie de l'économie de marché ; la corporéité est un supplément d'âme, le design doit dépasser ses conditions actuelles d'exercice pour interroger d'un point de vue phénoménologique la matière. Ce dépassement est nécessaire afin de réorganiser notre rapport aux contenus, de saisir les vraies implications matérielles des objets que l'on utilise afin que le rapport aux dispositifs numériques se place d'un point de vue anthropologique et non plus seulement du point de vue du "consommateur"

###Méthodologie de travail



###Corpus / objets et exemples présentés 



###Défis techniques
*Non pertinent *

###Conversations sur le pad collectif


###Références citées : publication scientifique mais aussi pages web

Emmanuël Souchier 

###Vos remarques personnelles


###Questions /remarques dans la salle 

Question sur le rapport à la valeur des différentes propositions : on doit toujours choisir la typo dans les liseuses.

Réponse : il y a des points de passage entre l'écran et la typo, notamment au niveau de l'écran. Mais on se rattache à un volume qu'on peut ouvrir avec les liseuses. Elsa Tadier rajoute que la perspective sémiotique peut être intéressante parce qu'elle va interroger les connotations de la matérialité. La thèse de Blanchard sur la sémiotique de la typographie en traite ; Selon les différents dispositifs matériels qu'on utilise, on aura ainsi une diffusion différente du contenu (cela implique de faire un lien entre la valeur qu'on perçoit et la valeur économique). 

