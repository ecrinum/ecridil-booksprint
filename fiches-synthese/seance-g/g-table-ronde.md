#Protocole éditorial pour les fiches synthèses

##Principes 

   * Une prise de note est effectuée de manière collaborative pendant le colloque. 
   * La fiche synthèse est produite pendant et après la communication et peut s'appuyer sur la prise de note collaborative.
   * Les synthèses doivent être relativement bien rédigées (on n'est plus dans la simple prise de note)
   * L'objectif des synthèses sera de produire une grille de lecture de l'ensemble du colloque. Cette grille de lecture servira la seconde phase d'édition.
   * Tous les champs ne doivent pas nécessairement être remplis - parfois ils ne seront pas pertinents - c'est plutôt à titre indicatif
##Vos prises de notes


##Fiche synthèse

###Idées principales défendues
*5 lignes max par idée défendue*

- En tant qu'éditeur, on réécrit les textes qu'on reçoit pour les publier
- On doit beaucoup repenser la manière dont on écrit
- La question des revues doit particulièrement être repensée, particulièrement pour la publication scientifique. Il faut notamment réexaminer l'architecture (des idées sont avancées, comme la possibilité de rendre publique l'évaluation, et notamment les noms de l'évaluateur) 
- Il est nécessaire de développer l'Open Access, les revues doivent donc utiliser des licences comme Creative Commons, Scholarly Commons, dans une logique d'adoption des parties du "fair" (faire ?). On veut aussi que les outils avec lesquels on crée soient libres, pour ne pas se retrouver dans une situation où le format propriétaire ferait disparaitre les documents créés au préalable sous formats propriétaires. 
- Le modèle économique actuel n'a aucun sens. Il est essentiel que les institutions académiques financent la recherche, et l'on pourrait envisager que ce soient également elles qui éditent.
- En conclusion, il existe un déphasage très fort entre les pratiques de chercheurs et le système de recherche, notamment éditorial.

###Méthodologie de travail
*5 lignes max* 
- La méthodologie de production de contenu scientifique change fortement depuis l'avènement du numérique (dépôt des articles un an après publication en libre) ; Érudit est cité comme exemple de travail fondamental pour promouvoir la recherche scientifique francophone.
- La méthodologie est particulièrement modifiée (semble-t-il) au niveau de la recherche en design, avec ces trois idées nouvelles qui s'inscrivent dans la réalité contemporaine : Demo or die, Get visible or Vanish, Deploy or Die. Ces trois idées participent d'un renouvellement des obligations scientifiques en matière de design (ce n'est pas nécessairement le cas dans les autres champs des SHS. 
- En Sciences Exactes, il n'est pas rare d'utiliser des morceaux d'article (on se sert de l'ensemble de la production scientifique comme d'une ressource de données), avec des choix parfois très granulaires. À l'inverse, les SHS progressent de façon discursive, dialogique. Les méthodologies sont distinctes, notamment parce que les enjeux ne sont pas non plus les mêmes en fonction des différentes disciplines (même si l'on peut s'en inspirer).

###Corpus / objets et exemples présentés 
*5 lignes max*

###Défis techniques
*5 lignes max*

###Conversations sur le pad collectif


###Références citées : publication scientifique mais aussi pages web
*présentées en liste*

###Vos remarques personnelles
*5 lignes max*

###Questions /remarques dans la salle 

- question 1 : qui, quoi, réponse

- question 2 : 

