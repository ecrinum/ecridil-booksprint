# Booksprint Alt

## Acte d'écriture / geste

### @Jahjah_PAD

matérialité éditoriale est retravaillé par les lecteurs, leur
interprétation, ils deviennent des actants.

Gestes (configurations) : continu (tourner la page qui crée de la
désorientation), discontinu (hyperlien qui crée le fil d'ariane) :
fragmentation

Chaque métier de la fabrication du livre a laissé une trace de son
passage sur le livre.

renégociation constante entre les acteurs travaillant la tension entre
une linéarité-labyrinthe et une discontinuité-fil d'Ariane.

### @Develotte_Pad

Question d'une épistémologie pluraliste qui intègre plusieurs acteurs
dans une réflexion sur la compréhension du monde

### @Rio_PAD

La production editoriale peut être une forme d'accompagnement de
l'écriture

Un projet R et D, mené avec une entreprise : Adrénalivre.

Mission : créer des livres dits interactifs numériques. Travailler sur
le storytelling interactif.

Créer de la fiction inspirée des livres dont vous êtes le héros.

### @Fauchié_communication ou @Fauchié_citations

«Versionner du code c'est versionner du texte. »

### **@Meynard/Greslou_PDF** :

Travailler sur les lettres de Stendhal et en faire une édition
multiformes adaptable à tous les supports et différentes selon les
supports, dans l'optique d'une circulation et complémentarité. Le tout
est fait dans un respect des volontés de mise en page de Stendhal et une
adaptation de ses manuscrits "pseudo-diplomatique". Le lecteur est
toujours encadré par une notice, ou un préambule qui lui présente la
contextualisation et les choix éditoriaux de l'œuvre à laquelle il a
affaire.

Le lecteur a un rôle d'acteur. Que devient l'auteur, que deviennent les
textes de l'auteur ? Et que devient le texte ?

Volonté de vouloir toucher tous les publics, possibilité pour chaque
public de trouver la plateforme de lecture qui lui est la plus adaptée.
Ainsi, il y a une perspective de mouvement, l'édition n'est pas figée
dans un seul support.

Sur le site, utilisation du mot "publication" défendu par l'idée de
"rendre public" les manuscrits mais aussi "rendre public" le travail
des chercheurs.

Travailler dans l'optique d'une œuvre multisupports à l'édition
multiformes. Selon les publics, rajouts de compléments pédagogiques,
certains fiches possèdent des complément multimédias.

Origines du projet : chantier de recherche, idée de l'exploitation,
travail collectif. Un des premiers projets en humanités numériques car
2006. Ce n'est pas fini, recherche et expérimentation. Spécialistes
dans différents domaines. Les chercheurs sont des "Bricoleurs":
ambition démesurée.

Potentialité du numérique qui nous permettrait d'aller plus loin mais
on ne l'utilise pas car "Qui nous lit ?"

### @meynard-greslou_slide

n°12 et peut-être n°4 et n°9

@Epron_fiche #Edition_critique :

"Souhaitent intégrer dans l'abécédaire une édition critique du
"Traité de documentation" de Paul Otlet ce qui permettrait de
mobiliser un réseau d'experts pour éclairer l'œuvre."

### @Table Ronde_M_PDF

2. magasine «lettres québécoise» (critique littéraire) : envie de se
projeter dans l'avenir, alors qu'il était vieux et dépassé, pour tout
changer et tout recentré la stratégie papier (marketing) : nouveaux
collaborateurs, nouvelles maquettes, afin de s'inscrire dans le milieu
littéraire (chez les lecteurs et les chercheurs). succès de la stratégie
: développer d'autres projets ailleurs que dans le papier (càd vers le
numérique), quelque chose de propre à eux et absent ailleurs.

### @Souchier-fiche :

Réfléchir à l'écriture comme technique de l'intellect, comme vecteur
anthropologique (l'humain qui

s'adresse à l'Autre).

Approche anthropologique : l'écriture répond au besoin communicationnel
de l'être humain. L'écriture

doit être repensée selon sa fonction.

### @Souchier-fiche :

L'écriture au sens où nous l'entendons permet à l'Homme de
communiquer, de s'émanciper : sortie du contexte et de la situation de
l'oralité ==> première technologie de l'intellect. C'est à l'homme,
l'autre moi-même qui s'exprime à sa place, elle se déploie dans
l'espace. À travers le mythologique, au delà du temps et de
l'imagination - vers l'Autre (!).

question ::::::::::

l'écriture vient d'un besoin de compter. => pas forcément. Ça c'est
la théorie de Goody surtout, il me

semble. Mais chez AM Christin on trouve une autre façon de concevoir
l'écriture (origine de l'écriture

dans l'image).

### @Rio_Pad:

régime d'écriture, ou modèle littéraire en soi : gouvernementalité
numérique

incitation du cadre (interface) et de la contrainte (technique :
seulement accessible en ligne)

statégie de contournement ou d'évitement face à cette objet/interface

moyens qui permettent de structurer l'écriture : matérialité textuelle
et iconique

Mise en place d'un cadre + d'une forme de contrainte

exemples de matérialités : schématisation avec des cartes; arborescence
pour représenter la continuité ou la discontinuité du récit
(visualisation graphique)

## Architexte

### @Perret_diapo10 #architexte

### @Masure-fiche :

L'ePub est un mini-site web encapsulé. Fonctionne avec les technologies
web standard (HTML, CSS, JS).

L'ePub est né de technologies web, mais a pris du retard sur le web
(ironie!). Format «pauvre», limité.

Le web comme architecture de lecture; le livre comme architecture de
lecture.

###  @Haute-fiche :

Projet de montage de revue (Chaire Art et sciences Ensad + polytech) qui
réponde aux enjeux suivants :
- multimodale
- modulable
- openscience / opentech
- multisupport (smartwatch + POD)
- paramétrique (déclinaison des contenus en fonction des environnements
de lecture)

* Chaînes de publication
actuellement les chaînes tentent de mettre en place une communication
fluide en mettant en place un "single source publishing", soit un
workflow basé sur le balisage du contenu avec ensuite visualisation
distincte. Pb : ce modèle est surtout faitpour le textuel, pas de
multimédia. Adoption de gabarits qui finit par être prescriptif.
Le papier n'est pas encore très bien intégré dans cette chaîne.
Standards ouvert : html / CSS / javascript / PHP
CSS print : peut gérer l'aspect d'une page web lors de l'impression.
Pb : outils payants.
Pagedmedia tente de développer de outils opensource.

### @Blanc-réponse :

Le logiciel a une influence sur la forme si on utilise que des logiciels
qui sont propriétaires contrairement à l'utilisation du web.

###  @perret_twitter

RT @antoinentl: le balisage est du texte qui décrit du texte:
*architexte* (toujours

### @arthurperret_tweet #ecridil2018

https://twitter.com/antoinentl/status/991340727522250752

###  @Rio_Pad

###

principe de négociation : il ne s'agit pas juste de transformer
(maléabilité d'une interface), mais comprendre le fonctionne d'une
écriture interactive (non connu de tous, dont une habitude au logiciel)
interface qui commande/oriente le texte

L'éditeur voulait rendre cela plus joli et plus efficace.
Pour eux, ils ne s'agissait pas seulement d'une maniabilité d'une
interface mais qu'il fallait comprendre le potentiel changement du
paradigme d'écriture (+ acculturation au logiciel)

Le logiciel considéré comme un archi texte qui allait commander la
structure du texte

Il y avait derrière cela une reconfiguration d'une pratique sociale :
l'écriture

@Souchier-fiche :

nous pensons à travers les formes du codex. admirable machinerie
intellectuelle qu'est le codex. il faut en sortir pour aborder le
numérique

## Clôture

### @Table Ronde C PAD

**Nolwenn T.** : « Pour les éditeurs, le livre numérique, c'est la
clôture. Un espace protégé dans l'espace des lectures industrielles. »
Définir littérature numérique : on espère qu'à un moment ça deviendra
juste de la littérature. Pour les éditeurs, le livres numériques, c'est
la clôture. Quand les formes de langage s'évaporeront, tout redeviendra
littérature Pour les éditeurs, le livre numérique c'est la clôture,
rempart à des questions de lectures superficielles...

### @Laborderie_PP

slide n°4

### @Laborderie- PAD :

La question de **#clotûre** est-elle seulement valable pour le livre
objet ? Comment s'insère-t-elle au sein du livre numérique ?

Comment différencier livre numérique de site web ? Peut-on distinguer
plusieurs types/formes de clotures ?

Cloture : gageure du livre, articuler la cloture du livre

**Foucault** : "Le livre est un nœud dans un réseau"

La cloture du livre ne correspond pas à la cloture du texte. Et celle du
texte ne correspond pas à celle de l'œuvre. C'est l'**#imprimerie**
qui clot le texte, avant cela, le texte n'est pas vraiment contenu dans
un objet fermé. Cloture numérique : matérielle et symbolique.

La cloture définit un **#espace** que le lecteur peut arpenter

Dualité dans laquelle se négocient les frontières et les limites du
livre numérique. **#espace #cloture**

La cloture est la condition entre signification et interprétation, voire
d'un champ de connaissances

Clôture ne signifie pas fermeture : permet l'enrichissement et
l'augmentation

L'application interroge frontière entre linéarité et cloture **#espace
#clôture**

« Le livre est un objet clos » **RETROUVER LA CITATION** Pas toujours
-- il n'est clos que dans le moment et l'espace de l'imprimé qui
n'est pas l'unique forme du livre. **#espace #cloture**

La cloture du livre n'implique pas nécessairement la cloture du texte,
et la cloture du texte pas forcément celle de l'œuvre.

La cloture du livre n'est pas cloture du texte et non plus cloture de
l'œuvre. Avec le volumen, pas de cloture, aucune percpetion des
limites et des extrêmités... Un flux du texte à l'image de la parole
qui le consigne.

Ni cloture physique, ni cloture symbolique, mais un flux du texte.

Avec le volumen, c'est un défilement (une performance du texte)

Le codex vient définir des clôtures, une spatialisation de la parole

Distinguer 2 types de codex :

-   Manuscrit (matériel, intellectuel, spirituel) / - manuscrit
    > [cloture = condition de l'Avènement du texte]

<!-- -->

-   Le livre médiéval est un espace ouvert par la glose, etc.

<!-- -->

-   Texte qui est toujours inachevé, qui échappe à la cloture matérielle
    > du livre

<!-- -->

-   L'œuvre déborde l'espace du livre et vice-versa... L'imprimerie
    > isole le texte -- organise l'unité discursive et fusionne livre
    > et appareil textuel : une clôture auparavant étrangère au texte.

La notion de clôture narrative en littérature. La norme : la clôture
comme fermeture. L'œuvre répond à une intentionnalité, conclusive et
cloturante. Le livre appelle un dénouement. Pour Aristote, la clôture
donne unité au récit... Mortimer : « la clôture est un fait de la
lecture »

Le récit appelle un dénouement, la fin d'un récit. **La cloture est
créatrice d'ordre, de sens**. Le sujet peut-il exiter sans la fin ?

La cloture interroge la fin mais aussi les fins. Parler de cloture =
parler de sens

Une histoire peut-être exister sans sa fin ? Même si certains textes
ouvrent le texte en guise de clôture.

Principe de morcellement et de fragment : volonté d'échapper à un mode
clôt.

Aujourd'hui, lecteur tiraillé entre attente et refus de la clôture -
désir d'une certaine mise en ordre, mais refus d'une résolution du
monde trop prescriptive.

Cloture numérique : matérielle et symbolique.

L'objet livre est clos mais pas fermé : une dialectique entre
clôture/fermeture matériel/symbolique

Double fonction d'ouverture et de fermeture

C'est dans cette dialectique que se pose la question du livre de demain

Clôture ne signifie pas fermeture : permet l'enrichissement et
l'augmentation

Sans clôture, on est plus dans le livre -- c'est la condition du
livre, d'un champ de connaissance et de signification. Il faut que ce
soit un ESPACE que le lecteur arpente.

**Distinction entre #livre enrichi et #livre augmenté au regard de
cette cloture : **

Enrichissement : pratique issue du livre et de l'édition critique
texte, contenu et données.

Avec le numérique, nouvelle dimension : hypertextes, médias, etc.
Enrichissement du texte par le lecteur. L'enrichissement clot le texte
sur sa propre histoire.

Enrichissement est issu de la lecture que l'augmentation ouvre vers
d'autre pratique. L'augmentation est du coté des usages et des
technologies.

L'augmentation ouvre

-

Augmentation : se situe du côté des usages technologiques technologie
permettant d'augmenter les capacités mentales et psychiques

Comprise comme une technologie permettant d'augmenter capacités
mentales et pshychiques tels alphabets et algorythmes

Augmentation = extension virtuelle du lvire dans le web ou avec appli de
réalité virtuelle et augmentée

Doivent se situer en dehors d'une cloture dont la fonction reste de
devoir délimiter l'œuvre

Prolongement du livre dans un espace extérieur et avec lequle il
s'articule

Enrichi à l'intérieur et augmenté à l'extérieur

-

**Le livre est un #réseau**

Qu'est-ce qui distingue un livre d'un site web ? Pour Laborderie,
c'est la cloture. Partage point de vue de **#François Bon** : "la
nouvelle forme du livre c'est le site". La notion de cloture est
terriblement présente dans l'espace numérique.

Métaphore de la cellule et du jardin, **#espace clos** mais en
expansion

### @Raymond_visuel p. 6

Recadrage de la slide [photo de *Presque cent mille milliards de
Nathalie*] #curiosité #clôture

## Curiosité

### @Tréhondart-fiche

(mot-clé #vision_augmentée) : "Il permet une vision augmentée de
l'art grâce à des images en haute résolution ainsi que la possibilité
de zoomer sur les tableaux."

###  @Gascuel_fiche

Travaille aux éditions _Googtime_. Le livre se trouve dans une période
charnière, au carrefour de deux moments : celui du papier et celui
d'une transmédialité : un nouvel objet, connecté -- sans application
qui font l'intermédiaire entre les objets connectés.

## Écriture en mouvement

### @Drochon_citationdiapo8 #écriture_en_mouvement :

"Envisager la transcription orale d'un point de vue typographique
nécessite

obligatoirement de la réaliser en mouvement. Le collectif *The Moving*

poster explore les possibilités d'un point de vue rafraichissant la
mobilité

de l'écrit, passant du GIF aux formes imprimées. Barbara Brownie, dans
on

ouvrage *Transforming Type* y étudie comment les lettres se transforment
à

l'écran."

### @Perret_diapo4 #écriture_en_mouvement + @Perret_fiche :

"définitive, l'écriture gagne une valeur d'interaction très forte
avec le numérique et ses paramétrages font partie du contrat de lecture
ainsi que de l'expérience de l'utilisateur."

### @Meynard/Greslou_PDF :

Perspective de mouvement, pas une édition figée avec un apparat critique
développé, aux normes de l'édition scientifique. Souci de
complémentarité entre les versions selon les supports/plateformes.

### @Antoinentl

Tweet : je suis fan de cette définition du livre numérique de
[[@culturelibre]{.underline}](https://twitter.com/culturelibre) : un
livre numérique est un livre qui peut être acquis par une bibliothèque
(et c'est tout). si la plateforme qui permettrait cet accès n'existe
pas : créez-la!
[[#ecridil2018]{.underline}](https://twitter.com/hashtag/ecridil2018?src=hash)
intervenant ? Charbonneau ?

### @Agostino_fiche : ouverture aux changement au fil du temps

L'Anthologie n'est pas un texte, ni n'est un objet clos. Ses racines
sont obscures : on peut parler du Codex Palatinus 23 (Anthologia
Palatina)_ ou encore de l'_Anthologie Grecque (Anthologia Graeca)_.
Plus largement, on parle de près de 2600 ans d'épigrammes, inscriptions
et poèmes.

Corpus de l'Anthologie palatine dans fiche synthèse : la structure de
l'API, la méthodologie de travail

On ouvre donc le projet à tous -- des lycéens à la philologie
universitaire pour réintégrer le corpus à la circulation dans
l'imaginaire, aussi contemporain.

On crée une API spécifique à l'Anthologie permettant de remplir les
visées du projet -- les plus ouvertes possibles.

Travail avec Json -- ouverture aux usages multiples, tant savants
qu'amateurs, voire ludiques.

Ouvert au grand public

On réinvestit les données sur toute sortes de plateformes, dont un
_bot_ sur Twitter pour faire circuler le texte, son sens.

###  @Masure-fiche :

Modalités du design en jeu ici :

Design éditorial : hiérarchie + structuration des contenus.
Design graphique : le langage visuel comme organisation sémantique.
Design d'interfaces : expertise de lecture à l'écran; sensibilité.

Quelles sont les nouvelles possibilités permises par ces nouvelles
techniques? (Parallèle : nouvelles techniques, plus spécifiquement
nouvelles techniques d'écriture.)

Les types d'écrits web portent généralement sur eux-mêmes (i.e. web,
design). Le web et le design écrivent justement sur eux-mêmes avec ces
technologies. La plupart des publications web sont

Les chercheurs sont souvent en manque de littératie numérique. Pourtant,
le web est né dans un contexte de recherche, (co-)inventé par (entre
autres) le physicien-chercheur Tim Berners-Lee alors qu'il travaillait
au CERN.

### @Table ronde (C)

L'échange entre l'écriture numérique et papier est questionnée de plus
en plus par Bleu Orange car on est confronté à l'obsolescence des
#technologies. La revue est toujours confrontée à ces différents
langages. En train de revoir la mission pour revoir la question de
#traduction.

Essayer de tirer partie des nouveaux outils offerts.

Quelques éditeurs de livre d'art qui ont essayé de développer leurs
collections en epub. Un éditeur de livre d'art qui fait auj du livre
numérique : idée de stratégie de disinction par l'innovation que
mettent en œuvre ces éditeurs à l'heure actuelle. Montrer qu'on reste
branché et qu'on est moderne.

Tirer partie des possibilités de l'écran en intégrant des vidéos, des
éléments interactifs.

Traduction ou adaptation ? car traduction = #interprétation

L'échange entre l'écriture numérique et papier est questionnée de plus
en plus par Bleu Orange car on est confronté à l'obsolescence des
technologies. La Revue BleuOrange toujours confrontée à ces différents
langages. En train de revoir la mission pour revoir la question de
traduction. Notamment la traduction du français vers l'anglais, à
rebours de la pratique actuelle de celle de la traduction de textes de
l'anglais vers le français.

NT : Définir littérature numérique : on espère qu'à un moment ça
deviendra juste de la littérature.

"Travailler collectivement pour mieux appréhender l'objet-création. "
QUI PARLE ?



@Souchier-fiche : Goody : le livre (codex) comme petit véhicule
plastique.

Ce n'est pas le livre qui est transformé par le numérique, mais la
relation intime de l'écriture à ses supports et à ses usages

Nous avons aujourd'hui des formes hybrides et passagères mais : Le
livre comme matrice culturelle ne trouvera dans le numérique que le
prolongement de ce qu'il a déjà envisagé dans sa forme même :

Ne pas rester prisonniers des formes instituantes , s'en extraire car
ces cadres agissent comme...

Théâtre de mémoire, à la fois support matériel et technologie de
l'intellect, assume corporellement la pérennité et la matérialité? de
l'écriture

technologie de l'intellect au sens de Goody

Le livre offre à l'écriture une portabililté :

En l'assignant à une forme et des formats, le livre répond aux
nécessités anthropologiques de l'écriture.

En assignant le livre à une forme, des supports, un format, le livre
répond aux questions de l'écriture

Travailler sur une ouverture de la notion d'écriture au sens
anthropologique - appel à travaux

## Espace / espace public

### @Epron_fiche #univers :

"Nous nous trouvons devant un univers visuel sphérique pour renvoyer à
la vision humaniste d'un nouveau monde. Des mots flottent dans la voûte
céleste et les données sont visualisées en en 3D."

###

### @Charbonneau_fiche

« Le droit d'auteur n'est pas un problème juridique, mais de
gouvernance. »

Sans plateforme, il n'y a pas de diffusion et, conséquemment, le livre
n'existe tout simplement pas. Parmi les enjeux que cela soulève, il
faut penser à ajouter une strate de métadonnées aux catalogues des
bibliothèques.

Plusieurs questions se posent : comment les agissement des communautés
s'insèrent dans la documentation numérique et comment bâtir un marché
sur des bases aussi mouvantes que sont celles du numérique ?

C'est aux instances de gouvernance que revient la tâche de réfléchir la
place du numérique dans l'espace commun, public et ne pas laisser des
intérêts privés, préconisant la rentabilité (GAFAM, particulièrement),
dicter les paramètres qui délimitent et définissent l'espace numérique.

### @Charbonneau_visuels

[[Tableau]{.underline}](http://www.culturelibre.ca/files/sites/26/2018/05/img_3200-2-975x1008.jpg)
**public c. privé** dans les visuels [[en
ligne]{.underline}](http://www.culturelibre.ca/recherches-juridiques/2018-encoder-les-communs-de-la-lecture/)

### @Table ronde «Publier la recherche»_notecoll.

« Question : tout le budget de l'acquisition de la bibliothèque passe
dans l'abonnement aux revues. Bibliothèque devient noyautée par des
forces extérieures. Cette année, on ne peut plus acheter de bouquins. »

### @Charbonneau_tweet

Tweet par @Tolebrun [SOURCE :
[[https://twitter.com/i/web/status/991312431422033920]{.underline}](https://twitter.com/i/web/status/991312431422033920)]

#ECRIDIL2018 Charbonneau: "Il faut conceptualiser le droit d'auteur
en contexte numérique comme un chantier. Le droit d'auteur n'est pas
un problème juridique, mais un problème de gouvernance"
[[@em_guiraud]{.underline}](https://twitter.com/em_guiraud)

### @Charbonneau_tweet

Tweet par @carnetdlis
[SOURCE:[[https://twitter.com/carnetdlis/status/991312534014775297]{.underline}](https://twitter.com/carnetdlis/status/991312534014775297)]

[[#ecridil2018]{.underline}](https://twitter.com/hashtag/ecridil2018?src=hash)
Olivier Charbonneau
[[@culturelibre]{.underline}](https://twitter.com/culturelibre) «
Encoder les communs de la lecture » : serions-nous tombés dans un trou
noir avec le numérique ?
[[#humour]{.underline}](https://twitter.com/hashtag/humour?src=hash) de
bibliothécaire ;-)

### @Table ronde «Publier la recherche»_notecoll.

«Q : comment peut-on développer l'open access, par quels moyens
concrets ? NS : les revues doivent adopter des licences et des labels.
JB : on parle d'open access au niveau de la diffusion, on veut aussi
que les toutils le soient. Format propriétaire d'adobe ayant disparu:
toutes les publications ont disparu. Il faut vraiment que ce soient des
opentech, par juste un format propriétaire.Il ne faut pas oublier les
formats ouverts et donc les technologies ouvertes. »

### @Table ronde «Publier la recherche»_notecoll.

[Réponse à la question « y'a-t-il des différences et/ou similarités
entre la France et le Québec dans les enjeux de la publication ?»] «LH
: modification des injonctions qui nous sont faites en temps que
chercheurs: Publish or perish, 1960. En 1998, reprise de Grunfeld: demo
or die. Get visible or vanish. Deploy or die. Ce qui travaille derrière,
c'est le dépôt de brevet, le prototypage, de rencontrer le marché, le
grand public, une communauté de pairs.»

### @Drochon_citationdiapo #espace_commun #hybridité:

"Pour ce prototype fonctionnel, il s'agissait d'approcher un espace
commun de sérendipité, une hybridation possible d'une affiche et d'un
agent conversationnel"

### @Paquienséguy_fiche

Question de Marcello : « La question du iPad et de Android. Il semble
que des acteurs institutionnels ne se posent pas la question des
alternatives --- on peut éviter les plateforme commerciales. Des
pratiques se développent complètement véhiculées par deux entreprises.
Est-ce par ignorance et incompétence ou par choix? C'est un système de
production, les musées publics deviennent des propriétés intellectuelles
de Apple ou Google. Vos analyses ont-elles permis de rendre compte de
ces choix? »

### @Laborderie- PAD

Métaphore de la cellule et du jardin, #espace clos mais en expansion
Espace en expansion dans la cellule, dans le jardin, métaphore assez
fructueuse pour penser les nouvelles formes du livre dans l'espace
numérique.

Sur la question de l'augmentation : Prolongement du livre dans un
espace extérieur et avec lequle il s'articule

L'œuvre déborde l'espace du livre et vice-versa..

La cloture définit un #espace que le lecteur peut arpenter

###  - Tweet :

@tolebrun Tom Lebrun : "#ECRIDIL2018 Poreuse : sur la question des
configurations, Poreuse est vu comme un espace de désorientation. Tous
se perdent dans la configuration proposée. La cause ? Un tressage
polyphonique, mais aussi un horizon d'attente pour une lecture
discontinue au travers de l'hyperlien"

#ECRIDIL2018 @AnthonyMasure, refaire du Web un espace public, évitant
la privatisation des plateformes... https://t.co/Td0TKyssrl

@Tolebrun Tom Lebrun : "#ECRIDIL2018 "Le livre se définit normalement
par sa clôture, c'est un espace fermé. Il y a une oxymore à parler de
livre "numérique", car un livre numérique est par définition ouvert,
car connecté."

@ENumérique : #ECRIDIL2018 @AnthonyMasure, refaire du Web un espace
public, évitant la privatisation des plateformes

@Tolebrun : "On peut donc imaginer la collaboration de personnes qui
dialoguent pour arriver à la publication de secrets dans un espace
commun de lecture"

@Tolebrun : "#ECRIDIL2018 Dans toutes les vidéos de booktubeuses, la
matérialité du livre est très présente, le livre occupe l'espace
physique et symbolique. Sur les réseaux sociaux (instagram) le livre
existe aussi beaucoup comme objet."

@Tolebrun : "#ECRIDIL2018 Poreuse : on aimerait proposer la notion d'
"autorité redistribuée". L'espace éditorial se distribue au travers
de différents actants. On peut faire l'hypothèse d'une autorité
distribuée, dans un processus constant de croissance et de
développement"

@Tolebrun :

"#ecridil18 Le livre poreuse met à l'épreuve les concepts de
l'édition traditionnelle; l'auteure J. Mézenc parle d'œuvre hybride,
où se croisent et se mélange les voix de 3 personnages. Polyphonie, mais
aussi matérialités mélangées. Un paratexte qui construit une errance"
suite... : ... Avec des termes de labyrinthe, de fil d'ariane, etc.
L'espace éditorial, supports comme gestes, s'organisent d'une manière
spécifique ==> l'enjeu de la présentation est de comprendre comment

Question de Marcello (vient de la fiche synthèse de Paquienséguy): « La
question du iPad et de Android. Il semble que des acteurs
institutionnels ne se posent pas la quesiton des alternatives --- on
peut éviter les plateforme commerciales. Des pratiques se développent
complétement véhiculées par deux entreprises. Est-ce par ignorance et
incompétence ou par choix? C'est un système de production, les musées
publics deviennent des propriétés intellectuelles de Apple ou Google.
Vos analyses ont-elles permis de rendre compte de ces choix? »

### Paquienséguy:

capture d'écran de la slide «1. Les e-albums. En droite ligne des
cd-rom culturels produits et promus par la Réunion des Musées Nationaux
à la fin des années 1990 ; Glissement du « multimédia » au « transmédia
» de Jenkins et son « univers narratif original » ; Complément aux
grandes expositions Entre le catalogue d'exposition, la conférence
introductive et la trace».

###  Table ronde «Publier la recherche» :

extrait du pad public.« Question : tout le budget de l'acquisition de
la bibliothèque passe dans l'abonnement aux revues. Bibliothèque
devient noyautée par des forces extérieures. Cette année, on ne peut
plus acheter de bouquins. »

###  Table ronde «Publier la recherche» : extrait du pad public:

«Q : comment peut-on développer l'open access, par quels moyens
concrets ? NS : les revues doivent adopter des licences et des labels.
JB : on parle d'open access au niveau de la diffusion, on veut aussi
que les toutils le soient. Format propriétaire d'adobe ayant disparu:
toutes les publications ont disparu. Il faut vraiment que ce soient des
opentech, par juste un format propriétaire.Il ne faut pas oublier les
formats ouverts et donc les technologies ouvertes. »

@Souchier-fiche : Replacer l'écriture dans son espace propre (et donc
sous toutes ses formes).

Revenir à quatre schèmes fondamentaux :

• #mémorisation

• #circulation

• #narrativisation

• #visualisation

###  @Rio_Pad

Mettre en scène un autre univers que de la littérature
Mise en visualisation de données complexes, non orienté multi facettes.

@Lacelle_fiche

«Créer des synergies entre les acteurs de l'édition et développer de
l'expertise dans le domaine de l'édition jeunesse. Comment soutenir la
transformation en faisant des contenus de qualité? Comment assurer sa
découvrabilité (et pourquoi personne n'en a parlé ici?) par les
familles et les enseignants?. C'est au cœur des préoccupations
éditoriales.»

## Fragment

### @Drochon_captured'écrandeladiapo6 #fragment

### @Tréhondart-fiche (mot-clé #fragmentation):

"Il y a un risque de fragmentation du contenu dans le livre d'art
numérique".

###  @Laborderie PAD :

Principe de morcellement et de fragment : volonté d'échapper à un mode
clôt.

#cellule/#fragment

### Charbonneau:

Critique ces métaphores. enclosures au R-U, émergence de la propriété
individuelle [?]
Mouvement des enclosures au Royaume Uni
Dans la cellule, il y a aussi l'infection
La cellule n'est pertinente que s'il y a une masse critique (plusieurs
cellules pour faire un corps)
Masse critique : plusieurs cellules pour faire corps
Idée de la rhétorique des clotures qui va profondément vers le droit
propriétaires.

Membrane cellulaire plutôt que cloture. Lieu de circulation ente mondes
intérieur et extérieur
On a besoin de points dans le web où il y a une sorte de condensation du
savoir, de l'imaginaire mais aussi un mouvement centrifuge qui va vers
l'extérieur.

### @Jahjah PAD :

Gestes (configurations) : continu (tourner la page qui crée de la
désorientation), discontinu (hyperlien qui crée le fil d'ariane) :
fragmentation

###  @Rio_PAD :

Mise en scène : Format court et sériel, saisons et épisodes -> autre
univers que la littérature.

## (Humanités numériques)

### @Fauchié_communication ou @Fauchié_citation

> [L'homme] est *parmi* les machines qui opèrent avec lui.
>
> Gilbert Simondon, *Du mode d'existence des objets techniques* |

+-----------------------------------+-----------------------------------+
| > Note                            | > Pour citer très rapidement      |
|                                   | > Gilbert Simondon, philosophe de |
|                                   | > la technique bien connu,        |
|                                   | > l'homme peut et doit s'associer |
|                                   | > aux machines, aux systèmes      |
|                                   | > informatiques dans notre cas,   |
|                                   | > et non plus être un opérateur   |
|                                   | > ou un surveillant.              |
+-----------------------------------+-----------------------------------+

### @Fauchié_communications ou @Fauchié_citations

> Entretenir un rapport créatif aux machines n'est pas évident d'autant
> que ce dernier est parfois, voire souvent, empêché par les machines
> elles-mêmes et par la structure productive dans laquelle elles
> s'inscrivent.
>
> Sophie Fétro, "Œuvrer avec les machines numériques", *Back Office*

### @Epron_fiche #objet_éditorial_numérique :

"Benoît Epron et Catherine Muller présentent leur projet d'un nouvel
objet éditorial numérique,"l'abécédaire des mondes lettrés" lancé en
2017." OU @Epron-diapo2 ? +
[[http://abecedaire.enssib.fr]{.underline}](http://abecedaire.enssib.fr)

### @Perret_diapo14 #humanités_digitales + @Perret_tweet [ ]{.underline}

[[https://twitter.com/arthurperret/status/990951172528922625]{.underline}](https://twitter.com/arthurperret/status/990951172528922625)
(suggestion de lecture cohabitation écrit/écran)

### @Table ronde «Publier la recherche» photoLOB

[[https://albums.photographie.loupbrun.ca/#15252729030055/15252747485597]{.underline}](https://albums.photographie.loupbrun.ca/#15252729030055/15252747485597)

(une photo de LOB représentant Julie, Nicolas et Servanne). Peut-être
accompagnée d'un extrait du pad collectif : « Q : Selon vous, quels
sont les enjeux les plus urgents pour repenser les designs de
publication ? »

###  @Laborderie PAD :

Principe de morcellement et de fragment : volonté d'échapper à un mode
clôt.

#cellule/#fragment

###  @Charbonneau_notecoll:

Critique ces métaphores. enclosures au R-U, émergence de la propriété
individuelle [?]
Mouvement des enclosures au Royaume Uni
Dans la cellule, il y a aussi l'infection
La cellule n'est pertinente que s'il y a une masse critique (plusieurs
cellules pour faire un corps)
Masse critique : plusieurs cellules pour faire corps
Idée de la rhétorique des clotures qui va profondément vers le droit
propriétaires.

Membrane cellulaire plutôt que cloture. Lieu de circulation ente mondes
intérieur et extérieur
On a besoin de points dans le web où il y a une sorte de condensation du
savoir, de l'imaginaire mais aussi un mouvement centrifuge qui va vers
l'extérieur.

### @Soubret_Pad

Les humanités numériques ne sont pas si éloignées du design.

### @Soubret_Google

Il ne s'agit décidément pas d'un design plus proche de l'art et qui
chercherait une beauté « libre » (contemplation). Il s'agit d'un design
innovant, proche des humanités numériques 2.0 définies par Masure et non
un design d'invention comme pourrait l'être celui des humanités
numériques 3.0 ou le concept émergent d'« art thinking » dans les
cercles de la recherche en innovation.

###  @Jahjah PAD :

Gestes (configurations) : continu (tourner la page qui crée de la
désorientation), discontinu (hyperlien qui crée le fil d'ariane) :
fragmentation

### @Rio_PAD :

Mise en scène : Format court et sériel, saisons et épisodes -> autre
univers que la littérature.

### @Masure_fiche :

Si le savoir n'existe pas sans transmission, il semble alors nécessaire
de repenser les formes et formats des publications scientifiques.
La pensée comme mode de saisie (cf. Gottlob Frege) et non comme mode de
production de la pensée.

Format qui doit être attrayant (pour favoriser conditions de saisie).

Il faut penser le web pour les chercheurs. (cf. Tim Berners-Lee) Il faut
refaire du web un espace public.

Aujourd'hui, le design et le web ont pris une toute autre direction.
Dérives. Le web capitaliste, le web des plateformes (et donc
propriétarisation*, web fermé), le web de la captation (captation des
savoirs à des fins capitalistes/marchandes).

Problème (triste) : les chercheurs ne sont pas lus --- certains ne sont
jamais lus! :'(

Une solution : l'autopublication. (Publications alternatives.) Certains
chercheurs se sont tournés vers l'autopublication (publication sur leur
blogue personnel).

Les chercheurs sont souvent enfermés dans leur format/forme de leurs
contenus.

### @Masure-slide 12

### @Haute-fiche :

Critique salutaire des solutions clé en main proposées par des
éditeurs/diffuseur importants dans le monde du libre accès (loden,
SPIP). Importance de trouver une identité éditoriale aussi dans le
design.

###  @Gascuel_fiche : l'en-dehors des digital humanities

Une logique plus industrielle, _marketing_ ou commerciale, par rapport
aux autres intervenants. Gascuel travaille avec des éditeurs, des boîtes
avec des équipes qui cherchent l'objet-livre de demain. « On travaille
sur des problématiques d'éditeurs -- financièes et non celles de
chercheurs. »La recherche commerciale est plus rapide, performante,
efficace que celles des universitaires : elle a un but et les gens y
travaille activement... Il y a une recherche, mais l'orientation est
commerciale -- on cherche à répondre aux besoins de consommateurs,
leurs désirs

###  @Tadier_pad:

Le design a un rôle essentiel car il peut inaugurer un mouvement qui va
inscrire les dispo lecture / écritre dans la dimension humaine
Nous remettre au cœur de nos outils

###  @Tadier_Photo

Intervenant + slide en background flou + camera
https://albums.photographie.loupbrun.ca/#15252729030055/15252736796629

Intervenant + slide en background flou + public
https://albums.photographie.loupbrun.ca/#15252729030055/15252736808802

### Table ronde «Publier la recherche» :

https://albums.photographie.loupbrun.ca/#15252729030055/15252747485597
(une photo de LOB représentant Julie, Nicolas et Servanne). Peut-être
accompagnée d'un extrait du pad collectif : « Q : Selon vous, quels
sont les enjeux les plus urgents pour repenser les designs de
publication ? »

@Souchier-fiche : Livre et écriture sont si indissociablement liés dans
notre culture/époque; il nous faut les dissocier, penser l'écriture
sous toutes ses formes; ne pas s'enfermer dans le paradigme du codex.
S'affranchir de de l'emprise intellectuelle du livre.

Approche anthropologique : l'écriture répond au besoin communicationnel
de l'être humain. L'écriture doit être repensée selon sa fonction.

Usages du livre : matérialité, gestuelle et lieux communs. Constat :
détournement du culte du livre par intérêts capitalistes.

dire que le numérique nous permet de repenser de manière reflexive les
usages et les discours est une tarte à la crème. le numérique nous
oblige de repenser l'ancien monde : soit. mais profiter de
l'injonction numérique pour replacer le livre dans l'espace qui est le
sien : la culture, l'histoire

Replacer le livre dans l'Espace qui est le sien : celui de la culture
et de l'histoire

nécessités anthropologiques

quelle nécessités humaines et communicationnelles, et préoccupations
culturelles le livre a prétendu répondre au cours de ces 2 millénaires

autre hypothèse : le Graal de l''industrie num. : réside dans ce que
le codex a mis au point à travers les siècles : des communs que les
industriels cherchent à faire émerger grâce aux usages démultipliés par
les utilisateurs. : c'est la pratique des usagers qui est visée

Ce ne sont plus le livre ni l'info qui sont visés par l'industrie,
mais la pratique des usagers

Image crue de notre société dans laquelle tout peut être marchandisé

### @Marcoux_fiche

«On peut résumer la culture numérique comme une prise de conscience de
notre rôle dans la société.»

## Jeu

###

### @Develotte_pad

Univers narratif complexe mis en contexte éducatif.

Susciter des interactions adulte enfant.
multimodalité, rapports corps-écran

Jeu renforce l'implication de l'enfant dans l'histoire

Inviter l'enfant à interagir, puis temps libre laissé à l'enfant pour
qu'il puisse explorer de lui-même

### @Develotte_PP

n°15

###

### @Laborderie_PDF

Fonction communicationnelle de l'écriture

L'information en soi n'offre aucune prise, aucune réalité en contexte
numérique si elle n'est pas socialisée. C'est ce que les enseignants
ont constaté en observant leurs élève

### @Tréhondart-fiche (#attentes #outils) :

"Les lecteurs résistent quand même au spectaculaire. Il y a parfois des
outils qui déçoivent l'attente du lecteur (comme par exemple les
rectangles transparents cliquables sur un tableau qui donnent accès à
des éléments biographiques sur le peintre, inattendus pour le
lecteur)".

###  @Haute :

* Ajouter des médias un peu inhabituels pour encourager une autre
médiation de l'article ou du contenu savant.

###  @Masure-slide 37  @Anthologie palatine [SLIDES] / IMAGES : LYCÉENS EN ITALIE / + PAD COLLECTIF

son permet de faire ce que l'on veut des données : un usage savant, un
usage pop --- Méléagre in love, poème écrit par des étudiants parisiens
--- le grec arrive comme traduction
Usage complètement pop : appropriation par des étudiants qui ont fait un
site "pop"
Renversement car le grec arrive comme une traduction, l'original
devient la version française.

###  @Rio_PAD

Un projet R et D, mené avec une entreprise : Adrénalivre.
Mission : créer des livres dits interactifs numériques. Travailler sur
le storytelling interactif.
Créer de la fiction inspirée des livres dont vous êtes le héros.

## Mise en scène du livre

### @Deseilligny_PP

n°2, n°8, n°13

### @Deseilligny- PAD :

La communication propose une étude du #livre imprimé en #régime
#numérique, à travers une analyse des #processus de communications et
de #mise en scènes du livre en tant qu'objet imprimé en contexte
numérique.
Le livre devient le sujet principal d'images et d'approches en
communication en tant qu'il est porteur d'une signification
## Livre comme support d'accessoire de mode, #design visuel
La marque qui utilise le livre dans ses publicités aspire ces
connotation symboliques et affectives.
Le livre fabrique un ethos de marque culturelle

Héritage d'une culture visuelle = formes modernes des cabinets de
curiosité (surtout chez les blogueurs)
Rhétorique visuelle ou picturale - codes de la nature morte / punctum.

Comment les professionnels (libraires, #éditeurs) du livre mettent en
scène les objets-livres, comment l'iconographie du livre est abordée
aujourd'hui ?

###  @Table Ronde {C}- PAD :

Volonté de développer des #beaux livres #numériques, vendus très chers
car le livre fait la personne que l'on est.

Projets éditoriaux numériques : il s'agit surtout d'expérimentation
(confrontés à des contextes industriels contraignants).
statut d'objet livre dans ses dimensions culturelles et esthétiques qui
font que ces éditeurs ont du mal à passer au beau livre numérique

C'est la qualité des interactions dans le livre qui fait changer de
qualification ce dernier, et qui l'amène vers le statut de beau livre.

Quelques éditeurs de livre d'art qui ont essayé de développer leurs
collections en epub. Un éditeur de livre d'art qui fait auj du livre
numérique : idée de stratégie de disinction par l'innovation que
mettent en œuvre ces éditeurs à l'heure actuelle.

Question de la #traduction ramène la question de l'écriture, comment
écrire une œuvre #hypermédiatique sur le plan autant littéraire que
programmatif ?
Volonté de développer des #beaux livres #numériques, vendus très chers
car le livre fait la personne que l'on est.

L'échange entre l'écriture numérique et papier est questionnée de plus
en plus par Bleu Orange car on est confronté à l'obsolescence des
technologies. La Revue BleuOrange toujours confrontée à ces différents
langages. En train de revoir la mission pour revoir la question de
traduction. Notamment la traduction du français vers l'anglais, à
rebours de la pratique actuelle de celle de la traduction de textes de
l'anglais vers le français.

Travailler aussi la dimension esthétique au-delà de la dimension
juridique. À propos du devenir authentique des techniques : une
technique apparaît avant qu'on sache vraiment à quoi elle sert.
Travailler collectivement pour mieux appréhender l'objet-création.


@Masure-fiche :

Anthony Masure, en designer graphique, note la gestion typographique
parfois «hasardeuse», faite sans regard aux codes et aux pratiques
pourtant bien établies dans les techniques de mise en page (histoire du
livre, histoire de la typographie : plusieurs siècles de
perfectionnement). Les qualités graphiques des objets de lecture
reçoivent peut d'attention sur le plan de la forme et prennent peu en
compte le retour des lecteurs (pour qui on écrit pourtant!). Le design
d'interface du livre web est de qualité très variable.

@Masure-slide 5
@Masure-slide 35

@CarnetDLIS
RT @servanne_m: #ecridil2018 Magnifique présentation d'Elsa Tadier
qui rappelle étrangement celle d'@ODeseilligny hier matin (l'image du
li...https://twitter.com/servanne_m/status/991345477261111296

@Tadier_Slide 5-6
Détail 1 (en haut, à gauche) : Léonard de Vinci, L'annonciation,
98x217cm, vers 1475-1480 -- huile et tempera sur bois. Florence, Uffizi
Gallery.


Détail 2 (en haut, à droite) : Hans Memlong, La Vierge et l'Enfant entre
Saint Jacques et Saint Dominique, 130x160cm, vers 1485-1490 -- huile sur
bois.
Paris, Musée du Louvre.


Détail 3 (en bas, à gauche) : Agnolo Bronzino, Portrait de Lucrezia
Panciatichi, 102x85cm, vers 1545 -- huile sur bois. Florence, Uffizi
Gallery.


Détail 4 (en bas, au centre) : Carlo Crivelli, Polyptique, 1473 --
détrempe et peinture sur bois. Ascoli Piceno, Cathédrale Sant'Emidio. Le
détail porte
sur la main d'un panneau latéral représentant San Paolo (136x39cm).


Détail 5 (en bas, à gauche) : Pierre Mignard, portrait de Françoise
d'Aubigné, marquise de Maintenon, 128x97cm, 1694 -- huile sur toile.
Musée de
Versailles.

À gauche : Bernard Van Orley, Triptyque Haneton, Bruxelles 1487/88 --
1541


En haut à droite : Cornelis Janssens van Ceulen, Portait of Queen
Henrietta Maria in mourning


En bas à droite : Attribué à Pier Francesco Foschi, Portrait of a woman,
1540-1565.

## Pluridisciplinaire

### **@Meynard/Greslou_PDF** :

Travailler dans l'optique d'une œuvre multisupports à l'édition
multiformes. Selon les publics, rajouts de compléments pédagogiques,
certains fiches possèdent des complément multimédias.

Adaptation aux différents supports, complémentarités

### @Perret_fiche #hybridité :

"On se trouve face à des perspectives intéressantes en terme
d'épistémologie mais il faut mener un travail hybride. Il est alors
nécessaire de monter en compétences à la fois en terme de savoir-faire
que d'érudition se situant alors dans une continuité humaniste."

### @Paquienséguy_visuelp.1

Capture d'une partie de la slide «1. Les e-albums. En droite ligne des
cd-rom culturels produits et promus par la Réunion des Musées Nationaux
à la fin des années 1990 ; Glissement du « multimédia » au « transmédia
» de Jenkins et son « univers narratif original » ; Complément aux
grandes expositions Entre le catalogue d'exposition, la conférence
introductive et la trace»

### @Tréhondart-fiche + @tréhondart_photo

https://albums.photographie.loupbrun.ca/#15252729030055/15252746697590

"Nolwenn Tréhondar présente les résultats d'un projet de recherche qui
vise à dresser un bilan du livre numérique en France. C'est un ouvrage
collectif réalisé dans une démarche pluridisplinaire"

### @Drochon_tweet #approche

[[https://twitter.com/tolebrun/status/990955369940897792]{.underline}](https://twitter.com/tolebrun/status/990955369940897792)

"[[#ECRIDIL2018]{.underline}](https://twitter.com/hashtag/ECRIDIL2018?src=hash)
Julien Drochon anime avec
[[@AnthonyMasure]{.underline}](https://twitter.com/AnthonyMasure) un
programme de recherche (Vox Machine) qui cherche à relier
recherche-création avec une approche d'enseignement".

###  @Masure-fiche :

Quels rôles peuvent jouer les designers dans la transmission des savoirs
à un public élargi?
Rendre le livre attrayant à tous; sensibiliser le milieu de la recherche
à ces problématiques.

### @Gascuel_fiche : TRANSMEDIA ==> AU-DELÀ DES FRONTIÈRES, OUVERTURE DU SPECTRE-LIVRE

L'objet transmédia est quelque chose qui dépasse largement le spectre
du livre traditionnel, papier. Il est un objet connecté au monde, aux
objets et est plus un outil qu'un objet fermé. « Trans- », il dépasse
les notions de clôtures traditionnelles. On trouve, parmi les prototypes
de ces objets, des étiquettes de vin où sont reliées des recettes qui
s'y accordent, Don Camillo, bouquin par lequel on accède à un film,
récit pour enfants (_Petit ours brun_) qui se relie à des épisodes de
séries jeunesse. Le livre transmédia, dans ces exemples, est une clé
d'accès à un contenu numérique. C'est un gadget.

###  @Rio_PAD:

condition du récit enchâssé : arborescence
logique programmatique qui se rajoute à la logique créative de la
littérature

### @Lacelle_fiche

«On remarque le manque des «traducteurs» entre les maillons, les mondes
de la pratique et de la recherche.»

### @Lacelle_visuel[texte issu du pwp, p. 23-28]

«Un besoin d'accompagnement des acteurs de la chaîne de production. Un
besoin de soutien technique. Des financements peu accessibles et peu
adaptés aux réalités de production. *Des objets qui ne rentrent dans
aucune «case».*»

## Sémiotique

### @Tréhondart-fiche (mot-clé #sémiotique_sociale) :

"La sémiotique sociale est une méthodologie en construction".

###  @Tadier_Pad :

Question sur le rapport à la valeur des différentes propositions : on
doit toujours choisir la typo dans les liseuses.

Réponse : il y a des points de passage entre l'écran et la typo,
notamment au niveau de l'écran. Mais on se rattache à un volume qu'on
peut ouvrir avec les liseuses. Elsa Tadier rajoute que la perspective
sémiotique peut être intéressante parce qu'elle va interroger les
connotations de la matérialité. La thèse de Blanchard sur la sémiotique
de la typographie en traite ; Selon les différents dispositifs matériels
qu'on utilise, on aura ainsi une diffusion différente du contenu (cela
implique de faire un lien entre la valeur qu'on perçoit et la valeur
économique).

###  @Palatine_PAD : sur la circulation du sens

Collaboration avec deux lycées qui travaillent à l'édition des textes
sous la supervision de professeurs de grec ou de latin
Travail autour de l'imaginaire car la plateforme le permet (lien
youtube, iconographie de vases)
alignement des différentes traductions, travail autour de la notion
d'imaginaire collectif et populaire

Proposer une structuration ouverte des données : création d'une API
donc base de données ouverte. N'importe qui peut participer, proposer
des versions (plutôt que des traductions. il n'y a plus l'original,
que des résonances de texte)

###  @Tadier_Pad

rupture sémiotique (Souchier)
Perception du volume à requestionner avec les tablettes.

## Sensorialité

### @Laborderie_PDF

Modéliser des usages et proposer au lecteur : lire, explorer,
éditorialiser.

### @Drochon_citationdeladiapo #dispositif_sensible #hybridité + @Drochon_captured'écrandiapo2 :

"Il [Vox Machine] vise notamment à expérimenter le rapprochement des
interactions vocales et de l'écrit par la production de dispositif
sensible multi utilisateur hybridant le visuel et le vocal".

### @Paquienséguy_visuelp.2

Capture d'une partie d'une slide («L'histoire de l'art v.s le
numérique?» ) : « qui générerait de nouvelles pratiques? voir avant,
revoir, voir pendant, voir plusieurs choses en même temps, dont
l'appropriation est quasi physique: toucher, emporter avec soi, zoomer,
écouter... »

### @Tréhondart-fiche (mot-clé #sensualité_livresque) :

"Il renoue également avec une certaine sensualité livresque
(mouvements, animations etc...)."

###  @Tadier _Fiche

- La citation de Merleau Ponty permet d'installer le propos, de par
ses nombreuses évocations du corps et de sa réalité physique.
- Tadier pose la question : pourquoi tient-on tant à l'expression livre
numérique ?
- Pour Tadier, il existe une intimité entre fond et forme. Cette
proximité est liée au rôle des corps, au rôle du designer mais également
au rôle du lecteur.
- Il est donc nécessaire d'insister sur la place du corps en contexte
numérique (c'est la thèse de Tadier, d'ailleurs dirigée par Emmanüel
Souchier)

- La réflexion doit également s'inscrire dans une pensée graphique,
dirigée en fonction des corps de métiers. La question à garder en tête
est la suivante : "Qu'est-ce que le livre meut en moi et comment
est-ce que je meus le livre ?"

- Thèse de Tadier : une histoire du corps qui participe de la culture du
livre. Ce corps confronté au livre est pluriel, normé, mais aussi
émotif, en mouvements, sensoriel.

- L'idée que la question du corps est primordiale pour celle du design
n'est pas neuve mais se pose autrement avec les dispositifs numériques.
Tadier donne l'exemple d'Hegerton (1821), un livre de dévotion anglais
du XVe siècle qui reprend esthétiquement le mythe de l'eucharistie ==>
tout le livre saigne.

###  @Masurefiche :

Interfaces : dimension de sensibilité ; rendre les contenus sensibles.

###  @carnetdlis par rapport à @Tadier_tweet

@#ecridil2018 Elsa Tadier : le livre suscite le corps à corps. Le corps
comme partie intégrante du livre ?

@ENumeriques_tweet
#ECRIDIL2018 Elsa Tadier nous parle des rapports entre #corps, #livre
et #design : la place du #corps dans le... https://t.co/adiey0Mi2G

@Haute-fiche :

La publication scientifique ne tient pas en compte les aspects
graphiques et sensibles. On observe notamment :
- Présupposé d'une idéalité du sens face à la forme.
- pb économique (comment maintenir les revues, personnel...)
- propositions clé en main ont entraîné un formatage technique des
revues mises dans des tp hypernormés, ce qui facilite production et
consultation, mais au dépens du design. Une même maquette pour des
propositions très différentes, comme si le contenu pouvait se passer de
sa forme, qu'une forme pouvait se prêter à de multiples contenus divers
et variés.

Qu'est-ce que serait une forme de publication pour une recherche en art
et création, et non pas sur l'art et la création ?
Tentative de créer une revue en recherche à la fois sur/avec/en
création

@Tadier_Slide_2
Un corps humain est là, quand entre voyant et visible, entre
touchant et touché, entre un œil et l'autre, entre la main et
la main se fait une sorte de recroisement, quand s'allume l'
étincelle du sentant sensible (...).
Maurice Merleau-Ponty, L'œil et l'esprit, Paris, 1960

@Tadier_Slide_4
Psautier et Rosaire de la Vierge (à partir du f. 27 puis) Litanies

Paquienséguy: Capture d'écran d'une partie d'une slide («L'histoire
de l'art v.s le numérique?» p.2) : « qui générerait de nouvelles
pratiques? voir avant, revoir, voir pendant, voir plusieurs choses en
même temps, dont l'appropriation est quasi physique: toucher, emporter
avec soi, zoomer, écouter... »


@GASCUEL_FICHE
Le livre connecté est un objet -- sa numéricité, son code n'est pas
l'intérêt : l'expérience de « lecture est sensorielle » ; sens et
sensorialité sont consubstantielles.


@Souchier-fiche : Importance de la matérialité sensible du livre
(communication).

Porte la présence du geste, de la main. Trace des voix et des corps.
Modèle la manière de voir le monde

émancipation de son corps, de l'espace et du temps : c'est la première
techno de l'intellect de l'humanité

Sortir de l'échange direct - l'écriture = LA première technologie de
l'intellect de l'humanité. On n'arrive

plus à en sortir tellement elle nous a enveloppé

## Forme

### @Soubret_Pad

Design et esthétique : utiliser le design à des fins esthétiques.
Différence avec l'art : le design a une fonction. alors que l'art
n'existe que pour être contemplé.

Le design pour améliorer le processus de gestion; **visée performative
du design**. Transfert.

Dialogue qui affirme la forme de l'ouvrage (un ouvrage qui assume
ouvertement sa forme). Auparavant, erreur d'imposer une forme à
n'importe quel fond (ex. templates inadéquats); pas nécessairement
approprié. Design approprié.
Le livre papier comme « point de contact » (parmi d'autres) entre les
auteurs.

Le design thinking comme exploitation du design; instrumentalisation du
design. J-L Soubret décrit cette instrumentalisation du design comme un
mal nécessaire pour le monde de l'édition, pour rendre le design
compréhensible aux non-designers (gens d'affaires, littéraires,
investisseurs, autres acteurs du livre...)

### @Soubret_Googledoc

Le livre est historiquement la forme de consécration de disciplines en
tension entre les mondes intellectuels et académiques et procède d'un
projet, forme privilégiée du design. Je précise donc par là le genre que
j'aborde : le transfert qui n'est pas nécessairement une vulgarisation
mais gagne à une formalisation abordable pouvant aller jusqu'à une
certain niveau de modélisation des arguments (quitte à les discuter de
manière réflexive et subtile, à ne pas en ignorer la complexité) et de
visualisation versus lecture (métaphores, illustrations, image du
texte...).

###

### **@Meynard/Greslou_PDF** :

Travailler dans l'optique d'une œuvre multisupports à l'édition
multiformes. Selon les publics, rajouts de compléments pédagogiques,
certains fiches possèdent des complément multimédias.

Origines du projet : chantier de recherche, idée de l'exploitation,
travail collectif. Un des premiers projets en humanités numériques car
2006. Ce n'est pas fini, recherche et expérimentation. Spécialistes
dans différents domaines. Les chercheurs sont des "Bricoleurs":

Perspective de mouvement, pas une édition figée avec un apparat critique
développé, aux normes de l'édition scientifique. Souci de
complémentarité entre les versions selon les supports/plateformes.

### @Epron_fiche #design :

"Le livre imprimé devient alors un réel projet de design éditorial qui
répond à un besoin d'outil d'écriture savante."

### @Raymond_visuel p.3

[extraits de *Révolutions*]. #mise_en_sène #fragment

### @Raymond_photoLOB

[https://albums.photographie.loupbrun.ca/#15252729030055/15252736962350]
où l'on voit Raymond parler. Peut-être accompagnée de l'extrait du pad
collectif : «Considérer la contrainte comme structure portant en germe
une potentialité en attente de réalisation.» #forme

### @Masure -fiche :

En transformant les savoirs (en leur donnant forme), le design les fait
vraiment exister dans le monde.

###  @Masure-réponse :

Un travail d'amélioration en terme de littératie numérique pour les
chercheurs. (opposition complète entre le format et le contexte (?) ex :
une thèse sur la bd numérique uniquement disponible sur papier.
Masure-réponse : : Aussi, reconnaissance des blogs comme pratiques de
recherche. Le peer review est l'Alpha et l'Oméga !

###  @Masure-slide3  @Tadie_Pad -

Pour Tadier, il existe une intimité entre fond et forme. Cette proximité
est liée au rôle des corps, au rôle du designer mais également au rôle
du lecteur.

###   @Haute-réponse :

Les chercheurs sont évalués sur les articles, les publications et si on
veut innover, différente du « long paper », il faudrait que ces formes
puissent être reconnues comme résultat de recherche. Il faut des
contextes éditoriaux nouveaux, qui évalue pareillement, mais s'ouvre à
de nouveaux contenus. Il faut de nouvelles plateformes, il faut les
inventer ou encore accorder à celles existantes (ex : revue BleuOrange),
un statut légitime

### @Drochon_citationdiapo29 #protéiforme

"Le livre si on le définit du côté de ses producteurs comme la
réalisation

d'un artefact édité (agglomération, discussion, sélection, vérification
et

diffusion) peut potentiellement être protéiforme. Le Livre peut être

envisagé comme un podcast, comme un livre audio, comme la convergence

des médium."

@Souchier-fiche : Replacer l'écriture dans son espace propre (et donc
sous toutes ses formes).

Revenir à quatre schèmes fondamentaux :

• #mémorisation

• #circulation

• #narrativisation

• #visualisation

Une double exigence : tension entre l'économie capitaliste du flux
(d'information) et la question des formats qui nécessitent une
réification.

Anonymat des artisans, auteurs, etc.

Silex chargé de gestes qui en font l'efficacité

### @Lacelle_visuel (pwp p.4-5)

«Des banques de livres numériques en accès libre et gratuit pour les
enfants se multiplient depuis le début du millénaire. Cepandant, la
production de ces livres numériques pour enfants reste encore très
marginale dans le milieu québécois, parce que mal connue ou encoremal
adaptée à toutes les possibilités du numérique

et aux caractéristiques du lectorat.»

## Collectif

### @Sauret_tweet

> AU SUJET DU PAD COLLECTIF
>
> [[https://twitter.com/nicolasauret/status/990942027020939265]{.underline}](https://twitter.com/nicolasauret/status/990942027020939265)

### @Sauret_tweet AVEC IMAGES SUR TWEET

AU SUJET DE GIT :
[[https://twitter.com/nicolasauret/status/991318185721909248]{.underline}](https://twitter.com/nicolasauret/status/991318185721909248)

> C'est aussi l'exemple de la chaine de
> [[@SensPublic]{.underline}](https://twitter.com/SensPublic) sur
> framagit/gitlab encore en bêta mais déjà opérationnelle (100 articles
> publiés). Ca mériterait un article collectif
> [[@ENumeriques]{.underline}](https://twitter.com/ENumeriques)
> [[@servanne_m]{.underline}](https://twitter.com/servanne_m)
> [[@monterosato]{.underline}](https://twitter.com/monterosato)
> [[#ecridil2018]{.underline}](https://twitter.com/hashtag/ecridil2018?src=hash)

### @Epron_fiche #écriture_collaborative :

"Le numérique permet ainsi de bénéficier de la possibilité de
l'écriture collaborative, de faire des liens entre les différentes
entrées et d'insérer des hyperliens." @Epron-tweet
([[991302564229664774]{.underline}](https://twitter.com/placedproject/status/991302564229664774))

### @Charbonneau_tweet

TWEET PAR @TOLEBRUN

[[#ECRIDIL2018]{.underline}](https://twitter.com/hashtag/ECRIDIL2018?src=hash)
Charbonneau "Nous devons réfléchir au design institutionnel. Le libre
accès doit être une hypothèse de travail dès le départ. Cela introduit
le concept de gratuité comme étant la plus-value numérique par
excellence"

### @Charbonneau_tweet

*THREAD* PAR @ANTOINENTL

les plateformes *disent* quel est le droit associé à un objet
numérique -- par exemple un livre --, il faut prendre garde à ce que
permet la plateforme de distribution/diffusion de cet objet
[[#ecridil2018]{.underline}](https://twitter.com/hashtag/ecridil2018?src=hash)

je suis fan de cette définition du livre numérique de
[[@culturelibre]{.underline}](https://twitter.com/culturelibre) : un
livre numérique est un livre qui peut être acquis par une bibliothèque
(et c'est tout). si la plateforme qui permettrait cet accès n'existe
pas: créez-la!
[[#ecridil2018]{.underline}](https://twitter.com/hashtag/ecridil2018?src=hash)

### @Charbonneau_tweet

TWEET PAR @TOLEBRUN

> [[#ECRIDIL2018]{.underline}](https://twitter.com/hashtag/ECRIDIL2018?src=hash)
> Charbonneau "Réfléchissez sur quelle plateforme vous rendez
> disponible vos documents numériques, car certaines interdisent leur
> acquisition par les bibliothèques. Or un livre numérique, pour moi,
> c'est un livre qui peut être acquis par une bibliothèque avant tout"

### @Agostini_fiche

"L'Anthologie, compte plus de 100 auteurs, s'étend sur 16 siècles...
On doit la penser comme un réseau de résonnances de topoï (du Carpe Diem
à Brassens) inscrit dans la culture (souvent populaire) et l'histoire".

-

### @Agostini_fiche

"Défi technique : la plateforme de l'Anthologie palatine

Création de plateformes adaptées aux usages multiples et propices à une
dissémination des textes".

### @Paquienséguy_tweet

Image de la période de questions via twitter
([[https://twitter.com/hashtag/ecridil2018?f=tweets&vertical=default&src=hash)]{.underline}](https://twitter.com/hashtag/ecridil2018?f=tweets&vertical=default&src=hash))
avec Norwell et Paquienséguy. Peut-être accompagné du texte issu du pad
synthèse: «Le format du CD-ROM culturel réinvesti permettrait, peut-être
plus que les autres, de créer au profit de l'artiste.»

### @Drochon_citationdiapo10 #installation_collaborative :

"Longhand Publishers, une installation collaborative de création de
livres du

collectif de designers anversois Indianen, a invité les visiteurs à
concevoir

des pages pour des livres spéciaux.[...]. Dans les limites de la
machine, les visiteurs ont travaillé avec des formes, des motifs et des
lignes pour créer un design."

### @Fauchié_photo ou @Fauchié_slide

[[https://albums.photographie.loupbrun.ca/#15251447512345/15251447639122]{.underline}](https://albums.photographie.loupbrun.ca/#15251447512345/15251447639122)

« Git est un système conceptuel, des interfaces compréhensibles par le
commun des mortels permettent son utilisation. »

### @Marcoux_visuel (image du pwp p. 3 / texte du pwp p.29) #collectif. Image à cadrer à la p. 3 [image du b7]

«Écriture de guides pratiques pour utilisateurs des équipements
collectif ; Écriture de l'histoire de ces projets, composée de récits de
vie, de témoignages, d'expériences ; Partage des apprentissages tirés de
ces expériences, sous la forme d'essais, d'analyses critiques, d'études
sur les conditions de succès ; D'autres sujets de publication pertinents
(règles de vie, nouvelles, promotion, livres d'art).»

## Matérialité

### @Jahjah_PP

n°22

### @jahjah_tweet

Un échange de tweet entre MVR et F.Bon.

> @monterosato : «un auteur support. »
>
> **françois bon** @fbon
>
> @marc_jahjah @monterosato voire même : l'auteur en tant que tel comme
> nouvelle matérialité relais? (je plaisante pas tant que ça)

### @Tréhondart-fiche, @Tréhondart-tweet(mot-clé #design) :

Dérive du design éditorial, marketing car le but est, avant tout, de
mettre en valeur les œuvres".

### @Tadier_tweet

PAR JULIE BLANC
[[[https://twitter.com/Julie_McFly/status/991346420467863552]{.underline}](https://twitter.com/Julie_McFly/status/991346420467863552)]

> "Avec les livres numériques, le texte et son support ne vieillissent
> plus ensembles" Elsa Tadier
> [[#Ecridil2018]{.underline}](https://twitter.com/hashtag/Ecridil2018?src=hash)

###  @Tréhondar-diapo3 (mot-clé #matérialité)  @Tadier_Fiche

- Plus largement, en tant que lectrice, Tadier rappelle qu'elle
appréhende physiquement la matière lorsqu'elle tourne les pages
(pratique de la pause pour jauger le volume notamment, afin d'avoir une
perception de l'objet).

- Tadier insiste enfin sur le fait que le design se dissocie de
l'économie de marché ; la corporéité est un supplément d'âme, le
design doit dépasser ses conditions actuelles d'exercice pour
interroger d'un point de vue phénoménologique la matière. Ce
dépassement est nécessaire afin de réorganiser notre rapport aux
contenus, de saisir les vraies implications matérielles des objets que
l'on utilise afin que le rapport aux dispositifs numériques se place
d'un point de vue anthropologique et non plus seulement du point de vue
du "consommateur"

@carnetdlis_tweet

https://twitter.com/carnetdlis/status/991345879427796992

#ecridil2018 Elsa Tadier : relations du corps aux objets, à la
matérialité, construit un savoir #corpscommuniquant

###  @Fauchié_tweet

RT @Julie_McFly: "Avec les livres numériques, le texte et son support
ne vieillissent plus ensembles" Elsa Tadier #Ecridil2018

### @Tadier_Slide_4

Psautier et Rosaire de la Vierge (à partir du f. 27 puis) Litanies

###  @Tadier_PAD

œuvres historiques représentant des livres touchés -> inscription dans
la culture du rapport au livre du corps.
Le dispositif livresque nous renvoie à la nécessité anthropologique du
corps communiquant. Nous inscrit dans notre culture et nous renvoie à
notre condition d'humain

### @Drochon_captured'écrandiapo27 #livre  @Drochon_citationdiapo29 #dipositif

"Le choix du dispositif détermine l'expérience que le lecteur souhaite
faire

de la consultation de contenu. Le livre est un canal particulier de

l'expérience que nous souhaitons faire de ce contenu."

## Immersion

### @Epron_diapo11 #immersion

### @Laborderie_PDF :

Expérimentation en milieu scolaire, mise en œuvre de recherches sur le
web, l'entrée privilégiée dans le texte se fait par mode sonore.
Certains élèves ont véritablement surfé sur l'appli comme sur un site
web.

### @Paquienséguy_phototwitter

Image de la période de questions via twitter
(https://twitter.com/hashtag/ecridil2018?f=tweets&vertical=default&src=hash)
avec Norwell et Paquienséguy. Peut-être accompagné du texte issu du pad
synthèse: «*Le format du CD-ROM culturel réinvesti permettrait,
peut-être plus que les autres, de créer au profit de l'artiste.*»

## Parcours

### @Drochon_citationdiapo8 + @Drochon_captured'écrandiapo9 #expérience

"Faire l'expérience d'une lecture numérique c'est avant tout faire
l'expérience d'une dissociation".

### @Epron_fiche #parcours_de_lecture, #expérience :

"Les potentialités de parcours de lecture non contraints : organisation
des termes offre des potentialités libre bien que prévu par les liens
proposés entre les différents termes (papier = matéralité contraint le
parcours de lecture)"

### @Tréhondart-fiche :

- mot-clé #lecture_numérique : "La lecture numérique doit pouvoir
donner la possibilité de faire son choix librement"

## Typographie

### @Perret_fiche #typographie + @Perret_photo

https://albums.photographie.loupbrun.ca/#15252729030055/15252736759630

"Arthur Perret part du postulat que la typographie est tout ce qu'il
reste du papier et que c'est un point de passage entre l'imprimé et
l'écran."

### @Perret_tweet #typographie : [[ 991338605451862016]{.underline}](https://twitter.com/AnthonyMasure/status/991338605451862016) ?
