#Fiche synthèse : {B2} Françoise Paquienséguy, « Représenter la création artistique : les catalogues d'exposition numérique, comment aborder l’œuvre et son auteur ? »


###Hypothèse de travail de l'intervenant
*Le format du CD-ROM culturel réinvesti permettrait, peut-être plus que les autres, de créer au profit de l'artiste.*

###Positionnement théorique : auteur.s, courant.s de pensée, théorie.s, 
*F. Paquienséguy s'intéresse aux conditions de productions et aux logiques éditoriales, plus qu'aux contenus produits. Ces e-albums sont crées dans une logique d'accompagnement des grandes expositions. Ils sont à réfléchir dans la continuité des CD culturels des années 1990, dans un passage de l'hypermédia au transmédia.*

###Idées principales défendues
*Le multimédia devient transmédia. On met à disposition du lecteur des ressources et des outils, mais une forte logique de rentabilité (l'exemple de l'exposition de Monet en 2011, réinvestie et revendue en 2016). D'où vient l'innovation (peut-être du CD augmenté qui permet de créer au profit de l'artiste) ?*

###Méthodologie de travail
*Paquienséguy présente trois catégories d'albums complémentaires, ayant tous pour objectif de toucher tous les publics, de valoriser économiquement des expositions et des archives.*
1. Les expositions à domicile : proposent une visite salle par salle, le téléchargement d'audio-guides, un accès aux œuvres 3D, les commentaires sur les œuvres, un PDF téléchargeable. Ce format permet un ersatz, ou une préparation.
2. Les mini-catalogues: issus d'une sélection éditoriale, ressources produites par les commissaires, une reproduction HD des œuvres (on peut zoomer) avec textes (convocable par des bandeaux), exploite aussi des éléments vidéos. Ce format permet une approche contextualisée (en fonction de la discipline artistique), sert de complément (appel ou rappel) de l'exposition.
3. Le CD culturel réinvesti: mise sur les possibilité du numérique (affiche, navigation, autonomie du lecteur) pour augmenter la production de l'artiste (s'applique davantage aux artistes contemporains, ces choix éditoriaux étant plus loin de la certitude des commissaires). Ce format repose sur l'interactivité, l'hyper-navigation, la discontinuité.

###Corpus / objets et exemples présentés 
*Les e-albums de la Réunion des Musées Nationaux*
Le e-album de Hopper, par exemple, offre peu de tableaux, un accès égal, sans hiérarchie, des liens sur des ressources exogènes et peu de texte. Repose sur une expérience immersive.
Le e-album de Niki de Saint Phalle, intègre des séquences vidéos d'un documentaire biographique.
*5 lignes max*


###Références citées : publication scientifique mais aussi pages web
*présentées en liste*

###Vos remarques personnelles
*5 lignes max*

###Questions /remarques dans la salle 

Marcello demande :
    La question du iPad et de Android. Il semble que des acteurs institutionnels en ne se posent pas la quesiton des alternatives — on peut éviter les plateforme commerciales. Des pratiques se développent complétement véhiculée apr deux entreprises. C'est par ignorance et incompétence ou par choix? C'est un système de production, le musée publique devient des propriété intellectuelle de Apple ou Google. l'anlayse a telle permis de rendre compte de ces choix?

Nolwenn Tréhondar: 
    En toute connaissance de cause. Le responsable de la RMN ne veut pas de epub pour es questions de maquette. le système fermé propriétaire de iOS était mieux; permet de faire une mise en page, de masquer les modèles imposés qu'on trouve dans les ebooks. Une décision d'éditeur: pour les beaux livres ça semblait mieux. Deuxièmement, on veut faire de l'Argent: l'idée est de travailler à la chaine, essayer d'innover un petit peut à chaque fois Ils font des tests. Le problème de ces objets: ils ne se vendent pas. Une volonté de s'inscrire dans la rhétorique de Apple. 
Une question sur le copy fraud quand la RMN s'approprie des oeuvres demeure sans réponse...

