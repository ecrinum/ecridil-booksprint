#Fiche synthèse : Nolwenn Tréhondart, "Le design éditiorial des livres d'art numériques, au prisme de leurs pratiques de conception et de réception"


###Hypothèse de travail de l'intervenant

Nolwenn Tréhondar présente les résultats d'un projet de recherche qui vise à dresser un bilan du livre numérique en France. 
C'est un ouvrage collectif réalisé dans une démarche pluridisplinaire.


###Idées principales défendues
Plusieurs idées se sont dégagées : 
- La lecture numérique doit pouvoir donner la possibilité de faire son choix librement.
- L'évocation du risque de fragmentation des contenus et d'une perte d'unité.
- Le tiraillement du livre d'art entre la médiation savante (et raisonnée) et l'injonction à la modernité.


###Méthodologie de travail
Ils ont réalisé des entretiens individuels mais aussi des groupes de discussion dont l'analyse a permis de mieux cerner l'action de ces interprétants. 

Méthodologie en construction : Sémiotique sociale.
Le but est de circonscrire les éléments qui interviennent dans le processus d'interprétation. 


###Corpus / objets et exemples présentés 
Edward Hopper d'une fenêtre à l'autre : livre d'art numérique.


###Caractéristiques du livre d'art numérique 
Il permet une vision augmentée de l'art grâce à des images en haute résolution ainsi que la possibilité de zoomer sur les tableaux. 
Il renoue également avec une certaine sensualité livresque (mouvements, animations etc...). 

###Bilan du livre d'art numérique  : 
- Les lecteurs résistent quand même au spectaculaire. Il y a parfois des outils qui déçoivent l'attente du lecteur (comme par exemple les rectangles transparents cliquables sur un tableau qui donnent accès à des éléments biographiques sur le peintre, inattendus pour le lecteur). 
- Risque de fragmentation du contenu
- Dispositif onéreux
- Dérive du design éditorial, marketing : Ils ont avant tout le désir de mettre en valeur les oeuvres. 


###Références citées : publication scientifique mais aussi pages web
\url{https://www.grandpalais.fr/fr/article/dune-fenetre-lautre-lapplication-hopper-pour-lipad}

###

