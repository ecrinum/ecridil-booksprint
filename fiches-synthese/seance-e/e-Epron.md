#Fiche synthèses : Benoît Epron et Catherine Muller, "L'abécédaire des mondes lettrés, un outil d'écriture collaborative savante"

Benoît Epron et Catherine Muller présentent leur projet d'un nouvel objet éditorial numérique, "l'abécédaire des mondes lettrés" lancé en 2017. 
Ce projet répond à l'initiative  de Christian Jacob dans les « Mondes lettrés : fragments d’un abécédaire » publiés dans l’ouvrage, *Imaginaires des bibliothèques, *paru en 2012. 

Le livre imprimé devient alors un réel projet de design éditorial qui répond à un besoin d'outil d'écriture savante. 


###But du projet :
Répondre au besoin d'écriture collaborative et savante. 
Désir d'une expérience de lecture non contraignante.


###Qu'est-ce que "l'abécédaire des mondes lettrés" ?
C'est un dictionnaire inachevé que l'on peut augmenter et enrichir. 
Le numérique permet ainsi de bénéficier de la possibilité de l'écriture collaborative, de faire des liens entre les différentes entrées et d'insérer des hyperliens. 


###Problématiques scientifiques du projet :
- Les potentialités de parcours de lecture non contraints malgré un parcours prévu. 
- La granularité correspond à ce projet éditorial en particulier.


###Les choix de conception :
Choix d'un design sobre en noir et blanc.
Nous nous trouvons devant un univers visuel sphérique pour renvoyer à la vision humaniste d'un nouveau monde.
Des mots flottent dans la voute céleste.
Visualisation des données en 3D.
L'utilisateur se trouve au centre de l'univers et donc en pleine immersion. 
Les données sont réparties de manière non-orientées (alphabétique, corrélative ou associative).


###Bilan du projet :
Problématique d'écriture collaborative classique (validation, espace de commentaire, encodages différents).
Envisagent de créer un point d'entrée à partir des références bibliographiques à la fin des notices.


###Présentation d'HyperOtlet :
Veulent intégrer dans l'abécédaire une édition critique du "Traité de documentation" de Paul Otlet ce qui permettrait de mobiliser un réseau d'experts pour éclairer l'oeuvre. 
Cela pose la question de la granularité (ou comment découper le texte) mais permettrai la valorisation et la promotion de l'oeuvre. 


###Corpus / objets et exemples présentés 
Christian Jacob, « Mondes lettrés : fragments d’un abécédaire », *Imaginaires des bibliothèques (*2012).
HyperOtlet, édition critique du "traité de documentation" de Paul Otlet. 


###Références citées : publication scientifique mais aussi pages web
\url{http://abecedaire.enssib.fr/}

