#Fiche synthèse
# « Une plateforme pour le livre anthologique à l'époque numérique. Le cas de l'*Anthologie Palatine* »


###Hypothèse de travail de l'intervenant
Avoir pour projet une \_édition\_ l'Anthologie palatine, demande avant tout interroger la nature du corpus.

L'Anthologie n'est \_un\_ texte, ni n'est un objet clos. Ses racines sont obscures : on peut parler du \_Codex Palatinus 23 (Anthologia Palatina)\_ ou encore de l'\_Anthologie Grecque (Anthologia Graeca)\_. Plus largement, on parle de près de 2600 ans d'épigrammes, inscriptions et poèmes.

Visant à rendre compte, dans la forme du rendu, de la nature multiforme du corpus, la plateforme et l'édition du projet est elle-même multiples : travail collectif et processus, ouvert sur le temps et dans le monde ;


###Positionnement théorique : auteur.s, courant.s de pensée, théorie.s,
Pensée par l'équipe de la Chaire de recherche sur les Écritures numériques comme « projet », l'\_Anthologie\_ se veut une pratique/réflexion d'éditorialisation. En effet, on parle de dynamiques ouvertes et collectives, par des acteurs divers -- spécialistes ou amateurs.

###Idées principales défendues
L'\_Anthologie\_, compte plus de 100 auteurs, s'étend sur 16 siècles... On doit la penser comme un réseau de résonnances de \_topoï\_ (du \_Carpe Diem\_ à Brassens) inscrit dans la culture (souvent populaire) et l'histoire.

On ouvre donc le projet à tous -- des lycéens à la philologie universitaire pour réintégrer le corpus à la circulation dans l'imaginaire, aussi contemporain.


###Méthodologie de travail

On crée une API spécifique à l'Anthologie permettant de remplir les visées du projet -- les plus ouvertes possibles.

Travail avec Json -- ouverture aux usages multiples, tant savants qu'amateurs, voire ludiques.

Ouvert au grand public

On réinvestit les données sur toute sortes de plateformes, dont un \_bot\_ sur Twitter pour faire circuler le texte, son sens.


###Corpus / objets et exemples présentés
Cf. Méthodologie

###Défis techniques
Création de plateformes adaptées aux usages multiples et propices à une dissémination des textes. 

