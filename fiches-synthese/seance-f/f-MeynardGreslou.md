#*Cécile Meynard et Elisabeth Greslou, « Du manuscrit au numérique : une édition multiformes et multisupports de Stendhal » *



##Vos prises de notes

CF. plus bas

##Fiche synthèse


###Hypothèse de travail de l'intervenant

Travailler sur les lettres de Stendhal et en faire une édition multiformes adaptable à tous les supports et différentes selon les supports, dans l'optique d'une circulation et complémentarité. Le tout est fait dans un respect des volontés de mise en page de Stendhal et une adaptation de ses manuscrits "pseudo-diplomatique". Le lecteur est toujours encadré par une notice, ou un préambule qui lui présente la contextualisation et les choix éditoriaux de l'oeuvre à laquelle il a affaire. 

###Positionnement théorique : auteur.s, courant.s de pensée, théorie.s, 

Que devient l'auteur, que deviennent les textes de l'auteur ? Que devient le texte ?

###Idées principales défendues

Volonté de vouloir toucher tous les publics, possibilité pour chaque public de trouver la plateforme de lecture qui lui est la plus adaptée. Ainsi, il y a une perspective de mouvement, l'édition n'est pa figée dans un seul support. Édition qui se conforme aux standards mais en prenant des liberté stendhaliennes. Sur le site, utilisation du mot "publication" défendu par l'idée de "rendre public" les manuscrits mais aussi "rendre public" le travail des chercheurs. 


###Méthodologie de travail

Travailler dans l'optique d'une oeuvre multisupports à l'édition multiformes. Selon les publics, rajouts de compléments pédagogiques, certains fiches possèdent des complément multimédias. 
Plusieurs éléments techniques : 
1. Format XML "maison" avec l'ambition de tout repérer, tout annoter : ce matériau a permis de faire un site où à première vue, il s'Agit d'une transcription.
2. TEI, dans le but de générer des flux vers Indesign pour faire l'édition papier
3. Génération d'un epub équipé de multicompétences.

###Corpus / objets et exemples présentés 

Stendhal : lettres
\url{http://ecridil.ex-situ.info/corpus-du-colloque/manuscrits-de-stendhal-journaux-et-papiers-de-stendhal-publication-hybride}

###Défis techniques

Adaptation aux différents supports, complémentarités

###Conversations sur le pad collectif

Origines du projet : chantier de recherche, idée de l'exploitation, travail collectif. Un des premiers projets en humanités numériques car 2006. Ce n'est pas fini, recherche et expérimentation. Spécialistes dans différents domaines.  Les chercheurs sont des "Bricoleurs": ambition démuserée. 
Du coup, choix d'un corpus test (journaux et papiers) de Stendhal pour permettre des expériences (?) 
Chaine éditoriale multisupports  : Clélia  - 
Éléments techniques : 1er temps : Format XML maison au départ (pas de TEI) 
Ambition de tout repérer, tout annoter, ce matériau a permis de faire un site où on voit d'abord une transcription puis dans un 2e temps, en passant par la TEI, on a généré des flux vers InDesign pour faire des journaux et papiers. 
Ambition de tout annoter : site Internet 
2e temps, TEI : générer des flux vers Indesign pour l'édition papier 
3e temps : génération d'un epub équipe multicompétences  

Idée; avoir une circulation entre les différents supports et formats pour toucher un public divers  L'idée est d'avoir une circulation entre les différents supports et les différents formats adoptés (Avoir les manuscrits de Stendhal sur son iPhone).Les contenus ne sont pas identiques sur ces différents supports (site, epub...) 
Publications concernant le site car c'est une manière de rendre public à la fois les manuscrits, le travail de recherche [procédé assez nouveau en litt.], rajout de compléments pédagogiques, fiches avec compléments multimédias, travail de l'auteur dans la transcription pseudo-diplomatique
Articles et notes liés 
Contextualisation des manuscrits  
Perspective de mouvement, pas une édition figée  avec un apparat critique développé, aux normes de l'edition scientifique. Essayer de respecter la MEP de Stendhal — principes de colonnes, mots plus gros, etc. Grande quantité de notes, de notices, de préambules, d'introduction. Édition qui se conforme aux standards mais en prenant des liberté stendhaliennes

Format epub : idée de rester plus près du texte de Stendhal, toujours possibilité de retourner su le site epub : voulaient toucher un autre public - rester plus proche du texte de Stendhal — éliminer énormément d'apparat critique et de notes 
Repenser l'organisation : faire des renvois à l'intérieur de l'epub, deux types de sommaires, chronologique comme dans l'ouvrage et par corpus. Souci de complémentarité.  


3 aspects de lecture : 
#1.page de gauche -> manuscrit, page de droite : transcription pseudo diplomatique avec respect des ratures, fautes d'orthographe, disposition... 
#2 séparation entre les documents par des traits 
#3 epub : souci pédagogique accru, souci didactique souci d'accompagnement du lecteur, contextualisation du texte, un que sais-je.    

Ce genre de publication manque de circuit pour être présenté. Le lecteur a un rôle d'acteur. Que devient l'auteur, que deviennent les textes de l'auteur ? Que devient le texte ?  éditorialisation du texte de Stendhal

###Références citées : publication scientifique mais aussi pages web


###Vos remarques personnelles


###Questions /remarques dans la salle 

- question 1 : Q: (René Audet) : On a beaucoup parlé de bricolage. Comment peut se produire la montée en régime? Cette chaine peut elle devenir, un standard, être réutilisée, etc? Grands moyens, grandes compétences. est-ce qu'on n'est pas constamment repoussé dans notre position de chercheur car on n'a pas les moyens pour mettre dans le marché commercial ce type de recherches ... 
R: ingénieure de rcherche du CNRS, l'idée c'est de consolider des outils qui ne contraignent pas les projets, mais des outils qu'on peut adapter facilement en utilisant des standards comme la TEI. 

Benoit : pas du tout l'habitude de valoriser nos projets dans un sens commercial. manque d'interactions avec nos usagés. Comme on ne vend pas, on ne sait pas. Ne s'expriment pas. 

Potentialité du numérique qui nous permettrait d'aller plus loin mais on ne l'utilise pas car "Qui nous lit ?"



