#{F} Fabrice Marcoux : La co-édition grâce à Booktype au B7 : mise à l’épreuve du livre numérique


##Fiche synthèse


###Hypothèse de travail de l'intervenant

Comment se réapproprier un espace (projet du B7) et le documenter, avec quels outils?

Comment **créer** et **produire** des documents (livres) dans tous les **formats**? Sur une **base collaborative**?

###Positionnement théorique : auteur.s, courant.s de pensée, théorie.s, 
*5 lignes max*

###Idées principales défendues

Le B7 comme espace d'expérimentation sociale. Repenser le **travail collaboratif** via le chat, l'assistance et la collaboration.

**Design social**, **collaboratif**. Le Booktype fonctionne pour les auteurs individuels, mais aussi (voire surtout!) pour **plusieurs auteurs/co-auteurs**.

Le Booktype est notamment un outil utile lors des *book sprint.*

###Défis techniques

Interventions (gestes techniques) limitées (pas d'intervention sur les styles CSS par exemple).

##Notes collectives

Des outils collectifs qui seront à disposition du B7, espace collectif à Pointe-Ste-Charles.
Le Bâtiment 7 -- « Fabrique d'autonomie collective » 
En fait une zone d'expérience et d'innovation sociale -- lieu actif (naissance des CLSC, l'aide juridque et carrefour d'éducation populaire)
Lieu récupéré par la communauté
Transformer ce patrimoine industriel comme une zone d'expérimentation sociale
Pte St-Charles = Tradition de mobilisation 

Le but : appropriation des lieux par la communauté ; services à la communauté : épicerie, CPE, etc.

BOOKTYPE : sert à documenter les pratiques et faire des manuels didactiques pour l'usage des équipements. Utilisé par plusieurs autres organismes. MOOC à l'université de Lyon, rapport d'Amnesty International,. pour booksprint, notamment. 

Pensé comme un réseau :

   *     Groupes
       * Chats, commentaires
       * 

       * Le but est de travailler collectivement sur les projets.Il reste encore à installer sur les serveurs du B7
       * 
Espace de travail

   * Tableau de bord :
   * Gestion du processus collaboratif (groupes et status) 
   * Compliqué : modification du CSS (préétabli)
On ne peut pas changer les CSS, cela limite le travail de postproduction.

Avantage de booktype = logiciel libre 
Accessible sous floss manuals. On peut le faire en communauté...

Culture numérique et innovation sociale
On peut résumer la culture numérique comme une prise de conscience de notre rôle dans la société.
La création d'EPub avec la plateforme Booki : plusieurs usages et niveaux de facilité/difficulté
Un principe près de Wikipédia + un chat. 


Générer des revenus ? 

