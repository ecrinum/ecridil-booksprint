#{F} Antoine Fauchié : Git comme nouvel ingrédient des chaînes de publication


##Fiche synthèse


###Hypothèse de travail de l'intervenant

L'utilisation de Git n'est plus une question; c'est une **affirmation** (déjà à l’œuvre dans certaines chaînes éditoriales).

###Positionnement théorique : auteur.s, courant.s de pensée, théorie.s, 

La *numérotation* de fichiers est une méthode très limitée : on écrase les fichiers à chaque fois (étapes définitives). :(

###Idées principales défendues


       * Permettre une **visibilité globale** sur le projet
       * Porosité entre les **étapes**
       * Porosité entre les **métiers**
Il faut que les textes soient **réintégrés** dans l'environnement numérique / Git (qui fonctionne sur des **fichiers texte**!)

###Méthodologie de travail


       1. Créer un projet
       1. Recevoir un texte
       1. Retours / demandes de modification (designer, auteur, éditeur)
       1. Mettre en commun les modifications
       1. Générer formats
Utiliser Git dans la chaîne éditoriale, mais en faisant abstraction des commandes verbeuses *(i.e. éviter l'interface de ligne de commande)*. Intégrer des **interfaces graphiques** (Gitlab, GitHub, etc.) pour une utilisation plus conviviale, tout en utilisant Git (et ses concepts : commit, push, pull, merge, etc.) en arrière-plan.

###Corpus / objets et exemples présentés 

Simondon : l'homme est parmi les machines qui opèrent avec lui
L'homme peut et doit agir avec la machine, et ne plus être un simple opérateur.

Sophie Fetro : oeuvrer avec les mahcines numériques
Entretenir un rapport crétif aux machines, malgré que ces machines et leur structures productive l'empêchent.

Versionner David Demaree, *Git par la pratique* (git for humans)

###Défis techniques


###Références citées : publication scientifique mais aussi pages web

*Quire* : suite d'outils éditoriaux développée et utilisée dans la production de vrais livres par Getty Publications (@gettypubs).


##Notes

préambule : communication s'inscrit dans une chaine + gloable sur les chaine de publication et notamment des livres
publication modulaire : diff. com, écrits, mémoires

Git formalise des pratiques peu présente dans l'édition pourtant essentielles pour travailler avec des outils numériques

constats : gestion des fichiers : numérotation très limitée des fichiers
    Points faibles de gestion de projet: étapes définitives, difficilement réversible, manque de visibilité sur un projet, manque de porosité entre les étapes
manque de porosité entre les étapes de publication et entre les métiers (les humains)

Comment un système de gestion de version pourrait améliorer la fluidité ? 

étapes de la publication : 
    créer un projet
    recevoir un texte
    signaler des modifications
    mettre en commun des modifications (par dif. acteurs d'un projet)
    générer les formats
    
Versionner David Demaree, *Git par la pratique* (git for humans)
versioning permet de penser le processus d'évolution. + avoir son mot à dire sur le processus d'évolution.

le code et texte ont bcp de points communs
remarque : ils sont surtout de plus en plus mélangés

Commandes : init, add, branch/commit, fetch/diff/merge (affiner le travail collaboratif), push
le push permet aussi de déclencher des actions sur certaines branches, pourquoi pas générer un pdf, ou autre.

Utliser Git = utiliser un système conceptuel, mais avec des interfaces compréhensibles. 
Sur la forme j'utilise, peut-être par erreur, une présentation de texte blanc sur fond noir qui donne une impression de terminal.

On peut associer des interface au syst de commandes

Simondon : l'homme est parmi les machines qui opèrent avec lui
L'homme peut et doit agir avec la machine, et ne plus être un simple opérateur 

Sophie Fetro : oeuvrer avec les mahcines numériques
entretenir un rapport crétif aux machines : malgré que ces machines et leur structures productive l'empechent.

Réintégrer les textes : possibilité d'intervenir sur le texte 

Getty Publication : ont intégré un dispositif d'édition entièrement basé sur git.

*Période de questions*

