#JM Gascuel


##Fiche synthèse


###Hypothèse de travail de l'intervenant
Travaille aux éditions \_Googtime\_. Le livre se trouve dans une période charnière, au carrefour de deux moments : celui du papier et celui d'une transmédialité : un nouvel objet, connecté -- sans application qui font l'intermédiaire entre les objets connectés.

###Positionnement théorique : auteur.s, courant.s de pensée, théorie.s,
Une logique plus industrielle, \_marketing\_ ou commerciale, par rapport aux autres intervenants. Gascuel travaille avec des éditeurs, des boîtes avec des équipes qui cherchent l'objet-livre de demain. « On travaille sur des problématiques d'éditeurs -- financièes et non celles de chercheurs. »

###Idées principales défendues
Le livre sera transmédia -- et non multimédia, ou ne sera pas. Le livre transmédia est donc à penser comme le « chainon manquant » entre le livre papier et le livre \_hors-papier\_.

La recherche commerciale est plus rapide, performante, efficace que celles des universitaires : elle a un but et les gens y travaille activement... Il y a une recherche, mais l'orientation est commerciale -- on cherche à répondre aux besoins de consommateurs, leurs désirs.

Le livre connecté est un objet -- sa numéricité, son code n'est pas l'intérêt : l'expérience de « lecture est sensorielle » ; sens et sensorialité sont consubstantielles.

###Méthodologie de travail

Recherche avec l'équipe de Googtime pour la création de nouveaux médias, objets...


###Corpus / objets et exemples présentés
L'objet transmédia est quelque chose qui dépasse largement le spectre du livre traditionnel, papier. Il est un objet connecté au monde, aux objets et est plus un outil qu'un objet fermé. « Trans- », il dépasse les notions de clôtures traditionnelles. On trouve, parmi les prototypes de ces objets, des étiquettes de vin où sont reliées des recettes qui s'y accordent, Don Camillo, bouquin par lequel on accède à un film, récit pour enfants (\_Petit ours brun\_) qui se relie à des épisodes de séries jeunesse. Le livre transmédia, dans ces exemples, est une clé d'accès à un contenu numérique. C'est un gadget.

###Défis techniques
Trouver les moyens et outils existants qui permettent un détournement pour créer un nouvel objet.


###Questions /remarques dans la salle

Q : Peut-on trouver ces livres transmédia en bibliothèque ?
R : Présentement un essaie avec la médiathèque de l'USES pour aider les les jeunes à faire du livre numérique. Les bibliothèques disposent des objets physiques.

Q : Qu'en est-il des questions d'accès et de droits ?
R : On travaille sur des Blockchains, privé -- AtosOrigin
