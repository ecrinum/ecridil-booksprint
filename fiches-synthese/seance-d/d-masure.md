#Anthony Masure : Mutations du livre web

##Fiche synthèse

###Hypothèse de travail

**En transformant les savoirs (en leur donnant forme), le design les fait vraiment exister dans le monde.**

Quels rôles peuvent jouer les designers dans la transmission des savoirs à un public élargi?

Penser les conditions de mode de saisie des idées.

###Positionnement théorique : auteur.s, courant.s de pensée, théorie.s, 

**Modalités du design** en jeu ici :
    

       * **Design éditorial** : hiérarchie + structuration des contenus.
       * **Design graphique** : le langage visuel comme organisation sémantique.
       * **Design d'interfaces** : expertise de lecture à l'écran; sensibilité.
Interfaces : dimension de **sensibilité** ; rendre les contenus sensibles.

**Si le savoir n'existe pas sans transmission, il semble alors nécessaire de repenser les formes et formats des publications scientifiques.**


###Idées principales défendues

La pensée comme **mode de saisie** *(cf. Gottlob Frege)* et non comme mode de production de la pensée.

Format qui doit être **attrayant** (pour favoriser conditions de saisie).

Il faut penser le web *pour* les chercheurs. (cf. Tim Berners-Lee) Il faut refaire du web un **espace public**.

###Défis techniques

Rentabiliser un texte et favoriser la circulation du savoir.

Rendre le livre attrayant à tous; sensibiliser le milieu de la recherche à ces problématiques.

###Corpus

   * Essai *Design et humanités numériques* (2017)
   * *Le design des programmes, des façons de faire du numérique* Thèse de doctorat en esthétique, 2014, www.softPhD.com (avec site web complémentaire Esthétique des données \url{http://esthetique-des-donnees.editions-b42.com/#index)}
   * Revue *Back Office* \url{http://www.revue-backoffice.com/}

###Références citées

   * Frank Chimero, The Shape of Design (2012)
   * Tim Berners-Lee, physicien-chercheur au CERN, (co-)inventeur du web.
   * Brad Frost, *Atomic Design* (2015)
   * Antoine Fauchié, liste de livres web (2011-2017) \url{https://github.com/antoinentl/web-books-initiatives}
##Notes personnelles

soutDesign et humanités numériques : vers une convergence du livre et du web?sout

#Mutations du livre web

Le design comme sphère de recherche.

L'ePub est un mini-site web encapsulé. Fonctionne avec les **technologies web standard** (HTML, CSS, JS).

L'ePub est né de technologies web, mais a pris du retard sur le web (ironie!). Format «pauvre», limité.

Anthony Masure s'intéresse à l'ePub en **publication de recherche** (déjà établi pour autres).

Le web permet une interopérabilité entre les médias/plateformes; permet la pluralité des terminaux de lecture/consommation (téléphones, ordinateurs, etc.)

Pour une philosophie des techniques : le design des livres (structuration des contenus) comme nouvelle technique d'écritures.

Quelles sont les nouvelles possibilités permises par ces nouvelles techniques? (Parallèle : nouvelles techniques, plus spécifiquement **nouvelles techniques d'écriture**.)

Les types d'écrits web portent généralement sur eux-mêmes (i.e. web, design). Le web et le design écrivent justement sur eux-mêmes avec ces technologies. La plupart des publications web sont 

Les chercheurs sont souvent en manque de **littératie numérique**. Pourtant, le web est **né dans un contexte de recherche**, (co-)inventé par (entre autres) le physicien-chercheur **Tim Berners-Lee** alors qu'il travaillait au CERN.

Aujourd'hui, le design et le web ont pris une toute autre direction. **Dérives**. Le **web capitaliste**, le **web des plateformes** (et donc propriétarisation*, web fermé), le web de la captation (**captation des savoirs** à des fins capitalistes/marchandes).

Problème (triste) : les chercheurs ne sont pas lus — certains ne sont *jamais* lus! :'(

Une solution : **l'autopublication**. (Publications alternatives.) Certains chercheurs se sont tournés vers l'autopublication (publication sur leur blogue personnel).

Les chercheurs sont souvent enfermés dans leur format/forme de leurs contenus.

Anthony Masure, en designer graphique, note la gestion typographique parfois «hasardeuse», faite sans regard aux codes et aux pratiques pourtant bien établies dans les techniques de mise en page (histoire du livre, histoire de la typographie : plusieurs siècles de perfectionnement). Les qualités graphiques des objets de lecture reçoivent peut d'attention sur le plan de la forme et prennent peu en compte le retour des lecteurs (pour qui on écrit pourtant!). Le design d'interface du livre web est de qualité très variable.

Contextes multiples du web (du téléphone jusqu'au frigo, en passant par l'imprimante et la voiture, et bien sûr par l'ordinateur de bureau).

Quels rôles peuvent jouer les designers dans la transmission des savoirs à un public élargi?

Plusieurs modalités du design :
    

       * **Design éditorial** : hiérarchie + structuration des contenus.
       * **Design graphique** : le langage visuel comme organisation sémantique.
       * **Design d'interfaces** : expertise de lecture à l'écran; sensibilité.
La pensée comme mode de saisie (cf. Gottlob Frege) et non comme mode de production de la pensée. Problématique : penser les conditions de ces opérations de saisie. Le design est beaucoup plus que du simple «embellissement».

En transformant les savoirs (en leur donnant forme), le design les fait vraiment exister dans le monde.

Les chercheurs gagneraient à s'intéresser davantage à la forme.

Format qui doit être attrayant (pour favoriser conditions de saisie).

**Modèle économique** : vendre des accès, puis ouvrir les contenus en Creative Commons (CC) après 1 an de publication (donc après une certaine rentabilité du texte). Rentabiliser + ouvrir la connaissance à tous.

Il faut penser le web *pour* les chercheurs. (cf. Tim Berners-Lee) Il faut refaire du web un **espace public**.

Autorité distribuée : avenues technologiques (paramètres de contrôle de lecture par l'utilisateur, notion de **contrôle partagé** : fontes variables, longueur des lignes, etc.; contraintes éditoriales de base, mais autorité partagée avec l'utilisateur/lecteur).

Le web comme architecture de lecture; le livre comme architecture de lecture.
