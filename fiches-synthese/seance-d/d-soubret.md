#{D} Jean-Louis Soubret, « Design thinking et édition - point d'avancement »

##Idées principales défendues

**Transfert/exploitation** du processus propre au **design** pour améliorer la **gestion**.

##Méthodologie de travail

Application du *design thinking* à d'autres sphères que celle du management et du marketing. Le design thinking pour résoudre des problèmes en mode collaboratif (voire : co-création dans le milieu littéraire, de production des contenus, en recherche, etc.). 

##Corpus / objets et exemples présentés 

Titre : *Business Model Generation*
Auteurs : Alexander Osterwalder et Yves Pigneur
Date de publication : 2010
Éditeur : Wiley (Strategyzer Series)
URL : \url{https://strategyzer.com/books/business-model-generation/}

Titre : *This is Service Design Thinking*
Auteurs : Jakob Schneider et Marc Stickdorn
Date de publication : 2012
Éditeur : Wiley
URL : \url{http://thisisservicedesignthinking.com}

Titre : *Design et innovation dans la chaîne du livre. Écrire, éditer, lire à l'ère numérique*
Auteurs : Stéphane Vial et Marie-Julie Catoir-Brisson (dir.)
Date de publication : 2017
Éditeur : Presses universitaires de France
URL : \url{https://www.puf.com/content/Design\_et\_innovation\_dans\_la\_chaîne\_du\_livre}

###Notes personnelles

1970 : **comprendre** l'approche des designers, leur **parcours cognitif**.

Expliquer le processus de design aux chefs d'entreprise, aux non-designers.

Comment transformer idées du design (abstraites, voire complètement laides) pour les rendre accessibles, intelligibles aux non-designers?

*#structure

Le design est, par nature, **non structuré**. Comment lui donner *une sorte de structure*?

*#esthétique

Design et esthétique : utiliser le design à des fins esthétiques. Différence avec l'art : le design a une fonction. alors que l'art n'existe que pour être contemplé.

Design 2.0 : non spéculatif, alliance fond-forme (pas tout à fait arbitraire).

*#transfert

Le design pour améliorer le processus de gestion; **visée performative du design**. Transfert.

Les humanités numériques ne sont pas si éloignées du design.

Dialogue qui affirme la forme de l'ouvrage (un ouvrage qui assume ouvertement sa forme). Auparavant, erreur d'imposer une forme à n'importe quel fond (ex. templates inadéquats); pas nécessairement approprié. **Design approprié**.

Le livre papier comme « **point de contact** » (parmi d'autres) entre les auteurs.

*#exploitation

Le design thinking comme exploitation du design; instrumentalisation du design. J-L Soubret décrit cette instrumentalisation du design comme un mal nécessaire pour le monde de l'édition, pour rendre le design compréhensible aux non-designers (gens d'affaires, littéraires, investisseurs, autres acteurs du livre…)
