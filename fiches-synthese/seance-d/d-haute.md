#Fiche synthèse


###Hypothèse de travail de l'intervenant

La publication scientifique ne tient pas en compte les aspects graphiques et sensibles. On observe notamment :
- Présupposé d'une idéalité du sens face à la forme. 
- pb économique (comment maintenir les revues, personnel...)
- propositions clé en main ont entraîné un formatage technique des revues mises dans des tp hypernormés, ce qui facilite production et consultation, mais au dépens du design. Une même maquette pour des propositions très différentes, comme si le contenu pouvait se passer de sa forme, qu'une forme pouvait se prêter à de multiples contenus divers et variés. 

Qu'est-ce que serait une forme de publication pour une recherche en art et création, et non pas sur l'art et la création ? 
Tentative de créer une revue en recherche à la fois sur/avec/en création 

###Positionnement théorique : auteur.s, courant.s de pensée, théorie.s, 

Approche de design : replacer le design au coeur de la réflexion sur la construction d'une revue savante en recherche-création, là où la construction des revues a souvent été logocentrée. 

###Idées principales défendues

Projet de montage de revue (Chaire Art et sciences Ensad + polytech) qui réponde aux enjeux suivants :
    - multimodale 
    - modulable 
    - openscience / opentech
    - multisupport (smartwatch + POD)
    - paramétrique (déclinaison des contenus en fonction des environnements de lecture) 


###Méthodologie de travail
Il s'agit de façonner un design des publications scientifiques multisupport, prenant en compte une éditorialisation de + en + forte notamment avec l'insertion de différents médias. 

###Corpus / objets et exemples présentés 

Revue design 

###Défis techniques

* Iconographie 
- quelle valeur ? revaloriser l'image 
- Latour et les "inscription" (éléments graphiques hétérogènes = comme matérialisations et vues de l'esprit) 

* Ajouter des médias un peu inhabituels pour encourager une autre médiation de l'article ou du contenu savant. 

* Embrasser tous les supports ;
- web / epub / 

* Usages : quels nouveaux usages inventer pour des supports "extrêmes" comme l'a smartwatch 

* Chaînes de publication 
actuellement les chaînes tentent de mettre en place une communication fluide en mettant en place un "single source publishing", soit un workflow basé sur le balisage du contenu avec ensuite visualisation distincte. Pb : ce modèle est surtout faitpour le textuel, pas de multimédia. Adoption de gabarits qui finit par être prescriptif. 
Le papier n'est pas encore très bien intégré dans cette chaîne. 
Standards ouvert : html / CSS / javascript / PHP 
CSS print : peut gérer l'aspect d'une page web lors de l'impression. Pb : outils payants. 
Pagedmedia tente de développer de outils opensource.


###Conversations sur le pad collectif


###Références citées : publication scientifique mais aussi pages web
*présentées en liste*

###Vos remarques personnelles

Importance de l'approche pro-image et pro-médias. 

Critique salutaire des solutions clé en main proposées par des éditeurs/diffuseur importants dans le monde du libre accès (loden, SPIP). Importance de trouver une identité éditoriale aussi dans le design.  

###Questions /remarques dans la salle 

Constat (!) : Pour rebondir là-dessus... le numérique doit prendre en compte le triangle contenu-designer-informaticien

   * R (Masure): Dès lors que le designer met en forme du contenu, il devient aussi co-auteur mais le logiciel est également un agent éditorial, ça fait complètement éclater les frontières. 
   * R (Blanc) : Le logiciel a une influence sur la forme si on utilise que des logiciels qui sont propriétaires contrairement à l'utilisation du web. 
   * R (Masure) : Un travail d'amélioration en terme de littératie numérique pour les chercheurs.  (opposition complète entre le format  et le contexte (?) ex : une thèse sur la bd numérique uniquement disponible sur papier.
   * R (Haute) : Les chercheurs sont évalués sur les articles, les publications et si on veut innover, différente du « long paper », il faudrait que ces formes puissent être reconnues comme résultat de recherche. Il faut des contextes éditoriaux nouveaux, qui évalue pareillement, mais s'ouvre à de nouveaux contenus. Il faut de nouvelles plateformes, il faut les inventer ou encore accorder à celles existantes (ex : revue BleuOrange), un statut légitime. 
   * R (Masure) : Aussi, reconnaissance des blogs comme pratiques de recherche. Le peer review est l'Alpha et l'Oméga ! 