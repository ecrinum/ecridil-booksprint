#**Nathalie Lacelle, Marie-Christine Beaudry et Prune Lieutier, « Projet de soutien au développement de démarches d’édition numérique jeunesse au Québec»**

Contexte : Un projet « soutien de dvl… qc.» financé, suite à un appel a projet qui porte sur la culture et le numérique. L'équipe fait aprtie de la Chaire de recherche sur la littéracie intermodale, s'intéresse à la disponibiloté d'oeuvre numérique pour enfant (on muni les enfant de tablette, que mettre dessus?)
Offrir des oeuvres de qualité correspondant aux compétences qu'on demande de mobiliser. Chercheurs didactique, art, lecture, écriture, Perspective plurisémiotique et processus cognitifs mis en oeuvre avec les nouvelles formes de textualités issues du numérique.
Il faut : S'intéresser à l'émergence de nouvelles formes. Le livren umérique exige une utilisation pertinente des particularités du numérique. *Penser l'expérience spécifique au numérique comme faisant partie des critères de littérarité pouvant qualifier les oeuvres.* 
Conséquence:  Reconfiguration de la place des acteurs du monde éditorial. 
Contexte : Beaucoup de livre numérique. Peu pour les enfants. Faire vivre une vraie expérience : pourtant, décus par l'offre québécoise. Développer une offre culturelle numérique transmédiale au québec. 
Mais :  Reste marginal (lectorat restreint). Difficultés des enfants à comprendre le lien entre l'histoire et le geste de navigation, comment releir les deux.
Volonté et questions: Créer des synergies entre les acteurs de L'édition et créer de l'expertise dans le domaine de l'édition jeunesse. Deux axes : comment soutenir transofmration, transition En faisant des contenu de qualité? Comment assurer sa découvrabilité? (pourquoi personne n'en a parlé au colloque? au coeur des préoccupations). 
Méthode et objectif: Documenter les enjeux, les besoins, les meilleures pratiques de diffusion et de réception, pour co-construire un outil de référence pour développer un plan pour le projet et chacun de ses acteurs participants.
Se rendre de la conception jusqu'à l'élève. 
Partenaires: Collaboration entre le milieu pratique et les chercheurs. Des gens de tous les médias, des cherchers, didactique, langue, langage.
Hybridité et multimodalité: croisement conceptuel pour mieux comprendre les objets. 
Développement: 1. Mettre sur pied le livre blanc de l'édition numérique jeunesse au québec.

       1. Accompagner les intervenants dans le développement de leurs projets. Les guider dans leur manière de penser le projet.Outil et méthodologie: collecte de données, pour soutenir le dévelopement de l'expertise : 

       1. Documenter la production, la diffusion, la réception. Recherche Action/Formation imposé par le frqsc : (enquêtes en ligne auprès des éditeurs des enseignants, des bibliothécaires. Entrevues individuelles et de groupe. Recension écrite aussi.) 
       1. Le projet: le livre blanc sera évolutif et numérique.
       1. Plan d'action avec les éditeurs. Entrevues individuelles. Séances de travail. Résultats:  Donner les bons réflexes, poser un cadre. 
Les éditeurs veulent avoir des données sur le marché. Peu de données sur les modes d'achats des livres numériques. 
Outils pratiques, listes de contacts, de concours. 
Informations sur la technologue à surveiller. Comprendre aujourd'hui avant d'explorer demain. 
Vrai besoin d'accompagnement des acteurs de la chaine de production. S'accoutumer au numérique. Changement de culture (commande aux auteurs). Besoin de soutien technique. besoin d'un chef technique. Anticiper les usages futurs. 
Financement peu accessible et peu adapté. Rentre dans peu de cases. Soutiennent peu l'expérimentation à l'usage pédagogique. Mettre en place des mécaniques de revenus, qui ne seraient pas là si le financement avait été adapté. 
Défi de compreéhension et d'arrimage entre les secteurs. Chronologie de travail : respecter les temps d'itération de chacun. 
On remarque le manque des « traducteurs » entre les maillons (pratique et recherche, comprendre les deux mondes). Comment on traduit ça en séquence numérique. Transformer le didacticte en numérique. 
Grande variété des équipements et intérêt et formation des enseignants.

##**Fiche synthèse **

**Hypothèse de travail de l'intervenant**
*Dans le contexte d'un appel de projet sur la culture et le numérique, l'équipe de la Chaire de recherche sur la littéracie numérique s'intéresse à soutenir l'offre d'oeuvres de qualités de littérature jeunesse au Québec, en partant du constat que l'offre est réduite, inadéquate.*
###
**Positionnement théorique : auteur.s, courant.s de pensée, théorie.s, **
*L'équipe du projet est multidisciplinaire (didactique, art, littérature, langue et écriture) et vise à développer une offre d'oeuvres québécoises transmédiale. Pour se faire, l'équipe veut créer un livre blanc de l'édition numérique jeunesse au Québec et documenter les enjeux, les besoins et les pratiques de diffusion et de réception pour co-construire un outil de référence et accompagner les intervenants.*
###
**Idées principales défendues**
*Il faut penser l'expérience spécifique au numérique comme faisant partie des critèes de littérarité pouvant qualifier les oeuvres [par oppositiona vec la définition française du livre numérique donnée en début de colloque]. L'équipe soutient qu'il faille créer de l'expertise et des synergies entre les acteurs du milieu du livre jeunesse en encourageant la qualité du contenu et la découvrabilité des oeuvres.*
###
**Méthodologie de travail**
*D'abord, une collecte de donnée sur le terrain a permis de documenter la production, la diffusion et la réception, il s'agit d'un format imposé par l'organisme subventionnaire (FRQSC). Ensuite, la création d'un livre blanc évolutif et numérique sollicitera le travail de chercheurs et de praticiens du numérique. Finalement, l'équipe élaborera des plans d'action avec les éditeurs.*
###
**Défis techniques**
*Les résultats de la première phase de recherche ont démontrés un besoin significatif chez les éditeurs de soutien technique et financier. Aussi, un défi de compréhension et d'arrimage entre les secteurs (financiers, entreuprenariaux), mais également un manque de «traducteur» entre les maillons de la pratique et de la recherche.*
###
**Références citées : publication scientifique mais aussi pages web**
*présentées en liste*

###**Questions /remarques dans la salle **
Q.MVR : cométences techniques des enfants et littératie digitale à développer. Les outils formatent ce que les enfants apprennent — gérer l'apprentissage du numérique et formatage par les plateformes. Workflow produit par les acteurs commerciaux. 
R. Deux temps : aussi recours au papier. principe de l'écriure appelle une appropriation du moyen. Compétence technique à acquérir pour l'écriture enchâssée. Recours externe pour structurer, une préécriture. 
MVR : on ne réfléchit même pas à pourquoi on utilise word
R: c'est le seul éditeur dans l'oeil des auteurs.
R: délinéarisation semble poser problème. Multimodalité difficile, les enfants sont privés d'une partie de l'hisoire et ont du mal à cheminer dans le récit. Qu'est-ce qui peut être une expérience enrichissante? 
Certains producteurs veulent mettre beaucoup d'interactivité même si ça ne sert pas l'histoire. Ex : le souffle, les enfants crachent. Méconnaissance de l'usage des enfants des ipads. Regard d'adulte, décalage avec les enfants. les tout petits doivent être accompagnés. Ados, développent des pratiques formatées ne correspondant pas à un usage adapté. 
Q. Utilisation du kit, intégrée dans le format? auteurs formés à l'interface, qui sont les auteurs?
R. Ils sont formés, formation de prise en main et tutoriel en ligne par adrénalivres. formation 2h, sur place ou par skype. Suivi avec slack. Jeu de complémentarité et d'accompagnement. Limites : ne vivent pas de l'écriture, écrivent le soir alors que personne n'est à l'entreprise pour leur répondre. Tous les profils d'auteurs. Recherche de l'autoédition. l'application peut donner lieu à une publication hors adrénalivre. le contenu peut être créé sans être éditorialisé. Ces auteurs-là ne sont pas formés. Obligation de rendre son livre sur une forme logicielle. On les incite à ne pas utiliser word et à tout de suite travailler.

###
