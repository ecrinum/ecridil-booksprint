#Protocole éditorial pour les fiches synthèses

##Principes 

   * Une prise de note est effectuée de manière collaborative pendant le colloque. 
   * La fiche synthèse est produite pendant et après la communication et peut s'appuyer sur la prise de note collaborative.
   * Les synthèses doivent être relativement bien rédigées (on n'est plus dans la simple prise de note)
   * L'objectif des synthèses sera de produire une grille de lecture de l'ensemble du colloque. Cette grille de lecture servira la seconde phase d'édition.
   * Tous les champs ne doivent pas nécessairement être remplis - parfois ils ne seront pas pertinents - c'est plutôt à titre indicatif
##Vos prises de notes

objet design est envisqer q laspect production , ps recpetion
relqtion de cocretion dans les dispositifs nu,m
projet de recherhe et developppement
pr=sentqtion dun projet de devloppement interdisciplinaire = livres interqctifs num = le studio adrenqlivre
fictions interqctives avec un qsect s=riel = livre dont vous etes le heors 
associer a un lqb
commqnde de lentreprise= opti,iser un kit de redqction
outil qui permet qux auteurs une ecriture= structurer schemas interactifs
contraintes de ecriture= travqiller sur des series, temps de lecture particulier, plus de choix dans la fiction mieux cela est;

recits enchqnces qrborescence logique mqthemqtiaue progrqatique

commande est dqmliorer loutil de creqtion en plqce pqr lq co,pqgnie

idee est pqs juste de donner des cdes ,qis reconfigurer une pratiaue sociqe= ecriture ; aller a la source

ils ont mis en plqce un protocole; phase dentretien, phase dobservation 
pour voir si lectirure peut etre un kit et en kit



##Fiche synthèse


###Hypothèse de travail de l'intervenant

Florence Rio s’intéresse au design au niveau de la production et non de la réception

###Positionnement théorique : auteur.s, courant.s de pensée, théorie.s, 
*5 lignes max*

###Idées principales défendues
*5 lignes max par idée défendue*
**Présentation de Florence Rio**
 
Ils ont reçu une commande de l’entreprise Adrénalivre (qui produit des applications de fictions interactives sous la forme de séries de livres dont vous êtes le héros)
 
L’objectif est de créer un kit d’écriture bien adapté pour les auteurs 
 
Hypothèse : Nécessité d’aller à la source de l’écriture, à la matérialité textuelle afin de créer un nouveau modèle littéraire adapté aux besoins des auteurs et de l’entreprise.
 
Ils ont observé les manques de l’outil de création twine (qui était l’outil proposé jusque là par Adrénalivre)
 
Ils ont observé les alternatives que les auteurs ont trouvées pour combler le manque de l’outil Twine : utilisation de word, de papier avec des structures arborescentes, cartes ou grilles, autres logiciels, etc.
 
Pistes à développer pour la création du kit : instrumentaliser l’écriture, celle-ci doit être envisagée comme un espace de reénonciation, de renégociation des rôles afin d’automatiser au final une nouvelle forme d’écriture
 
###Méthodologie de travail
*5 lignes max* 

###Corpus / objets et exemples présentés 
*5 lignes max*

###Défis techniques
*5 lignes max*

###Conversations sur le pad collectif


###Références citées : publication scientifique mais aussi pages web
*présentées en liste*

###Vos remarques personnelles
*5 lignes max*

###Questions /remarques dans la salle 
 
Questions :
Compétences techniques des auteurs et leurs liens les outils numériques à leur disposition
 
Réponse : En général, la majorité des auteurs ont recours à word, au papier, etc.  puisque cela est plus facile que de se réapproprier un nouvel outil; les rares qui n’utilisent que Twine ne l’exploite pas complètement
 
2 Question sur les auteurs d’Adrénalivre : 
Réponse : Auteurs sont formés chez Adrénalivre et ils ont aussi un tutoriel en ligne. Ils proviennent d’un éventail large de profils (profils expérimentés ou apprentis). Ils n’ont pas d’obligation d’utiliser le logiciel durant le processus de création, mais le produit final doit être sur Twine.
