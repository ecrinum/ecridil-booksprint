#Christine Develotte et Mabrouka El Hachani, «* L’alliance du papier et de la tablette numérique : les spécificités d’une nouvelle écriture de fiction jeunesse » * 

##

##Vos prises de notes

CF. plus bas

##Fiche synthèse


###Hypothèse de travail de l'intervenant

Comparaison et présentation de deux livres/applications pour enfants, format application, avec des fonctions et contraintes différentes. Le but est le même : celui de développer l'autonomie de l'enfant, de le plonger dans un contexte éducatif ludique. 
À propos de la structure narrative :
- Raconte moi le vent : récit de voyage, pages désignées par il ou elle, raconté et dessiné
- La grande histoire du petit trait : enfant amené à vivre une expérience


###Fonctionnement des applications 

Multimédias, audio, vidéo, textes, images. Progressions différentes et enjeux de lecture variés. 
Spécificités du mode de lecture : *raconte moi le vent* : besoin de l'aide de l'adulte
Pour *Grande histoire du petit trait *: progression, gestes simples, l'enfant peut lire seul

###Idées principales défendues

Univers narratif complexe mis en contexte éducatif dans le but de susciter des interactions adulte enfant. Le jeu renforce l'implication de l'enfant dans l'histoire.


###Méthodologie de travail

articulation du livre et de l'application (temporelle, spatiale, narrative)
spécificité d'exploration des lectures (postures corporelles, gestes prescrits)
articulation du livre et de l'application telle que voulu par l'auteur

###Corpus / objets et exemples présentés 

Raconte-moi le vent
La grande histoire du petit trait.

###Défis techniques

Applications qui fonctionnent de manière différentes : pour *Raconte-moi le vent*, contrainte technique : il faut que l'enfant tende les bras pour enclencher l'animation de l'image. Pour *La grande histoire du petit trait* : beaucoup plus flexible. Le corps se rapproche de l'écran à chaque action.


###Conversations sur le pad collectif

###
Univers narratif complexe mis en  contexte éducatif.
Grilles de résultats : spécificités d'écriture du livre, de l'application. Sur le niveau de la multimodalité, mais aussi rapport corps et écran
Susciter des interactions adulte enfant. 
multimodialité, rapports corps-écran
articulation du livre et de l'application (temporelle, spatiale, narrative)
spécificité d'exploration des lectures (postures corporelles, gestes prescrits)
articulation du livre et de l'application telle que voulu par l'auteur


Spécificités du livre : 
    Livre 1 : Raconte moi le vent
    texte au lexique un peu recherche, monde de l'enfance, codage visuel, discours des personnages

Livre 2 :
La grande histoire du petit trait : très épuré, prédominance de l'espace blanc

Rapport de type transmédia
Pour passer du livre physique au livre numérique : indicateur
Plonger dans d'autres espaces

Structure narrative  Raconte moi le vent : récit de voyage, psges désignés par il ou elle, Raconté et dessiné
La grande histoire du petit trait : enfant amené à vivre une expérience

Jeu renforce l'implication de l'enfant dans l'histoire

Spécificités d'écriture de l'appli ; 
Raconte moi le vent : on a l'audio + image qui s'anime
Signes passeurs, mais le geste à effectuer est le même

La grande histoire du petit trait : audio, animation de dessin, incitation à dessiner ou à gommer (l'enfant fait quelaue chose sur la tablette)
progression de la difficulté. 

Raconte moi le vent : contrainte technique : il faut que l'enfant tende les bras pour enclencher l'animation de l'image
Grande histoire du petit trait : beaucoup plus flexible. Le corps se rapproche de l'écran à chaque action.

4 tableaux interactifs pour Raconte moi le vent

Deux supports indépendants

Spécificités du mode de lecture : raconte oi le vent : besoin de l'aide de l'adulte
Pour Grande histoire du petit trait : progression, gestes simples, l'enfant peut lire seul

Appropriation de l'offre éditoriale - partenariat entre les éditeurs et les bibliothèques de Lyon.
Inviter l'enfant à interagir, puis Temps libre laissé à l'enfant pour qu'il puisse explorer de lui-même



###Références citées : publication scientifique mais aussi pages web
*présentées en liste*

###Vos remarques personnelles
*5 lignes max*

###Questions /remarques dans la salle 

Discussions : 
    - qu'est-ce qu'un livre numérique ? Comment est-il amené à se transformer ?
    - lien entre narratologie et formes ludiques

être plus ou moins ouvert du côté de l'usager - différents types d'ouverture. 
(Raconte moi le vent : mode clos) - (Grande histoire : monde ouvert)

Emergence de nouveaux metiers et compétences. 
Transmission, médiatoin.
Réfléchir sur la question de l'offre destinée au public.
Epistémologie pluraliste : inté

Question d'une épistémologie pluraliste qui intègre plusieurs acteurs dans une réflexion sur la compréhension du monde

