// config
var inputFile = './book.html';
var outputFile = './book.proc.html';

var cheerio = require('cheerio');
var fs = require('fs');

fs.readFile(inputFile, 'utf8', function (err, data) {
    if (err) {
        throw  err;
    }

    // create a new instance of cheerio
    var $ = cheerio.load(data);

    // select `section` elements with class level2
    // and replace them with `div` tags
    $('section.level2').each(function () {
        var innerHtml = $(this).html();
        var className = $(this).attr('class');
        var id = $(this).attr('id');

        $(this).replaceWith('<div id="' + id + '" class="' + className + '">' + innerHtml + '</div>');
    });

    $('section.level3').each(function () {
        var innerHtml = $(this).html();
        var className = $(this).attr('class');
        var id = $(this).attr('id');

        $(this).replaceWith('<div id="' + id + '" class="' + className + '">' + innerHtml + '</div>');
    })

    $('section.level4').each(function () {        
        var innerHtml = $(this).html();
        var className = $(this).attr('class');
        var id = $(this).attr('id');

        $(this).replaceWith('<div id="' + id + '" class="' + className + '">' + innerHtml + '</div>');
    })

    fs.writeFile(outputFile, $.html(), function (err) {
        if (err) {
            throw err;
        } else {
            console.log ('Wrote to file ' + outputFile);
        }
    })
});
