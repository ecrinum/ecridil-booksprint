# Couverture {#couv}

v0

# FrontMatter {.frontmatter}

Contenu du front matter, on pourra mettre le préambule, notice et autre.

# Forme et design {.chapter}
[Forme et design]{.shorter}

## Frag1 {.fragment}

### m {.main .main-citation}

> Onec tincidunt, odio vel vestibulum sollicitudin, _nibh dolor tempor sapien_, ac laoreet sem felis ut purus. Morbi cursus bibendum consectetur.

[Pierre Paul]{.source}

### g {.glose .glose-slide}

![Pierre Paul](img/slide-01.jpg)

## Frag2 {.fragment}

### m {.main .main-slide}
![Julie Blanc, Lucile Haute](img/slide-02.png)

### g {.glose .glose-citation}
> Ceci est une belle citation, il est possible de la _mettre en forme_.

[Pierre Paul]{.source}

## Frag3 {.fragment}

### m {.main .main-citation-ext}

Nulla gravida et nibh in elementum. Praesent iaculis eu erat eget sagittis. Curabitur ut neque id nulla dignissim vehicula eu in augue. Donec pulvinar, velit eu cursus rhoncus, mauris turpis venenatis leo, id malesuada tortor enim nec purus.

[Tim Ingold, _Une brève histoire des lignes_, 2010]{.citation-auteur}

[Jahjah et Jacquot]{.source}

### g {.glose .glose-biblio}

[Tim Ingold]{.biblio-auteur}, [Une brève histoire des lignes]{.biblio-titre}, [Bruxelles : Zones sensibles, 2008]{.biblio-autre}

## Frag4

oulipo Vestibulum velit massa, rhoncus non congue vitae, hendrerit eu metus. Donec lorem nisi, rutrum quis erat id, egestas iaculis ex. Pellentesque lacinia blandit dignissim. Nam at lorem finibus, facilisis orci eu, pellentesque enim. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Fusce fermentum lorem quis finibus mattis. Vestibulum in ipsum cursus, mattis elit ut, sodales libero. Sed blandit iaculis ex, sit amet volutpat massa fringilla sed. Proin porta enim justo, sed hendrerit leo efficitur sit amet. Maecenas ultrices nunc ut mi convallis vestibulum. Morbi imperdiet blandit malesuada. Nulla rhoncus non turpis non gravida^[Nulla rhoncus non turpis non gravida. Quisque cursus turpis urna, ac auctor nunc ullamcorper id.]. Quisque cursus turpis urna, ac auctor nunc ullamcorper id.

Mauris at ligula id risus interdum cursus ut at massa. Fusce ut augue quis lacus maximus placerat at in quam. Ut id sem sed ipsum lobortis laoreet. Sed dictum sodales nisi. Fusce lacus nulla, maximus quis malesuada egestas, egestas ut nulla. Nulla id ultrices felis. Proin molestie quam nec varius vulputate. Nullam luctus sapien massa. Nullam ac dignissim leo. Cras at neque sed neque faucibus ultrices a quis dolor. Donec vestibulum elit sed augue porta, et imperdiet velit sodales. Suspendisse erat nibh, lacinia at varius sed, tempus vitae diam. Suspendisse potenti[^note].

[^note]: Suspendisse erat nibh, lacinia at varius sed, tempus vitae diam. Suspendisse potenti.

Praesent velit massa, placerat in risus vitae, commodo tempor lectus. Donec eget accumsan nisl, gravida ornare dolor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam sed leo vel metus vulputate lobortis. Cras ultricies sagittis metus, nec feugiat turpis rutrum iaculis. Nunc fermentum tristique erat et aliquam. Vivamus ut sagittis quam. Vestibulum fringilla iaculis euismod.

## Frag5

### m {.main .main-texte}

Integer ipsum felis, tincidunt eu laoreet et, cursus ac quam. Nulla gravida et nibh in elementum. Praesent iaculis eu erat eget sagittis. Curabitur ut neque id nulla dignissim vehicula eu in augue. Donec pulvinar, velit eu cursus rhoncus, mauris turpis venenatis leo, id malesuada tortor enim nec purus. Donec tincidunt, odio vel vestibulum sollicitudin, nibh dolor tempor sapien, ac laoreet sem felis ut purus. Morbi cursus bibendum consectetur. Nullam vel lacus congue nibh pulvinar maximus sit amet eu risus. Curabitur semper odio mauris, nec imperdiet velit pharetra non.

### g {.glose glose-slide}

![Pierre Paul](img/slide-01.jpg)

## Frag6

### m {.main .main-texte}

Nullam vel lacus congue nibh pulvinar maximus sit amet eu risus. Curabitur semper odio mauris, nec imperdiet velit pharetra non. Aenean accumsan nulla ac ex iaculis interdum. Curabitur hendrerit lacinia arcu non consequat. Nulla ornare, nibh id fermentum pharetra, dolor ante laoreet ligula, id sodales nunc leo ut libero. Nam ut ipsum sit amet nunc dignissim porta vel sagittis nunc. Vestibulum convallis fringilla ante sit amet ultrices. Vestibulum neque ex, ullamcorper sit amet diam sed, pharetra laoreet sem.

### g {.glose .glose-texte}

Aenean accumsan nulla ac ex iaculis interdum. Curabitur hendrerit lacinia arcu non consequat. Nulla ornare, nibh id fermentum pharetra, dolor ante laoreet ligula, id sodales nunc leo ut libero. Nam ut ipsum sit amet nunc dignissim porta vel sagittis nunc. Vestibulum convallis fringilla ante sit amet ultrices. Vestibulum neque ex, ullamcorper sit amet diam sed, pharetra laoreet sem.


## Frag1 {.fragment}

### m {.main .main-citation}

> Onec tincidunt, odio vel vestibulum sollicitudin, _nibh dolor tempor sapien_, ac laoreet sem felis ut purus. Morbi cursus bibendum consectetur.

[Pierre Paul]{.source}

### g {.glose}

#### g {.glose-slide}

![Pierre Paul](img/slide-01.jpg)

#### g {.glose-biblio}

[Tim Ingold]{.biblio-auteur}, [Une brève histoire des lignes]{.biblio-titre}, [Bruxelles : Zones sensibles, 2008]{.biblio-autre}


## Frag7

### m {.main .main-slide}

![Julie Blanc, Lucile Haute](slide-02.png)

### g {.glose .glose-texte}

Aenean accumsan nulla ac ex iaculis interdum. Curabitur hendrerit lacinia arcu non consequat. Nulla ornare, nibh id fermentum pharetra, dolor ante laoreet ligula, id sodales nunc leo ut libero. Nam ut ipsum sit amet nunc dignissim porta vel sagittis nunc. Vestibulum convallis fringilla ante sit amet ultrices. Vestibulum neque ex, ullamcorper sit amet diam sed, pharetra laoreet sem.

# --

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam malesuada lacus vehicula massa consequat, maximus faucibus libero laoreet. Nullam maximus, diam consectetur tincidunt gravida, ligula orci venenatis dolor, facilisis dignissim tellus libero vel turpis. Proin in elit mattis, egestas sapien eu, hendrerit est. Pellentesque aliquet dui non lorem pellentesque posuere. Donec et erat efficitur, vulputate erat vitae, rhoncus mi. Aliquam rhoncus sem libero, non luctus ex rutrum non. Proin ligula tortor, lacinia in urna et, suscipit lobortis dui. Praesent lacinia neque eget tincidunt sagittis. Nunc egestas dui ac gravida consequat. Nunc porta viverra massa quis pharetra.

Integer ipsum felis, tincidunt eu laoreet et, cursus ac quam. Nulla gravida et nibh in elementum. Praesent iaculis eu erat eget sagittis. Curabitur ut neque id nulla dignissim vehicula eu in augue. Donec pulvinar, velit eu cursus rhoncus, mauris turpis venenatis leo, id malesuada tortor enim nec purus. Donec tincidunt, odio vel vestibulum sollicitudin, nibh dolor tempor sapien, ac laoreet sem felis ut purus. Morbi cursus bibendum consectetur. Nullam vel lacus congue nibh pulvinar maximus sit amet eu risus. Curabitur semper odio mauris, nec imperdiet velit pharetra non. Aenean accumsan nulla ac ex iaculis interdum. Curabitur hendrerit lacinia arcu non consequat. Nulla ornare, nibh id fermentum pharetra, dolor ante laoreet ligula, id sodales nunc leo ut libero. Nam ut ipsum sit amet nunc dignissim porta vel sagittis nunc. Vestibulum convallis fringilla ante sit amet ultrices. Vestibulum neque ex, ullamcorper sit amet diam sed, pharetra laoreet sem.
