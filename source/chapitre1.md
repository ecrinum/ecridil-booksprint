<!DOCTYPE html>
<!-- vim:set sw=2 ts=2 sts=2 ft=html expandtab: -->
<html>
  <head>
    <title>MyPads</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link href="css/mypads.css" rel="stylesheet">
    <link rel="shortcut icon" href="img/mypads_fav.ico" />
    
    <script type="text/javascript" src="https://framasoft.org/nav/lib/jquery/jquery.min.js"></script>
<script type="text/javascript">
$('document').ready(function(){
script = document.createElement('script');
         script.type = "text/javascript";
         script.src="https://framasoft.org/nav/nav.js";
         document.getElementsByTagName('head')[0].appendChild(script);
});
</script>
  </head>
  <body>
    <div id= "mypads"></div>
    <script src="js/mypads.js"></script>
  </body>
</html>
