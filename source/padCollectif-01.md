## Ecridil - 30 avril

Ecridil 2018, journée 2, le 1er mai 2018 : \url{https://annuel2.framapad.org/p/ECRIDIL2018\_2}

##Introduction par René Audet \& Renée B.

L'Usine C est un lieu de fabrique : nous sommes dans le faire, et dans la rencontre.

Trois strates pour cet événement ÉCRIDIL : les présentations et les échanges sont la première strate, la seconde est le live tweet, et la troisième est un speedbooking – pas un ouvrage d'actes mais une synthèse comme un recueil de signaux.
le booksprint : laisser des traces sur le mode collaboratif

##Séance {A} « Matérialités du design du livre numérique » :

    {A1} Marc Jahjah et Clémence Jacquot : « Interroger la matérialité numérique d’une œuvre "hybride" : l’énonciation éditoriale de *Poreuse* »
Ça fonctionne ! ;-) Bienvenue !

Présentation du livre Poreuse de Juliette Mézenc : \url{http://ecridil.ex-situ.info/corpus-du-colloque/poreuse} 
Tressage des voix et sentiment de discontinuité : une oeuvre hybride.

Le lecteur peut contribuer au récit en se déplaçant à l'intérieur de celui-ci.
Labyrinthe et fil d'Ariane / perte et construction du sens : le livre conçu comme une énigme, avec ses petits cailloux 

Comment l'espace éditorial (support et geste) est-il organisé pour participer à la construction de parcours de lecture, de manière à résoudre l'énigme du récit ?
Énonciation éditoriale (Souchier)

Poreuse : Une "énigme" éditoriale
Actrices / occurences / Configuration / récepteur(gestes, indices, inférences)
Une constellation

Acteurs au plan "structurel" (l'éditeur publie.net) vs "local" (Juliette Mézenc, Christine Génin, Roxane Lecompte)

Quelle est l'instance qui détient l'autorité ? Les rôles comme la situation de communication évoluent. 
Négociation pour éviter les erreurs de cadrage et faciliter la compréhension. 

Extrait d'un mail de Juliette M : "Linéaire correct, ça veut dire quoi ?"
Intégration de lien hypertextuels ? L'ordre du récit et la distribution de la parole constament renégociés.

Garants / instances régulatrices : Possibles techniques, structurels, intentionnels, organisationnels, interprétatifs...
Constellation de poreuse dans les différences de matérialisation (imprimé, epub, mobi, pdf).

Poreuse conçu comme espace de désorientation :
    - organisation en tressage (polyphonie)
    - geste de lecture (discontinu - quoique le lien hypertexte permet ici au contraire de créer le fil d'ariane). Acquisition du sens dérouté car pas de résolution intuitive possible de l'énigme.

Indices pour le récepteur : jeu de couleurs (code couleur pour chaque personnage) / symboles / police de caractères / gestes / notice. 

Interprétant dynamique. 

L'espace éditorial = traduction d'un accord endogène. 

Proposition : Autorité distribuée. L'autorité n'est pas seulement situé dans le statut institutionnel. Renégociation perpétuelle. Compromis entre les différents actants. Autorité distribuée pour rendre compte de ce déploiement (contraintes anticipées / espace éditorial comme lieu de savoir). 

    {A2} Oriane Deseilligny : « Entre livre-objet et objet livre : représentations et circulations de la matérialité du livre en régime numérique »
Livre imprimé en régime numérique. Mise en scène du livre comme objet physique. 

mise en scène : la manière dont sont représentés les livres dans l’environnement numérique

- le livre comme éléments de design visuel à visée commerciale
- le livre comme sujet  coeur d'une composition visuelle

underlineCorpusunderline : image de livre dans les réseaux sociaux (Instagram)
Approche communicationnelle et sémiotique
Le livre pensé comme support d'accessoire de mode, objet de promotion, composition de blogueurs littéraires : le livre scénographique.
Simple adjuvant, signifiant parmi d'autres (?) 

déconstrectualisation du livre via instrumentalisation du livre par des marques
VS recontextualisation ...
    --> montre la vitalité du modèle livresque et attachement à un design matériel et graphique

### Instrumentalisation
Studium de publications sur instagram dans des campagnes de prmotion : plusieurs formes d'instrumentalisation du livre — faire-valoir
chaussures sur livres : fonction instrumentale de la pile de livres, le livre comme support , ce qui compte, c'est le volume du livre. On ne peut distinguer son titre, par exemple. 

livre co-énonciateur d'un style  : la pile de livre ne supporte plus la chaussure, suggère l'environnement vécu, habité, connote une pratique, un goût de la lecture.
le livre contribue à connoter un goût (la littérature) qui se présente comme un style de vie
Représenter une pratique réelle.

renvoie à un environnement instragrammable (monde parfait)

3. le livre instrumentalisé pour son titre: underlineeffet "seuil"underline du livre, le livre apparait pour pimenter une collection : active un imaginaire, c'est une approche marketing (capitaliste artiste identifié par...) #reférence?(Lipovetsky ? )
Célèbre une forme d'hédonisme

4. livre comme posture et activité de lecture
processus global de dépublicisation 

Conclusion (de l'instrumentation)
La marque s'appuie sur la valeur symbolique du livre — ethos de marque culturelle : pourvoir les clients d'un style ou d'un standing, dans la mesure ou l'objet se constitue comme un critère impératif — distinction (collection blanche, livres anciens, abîmés, coffee table books) légitimité éditoriale et culturelle (de la collection blanche). 
Le beaux livre : Lecture cursive et consultation rapide en boutique.


Livre ancien : destiné à créer un écart dans le champ visuel mais aussi par rapport à la mode connectée. 

### le livre scénographié

Le livre est instancié comme unité signifiante. Il est au centre de la composition visuelle sur Instagram. 

Blogueurs, maisons d'éditions : livre comme pôle  d'Attraction visuelle, objet de curiosité, agencement des différents objets. 
Horizon d'attente créé. 

Trois caractéristiques:
#1 forme moderne des cabinets de curiosité : éclectisme mis en tension, ontologie nouvelle et scénographique de leur lecture
#2 forme rhétorique visuelle et/ou picturale articulé autour de l'agencement des objets entre eux : esthétique de la nature morte, cherche à éveiller chez celui qui regarde une sorte de punctum (Barthes)
#3 Une poétique visuelle de l'immersion (p. ex. @abracadabooks sur Instagram) : plonger le lecteur dans un univers 

Conclusion : 
matérialité et énonciation éditoriale du livre à l'écran : 
la matérialité est très présente : présentation de livre et chronique qui exhobe la matérialité  (Les « Booktubeuses »): occupation de l'espace symbolique et physique.s

Les différents acteurs de l'énonciation sont présents et ce sont les éditeurs qui mettent l'accent sur cette énonciation éditoriale. 


Cas de certains auteurs sur instagram ou sur leurs sites: cherchent à éduquer à leur lecteur la chaine du livre.
Cherchent à éduquer le lecteur (jeunesse, ado), qui font des vidéos pour expliquer la chaîne du livre, la fabrication du livre comme objet "élaboré"
Exposition de la fabrique du livre via les réseaux sociaux -- les mondes professionnels représentent les médiations. 

   * p. ex. Les Opération Book Fairies, Les Stagiaires, Samantha : le livre dans l'espace public
    **{A3} Julien Drochon, « Plasticités plurielles des lectures numériques »**

Approche fragmentaire : essayer de ne pas utiliser le mot livre car a tendance à cristalliser des représentations que le chercheur essaye de déjouer?
Vox machines (sous la responsabilité de'Anthony Masure) : Programme de recherche qui  vise à explorer sous plusieurs angles (pratique : recherche-création ET théorique)

« Vox » : voir en lat. et classification 
classification vox  - forme typo vs performativité vocale. 

Expérimenter des choses autour de la .. vocale et de 
Représentation graphique de la discussion : une linéarité qui écarte les spécificités des nouveaux médias -- fragmentés. 
forme graphique de la discussion sur les réseaux sociaux (fil) : renvoie à une certaine linéarité de la discussion qui écarte ce que pourrait être la discontionuité du fragment.

prototype : fragments de discours vaporeux

comment on pourrait faire se rencontrer une forme issue à la fois de l'affiche et du chat bot (assistant conversationnel)

   * Dispositif faisant se rencontrer orateurs/performeurs : explorer les possibilités multiples d'utilisateurs sur téléphone mobile avec reconnaissance vocale 
       * Interface (gauche) qui enclenche la reconnaissance vocale ; (droite) affichage instantané de la transcription de la voix. ==> 
Proposer différentes formes (textuelles, iconiques...) et différents signes sur le même espace.

en parlant au tel : déclencher l'apparition de formes vues précédemment. 

retrouver un espace de partage commun sur la surface visible.

2e instrumentation :
rejoint la question de lA'rticulation et des décloisonnements entre la sphère privée domestique et sphère publique : imaginer un prototype qui s'appelle pour l'instant ?? privé

-> quelle place pour les utilisateurs pour  exister dans l'espace possible ??
Interface public : image vaporeuse qui permet de jouer les distances sur la lecture de l'image et quand on est prêt / engagé à dialoguer avec le dispositif  on est amené à communiquer un secret qui permet la visualisation et la publicisation de ?
D
Permet l'affichage de caractère que constitue l'écriture d'un ?? secret
Collaboration des personnes qui échangent le dit secret pou l'afficher sur l'espace public. 

The moving poster - expressivité typographique et format poster C
Décloisonne la forme vidéo 
Confronter une formalisation ??

La lecture numérique implique une dissociation support / contenu

Pour illustrer : atelier de fabrication de livre présenté en 2014, propose au public d'assembler ces livres à partir de formes fragmentaires existantes. Différents modules qui permettaient de mettre à niveau différents publics (experts ou amateurs), pour pouvoir faire exister une forme d'édition dans le cadre de cet atelier. Les principes de fabrication, reliures, constitution des contenus et formes se trouvent au meme endroit et s'adresse à différents types de publics. 

Lecture sur support numérique : 
Avec la multiplication de ces lecteurs/logiciels, que reste-t-il de la lecture du monde ? 
 
Landmarker 

Concept de Stay Radical \_ présenté dans le cadre de la biennale de venise. Contenu numérique et présence physique.  Imprimantes déterminent un espace de lecture . Parois s'ajustaient en fonction du caractère expert ou non de la conférence, ajuster le volume de la pièce. Extraits glanés des discussions ayant lieu dans l'espace.

Cet espace-là dans le cadre de la biennale était dédié à des temps de discussion. 
Extraits glanés lors de discussion qui sont glanés quand ils sont dans l'esapce

## Conclusion
Réfléchir à la question de ce que le livre peut proposer. Discrétisation de la ?? (forme?) des contenus, un livre peut être audio, peut être un dispositif d'atelier, ??
Permettre le choix de la forme du livre au lecteur (?) 

Réfléhr la question : ce que le livre peut propose r, une expréience spécifique de la consultation des contenus. Avec discrétisation : une possibilité protéiforme de ce qui fait le « livre » : plusieurs médias font le livre d'aujourd'hui (de l'audio, à l'image) ==> La forme du livre = le choix du lecteur à la participation d'une expérience esthétique. 


    Période de questions :

pour jahjah et jacquot: différents narrateurs : n'aurait-on pas besoin parfois d'un manuel d'utilisation des livres numériques ? (il y a déjà une notice)
Notice qui venait expliquer l'ancrage et qui permet de limiter l'interprétation de l'espace éditorial, qui permet de mettre un terme à l'interprétation infinie. Notice qui a fait l'objet de plusieurs élaborations du fait du retour des lecteurs. Notice qui a évolué. Dans la version imprimée : Code typographique qui vient palier à l'absence de couleurs dans l'édition imprimée. Superposition de plans économiques, structurels, matériels et l'espace devient ??

Question posée par M. Vitali-Rosati
pour tous les 4 : question de la structuration de l'autorité distribuée et de la valeur symbolique, ce qui est problématqiue pour expériences numériques est 
Est-ce que ce manque de reconnaissance est lié à une difficulté d'étiquetage des formes de l'autorité ou y'a-t-il des liens ??

   * Q : Est-ce que la problématique = légitimation ressentie par les lecteurs... Dans le livre, l'autorité est redistribuée, l'autorité est multiple. Le manque de reconnaissance est-il dû à un manque de reconnaissance symbolique des forme de l'autorité.
   * R : Le livre (comme objet) appelle l'ensemble de la chaîne, l'autorité des actants -- la porté symbolique de la tablette est tout à fait différente : le symbole de l'iPad n'est pas celui de l'objet-livre.tablette englobe les contenus, donc les noie. Gobe le lien avec le lecteur.

Avec une liseuse, on n'aurait pas vu l'autorité, celle de l'éditeur.

cette tablette (l'iPad) qui contient tous les contenus les noie

Question : (au sujet de « Représentions, mise en scène, autorité... ») représentation de mise en scène, cette visibilité des ouvrages dans d'autres contextes, notamment politique (exemple avec Macron et nouvelle photo présidentielle qui fait apparaître des livres). 

   * Comment les professionnels (libraires, éditeurs) du livre mettent en scène les objets-livres, comment l'iconographie du livre est abordée aujourd'hui, quelque chose de nouveau est-il en train de se passer ? 
   * Sur un site web de libraire ou d'éditeur, comment l'iconographie du livre est-elle abordée ?
   * 
-> c'est davantage la librairie qui est représentée comme espace de sociabilité, de dédicace, d'échanges. Avec les coups de coeur papier, etc. L'espace de la librairie est plutôt dédié à ?? et site web comme espace de représentation ??


Renée Bourassa : Le livre abordée en tant qu'objet de design amène une valeur marketing  assez forte. Pas tant son contenu que son image.

Question Souchier : 
Autorité de tous les acteurs reliés — l'objet porte l'autorité de tous
Cohérence du codex permet d'assurer une intégrité de tous les acteurs de l'énonciation éditoriale. Avec le numérique, on parle maintenant de fragments.

Marc Jahjah : Question des plans, stratifications... la notion de mélange de Tim Ingold parait plus pertinente.

    
    
Question René Audet (pour François Bon) : question de la matérialité, si dans un processus d'édition num on liquide la notion de livre numérique pour aller plutôt porter le texte vers ?? soit retour à la matérialité, comment on envisage cette matérialité ??
hybridation des lieux, librairie à la fois café et possèdant son propre dispositif d'impression à la demande
Liberté par rapport au contenu à lire. (Un livre imprimé à la demande n'a pas du tout le même matérialité que l'objet livre traditionnel)
    
A quoi sert l'éditeur ? cf. Benoit Epron

Question : de plus en plus on demande à l'auteur de faire son propre marketing. 

Pour les éditeurs et auteurs de Poreuse, Habitude de collaboration éditoriale antérieure ? Avaient-ils une attente ?

ref : Blanchot et le *livre à venir*
Anticipation par l'auteur de l'énonciation de l'éditeur, influence sur le texte
Jajah : oui l'auteure avait une habitude de travail avec Publie.net. Mais prise en compte modeste et fragmentée.


   * Renée Bourassa : Proposition d'arrêter d'opposer la matérialité du livre et la supposé immatérialité du numérique.    
    Notes NS des questions séance A :
    
- question 1 : 
    conception d'un parcours de lecture qui semble compliqué : 
        glyphes pour diff. narrateurs nous fait penser à la horde du contrevent (Alain Damasio)
        a t on besoin d'un manuel d'utilisation pour les livres numériques ?
réponse : le code éditorial et la notice cherche à liimiter les interprétations infinies de l'espace éditorial
espace editorial : superposition de plans : économiques, structurels, matériel, l'espace éditorial est le compromis entre les diff. plans : d'où l'idée d'autorité distribuée

- question 2 :  dans l'imprimé, est ce que c'était une notice ?
préface + notus qui puise dans le "mode d'emploi informatique ou électroménager" : fonctionnement à élucider très rapidement.

- question 3 : Marcello vitali rosati 

question sur l'autorité et l'espace symbolique.
pour les ex. numériques : légitimation ressentie par le lecteur, celui de la valeur symbolique du livre.

l'énonciation est multiple aussi dans le livre papier, l'autorité y était aussi très distribuée.

valeur symbolique et structuration de l'autorité ?

réponse Oriane : qu'est ce que ca aurait donné si le mannequin tenait une liseuse : l'autorité disparait, pas de titre, d'éditeur, etc.
réponse Julien D. : le livre a ce monousage, vs tablette : la portée symbolique est très différente. est ce que le symbole de l'ipad, c'est plutôt celui de la réussite des startup californienne.
les contenus sont alors noyés derrière la techno : gomme le lien avec le spectateur.

Question : benoit epron
remarque  : visibilité des ouvrage et de la lecture dans le contexte politique (Macron et les livres)
libraire et éditeurs en ligne : dynamique commerciale et symbolique

réponse oriane : 
    
Remarque R. Bourrassa
valeur marketing qui vient avec le design (le livre comme objet de design)
ce qui revient constamment : la visibilité, le livre adressée en fonction de ses valeurs de visibilité

Remarque Souchier :
    hypothèse sur l'autorité : liée à la cohérence en tant que dispositif matériel qui assure l'autorité de tous les acteurs de l'énonciation éditoriale.
    sur le numérique : c'est le fragment : des supports, des pratiques. quelle autorité pour ces fragments.
    
    proposition : Maurice Roche et Pichette

le mélange de Tim Ingold (dans Faire) : processus artisanal : faire correspondre une force et la matière : infléchir pour le faire correspondre dans un but qu'il découvre.
la stratification permet de retravailler cette notion de mélange : qui a fait quoi, écoulement des actions de chacun dans les actions des autres.

comment les reves devient un outil méthodologique (comment pense les forets ?) pour faire travailler les actions de chacun auprès des autres.

Question : Francois Bon
dans un processus d'édition numérique, peut on liquider la formule "livre numérique" : site web, retour à la matérialité (POD), alors que devient la matérialité ?
Julien D: Expérience de rencontres du lecteur avec le contenu à lire ? 

existence et hybridation des lieux pour le lecteur et l'expérience de lecture : la matérialité possible du livre peut alors exister.

Question : 
transposer l'autorité des éditeurs vers l'auteur ? marketing, etc.

la renégociation se fait sur quoi ? 

Jahjah : lisibilité : comment reconnaitre le non-reconnaissable ?
énonciation éditorial de Marc Arabiant : l'anticipation par l'auteur des contraintes éditoriales de l'éditeur : prise en compte et ajustement du texte.

##Séance {B}
###{B1} Nolwenn Tréhondart et Alexandra Saemmer, « Le design éditorial des livres d'art numériques, au prisme de leurs pratiques de conception et de réception »
Résultats d'un projet de recherche —dresser un bilan du livre numérique en France. 
Ouvrage collectif, Fruit de cette démarche pluridisplinaire qui s'intéressait à ces champs de tension 

Sémiotique sociale  est une méthodoligie en construction
Circonscrire les éléments qui interviennent dans le processus d'interprétation
dans l'acte d'interprétation (cf. Eco), interprétant (cf. Peirce)

- matérialité de la communication
- posture pragmatique
- sémiotique peircienne

Dispositif socio technique qui joue un rôle ??
Matérialités jamais accessibles telles quelles. Fatalement, il y a toujours un médiateur entre le signe et son objet.

L'interprétant est désigné à la fois comme un signe et comme le point de vue des signes qu'il fait naitre
Dans ce projet, volonté de cerner l'Action de ces interprétants socialement partagés. À partir de l'analyse des contenus de ces entretiens 
Edward Hopper d'une fenêtre à l'autre : livre d'art numérique

Livre d'art numérique reste encore peu connu du grand public. 

Objet aux contours mouvants qu'on a du mal à cerner. Adhésion forte des lecteurs au beau livre papier, à ses finitions. Achat de livres d'Arts papier dans l'idée de les posséder et de montrer qui il est, -> image de  soi, le livre représente qui on est. 
idée de la possession et de l'exhibition (coffee table book)
critique d'une ephemerité programmée : Contrecarre l'idéologie contemporaine de la consommation

Vision augmentée de l'art : haute résolution des images et possibilité de zoomer

Le terme art semlbe introduire d'emblée un 3e terme entre le livre et le numérique, mais le livre d'Art est engagé dans une négociation avec les contraintes du dispositif. 

Affiliation avec les discours d'accompagnement. — Association dans l'esprit des personnes interrogées entre numérique, Consommation et gratuité
pratique d'édition, discours numérique et ??
Renouer avec la sensualité livresque — mouvements, animation : motion design sur tablette


[Question : Mais qu'appelle-t-on "livre numérique" dans le domaine du livre d'art ? Application ? Format EPUB ? etc. Au-delà de la question esthétique, les dimensions légales et économiques sont déterminantes (TVA, lieux d'achat, compatibilité avec les dispositifs de lecture).]
Des injonctions modernistes ne cessent d'être adressée  (Joelle Le Marec) à ??


Normes d'attentes par rapport au livre numérique : 
    - la lecture numérique doit pouvoir donner la possibilité de faire son choix librement


Format court, vie éphémère, prix dérisoire
L'injonction moderniste est justifiée par l'importance de rendre l'art accessible. ( Mise en cause des intermédiaires savants) Pour de nombreux lecteurs, la lecture numérique permet de faire son choix librement. déjoue la lecture linéaire 

### unité éditoriale "pro-selective" : affranchissement de l'autorité du commissaire d'exposition. (empowerment du lecteur prend le pas sur l'objectif de médiation des savoirs)

Évocation du risque de fragmentation des contenus. perte de l'unité


### unité éditoriale "pro-totalisante"
Doutes quant à l'idée d'une représentation  quantitative vs logique  de l'articluation argumentative des contenus
La carte textuelle permettrait de comprendre l'articulation argumentatives des contenus

### unité éditoriale "pro-contemplative"

Le beau livre numérique, c'est un livre animé, qui est vivant, qui réagit. 
Accélération projetée comme norme d'attente de la lecture numérique

### unité éditoriale "pro-polysensorielle"
Promesse d'être projeté dans un univers narratif — le lecteur rencontre plutôt des explications savantes (sens vs cerveau)
Livre d'art numérique tiraillé entre sa mission de ??, entre désir d'expérimentation et d'horizons d'attente. 

désir d'expérimentation
discours qui vante la nouvelle liberté accordée aux lecteurs numériques
séduction du spectaculaire
Dispositif onéreux (ipad — prescription des grandes firmes) : dérive d'un design editorial qui suit de trop près les prescription des industriels.


###{B2} Françoise Paquienséguy, « Représenter la création artistique : les catalogues d'exposition numérique, comment aborder l’œuvre et son auteur ? »
titre définitif : Les  e-albums : entre création et éditorialisation

Perspective différente de celles entendues, s'intéresse aux conditions de production et aux logiques éditoriales plutôt que les contenues même.
Tension très forte entre création et éditorialisation, question pas à l'extérieur mais le contruit principalement. 

Les cd roms culturels, présents dans les années 90 ont beaucoup occupés l'avènement des musées et les ont conduit à proposer des multimédias interactifs qui avaient des caractéristiques proches des e-albums qu'on étudie aujourd'hui et qui emprunte des hyper liens à ??
Ces e-albums nous font passer de l'idéologie  du multimédia des CD roms au transmédia
La logique est d'accompagner des très grandes expos qui sont overbookées, qui font des nocturnes, (Mobilisation de grands nombres d'acteurs)et l'idée de ces e albums est de permettre d'accéder à ces oeuvres sans acheter l'album d'expo, souvent très couteux. 

Idée du souvenir. 
Depuis 2013, offre où l'on part sur des artistes dont les nms sont familiers ou sur une thématique. 
Caractéristiques de ces albums — ancrage technique —les institutions passent sous les fourches caudines  des firmes qui  permettent l'application sur différents supports

Objectif de la RMN : s'inspirer des albums papier pour proposer des versions numériques interactives, au coeur des oeuvres dans une mise en page propice à la lecture. 
 3 catégories de e-albums : 
-> toucher tout public 
valorisation de l'exposition et des stocks et archives de la RMN- outil de valorisation au senséconomique du terme,

##1. l'expo à domicile
Structure unique, salle par salle, audioguides, accès aux oeuvres en 3D. Notices — document pdf téléchargeable.
salle par salle, audioguides, 
téléchargement pdf 
On propose à l'utilisateur de se propulser dans la salle, de cliquer sur l'oeuvre et d'avoir accès directement à l'oeuvre. Reconstitution de l'espace de l'expositiion. Affichage de l'oeuvre + notice. Situer l'artiste, situer l'oeuvre. Préparation ou ersatz de visite? Ne pas avoir besoin de s'y rendre.


## 2. Mini catalogue
Avant-goût de l'expo, propose un certain nombre de sous-partie (cf. exemple sur Monet), ressources et textes produits par des professionnels, y compris comissaires d'expo. 
oeuvre répliquée + texte qui l'accompagne
Finit par nous faire intéresser à des détails picturaux qui sont parfois évoqués dans les textes. 
Bandeaux qui permettent de faire défiler ??
Autres éléments complémentaires : chronologies offertes qui exploitent des éléments vidéos, 
Commentaire *in situ* par le commissaire. Structuré en fonction de l'Artiste et de la catégorie d'artiste. 
Choix éditoriaux (toutes les oeuvres ne sont pas forcément présentes), conférences en vidéos
Complément et appel pour donner envie d'aller voir l'expo car incomplet. 


##3. CDrom culturel. 

Possibilités offertes par les technologies numériques, en terme d'affichage, de navigation, d'autonomie du lecteur, essayer d'augmenter la production de l'artiste par le numérique. 
Hyper navigation : avoir accès à tout
Numérique au service de la création de l'artiste, il va pouvoir l'accompagner  permettre d'oublier la normativité éditoriale
Artistes contemporains : plus documentés pour ce type d'affichage  (par ex. vidéo). 

   * p. ex. Hopper : 9 tableaux à partir desquelles ont accède aux autres. Le visiteur construit le parcours, lien qui ne sont pas préétabli.  Absence de hiérarchie. le lecteur maitrise son parcours de visite. Liens vers des ressources exogènes (INA, BNF, etc.) 
Appropriation des caractéristiques de l'artiste magnifiées par le numérique

Pour Nikki de St Phalle : des éléments, vidéos, tournés par cette artiste ont été à l'avant (parties d'un documentaire plus long) , générique d'ouverture de toutes les séquences, un extrait vidéo, possibilité d'accéder à énormément de vidéos

###Conclusion
Logique éditoriale et économique, éléments d'une fiction qui sont dispersés sur plusieurs plateformes médiatiques. 
Expo de Monet terminée depuis 2011 : éléments du e-album maintenant sur le site web 
Augmenter la version en ligne — réactualiser le e-album avec plus d'experts en histoire de l'art, spécialistes de l'artiste, etc. Le e-album, qui avait migré en accès gratuit, une fois augmenté, peut être vendu à nouveau. Logique de rentabilité, oubliée au profit de la création? 
Multimédia qui devient transmédia, lecture fragmentée, mise à disposition des ressources et d'outils dont l'utilisateur fera ce qu'il peut/veut avec une création de valeur ajouté : création de l'appropriation ??
D'où vient donc l'innovation ? Dans les 2 premiers types, elle ne vient pas des produits, seul le 3e type défend une création au service des artistes (??)


Question de M Vitali-Rosati : question de Ipad et android, il semble que plusieurs acteurs institutionnels comme Musée d'Orsay ne se posent pas la question de ??, pratiques qui se développent complètement véhiculées par deux entreprises qui ont mis la main sur le marché, système conditionné par ces entreprises du coup
R : pas d'E-Pub pour des raisons de maquette, mais aussi commercial, décision d'auteur et d'éditeur, ils font des essais par contre, logique de rentabilité  ils essaient de ne pas perdre d'argent.. Idée de travailler la chaîne en essayant à chaque fois d'avoir une petite innovation. Volonté de s'inscrire dans ?? 
Android, ça ne se vend pas

"On veut que ça fonctionne et on produit à la chaïn #e"
Continuité epub/appli 

Question : 
Problème de droit d'auteur (copyfraud) quand la RMN s'approprie des droits d'auteurs.
blocage des savoirs 


##**Table ronde {C} « Arts, littérature et formes numériques du livre »**
Lucile Haute, Ariane Savoie et Nolwenn Tréhondart

Plusieurs projets éditoriaux numériques : certains sont des livres, certains appartiennent au champ de la litt numérique.  
Quels critères pour le livre numérique ?

Souvent, ces objets sont d'ordre expérimentaux, confrontés à des contextes industriels contraignants
Cadre légal de l'existence de ces objets : en France, loi de 2011, qui donne une définition de ce que c'est un livre numérique. Tout ce qui est de l'ordre du design d'intéraction, de l'ergonomie, toutes les possibilités du numériques sont d'ordre accessoire. 

Se pose la question du beau livre numérique : quand passe-t-on d'une qualification à une autre ? Qualité des interactions pour changer de qualification. 

Éditeurs traditionnels en France : en les interrogeant, Sylvie ?? a noté qu'il y toujorus ce statut d'objet livre qui demeure attaché à ??

quels ont les freins ?

statut d'objet livre dans ses dimensions culturelles et esthétiques qui font que ces éditeurs ont du mal à passer au beau livre numérique

Quelques éditeurs de livre d'Art qui ont essayé de développer leurs collections en  epub.

Un éditeur de livre d'art qui fait auj du livre numérique : idée de stratégie de disinction par l'innovation que mettent en oeuvre ces éditeurs à l'heure actuelle. Montrer qu'on reste branché et qu'on est moderne. 
Par ex. Diane de Sellier qui vend des livres d'art à plus de 1 000 euros. C'est aussi montrer qu'on reste branché, qu'on est moderne.

E-album de la RMN : plus de 30 à l'heure actuelle : offre assez large, 1er éditeur de live numérique français.  


Pourquoi e-album d'expo et pas catalogue d'Expo ? Déjà cest moins onereux, ensuite plus rapide à fabriquer et à charger, raison principale c'est la plateforme, qui applatit la forme, on ne peut pas représenter sur l'appstore la variété des prix et des contenus (vs comparer les livres dans une librairie)

2 points de vue à partir d'un exmple :
     - histoire de l'art
     - histoire de la contitution du Musée de Budapest

2 livres dans un livre : jouer avec la tablette elle-même, qu'on peut incliner dans un sens ou dans l'autre. 

Subvention du CNL de 50 000 euros pour l'exception Hooper

Collection de l'ENSAD avec un catalogue par articte de 50 mots : motion design (mouvement, sensualité..)
Motion design : travailler le mouvement (entrée dans les mots)
Très application, très peu epub : plus léger, plus rapide à télécharger

Plutôt application que Epub.
RMN très application et très peu epub
Centre Pompidou a fait développer un CMS, stratégie différente qui leur a valu de ne rencontrer aucun public ; chiffres de ventes trops faibles, ils ont dû arrêter

remarque : le cms est censé réduire le

la RMN ne sont pas sur de pouvoir continuer à travailler avec Aquafadas  («Aquafadas est un éditeur de logiciels, spécialisé dans les solutions mobiles pour les entreprises et dans la publication numérique. Basée à Montpellier dans le sud de la France, Aquafadas est une filiale majeure au sein du groupe japonais Rakuten» tiré de wikipédia)
 Développent maintenant des liens pour l'élaboration d'epubs — pérennité?

Livre augmenté : jeu vidéo-livre. Application pour Iphone/Ipod et on va jouer. Le support imprimé est le chemin d'entré pour avoir accès aux contenus qu'il y a dans l'appli. Mais question de la pérénité.  

Problème de sites internet qui ne sont plus maintenus pour des expériences en réalité augmentée (par ex. zombies en QR code)

Application de réalité augmentée à partir du support imprimé (app pèse 1G). Si on a le support imprimé, on a déjà beaucoup de contenus. 

Epub : format qui a tendance à remplacer le PDF pour l'édition numérique, en particulier textuelle (roman), format lisible sur différents supports (tablette, liseuse). l'Epub n'est pas tant la création des contenus que la création des logiciels d'interprétation de lectre de ces contenus. 
Les lecteurs actuels n'ont pas assez de puissance pour des interpréteurs de contenus epub qui soient performants en termes de motion design.

Tirer partie des possibilités de l'écran en intégrant des vidéos, des éléments interactifs. 
Fréquence(s) de Célia Beaudard ?? : oeuvre qui est du texte, images, son, parfois ensemble, mélanger des médiums qui normalement se mélangent assez mal. Permet e glisser vers des enjeux qui sont plus du côté de "comment est-ce qu'on écrit pour ce genre de supports-là ?"

Comment écrit-on pour nos différents supports ? 

Bleu Orange : 
Revue de litt hypermédiatique en 2008. Conçue pour promouvoir la litt hypermédiatique francophone et soutenir les artistes franco. Plateforme de diffusion pour oeuvres en français. 
Soutenir les artistes 
Question de diffusion des oeuvres en français a amené ;a revue à penser a question de traduction et s'approprier certaines oeuvres en anglais canoniques — communautés minoritaires. Associé avec le département de traduction de Concordia. 
les étudiants sont appelés à traduire des oeuvres

Conçue comme une revue, qui rassemble plusieurs oeuvres sous un même couvert, oeuvres qui prennent des formes de plus en plus diféfrentes, qu'on essaye de rassembler sur une même plateforme. Question de la traduction ramène la question de l'écriture, comment écrire une oeuvre hypermédiatique sur le plan autant littéraire que programmatif ?
Plusieurs types d'oeuvres qui existent (récits avec hyperliens et hypertextes où l'oeuvre est construite par passage déjà condensé/circonscrits qui sont reliées par ça à d'autres passages)

Par ex. Testament de vie
Ce sont des hyperliens, la traduction fonctionne de la même façon que si l'on traduisait un roman.

2e exemple : luckysoap.com de J.R. Carpenter
Le code source révèle l'écriture même de cette oeuvreCode source de la page révèle l'écriture même de cette oeuvre. Texte existe comme formule à exécuter plutôt que produit fini. ( Mais avec les théories de la lecture, le texte est-il vraiment fini? n'est-il pas tout de même une formule à exécuter?)
Le texte n'existe pas de manière définie mais comme formule à traduire, code informatique à traduire. 
Sur la question de la traduction de Transmission : a dialogue, voir l'article d'Ariane Savoie : \url{http://nt2.uqam.ca/fr/cahiers-virtuels/article/la-litterarite-du-code-informatique} 
Traduction ou adaptation ? car traduction = interprétation (surtout dans le cas présenté, il a fallu agir sur le texte)

Le contenu ne peut pas être traduit tel quel



3e Hexes (ou Sextes traduit par Ariane Savoie) : \url{https://nickm.com/poems/hexes\_etc.html}
Contenu accès sur les fonctionnalités du langage numérique
définition comme poème? SIgnification à travers des codes extrêmement contingents — code hexadécimal. Renvoie aussi à des couleurs. Effet de rituel du défilement des mots, prière (sexte).
Éloigne du livre papier car impossible d'exister sur papier. 


Roman Mégawatt de Monfort : programme pattern qui allait exploiter ??

Roman What de Beckett
Création d'un algorythme pour prolonger et étirer les sections du texte original. Souligner la lourdeur dans l'écriture, puis finalement imprimer sur papier pour renvoyer à l'objet livre. 

L'échange entre l'écriture numérique et papier est questionnée de plus en plus par Bleu Orange car on est confronté à l'obsolescence des technologies
Revue BleuOrange toujours confronté à ces différents langages. En train de revoir la mission pour revoir la question de traduction. 

Notamment la traduction du français vers l'anglais, à rebours de la pratique actuelle de celle de la traduction de textes de l'anglais vers le français.


Question de Gascuel: 
Q :  qu'est-ce qu'un vrai livre numérique ?  
Le vrai numérique, où est-ce que ça commence ? Sachant que le numérique est avant tout  « transmédia » et non multimédia. les exemples présentés montrent cette dynique : le lecteur presque auteur. Le livre hors-papier se cherche, et se cherchera longtemps...
Commence à partir du langage transmédial : chaque lecteur est quasiment un auteur dans le livre numérique. Le livre hors papier se cherche et continuera de se chercher pendant longtemps. 

R : Définition légale du livre numérique afin d'appliquer la TVA réduite : livre ou jeu vidéo?
Travailer aussi la dimension esthétique au-delà de la dimension juridique.
Devenir authentique des techniques : une technique apparaît avant qu'on sache vraiment à quoi elle sert. 
Travailler collectivement pour mieux appréhender l'objet-création. Ariane : « Le livre, est plus un modèle qu'un réel objet » ; Nolwenn : « Pour les éditeurs, le livre numérique, c'est la clôture. Un espace protégé dans l'espace des lectures industrielles. » 
Définir littérature numérique : on espère qu'à un moment ça deviendra juste de la littérature. Pour les éditeurs, le livres numériques, c'est la clôture.  Quand les formes de langage s'évaporeront, tout redeviendra littérature
Pour les éditeurs, le livre numérique c'est la clôture, rempart à des questions de lectures superficielles...

Olivier Charbonneau : quelles dispositions juridiques peut-on anticiper autour d'un livre numérique ??

Q : Qu'est-ce qu'un beau livre ?
Comment animer une communauté artistique ? Surtout en pensant le droit d'auteur.

R : secteur économique qui cherche à ne pas perdre d'argent (quand c'est gratuit, c'est mieux Android, quand c'est payant, mieux sur AppStore, trop cher de faire les deux. ). 
Quand on distribue en gratuit, on a beaucoup de lecteurs sous Android, dès que cela devient payant, on perd 90% des lecteurs, ce n'est pas le cas sous iOS
L'imprimeur, il faut le payer, le développement, on peut le faire financer comme des projets de recherche.
Les artistes de BleuOrange, ce sont des gens qui font ça probono pour leur propre compte. Certains ont eu subventions mais jamais à des fins commerciales. Pour ce qui est de la traduction, la plupart des artistes sont ravis de se faire traduire. 
Bleu orange est une initiative para-universitraire, financé par différents groupes de recherche et par le bénévolat.
Initiative parauniversitaire, un peu comme recherche-création. Problème du copier/coller qui permet de faire des emprunts sans citer les oeuvres. 




##Séance {D} « Édition scientifique : enjeux » :
##
###**{D1}Anthony Masure, « Design et humanités numériques, vers une convergence du livre et du web ? » (Mutations du livre web)**

Suite à une intervention d'Antoine Fauchié sur les livres web, autre forme des livres numériques, autre forme à quoi ? 
Passage du EPub au livre web. Plusieurs contextes : 
- Multiples modalités d'édition numérique : dès lors qu'on a voulu plus d'interactions numériques, confrontation avec la mise à jour des liseuses
Tentative de convergence avec le web - le epub a pris du retard. Emergence de formes non fictionnelles.

Place de l'éducation de recherche dans ce contexte. 

S'appuie sur des standards du web —hyperliens, javascript etc.
Promesse de pérénité. Le livre web peut avoir potentiellement une conception moins pénible. Méthodologie à mi chemin entre philosophie et design.
Possibilité de tirer partie de la pluralité des terminaux (téléphones, desktop, tablette, etc.)
La plupart des livres web sont plutôt des manuels techniques mais que peut-on faire d'autre ?  

Rapprochement possible entre humanités numériques et design. Que peuvent faire les chercheurs dès lors que leur techniques sont changées ? 

littératie numérique : les chercheurs ont du mal à s'approprier certaines techniques. Les pratiques d'écriture changent.

Pratiques altermatives (notamment à un intérêt de rentabilité, approche quantitative).

Toujours un intérêt de rentabilité qui va modifier la manière de faire de la recherche, mais avec le web c'est différent. 
Humanités numériques : rencontre entre matériau à la technologie numérique et pratiques savantes en sciences humaines. 

hyperlien comme mode de navigation — mettre en connexion différentes informations de recherche. Contrer l'isolement des chercheurs.
Le web inventé par un chercheur : idéal (idée?) de mettre en connexion la recherche.

Captation capitaliste du travail des chercheurs (Springer, Elsevier)

Enjeu de diffusion : temps de publication trop long, manque de visibilté... la diffusion 
Les articles de recherche sont souvent peu lu, problème de lecture sur téléhpone car difficile de lire des articles sur téléphone. 

hypothèse : le savoir n'existe pas sans transmission : repenser les formes et les formats de leurs publications.
écrire ne suffit pas pour être lu : trouver des lecteurs.


co-construire les savoirs : communautés de recherche. Commentaires, pas du tout opposés à l'idée de livre.

Blog de ... : comentaires qui créent une communauté. 

Défi du design : quel design pour ces livres web ? Constat plutôt négatif, peu d'attention portée au contexte de lecture, peu de prise en compte des retours des lecteurs, qualités variable, gestion typo hasardeuse. 

constats :
- repenser le contenu même (diff. longueur de textes)

Quel rôle des designers dans la transmission des savoirs ?
Plusieurs modalités de design : design éditorial (hierarchie et structuration graphique et sémantique des contenus), design graphique, design d'interface (rôle sur l'expertise de la lecture à l'écran) 
design graphique (maquette, visuel)
design d'interface (expertise de la lecture à l'écran)

Penser un design qui va au delà de ?? penser n'est pas produire, mais saisir.
En transformant le savoir, le design en lui donnant forme, le fait exister.

En 2014, proposition d'une version web intégrale de sa thèse qui a permit de toucher un public bien plus large. Site beaucoup de visites, format pdf aussi. 
pdf : pas un format authentiquement numérique (Pierre-Damien Huygues), pas de  bon moteur de recherche. l'accessibilité par les bases de données des bibliothèques n'offre pourtant que ce format, qui n'est pas idéal pour la diffusion d'une thèse, par exemple.

Propositions 
- refaire du web un espace public, mais env. contradictoire du web qui n'est plus pensé pour les chercheurs.
- penser des publications de recherche dans un design spécifique 
- notion de « contrôle partagé »
- Dépasser la frontière des supports (web to print)
- Le livre web comme architecture de lecture

voir prepostprint : \url{https://prepostprint.org/doku.php/fr/ensadlab}
voir mckenzie Wa


**{D2}Jean-Louis Soubret, « Design thinking et édition - point d'avancement » **

À l'origine, éditeur de revue : Question des livres qui ne sont pas lus. Domaine des revues, canon des publications qui est celui des sciences techniques et médecine s'impose depuis 20 ans vs les sciences humaines et sociales, pour des raisons exogènes. Très puissant et difficile d'y échapper pour des questions de légitimation.

Les formes de publication endogènes à la fois aux sciences humaines et sociales 

Revues de communication — beaucoup de numéros avec des dossiers, des nos spéciaux, une écriture collective autour d'un thème. 

SHS — beaucoup moins d'auteurs par article que sciences de la nature.
Le livre est historiquement la forme de consécration des disciplines dont on parle : sciences de la com et design. On parle de Transfert : écriture scientifique qui s'adresse à de plus en plus de gens, souci de s'adresser à un public assez large. 

Beaucoup de livres ne sont pas lu, essentiellement livres papier. 
Enquête menée à Cornell : entre 90 et 2009, plus de 50% des livres n'étaient pas lus (pourtant gratuits)
Problème d'attractivité, de désirabilité des livres...design thinking peut y répondre
Alternatif : design domestiqué OU domestication du design (référence à Jack Goody)
En français, « Design Thinking » pourrait être « Domestication du design ou Design domestiqué » : parce qu'il s'agit d'un design explicable à des non-designers. Les designers ont effectivement une réelle difficulté à traiter de leur travail avec des interlocuteurs « profanes ». 

Design explicable à des non designers 
Comprendre comment les designers travaillent : recherche qui continue et qui se porte bien (approche cognitive)
Dans leur approche, idée qu'ils faisaient du desgin, expliquaient qu'au bout d'un moment c'était un design réflectif. 

2 caractéristiques : 
- décrit un processus : comment rendre ça compréhensible pour des gens qui ne sont pas designer, création d'un schéma en 5 points qui est explicites, donne l'impression que le design est quelque chose de simple et de processuel. 
- 

   * 
question de l'esthétique : selon Forsey design comme une activité, l'objectif serait de créer de la beauté dépendante, a pour objectif de faire que les objets créés par cette forme de design soit utilisés alors que l'objectif serait pour un artiste de créer des formes qui seraient contemplées.  
Formes utilisées vs contemplées

Possibilité d'utiliser le design comme un moyen de ??

Expliquer une discipline via le design — pas si éloigné des humanités numériques! Dans les humanités digitales, le design est important car on arrive à créer un dialogue qui assume la forme de ce qu'il discute. 
Le rôle des éditeurs tel qu'il est critiqué actuellement — imposer la forme (ex . revue) sur un fond. Les auteurs décident plutôt de s'affranchir de cet éditeur pour prendre en main leur ouvrage. 

création de 2 choses : 
Équipe collaborative dans laquelle il y a des gens qui parlent de fond (designers par ex)
design de service  : les auteurs ont considéré que le livre de papier était un point de contact parmi d'autres dans un processus qui reliait les auteurs entre eux et ensuite auteurs-lecteurs. Ouvert des systèmes en ligne qui ont permis de tester des prototypes, tester ce que les gens pensent des différentes étapes qu'ils proposent

Le design thinking peut être conçu comme un mode d'exploitation du design mais c'est probablement un mal nécessaire pour permettre de créer des livres et toucher une audience la plus grande possible. 


###**{D3}Lucile Haute et Julie Blanc, « Design des publications scientifiques multisupports »**

Une approche près de celle d'Anthony Masure (et complémentaire) -- projet de revue avec l'ENSAD et la Polytechnique, chaire Arts et science et aussi une revue. 
en train de monter une revue avec la chaire d'arts et sciences qui réunit l'ENSAD et Polytechnique, ambitieuse, multisupport, opentech, modulable. 

La revue
Modèle idéal visé :
Multisupport, modulable, multimodale, open-tech, paramétrique, openscience

Pour le multisupport, supports qui vont de la smart watch à l'écran 4k avec du printable ?? à la maison. Le modulable, multimodale et paramétrique : décliner plusieurs usages (téléchargement aux réseaux sociaux)
Évaluation en double aveugle. Plateforme opentech qui supporte ??

Un article scientifique : dispositif textuel évalué par les pairs et publié 
La publication scientifique ne tient pas vraiment compte des aspects graphiques . Grande pauvreté graphique. idéalité du sens face à la forme.
Contraintes économiques liées au budget de plus en plus serré pour les publi. académiques ex: Se passer du designer graphique

Des plateformes web proposent des publications clé en main : un formatage technique très fort et des templates hypernormés. Les publications doivent avant tout être textuelles, ce qui facilite le référencement, l'accès et la conservation

Un même maquette peut donc être appliquée à des contenus différents, ce qui sous-entend que le texte peut être détaché de sa forme, qui n'aurait pas de valeur.

Positionnement particulier de la revue : Place de l'iconographie, fonction et valeur qu'on va lui donner 
Le fait de se méfier de l'image pour l'écrit scientifique n'est pas nouveau. 
Bachelard sur la métaphore et les sciences : l'esprit scientifique doit lutter contre les images et les métaphores
Bruno Latour : donne une valeur particulières à ce qu'il appelle les inscriptions : 
l'image existe dans un contexte d'énonciation éditoriale : c'est d'abord le texte, à l'intérieur duquel on accroche des images.

est ce que l'image peut être la forme de publication elle même ?
Quelle serait la forme de la recherche en art, et non pas sur l'art ?

Mobilazing ?? : sert à faire de la modélisation 3D et visualisation 3d. 

Trailer youtube pour lire l'article, publiée sur le site de l'éditeur ACM
Volonté d'embrasser tous les supports. Solution : utiliser le web car permet d'embrasser plein de supports différents (epub possible mais moins d'enrichissements)
- le web
- ePub
-Montre intelligente!
Lien avec l'imprimé ? : le pdf est une norme qui date de 92, format A4
Quels nouveaux usages allons-nous inventer pour des supports exttrêmes type smart Watch. 

PDF est une norme de 1992 qui n'a pas été détronée, idem pour le format A4

Chaîne de publication : favorise actuellement une communication fluide à toutes les étapes du processus édit (environnement de créaiton, csm, environnement de lecture.) 
Single source publishing permet une interopérabilité entre les plateformes, les médias et les formats, workflow basée sur le balisage stylistique des textes
Surtout fait pour des structures textuelles. Non prise en compte des contenus multimédia. 
Le papier n"est pas encore bien intégré dans cette chaîne car dans le schéma, il y a une ramification de plus pour ça

Utiliser les standards du web sur l'ensemble de la chaine éditoriale, comme il s'agit du point central de la diffusion

Les technologies web ont été mises en place pour créer ??
- CSS print : ensemble d'instructions qui permet de gérer une page web lors de l'impression
- Design responsive : adapter le site web à l'écran — (encore peu d'exemples) il faut ajouter des balises de structure html juste pour le design, ce qui entre en conflit avec la structuration html du contenu.
- design paramétrique : propositions présentes dans l'epub, dans des livres web, d'ordre graphique (typo + grande), ordre fonctionnel, d'ordre structurel où le lecteur va choisir ce avec quoi il veut intéragir.
Capacité d'un contenu à être performé à travers des variations de présentation (?) 
Toujours écrire une seule fois le contenu, puis l'adapter de différentes manières.
Concevoir une revue art et sciences où les formes et les formats sont produits dans un même geste en intégrant le travail du designer très en amont de la chaine, nécessaire pour intégrer un très grand nombre de médias, essentiel pour la recherche.

formes et formats sont produits dans un meme geste editorial


***Période de questions***

Constat (!) : Pour rebondir là-dessus... le numérique doit prendre en compte le triangle contenu-designer-informaticien

   * R (Masure): Dès lors que le designer met en forme du contenu, il devient aussi co-auteur mais le logiciel est également un agent éditorial, ça fait complètement éclater les frontières. 
   * R (Blanc) : Le logiciel a une influence sur la forme si on utilise que des logiciels qui sont propriétaires contrairement à l'utilisation du web. 
   * R (Masure) : Un travail d'amélioration en terme de littératie numérique pour les chercheurs.  (opposition complète entre le format  et le contexte (?) ex : une thèse sur la bd numérique uniquement disponible sur papier.
   * R (Haute) : Les chercheurs sont évalués sur les articles, les publications et si on veut innover, différente du « long paper », il faudrait que ces formes puissent être reconnues comme résultat de recherche. Il faut des contextes éditoriaux nouveaux, qui évalue pareillement, mais s'ouvre à de nouveaux contenus. Il faut de nouvelles plateformes, il faut les inventer ou encore accorder à celles existantes (ex : revue BleuOrange), un statut légitime. 
   * R (Masure) : Aussi, reconnaissance des blogs comme pratiques de recherche. Le peer review est l'Alpha et l'Oméga ! 
   * 
Q (M. V-R) : Outre les documents, on peut parler de données et les API, peut-on les appeler « livre ». Créer et designer une structuration - indexation, éditorialisation, etc. : il peut y avoir des contrôle au niveau du design pour contrôler les limites épistémologiques ou encore, laisser le contrôle se perdre : ouverture aux autres plateformes, supports, etc. 

   * 

   * R (Masure):  Structures ouvertes : si les données sont bien structurées, peut être utile. Git par exemple.
   * R (Soubret) : De l'indiscipline du design, c-à-d à leurs manières de tout essayer, une liberté de création indispensable à préserver... Aussi, beaucoup de valeur dans les objets produits et dans la constitution d'équipes interdisciplinaires — ne pas forcément valoriser uniquement les résultats de la recherche, mais aussi la démarche. Un droit à l'expérimentation, Si on a pas droit à l'erreur et que l'on est pénalisé, on perd l'intérêt de l'expérimentation, pourtant fondamentale, incontournable à la recherche. Valeur dans la constitution d'équipes pluridisciplinaires 
   * R (Blanc) : nouveaux modules qui permettent de ne pas trop influencer la structuration de l'éditeur.  Il faut des API pérenne et il faut qu'on puisse changer le design en ayant pas à tout refaire. On cherche ce que, en temps que Designer, on peut afficher l'article. de quoi peut on se passer pour assurer que l'article soit assez pérenne. 


##Séance {E} « Édition scientifique : études de cas » [SÉANCE PARALLÈLE]
###
###**{E3} **Cécile Meynard et Elisabeth Greslou, « Du manuscrit au numérique : une édition multiformes et multisupports de Stendhal » 

Origines du projet : chantier de recherche, idée de l'exploitation, travail collectif. Un des premiers projets en humanités numériques car 2006. Ce n'est pas fini, recherche et expérimentation. 
Recherche-expérimentation 
Spécialistes dans différents domaines. 
"Bricoleurs": ambition démuserée. 
Du coup, choix d'un corpus test (journaux et papiers) de Stendhal pour permettre des expériences (?)
Chaine éditoriale multisupports  : Clélia

- Éléments techniques :
1er temps : Format XML maison au départ (pas de TEI)
Ambition de tout repérer, tout annoter, ce matériau a permis de faire un site où on voit d'abord une trascription puis dans un 2e temps, en passant par la TEI, on a généré des flux vers InDesign pour faire des journaux et papiers. Ambition de tout annoter : site Internet
2e temps, TEI : générer des flux vers Indesign pour l'édition papier
3e temps : génération d'un epub
équipe multicompétences

Idée; avoir une circulation entre les différents supports et formats pour toucher un public divers

L'idée est d'avoir une circulation entre les différents supports et les différents formats adoptés (Avoir les manuscrits de Stendhal sur son iPhone).Les contenus ne sont pas identiques sur ces différents supports (site, epub...)
Publications concernant le site car c'est une manière de rendre public à la fois les manuscrits, le travail de recherche [procédé assez nouveau en litt.], rajout de compléments pédagogiques, fiches avec compléments multimédias, travail de l'auteur dans la transcription pseudo-diplomatique, Articles et notes liés
Contextualisation des manuscrits

Perspective de mouvement, pas une édition figée
Edition avec un apparat critique développé, aux normes de l'edition scientifique. Essayer de respecter la MEP de Stendhal — principes de colonnes, mots plus gros, etc. Grande quantité de notes, de notices, de préambules, d'introduction.Préface, introduction, 2 notes de préambule... 
Édition qui se conforme aux standards mais en prenant des liberté stendhaliennes. 

Format epub : idée de rester plus près du texte de Stendhal, toujours possibilité de retourner su le site
epub : voulaient toucher un autre public - rester plus proche du texte de Stendhal — éliminer énormément d'apparat critique et de notes
Repenser l'organisation : faire des renvois à l'intérieur de l'epub, deux types de sommaires, chronologique comme dans l'ouvrage et par corpus.
Souci de complémentarité. 

Ulisse : projet de pièce de théâtre de Stendhal, rapidement abandonné. 

3 aspects de lecture : 
#1.page de gauche -> manuscrit, page de droite : transcription pseudo diplomatique avec respect des ratures, fautes d'orthographe, disposition...
#2 séparation entre les documents par des traits
#3 epub : souci pédagogique accru, souci didactique souci d'accompagnement du lecteur, contextualisation du texte, un que sais-je. 



Ce genre de publication manque de circuit pour être présenté. Le lecteur a un rôle d'Acteur. Que devient l'auteur, que deviennent les textes de l'auteur ?
Que devient le texte ?

éditorialisation du texte de Stendhal

###**{E1} **Enrico Agostini-Marchese, Elsa Bouchard, Joana Casenave, Arthur Juchereau, Nicolas Sauret et Marcello Vitali-Rosati, « Une plateforme pour le livre anthologique à l'époque numérique. Le cas de l'*Anthologie Palatine* » 

 pensée collective s'adapte bien à ce type de projet. 

qu'est-ce l'anthologie palatine ? Est-ce un livre ? Est-ce que c'est le codex Palatinus 23 ? 

Ou bien est-ce l'anthologie grecque : 3000 épigrammes (premiers datent du 6e siiècle avant JC)
recueil de poésie, d'épigrammes. 

Histoire : 
- Épigramme 6e siècle avant JC, épigrammes = formes poétiques courtes, inscriptions funéraires, votives. (Simonide)
Au 1er s av. JC, (Méléagre de Magara, Philippe de Thessalonique))création d'une antologie, ancêtre du manuscrit étudié. Puis antologie de Philippe de Tessalonique puis 6e s. après JC de ??
Constantin : 9e s ap. JC
Plus que 100 auteurs, 16  sicèles d'écriture, forme métrique prédominante mais pas que, inscriptions et thématiques différentes, langue variée aussi (grec 6è sicèle av. JC pas pareil que 10e s. après JC)
idée d'anthologie, Méléagre — tisser une couronne de fleurs — chaque poète est une fleur, qu'on collectionne
Comment faire pour éditer cette chose-là ? Plus qu'un livre, plus qu'un texte et qu'un ensemble de texte, il s'asigt d'un imaginaire collectif. Ne peut être pensé comme un codex. On ne peut pas faire l'édition de l'antologie palatine comme un livre. 

Topoi : reviennent et résonnent dans notre perception et notre imaginaire et il fallait en rendre compte. Idée de littérature brouhaha : littérature mais pas quelque chose qui se fait dans la tête d'un auteur puis texte unique mais plutôt dans quelque chose qui résonne comme un bruit de fond. 
idée de littérature brouhaha — Lionel Ruffel
Dynamiques ouvertes et collectives qui arrivent jusqu'à aujourd'hui. Que nous dit L'AP aujourd'hui?

Comment rendre compte d'une culture populaire ?
Quelque chose qui arrive jusqu'à nous aujourd'hui. Production d'un environnement ouvert pour pouvoir rendre compte de cette résonnance. 
D'un côté, la matérialité du codex, mais ce qui nous intéresse est la dynamique de création

Collaboration avec deux lycées qui travaillent à l'édition des textes sous la supervision de professeurs de grec ou de latin
Travail autour de l'imaginaire car la plateforme le permet (lien youtube, iconographie de vases) 
alignement des différentes traductions, travail autour de la notion d'imaginaire collectif et populaire

Proposer une structuration ouverte des données : création d'une API donc base de données ouverte. N'importe qui peut participer, proposer des versions (plutôt que des traductions. il n'y a plus l'original, que des résonances de texte)
Json permet de faire ce que l'on veut des données : un usage savant, un usage pop — Méléagre in love, poème écrit par des étudiants parisiens — le grec arrive comme traduction
Usage complètement pop : appropriation par des étudiants qui ont fait un site "pop"
Renversement car le grec arrive comme une traduction, l'original devient la version française. 
Bot qui va chercher sur l'API et qui tweet une éprigramme
textes très courts résonnent avec les tweets : simonide écrivait-il des tweets? Bot de l'Anthologie sur Twitter




###**{E2} **Benoît Epron et Catherine Muller, « L'abécédaire des mondes lettrés, un outil d'écriture collaborative savante » 
###
Nouvle objet éditorial numérique : Abécédaire des mondes lettrés
Développé avec un prestataire informaticien 
Entreprise partie d'un projet éditorial pour devenir un projet de design éditorial et de développement d'un outil d'écriture numérique. 
un réel projet de design éditorial des publication savante
Né en 2012. 
Cartographe du monde des bibliothèque en explorant ce qui ??
Approche toujours très personnelle, émouvant
Dictionnaire inachevé d'un univers savant avec des renvois sur des articles à rédiger plus tard 

donner corps à la matérialité de la connaissance.
Autorise beaucoup de liberté, sans se soucier d'anachronisme ou de hiérarchie de valeur
Forme éditoriale qui autorise beaucoup de libertés, on peut traiter sur le même plan un article sur la bibliothèque d'Alexandrie et un article sur ZOtero ou encore sur des objets ??

On peut jouer de la succession alphabétique des articles : ex Michel Foucault entre format et freemium
Formes de relations surprenantes : Foucault positionné entre deux articles plus terre à terre
Conception de Jacob utile pour essayer de poursuivre ce dictionnaire inachevé mais aussi pour profiter de la dimension collaborative du numérique

Répondre au besoin d'écriture collaborative et savante : écrits par des savants sur le monde du savoir. Aussi passe par des formes d'écritures renouvelées par les techniques numériques
Outil qui permet de conjuguer les deux dimensions éditoriales de l'encyclopédie (collaborative et réticulaire [en réseau] écriture en réseau, données liées

Problématiques scientifiques de ce projet : 
#1. Potentialités de parcours de lecture non contraints : l'organisation des termes dans l'abécédaire offre une potentialité de parcours de lecture totalement libre malgré les liens proposés entre les termes, suivre de lien en lien une cohérence intellectuelle telle qu'imaginée par les auteurs. L'outil abécédaire doit être vu à deux niveaux : d'une part un projet éditorial et d'autre part, un outil qui permet de construire des parcours de lecture parfois linéaires, comme on suivrait un parcours qui aurait été prétracé au départ. Cette proposition pose une problématique qui est celle du contrat de lecture : un certain nombre d'éléments ici sont dans une logique de cartoraphie au sein de laquelle le lecteur va se déplacer. Cette logique de navigation mobilise des artefacts qui sont facilement exploitables aujourd'hui et dont on n'a pas besoin de notice. On peut se permettre de naviguer ainsi, par exemple, parce que l'on est habitués à naviguer dans google maps.

#2. Niveaux de granularité : 
    - ouvrage
    - notice
    - ?

Manuel universitaire : pas forcément un parcours linéaire car on peut commencer et lire n'importe quel chapitre. 
granularité de l'ordre de la notice, choix du lecteur (contrairement à un polar par exemple)

\url{http://abecedaire.enssib.fr/}
Techniquement, c'Est un site web conçu avec javascript et CMS ??
Univers visuel sphérique
Nouveau monde dans lequel se reconnait les humanismes numériques. Choix d'aborder un design sobre entre autre dans un souci d'accessibilité et dans cet univers graphique, 
Logique sémantique vs alphabétique
Galaxie lettrée, le lecteur est invité à s'y balader. 
Pour repenser les modalités de navigation, choix d'adopter des principes de lecture et ?? particulièrement stimulant. immersion et constellation, grâce à la technologie de la 3D qui place l'utilisateur au centre de l'univers lettré et lui permet de se déplacer d'un point à un autre.
Place l'utilisateur au centre de l'univers des mondes lettrés (possibilité de se déplacer à 360 degrés)

Constellations : répartissent les données selon 3 types de noeuds :
- noeuds alphabétiques
- noeuds corrélatifs
- noeuds associatifs
Avec ce type d'arborescence, expérience de lecture non contrainte, ce sont les logiques signifiantes et non signifiantes qui construisent les liens entre les termes

Problématiques techniques déjà évoquées (différentes sortes d'encodage et gestion des références bibliographiques (ex à la fin d'une notice)). Toutes les références servant à toutes les notices pourraient être répertoriées, et ainsi devenir elles mêmes une entrée

Perspectives : projet en cours HyperOtlet (université de Bordeaux, Mondaneum de Mons, CRSH). L'idée est de produire une édition critique du traité de documentation de Paul Otlet pour pouvoir mobiliser autour de cette oeuvre un réseau d'experts (historiens, philosophes, économistes etc). afin d'éclairer l'oeuvre de Paul Otlet
HyperOtlet permet également une forme de valorisation : cet objet éditorial devrait servir à structurer l'oeuvre tout en rassemblant un réseau mais aussi à exposer l'oeuvre. 

Structure l'information, rassemble un réseau, expose l'oeuvre. Naviguer à partir d'un écran tactile


    
***Période de questions***

Q:  quel est l'ambition de l'objet final (pour l'antologie palatine), est-ce que c'est esthétique ? Comment vous l'imaginez ?  (Jardin français ou anglais? Foisonnant ou très règlé?)
R:   Marcello -> Il ne sait pas vraiment. Pas d'objectif final. L'idée est celle de design for losing control. Ouvrir à des pratiques et non des données.  Il pourrait y avoir des livrables, des affichages de données. Une impression est une parmi des millions de versions possibles. Permettre sa circulation libre dans la culture populaire.Ce matériel a du sens si on permet sa circulation dans la culture populaire. Édition des scolies de l'anthologie , faire une requête javascript. Ne répertorier que les commentaires des universitaires serait possible si c'est ce qui intéresse.

Q. : Jahjah pour Marcello et Enrico. Tension entre création de nouvelles formes édtoriales et necessité de les rendre visible socialement, comment travaillez-vous cette dialectique ? Comment placer en équipe cette topologie mentale ? À partir de concepts, de séminaires ? 
Pour Catherine et Benoît : comment vous animez la communauté des chercheurs ?
R: MVR : enjeu, application des lycéens. investir des lieux autres que la communication savante. Partir de ce qu'est l'AP. Livre, idée à laquelle on est si attachés, 2-3 siècles maximum.
Si on travaille avec un médiéviste, il va probablement dire que ce n'est pas un livre (?) 17-18e siècles qui posent les jalons de la forme du livre telle qu'on la conçoit. Récupérer des modèles qui existaient déjà. 
EAM : plus un manuel scolaire qu'un livre à lire et à analyser. Ils ont travaillé sur la traduction des épigrammes à côté de leur enseignement du grec. Il s'agissait ensuite de réinjecter des données dans la base de données, amusant pour les élèves du lycée. 
Concevoir une idée de plateforme, entrer dans le livre papier par la plateforme et la réadapter au fur et à mesure selon les usages fait par les étudiants.
Catherine Muller: pas de contraintes commerciales ni culturelles. Au départ, une vingtaine de notices rédigées par Christian Jacod, maintenant 60taine de notices. Question des liens de parentés, hiérarchiques, associatifs ou non. 
Benoit Epron : les gens qui participent intercalent leur contributions entre d'autres plus reconnues par le système académique car les notices n'ont aucune valeur de ce point de vue. Animation de communauté : entre les gens qui exploitent le même outil. Comme pour les outils informatiques.

Q: (René Audet) : On a beaucoup parlé de bricolage. Comment peut se produire la montée en régime? Cette chaine peut elle devenir, un standard, être réutilisée, etc? Grands moyens, grandes compétences. est-ce qu'on n'est pas constamment repoussé dans notre position de chercheur car on n'a pas les moyens pour mettre dans le marché commercial ce type de recherches ... (??)
R: ingénieure de rcherche du CNRS, l'idée c'est de consolider des outils qui ne contraignent pas les projets, mais des outils qu'on peut adapter facilement en utilisant des standards comme la TEI. 
Benoit : pas du tout l'habitude de valoriser nos projets dans un sens commercial. manque d'interactions avec nos usagés. Comme on ne vend pas, on ne sait pas. Ne s'expriment pas. 
Potentialité du numérique qui nous permettrait d'aller plus loin mais on ne l'utilise pas car "Qui nous lit ?"


##
##Séance {F} « Techniques de la fabrication et de la diffusion des livres en environnement numérique » [SÉANCE PARALLÈLE]


###{F1} Antoine Fauchié, « Git comme nouvel ingrédient des chaînes de publication » 

préambule : communication s'inscrit dans une chaine + gloable sur les chaine de publication et notamment des livres
publication modulaire : diff. com, écrits, mémoires

Git formalise des pratiques peu présente dans l'édition pourtant essentielles pour travailler avec des outils numériques

constats : gestion des fichiers : numérotation très limitée des fichiers
    Points faibles de gestion de projet: étapes définitives, difficilement réversible, manque de visibilité sur un projet, manque de porosité entre les étapes
manque de poorosité entre les étapes de publication et entre les métiers (les humains)

Comment un système de gestion de version pourrait améliorer la fluidité ? 

étapes de la publication : 
    créer un projet
    recevoir un texte
    signaler des modifications
    mettre en commun des modifications (par dif. acteurs d'un projet)
    générer les formats
    
Versionner David Demaree, *Git par la pratique* (git for humans)
versioning permet de penser le processus d'évolution. + avoir son mot à dire sur le processus d'évolution.

le code et texte ont bcp de points communs
remarque : ils sont surtout de plus en plus mélangés

Commandes : init, add, branch/commit, fetch/diff/merge (affiner le travail collaboratif), push
le push permet aussi de déclencher des actions sur certaines branches, pourquoi pas générer un pdf, ou autre.

Utliser Git = utiliser un système conceptuel, mais avec des interfaces compréhensibles. 
Sur la forme j'utilise, peut-être par erreur, une présentation de texte blanc sur fond noir qui donne une impression de terminal.

On peut associer des interface au syst de commandes

Simondon : l'homme est parmi les machines qui opèrent avec lui
L'homme peut et doit agir avec la machine, et ne plus être un simple opérateur 

Sophie Fetro : oeuvrer avec les mahcines numériques
entretenir un rapport crétif aux machines : malgré que ces machines et leur structures productive l'empechent.

Réintégrer les textes : possibilité d'intervenir sur le texte 

Getty Publication : ont intégré un dispositif d'édition entièrement basé sur git.

*Période de questions*

###

###{F2} Jean-Michel Gascuel, « Le livre de demain sera connecté et transmedia ou ne sera pas »

JM Gascuel :  précuseur dans les formats 90, puis reprise du travail à partir des technos "ipad"
Le livre est à un carrefour : le chainon manquant est le livre hors papier, un livre qui se cherche

Un livre qui se cherche... 

Donner accès à un film, à d'autres contenus, comme des séries télévisés, à partir de la couverture, d'une carte postale, d'étiquettes de vin

Papier connecté - le travail d'ajourd'hui sur le livre est un travail de connexion (sans besoin de télécharger des appli). Création d'un chaînon manquant entre le livre papier et connecté. 

Q : est-ce que vous donnez accès à des contenus gratuits seulement ?

Version numérique conçue comme un service. 
On peut faire des contenus gratuits

Q : Gérez-vous les questions de droit d'accès sous droits ?
On travaille sur la blockchain (privée).
On essaie d'harmoniser ce que fait la SACEM.

On travaille sur des problématiques d'éditeur, financières, pas de chercheur.

Harmonisation de ce que fait la SACEM 

Travail avec les bibliothéques et médiathèques  et les écoles

chaînon manquant = sens / sensorialité 

###{F3} Fabrice Marcoux, « La co-édition grâce à Booktype au B7 : mise à l’épreuve du livre numérique » 

Des outils collectifs qui seront à disposition du B7, espace collectif à Pointe-Ste-Charles.
Le Bâtiment 7 -- « Fabrique d'autonomie collective » 
En fait une zone d'expérience et d'innovation sociale -- lieu actif (naissance des CLSC, l'aide juridque et carrefour d'éducation populaire)
Lieu récupéré par la communauté
Transformer ce patrimoine industriel comme une zone d'expérimentation sociale
Pte St-Charles = Tradition de mobilisation 

Le but : appropriation des lieux par la communauté ; services à la communauté : épicerie, CPE, etc.

BOOKTYPE : sert à documenter les pratiques et faire des manuels didactiques pour l'usage des équipements. Utilisé par plusieurs autres organismes. MOOC à l'université de Lyon, rapport d'Amnesty International,. pour booksprint, notamment. 

Pensé comme un réseau :

   *     Groupes
       * Chats, commentaires
       * 

       * Le but est de travailler collectivement sur les projets.Il reste encore à installer sur les serveurs du B7
       * 
Espace de travail

   * Tableau de bord :
   * Gestion du processus collaboratif (groupes et status) 
   * Compliqué : modification du CSS (préétabli)
On ne peut pas changer les CSS, cela limite le travail de postproduction.

Avantage de booktype = logiciel libre 
Accessible sous floss manuals. On peut le faire en communauté...

Culture numérique et innovation sociale
On peut résumer la culture numérique comme une prise de conscience de notre rôle dans la société.
La création d'EPub avec la plateforme Booki : plusieurs usages et niveaux de facilité/difficulté
Un principe près de Wikipédia + un chat. 



Générer des revenus ? 

*Période de questions*

Q de Nicolas Sauret : n'est-ce pa l'écriture en soi qui s'est transformée pour intégrer de plus en plus délémnets programmatifs ?
L'écriture et la pensée de l'édition son indissociables.

R Antoine : C'était déjà présent, mais pas tangible parce que traité au niveau de l'éditeur.

Q Servanne : on a entendu parlé d'un projet de création avec git et le versionning. C'était un projet oulipien, ils utilisaient git comme contrainte créative, as tu d'autre exemple de la poétique de git ?

 R Antoine :  Nous on utilise git pour générer du texte.  Un commit fige l'instant précis de modification: des artistes recueillent les message de commit comme texte: on joue avec le texte sur le texte. Un métatexte poétique. Des artistes ont détourné les commits eux-mêmes pour créer des textes.

La plupart du temps les maisons qui ont intégré Git sont hors littérature.

Q Nicolas: Dans le projet dont parle Servanne, une dimension très assumée de l'industrialisation de l'écriture — rappelle le feuilletonnage du 19e et la manière dont on crée du roman. 

R Antoine: Git peut permettre de faciliter des processus de créaton et d'écriture, ça peut aussi industrialiser le texte — un avenue peut-être moins intéressante. Ça reste un outil. on en fait ce qu'on veut, Peut aussi faciliter le travail entre les humains. 

Emmanuel Charbonneau : Toutes vos intervention réfléchissent sur la manière dont la plateforme dictent les limites et les contraintes, créative peut-être, de la création. Comment l'outil va permettre d'intégrer ce que l'auteur va apporter à la communauté. Les contraintes qu'ils impliquent (les 3 outils présentés) ?

Chez Googtype, notre action n'a rien à voir avec rentrer dans les logiciels. On a été plus loin avec Aquafadas pour voir ce qu'on pouvait en faire, comment les détourner.

1. Les éditeurs papier doivent oublier la pensée papier.

2. Le public réclame des expériences numériques innovantes... et pas chères
3. auteurs veulent de nouvelles possibilités

Quelle est la taille du marché des livres homotétiques ?
[désaccord entre les intervenants]

Au moins , les éditeurs vendent un peu de livres homotétiques, alors que le livre numérique enrichi, ils n'en vendent pas.
Problème d'acceptation de ces livres (homotétiques) par les lecteurs.
Malgré tout, il y a un bond de l'auto-édition. C'est aux auteurs de prendre le marché en main.



##Table ronde {G} « Publier la recherche »  : Participants : Anthony Masure, Julie Blanc, Antoine Fauchié, Lucile Haute, Servanne Monjour et Nicolas Sauret




Q : Selon vous, quels sont les enjeux les plus urgents pour repenser les designs de publication ??
LH : trouver des moyens de valoriser et reconnaître les publications de recherches, des formes ?? construire les contextes dans lesquels on puisse valoriser des formes autres que textuelles 
JB : penser à intégrer des designers afin de penser aux usages possibles, penser la manière d'intégrer le public
SM : penser à l'évaluation par les pairs. Réécrire systématiquement les rapports d'évaluation qu'elle reçoit, c'est ce qu'elle fait au quotidien. Ouvrir l'évaluation par les pairs pour l'amener davantage sur le ton de la conversation que sur le ton péremptoire Retour par les pairs, incommunicable directement des reviewers à l'auteur. 

   * NS : repenser avant tout à la manière dont on écritPrendre en compte les spécifiités du numérique, repenser ce qu'est la revue. Avant tout, un espace public. que l'on peut traverser, habiter
SM : possibilité de rendre public le nom de l'évaluateur, repenser les fiches, repenser les critères sur lesquels on évalue, volonté d'un travail d'annotation bcp plus fin

Est-ce qu'on fait encore des fiches ?
travail très fin d'annotation, questionner directement le texte plutôt que de remplir des cases dans des fiches.
Q : y'a-t-il des différences et/ou similarités entre la France et le Québec dans les enjeux de la publication ?
LH : modification des injonctions qui nous sont faites en temps que chercheur. Injonctions faites aux chercheurs : Publish or perish, 1960. 
Injonctions aux chercheurs :
    années 1960 : publish or perish
    1998, reprise de Grunfeld: demo or die
        Get visible or vanish

deploy or die, ce qui travaille derrière, c'est le dépôt de brevet, le prototypage, de rencontrer le marché, le grand public, une communauté de pairs

NS : ne sait pas, Servanne non plus. Sur les financements, ensemble de politiques institutionnelles très fortes. Au CRSH, quand ta recherche est financée, tu as l'obligation de publier un an après ta publication en libre accès. Erudit, travail pour promouvoir la recherche et la culture francophone. Erudit s'est aussi développé avec l'idée de développer les publications en français
RA : politique obligeant au libre accès entrée en vigueur à partir des publications publiées cette année, pour les articles seulement.  Dépôt institutionnel à Udem, de plus en plus fréquent, contrainte par l'institution donne des résultats. 

Q : comment peut-on développer l'open access, par quels moyens concrets ? 
NS : les revues doivent adopter des licences et des labels. 
JB : on parle d'open access au niveau de la diffusion, on veut aussi que les toutils le soient. Format propriétaire d'adobe ayant disparu: toutes les publications ont disparu. Il faut vraiment que ce soient des opentech, par juste un format propriétaire.Il ne faut pas oublier les formats ouverts et donc les technologies ouvertes. 
NS : aspect récursif dans ce lien avec l'open acess et recure source(?) 

Q :  Quel(s) modèle(s) économique(s) peut-on inventer pour ces nouvelles publications ? 
LH : Capitalisme du savoir fonctionne en circuit fermé. des chercheurs financés par le public, pris dans l'ecosystème créé par les grandes revues. Tres urgent et important de ne pas se faire voler notre propre travail. UQAM, une des premières a renoncer à un grand nombre de bouquets d'abonnements.Travail de conception graphique, interactive qui est important. 
Cela ne veut pas dire que le travail d'éditeur ne doit pas exister
éditer, c'est organiser une discussion, mettre en forme un contenu.
L'auteur a aussi une exigence formelle graphique, le graphiste doit être rémunéré. 
Il ne faut pas laisser cet écosystème aux mains des industriels, vrai effort de la part des académiques pour investir ce champ de recherches
Effort de la part des institutions académiques : investir ce champ, qui est aussi un champ de recherche.

Q : est-ce que les modes de financement au Québec prévoit la prise en charge des frais à la publication en open access ? Est-ce qu'il y a des projets autour de l'optimisation des  interfaces des dépôts de en libre accès?
SM : les revues ont des concours spécifiques, elles peuvent avoir des subventions (c'est le cas à Sens Public), financements qui nous permettent de faire pas mal de développement, d'avoir une équipe d'éditeurs. 40 articles scientifiques en libre accès publiés l'année dernière. Travail d'accompagnement des auteurs. Comment peut-on travailler le texte qui est sur le dépôt via l'UdeM ?
LH : question des interfaces de travail, cruciale. Les interfaces se multiplient


Q : (remarque) tout le budget de l'acquisition de la bibliothèque passe dans l'abonnement aux revues. 
Bibliothèque devient noyautée par des forces extérieures. 
Cette année, on ne peut plus acheter de bouquins
Question de la diffusion de la recherche : Sens Public = revue de pairs offre extrêmement standardisée, mais en même temps le web et les plateformes permettent une offre variée 
Rendre la recherche présente pendant qu'elle se fait. Occuper le territoire de la recherche

NS : Déphasage entre des pratiques d'écriture de chercheurs et d'évaluation de la recherche par les institutions.
Penser des formats éditoriaux qui réintègrent ces pratiques d'écriture et de communication entre chercheurs.
SM : Pour l'instant, système d'évaluation très classique. 

Q  (Epron): dimension disciplinaire pas encore abordée. Comment cela se passe-t-il du côté des sciences exactes ?  Réflexion de ce type sur leurs pratiques de publication?
Mon hypothèse est que, pour eux, le système fonctionne bien. La réflexion de leur côté est-elle aussi dynamique ?
underlineNS : Articles, ressources de données pour Sciences pures, alors qu'en SHS, plutôt en mode conversationnel. On a peut-être envie d'aller vers des modèles conversationnels.Aller vers une espèce de constante de la fragmentation des écrits.underline
underlineLH : du côté des artstes, dans les choses qu'elle produit, il peut y avoir des formes qui sont plastiques d'abord, qui peuvent être touchées et toucher des profanes. Quand elle fait une performance, recherhce de quelque chose qui pourrait être publié. underline
underlineIl peut y avoir des formes qui sont d'abord plastiques, qui peuvent toucher des "profanes". Vulgarisation? Rentre dans la case des organismes subventionnaires.underline

Ecridil 2018, journée 2, le 1er mai 2018 : \url{https://annuel2.framapad.org/p/ECRIDIL2018\_2}
#

