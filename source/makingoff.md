# Ceci est un livre numérique {.frontmatter}

Ne vous fiez pas aux apparences. Cet objet que vous tenez entre les mains, dont
vous pouvez sentir le poids et caresser les pages, est bien un livre numérique.
Il s’agit plus précisément de la version «&nbsp;0&nbsp;» d’un essai de synthèse originale
des échanges et des discussions qui ont ponctué le colloque \#ÉCRIDIL2018.
Plus encore que pour tout livre édité aujourd’hui, le présent ouvrage est issu
d’une production balisée par des outils numériques singuliers, tablant sur une
intense collaboration grâce à des logiciels en code ouvert et des passerelles
technologiques entre web et impression.

 

## Quelques contraintes liminaires

Si le modèle, la forme et les contenus exacts de cet ouvrage étaient encore
inconnus avant le début du colloque, le comité scientifique, sous l’impulsion de
René Audet, Servanne Monjour, Nicolas Sauret et Jean-Louis Soubret, s’entendait en revanche sur
quelques contraintes liminaires. En premier lieu, il n’était pas souhaité
d’éditer des «&nbsp;actes&nbsp;» à proprement parler (cela viendra plus tard), ni
redoubler l’ensemble des archives (visuelles, sonores, textuelles) produites
pendant deux jours. L’idée était plutôt d’ouvrir les problématiques, les
réflexions et les corpus du colloque à un public élargi, le plus vite possible &mdash;&nbsp;«&nbsp;à chaud&nbsp;», juste après le colloque. Outre l’objet livre qui en résulte, ce
travail de booksprint se conçoit aussi comme une expérimentation questionnant
les enjeux théoriques de l’édition numérique. L’équipe de réflexion et de
travail a ainsi adopté des pratiques éditoriales tournées autant que possible
vers une édition collaborative, continue, susceptible de maintenir un dialogue
permanent entre forme et contenu. Il s’agissait ici de garder à l’esprit que les
structures formelles de l’édition et de l’éditorialisation sont en premier lieu
de nature scripturale. L’enjeu du dispositif de production réside alors dans
l’articulation de ces deux écritures&nbsp;: la structure-code et le discours.

 

## Atelier d’idéation {.break-frontmatter}

Une fois ces contraintes définies, une équipe d’éditeurs s’est attachée à
collecter, à prendre en note et à synthétiser les contenus présentés tout au
long du colloque. Outre une page collective de prise de notes (framapad) ouverte
à l’ensemble des participants, les éditeurs ont travaillé à la constitution de
fiches-synthèses consacrées à chaque communication. Lors d’un atelier d’idéation
animé par Jean-Louis Soubret, l’équipe d’éditeurs, accompagnée de plusieurs
participants, s’est employée à définir la problématique à laquelle le livre
devrait répondre et à imaginer la forme la plus à même de répondre à cette
problématique. À partir de plusieurs sources d’inspiration –&nbsp;le cabinet de
curiosités, l’anthologie, l’index...&nbsp;–, l’atelier d’idéation a fait émerger un
concept de livre composé de fragments textuels et visuels, lesquels seraient
agrégés en fonction de mots-clés, de manière à faire émerger de nouveaux
parcours de lecture entre les interventions.

## Booksprint

Sur les bases de l’atelier d’idéation, le booksprint a été lancé, hébergé à la
Chaire de recherche du Canada sur les écritures numériques (Université de
Montréal). Deux jours seulement pour un programme ambitieux&nbsp;:

1.  Catégorisation&nbsp;: émergence d’une première série de tags à partir des
    ressources produites pendant le colloque
2.  Identification des fragments pertinents : slides, citations, références,
    notes, visuels (photographies, schémas), etc.
3.  Tagging&nbsp;: étiquettage collaboratif de tous ces fragments
4.  Gabarits&nbsp;: création des gabarits pour chaque type de fragment
5.  Templates&nbsp;: création des templates md, html et css
6.  Requête&nbsp;: extraction de tous les fragments taggés, en lien avec les
    catégories identifiées
7.  Mashup&nbsp;: sélection et réagencement des fragments pour chaque catégorie,
    rédaction des textes originaux, ajustement des catégories
8.  Édition&nbsp;: édition continue et simultanée à la production des entrées


## La chaîne éditoriale {.break-frontmatter}

![chaine éditoriale](img/ecridil-booksprint-workflow.png)
 
Les éditeurs ont utilisé un archipel d’outils, tous basés sur des standards, des
formats et des codes source ouverts&nbsp;:

- framapad.org
- framagit
- markdown
- pandoc.org
- html5
- css-print
- pagedmedia.org, initiative opensource dont nous
avons été les heureux alpha-testeurs. L’outil a été mis à disposition par Julie Blanc,
co-conceptrice et développeuse de pagedmedia et participante à ÉCRIDIL.

 

## L’index {.break-frontmatter}

Au terme du booksprint, nous avons donc posé les bases d’un index extensible,
reconfigurable et imprimable du colloque.  Cet index a vocation de rendre compte
des idées, des concepts et des notions les plus importantes, sous une forme
fragmentaire, dialogique &mdash;&nbsp;presque chorale&nbsp;&mdash; mais aussi littéraire. Cette première proposition se veut bien évidemment
totalement ouverte &mdash;&nbsp;elle se conçoit par ailleurs autant comme un acte
d’écriture que de lecture&nbsp;: les contenus synthétisés et agrégés ici reflètent
d’abord l’interprétation de l’équipe d’éditeurs qui ont travaillé, en un temps
minimal, sur ce booksprint&nbsp;: Julie Blanc, Louis-Olivier Brassard, Joana
Casenave, Jeanne Hourez, Ximena Miranda, Servanne Monjour, Marie-Odile Paquin, Nicolas Sauret,
Jean-Louis Soubret, Lilie Pons et Emin Youssef.

Cette version est dite «&nbsp;0&nbsp;», car nous souhaitons poser les bases d’un index
ouvert des notions-clés du colloque, que nous vous invitons à enrichir et à
discuter (en ligne), et que vous pouvez sélectionner et ré-agencer à votre guise
pour construire votre propre anthologie (imprimable). Ce livre, réalisé en un
temps éclair (une quinzaine de jours), se conçoit donc comme une proposition
conceptuelle et éditoriale inspirée des présentations et des discussions qui ont
ponctué l’événement ÉCRIDIL.
