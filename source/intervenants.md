# Intervenants {.postmatter}

## Communications

|Intervenant.s|Portrait|
|:--|:-:|
|Marc Jahjah (Univ. de Nantes), Clémence Jacquot (Univ. de Montpellier)|![légende de l'image][jahjah_portrait]|
|Oriane Deseilligny (Univ. Paris 13)|![légende de l'image][deseilligny_portrait]|
|Julien Drochon (École Supérieure d’Art des Pyrénées)|![légende de l'image][drochon_portrait]|
|Nolwenn Tréhondart (Univ. de Lorraine), Alexandra Saemmer (Univ. Paris 8)|![légende de l'image][trehondart_portrait]|
|Françoise Paquienséguy (Sciences Po Lyon)|![légende de l'image][paquienseguy_portrait]|
|Anthony Masure (Univ. Toulouse - Jean Jaurès)|![légende de l'image][masure_portrait]|
|Jean-Louis Soubret (CNAM)|![légende de l'image][soubret_portrait]|
|Lucile Haute (Univ. de Nîmes), Julie Blanc (EnsadLab)|![haute][soubret_portrait]|
|Marcello Vitali-Rosati (Univ. de Montréal), Enrico Agostini-Marchese (Univ. de Montréal), Elsa Bouchard (Univ. de Montréal), Joana Casenave (Univ. de Montréal), Arthur Juchereau (Univ. de Montréal), Nicolas Sauret (Univ. de Montréal)|![agostini][agostini_portrait]|
|Benoît Epron (ENSSIB), Catherine Muller (ENSSIB)|![epron][epron_portrait]|
|Cécile Meynard (Univ. d'Angers), Elisabeth Greslou (Univ. de Grenoble)|![meynard][meynard_portrait]|
|Antoine Fauchié (éditeur)|![fauchier][fauchie_portrait]|
|Fabrice Marcoux (ASTED)|![marcourx][marcoux_portrait]|
|Jean-Michel Gascuel (Googtime)|![gascuel][gascuel_portrait]|
|Emmanuel Souchier (Celsa)|![souchier][souchier_portrait]|
|Olivier Charbonneau (Univ. Concordia)|![charbonneau][charbonneau_portrait]|
|Arnaud Laborderie (BnF)|![laborderie][laborderie_portrait]|
|Elsa Tadier (Celsa)|![tadier][tadier_portrait]|
|Dominique Raymond (Univ. de Montréal)|![raymond][raymond_portrait]|
|Christine Develotte (ENS de Lyon), Mabrouka El Hachani (Univ. Jean Moulin Lyon 3) |![develotte][develotte_portrait]|
|Florence Rio (Univ. de Lille), Bertrand Meslier (AdrénaLivre), Eric Kergosien (Univ. de Lille)|![rio][rio_portrait]|
|Nathalie Lacelle (UQAM), Marie-Christine Beaudry (UQAM), Prune Lieutier (UQAM)|![lacelle][lacelle_portrait]|

## Tables rondes

|Intervenant.s|Portrait|
|:--|:-:|
|Lucile Haute (Univ. de Nîmes), Ariane Savoie (UQAM), Nolwenn Tréhondart (Univ. de Lorraine)|![légende de l'image][savoie_portrait]|
|Anthony Masure (Univ. Toulouse - Jean Jaurès), Julie Blanc (EnsadLab), Antoine Fauchié (éditeur), Lucile Haute (Univ. de Nîmes), Servanne Monjour (Univ. de Montréal), Nicolas Sauret (Univ. de Montréal) |![légende de l'image][monjour_portrait]|
|Bertrand Gervais (UQAM), Benoit Bordeleau (UQAM), Nathalie Lacelle (UQAM), Prune Lieutier (UQAM), Annabelle Moreau (éditrice)|![légende de l'image][gervais_portrait]|

## Stands

|Intervenant.s|Portrait|
|:--|:-:|
|Jimmy Gagné (éditeur)|![légende de l'image][gagne_portrait]|
|Nicolas Tilly (École Supérieure d'Art et Design d'Orléans)|![légende de l'image][tilly_portrait]|
|Gisèle Henniges (designeur)|![légende de l'image][henniges_portrait]|
|Marcello Vitali-Rosati (Univ. de Montréal), Arthur Juchereau (Univ. de Montréal), Servanne Monjour (Univ. de Montréal), Nicolas Sauret (Univ. de Montréal), Joana Casenave (Univ. de Montréal) |![légende de l'image][vitali_portrait]|



[agostini_portrait]: img/photos/agostini.jpg
[agostini_slide]: img/slides/angostini_slide01.png
[charbonneau_portrait]: img/photos/charbonneau.jpg
[charbonneau_slide]: img/slides/charbonneau_slide01.png
[deseilligny_portrait]: img/photos/desseilligny.jpg
[deseilligny_slide]: img/slides/Deseilligny_slide01.png
[develotte_portrait]: img/photos/develotte.jpg
[develotte_slide]: img/slides/devlotte_slide01.png
[drochon_portrait]: img/photos/drochon.jpg
[drochon_slide]: img/slides/drochon_slide_01.png
[epron_portrait]: img/photos/epron.jpg
[epron_slide]: img/slides/epron_slide01.png
[fauchie_portrait]: img/photos/fauchie.jpg
[fauchie_slide]: img/slides/fauchie_slide01.png
[gagne_portrait]: img/gagne_portrait.jpg
[gagne_slide]: img/slides/gagne_slide01.png
[gascuel_portrait]: img/photos/gascuel.jpg
[gascuel_slide]: img/slides/gascuel_slide01.png
[gervais_portrait]: img/gervais_portrait.jpg
[gervais_slide]: img/slides/gervais_slide01.png
[haute_portrait]: img/haute_portrait.jpg
[haute_slide]: img/slides/haute_slide01.png
[henniges_portrait]: img/henniges_portrait.jpg
[henniges_slide]: img/slides/henniges_slide01.png
[jahjah_portrait]: img/jahjah_portrait.jpg
[jahjah_slide]: img/slides/jahjah_slide01.png
[laborderie_portrait]: img/photos/laborderie.jpg
[laborderie_slide]: img/slides/laborderie_slide01.png
[lacelle_portrait]: img/lacelle_portrait.jpg
[lacelle_slide]: img/slides/lacelle_slide01.png
[marcoux_portrait]: img/marcoux_portrait.jpg
[marcoux_slide]: img/slides/marcoux_slide01.png
[masure_portrait]: img/photos/masure.jpg
[masure_slide]: img/slides/masure_slide01.png
[meynard_portrait]: img/photos/meynard.jpg
[meynard_slide]: img/slides/meynard_slide01.png
[monjour_portrait]: img/photos/monjour.jpg
[paquienseguy_portrait]: img/photos/paquienseguy.jpg
[paquienseguy_slide]: img/slides/paquienseguy_slide01.png
[raymond_portrait]: img/photos/raymond.jpg
[raymond_slide]: img/slides/raymond_slide01.png
[rio_portrait]: img/rio_portrait.jpg
[rio_slide]: img/slides/rio_slide01.png
[sauret_portrait]: img/photos/sauret.jpg
[savoie_portrait]: img/savoie_portrait.jpg
[savoie_slide]: img/slides/savoie_slide01.png
[soubret_portrait]: img/photos/soubret.jpg
[soubret_slide]: img/slides/soubret_slide01.png
[souchier_portrait]: img/photos/souchier.jpg
[tadier_portrait]: img/photos/tadier.jpg
[tadier_slide]: img/slides/tadier_slide01.png
[tilly_portrait]: img/tilly_portrait.jpg
[tilly_slide]: img/slides/tilly_slide01.png
[trehondart_portrait]: img/photos/trehondart.jpg
[trehondart_slide]: img/slides/tréhondart_slide01.png
[vitali_portrait]: img/photos/vitali-rosati.jpg
[vitali_slide]: img/slides/vitali_slide01.png
