# Contributeurs au livre {.postmatter}

## Éditeurs

René Audet,
Julie Blanc,
Renée Bourassa,
Louis-Olivier Brassard,
Joana Casenave,
Jeanne Hourez,
Ximena Miranda,
Margot Mellet,
Servanne Monjour,
Marie-Odile Paquin,
Lilie Pons,
Nicolas Sauret,
Jean-Louis Soubret,
Emin Youssef.


## Atelier

Les éditeurs et

Emmanuel Château-Dutier,
Oriane Deseilligny,
Antoine Fauchié,
Jean-Michel Gascuel,
Clémence Jacquot,
Arnaud Laborderie,
Arthur Perret,
Françoise Paquienséguy,
Dominique Raymond,
Florence Rio,
Emmanuel Souchier,
Elsa Tadier.



<!-- Marie-Christine Beaudry,
Benoit Bordeleau,
Elsa Bouchard,
Olivier Charbonneau,
Christine Develotte,
Julien Drochon,
Benoît Epron,
Jimmy Gagné,
Bertrand Gervais,
Elisabeth Greslou,
Mabrouka El Hachani,
Lucile Haute,
Gisèle Henniges,
Marc Jahjah,
Arthur Juchereau,
Eric Kergosien,
Nathalie Lacelle,
Prune Lieutier,
Fabrice Marcoux,
Anthony Masure,
Bertrand Meslier,
Cécile Meynard,
Annabelle Moreau,
Catherine Muller,
Françoise Paquienséguy,
Dominique Raymond,
Florence Rio,
Alexandra Saemmer,
Ariane Savoie,
Jean-Louis Soubret,
Nicolas Tilly,
Nolwenn Tréhondart, -->
