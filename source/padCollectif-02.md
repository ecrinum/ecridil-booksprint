#E#Ecridil 2018 - 1er mai

 
Ecridil 2018, journée 1, 30 avril : \url{https://annuel2.framapad.org/p/ECRIDIL2018}


##Séance {H} : « Fondements repensés »

###{H1} Emmanuël Souchier, « Éléments pour une épistémologie du design en contexte numérique »

 
Recentré sur deux questions : qu'est.ce qu'un livre et comment est-il appelé à se transformer en contexte numérique?
Qu'est-ce qu'un livre? comment prendre cette question "au sérieux" ?
Retenir une leçon de Queneau  (Kant?) Lorsque Kant l'évoque en 1796, il est travaillé par des préoccupation analogues liées au droit et à la propriété intellectuelle. Profiter de l'injonction technologique posée par l'introduction du numérique dans le corps social et en retenir une dimension positive. 
 

Sortir des logiques marchandes et juridiques : retenir une dimension positive. 
 
dire que le numérique nous permet de repenser de manière reflexive les usages et les discours est une tarte à la crème.
le numérique nous oblige de repenser l'ancien monde : soit.
mais profiter de l'injonction numérique pour replacer le livre dans l'espace qui est le sien : la culture, l'histoire
 
Replacer le livre dans l'Espace qui est le sien : celui de la culture et de l'histoire
 
nécessités anthropologiques
quelle nécessités humaines et communicationnelles, et préoccupations culturelles le livre a prétendu répondre au cours de ces 2 millénaires
 
histoire du codex... (approche très ethnocentriques, d'autres cultures existent)
 
en quoi le numérique  a-t-il nécessairement un rôle à jouer dans cette histoire ?
 
Logique de la question, sous l'emprise intellectuelle de l'histoire du livre
 
Emprise intellectuelle héritée de l'histoire du livre
Représentations des connaissances.
Songer aux modalités de représentation des connaissances : intrasèquement lié à l'histoire du codex.
le plus difficile est de sortir des cadres institutatns, sortir de la pensée du codex 
Cadres instituants, la pensée du codex
 
nous pensons à travers les formes du codex. 
admirable machinerie intellectuelle qu'est le codex
il faut en sortir pour aborder le numérique
comprendre sa spécificité, comprendre le livre pour reformuler son questionnement. : Ce n'est pas le livre qui est transformé par le numérique, mais la relation intime de l'écriture à ses supports et à ses usages
 
Nous avons aujourd'hui des formes hybrides et passagères mais  : Le livre comme matrice culturelle ne trouvera dans le numérique que le prolongement de ce qu'il a déjà envisagé dans sa forme même : 
    
Ne pas rester prisonniers des formes instituantes , s'en extraire car ces cadres agissent comme... 
 
Théâtre de mémoire, à la fois support matériel et technologie de l'intellect, assume corporellement la pérennité et la matérialité? de l'écriture
technologie de l'intellect au sens de Goody
 
Le livre offre à l'écriture underlineune portabililtéunderline : 
En l'assignant à une forme et des formats, le livre répond aux nécessités anthropologiques de l'écriture.
En assignant le livre à une forme, des supports, un format, le livre répond aux questions de l'écriture
 
Travailler sur une ouverture de la notion d'écriture au sens anthropologique - appel à travaux
 
Porte la présence du geste, de la main. Trace des voix et des corps. Modèle la manière de voir le monde
émancipation de son corps, de l'espace et du temps : c'est la première techno de l'intellect de l'humanité
Sortir de l'échange direct - l'écriture  = LA première technologie de l'intellect de l'humanité. On n'arrive plus à en sortir tellement elle nous a enveloppé
 
L'écriture au sens où nous l'entendons permet à l'Homme de communiquer, de s'émanciper : sortie du contexte et de la situation de l'oralité ==> première technologie de l'intellect. C'est à l'homme, l'autre moi-même qui s'exprime à sa place, elle se déploie dans l'espace. À travers le mythologique, au delà du temps et de l'imagination - vers l'Autre (!). 
 
question :::::::::: 
l'écriture vient d'un besoin de compter. => pas forcément. Ça c'est la théorie de Goody surtout, il me semble. Mais chez AM Christin on trouve une autre façon de concevoir l'écriture (origine de l'écriture dans l'image).  Ca m'intéresse
je voulais faire le lien-tarte à la crème écriture/code ben mange ta tarte et toi, va faire un selfie -miam.
::::::::::
 
Goody : L'homme peut s'adresser à l'AUTRE
Le Codex offre un véhicule petit volumen polymorphe et plastique  
 
 
Attendus anthroplogiques de l'écriture et du codex : 
    effort pour sortir de l'impensé, il nous faut distinguer le livre de l'écriture, sa matérialité propre : "l'image du texte"
 
Livre et écriture sont si indissociablement liés dans notre culture/époque; nous devons les dissocier^^, de reconnaître l'écriture sous *toutes ses formes*; **penser l'écriture sans le livre**.
 
Nécessité communicationnelle vitale pour homo sapiens
nécessité anthropologie de la communication : l'écriture : circulation dans le corps social, toujours réductible au support/dispositif matériel
Numérique au service de la communication?
 
intrusion du numérique dans le registre du livre : chargé d'un fort capital symbolique ancré dans l'histoire et les mentalités
chargé d'un très fort capital symbolique, efficacité symbolique aussi
 
 
Ce que le numérique touche et vise n'est pa le livre mais l'écriture dont il se nourrit à travaers les dispositifs de textualisation
Ce que le numérique vise : pas le livre en soi, comme véhicule, mais bien l'écriture à travers la textualisation -- processus d’accessibilité.
textualisation pour se rendre accessible aux humain
 
Lecture : activité de lecture et d'écriture : tech intellectuelle fonctionne à partir de ce deux activités
Dispositifs de technologie de l'intellect qui fonctionnent à partir de la lecture
 
L'irréductibilité du livre - ce qu'il a et que d'autres dispositifs n'ont pas.
Ce qui est irréductible du livre vis-à-vis de tout autre dispositif... ? 
 
 
Les questions portent sur la représentation des connaissances

   * Représnetation des connaissances
   * DiffusionUne double exigence : tension entre l'économie capitaliste du flux (d'information) et la question des formats qui nécessitent une réification.
 
autre hypothèse : le Graal de l''industrie num. : réside dans ce que le codex a mis au point à travers les siècles : des communs que les industriels cherchent à faire émerger grâce aux usages démultipliés par les utilisateurs. : c'est la pratique des usagers qui est visée
Ce ne sont plus le livre ni l'info qui sont visés par l'industrie,  mais la pratique des usagers
 
Image crue de notre société dans laquelle tout peut être marchandisé
 
la relation que l'homme a au support : fonction de communication vitale qui est une des conditions de survie de l'espece humaine. => ENjeu sociétal 
L'industrie numérique a la nécessité de  développer de nouvelles formes de médias et médiations :
    - internet
    - smartphone qui deviennent la zapette de nos sociétés
 
les médias produisent l'équivalent des communs : mais deviennent des savoirs collectifs marchandisés, ce ne sont plus des biens communs.
 
Le livre a pu forger des cadres d'efficacité au terme de x siècles 
 
===
intéressant pour publishing sphere: le livre comme espace collectif et commun est remplacé par l'industrie du numérique qui devient l'espace principal de communication en marchandisant l'espace commun 
==============
 
 ================================
 !!!! Première partie copiée à partir, environ, de la 3000e sauvegarde de l'historique dynamique.  !!!!!!!
 =========================================
Anonymat des artisans, auteurs, etc.
Silex chargé de gestes qui en font l'efficacité
 
 Il y a donc cette interrogation qu'il faut poser d'un point de vue économique, politique et sociétal. Par rapport au livre, on reprend cette histoire longue d'un médias qui au fil des siècle a forgé ses cadres à travers travail, lecture, et je la met en tension avec l'appropriation marchande de la pratique des consommateurs qui permettent des jeux de reconfiguration sémiotique des médias reformais: design, affordance, sont-ils des termes si contemporains? 
 
Média qui a forgé la lecture et les lecteurs sur des siècles
 
Fonction communicationnelle de l'écriture
L'information en soi n'offre aucune prise, aucune réalité en contexte  numérique si elle n'est pas socialisée
 
 
Il faut revenir à la fonction communicationnelle de l’écriture, car l’information en soi n’orffre en traitement numérique aucune réalité si elle n’est pas socialisée par l’écriture
 
 
 nous sommes passés des termes suivants : citoyens, usagers, consommateurs pour devenir des data face à la consomation du livre
 
Il faut revenir à quatre schemes:
- mémorisation 
- circulation 
- narrativisation 
- visualisation 
permettent la compréhension sensorielle du corps, nous ne sommes que ça
 
4 Points constitutifs de l'écriture et de ses enjeux fondamentaux d'un point de vue communicationnel et anthropologique
 
 
###{H2} Olivier Charbonneau, « Encoder les communs de la lecture »

*-- Militant et chercheur : bibliothécaire et chercheur à Concordia, docteur en droit. « culturelibre.ca » : blog qui influence beaucoup la réflexion sur le commun*
==> LE MATÉRIEL ET SUR LES CARNETS
Bibliothécaire de puis 15 ans, j'ai eu le privilège de voir Rome bruler == > L'avènement d numérique est vu comme un trou noir pour le milieu du livre --- une catastrophe qu'on découvre a posteriori.
 
Approches pragmatique et conceptuelle
 
Comme s'approprier le numérique ? La question du droit d'auteur dans le numérique ? L'ignorer, le fuir ?
Une approche pragmatique ui le conceptualise comme un chantier : abjet auquel il faut réagir pour passer de l'avant. 
==> Présentation qui est le résultat du projet de doctorat.
 
(**1) Le droit d'auteur n'est pas un problème juridique, mais underlineun problème de gouvernanceunderline. **
Certes, des dispositions légales, mais on interroge souvent les moyens institutionnels ou organisationnels du droit d'auteur.. Associations professionnelles qui ont des prises de position par exemple ?
 
Politiques institutionnelles pour les exceptions au droit d'auteur?
2 approches conceptuelles du droit d'auteur :
 
(A) Le droit d'auteur comme bien privé (droit de propriété littéraire comme au XIXe siècle)
Carcan néolibéal de la conception du droit d'auteur
(B) L'utilitarisme (Common law)
 
distinction de l'approche entre les pays de droit civils et les pays anglo-saxons. DIstinction vient de l'histoire du droit
La conception du droit d'auteur dans la francophonie pose problème à notre appropriation du numérique. 
Distinction à la fois langagière et juridique. 
 
Au Canada, les deux régimes juridique et le Québec comme microcosme -- labo juridique où coexistent les deux modèles (provin. et fédé.)
 
Droit d'auteur :
    un bloquage épistémologique
    Un droit immuable qui passe par structures. 
Copyright :

   * Réfléchit le commun, la théorie cybernétique, voir le droit d'auteur comme système complexe où les relations priment. 
Le chantier -- s'approprier les questions épistémo. sur le plan des institutions

   *  
   *  résultats de mon travail de doctorant et invitaion à décortiquer le droit d'auteur
Le droit d'auteur = Ce n'est pas un problème juridique, mais de gouvernance
Iivible
Projet Micorfiches associe un chercheur à un illustrateur
**(2) Projet microfiches **\url{**http://microfiches.org/**}** : mobilisation des connaissances**
 
image qui représnete la main inv
La main invsible pour réfléchir à l'émergence du marché : 
Réfléchir à l'émergence du marché. Dans le numérique, vu qu'on a une nouvelle approche, on doit comprendre que le document est maintenant dissocié de son droit. 
 
Sur la matérialité numérique :le document est dissocié de son droit 
- Le **papier **: contrainte à l'utilisation du droit
- Le **numérique **: les droits et le document est imbriqué ... Une stipulation formelle associée au document précis sur les droits. Et impose une redéfinition des marchés. 
Jusque là, la matérialité du livre donnait des contraintes physiques. Dans le numérique, les documents sont imbriqués.
Stipulation formelle associée à ce qui est permis ou non. 
Dans les bibliothèques, division matérielle des documents et des droits asscoiés que nous n'avons plus avec le numérique
Réification qui amène une imbrication du document et du droit, imposant une redéfinition des marchés. 
La licence impose des particularités (exemple du centre Pompidou qui cesse de faire des livres numériques, expliqué par la licence d'Apple).
Avant, le marché était organisé par le document, maitnenat il l'est par association d'une licence
 
L'équation proposée : si le marché était basé sur le document, aujourd'hui le document est imbriqué à un droit imposé par license. = symbiose nécessaire pour comprendre et redéfinir la chaine éditoriale du livre.
Il faut redéfinir toutes les étapes de la chaîne du livre. 
 
Réflexion sur la structure potentielle du marché numérique : 
Comment obtenir un nouveau marché ? 
 
On observe les contrats de licences des biblio. (p. ex. au Qc, 80 millions pour abonnement dont 75% pour le numérique)
Comment les contrats sont organisés ?
Au québec, les universités dépensent 80 millions de dollars pour les documents et les 3/4 sont consacrés au numérique. 
 
3/4 des budgets sont alloués pour le numérique
Métadonnées juridiques : 
Analyse des licences et extraction des métadonnées juridiques

   * p. ex. UL : 721 lignes de données où sont classées 7 classes de droits pour analyse statistique. Constat : les biobliothécaires ne spolient pas les éditeurs. Une fiche pour chaque licence dans lesquels chacun des contrats sont dépouillés en 7 classes de droit
Analyse statistique
Objectif : démontrer que les bibliothèques ne lèsent pas les éditeurs
 
Théorie néolibérale : pourquoi la biblio paie ? 
La bliblio a une mission si importante qu'elle doit... [...] ?
Bibliothèques codifiées à la fois dans la loi et dans le marché, différemment
Pourquoi une bibliothèque paierait pour quelque chose qu'elle a en exception ? [?]
-> Les bibliothèques considèrent leur mission tellement importnate qu'elles sont prêtes à payer, voire à ne pas respecter la loi. Elles codifient leur mission dans le marché (par contrat) et dans les exceptions de la loi (régime de droit public).
 
Ces métadonnées permettent de dire le droit sur le document numérique. Il faut réfléchir par quelle plateforme on va rendre nos documents numériques. Parfois, ces plateformes interdisent l'acquisition par les bibliothèques.
Nécéssité de créer des plateformes qui permettent de faire exister les travaux dans les bibliothèques. Eviter les plateformes qui interdisent l'acquisition par les bibliothèques.
Il faut donc réfléchir aux plateformes... Si ce n'est pas accessible en bibliothèques, ce n'est pas un livre -- ce n'est pas une documentation. Le ctalogue de la biblio. devient catalogue où les métadonnées spécifient les droits de licence et doivent créer des plateformes pour créer le livre. Catalogue de biblio = proto-chaine de bloc de ce que peut être un commun pas encore numérique
Les plateformes disent le droit
Qu'est-ce que le livre numérique ? C'est un livre qui peut se retrouver en bibliothèque
Réfléchir à commnet ajouter une strate de métadonnées à son catalogue pour justifier ??
 
C'est ça le chantier des bibliothèques : une proto chaine de bloc dans un commun qui n'est pas encore numérique
Catalogue = proto-chaîne de blocs dans un commun qui n'est pas encore numérique
 
 
4. Réfléchir aux nouveaux arrangements, marchés et plateformes.
 
deisgn institutionel
Design institutionnel. En plus des interfaces techonologiques, il faut réfléchir comment els agissements des communautés s'insèrent dans la documentation numérique et comment bâtir un marché sur des contrats toujours à redéfinir. 
 
Libre accès comme hypothèse de travail, modèle fondamental introduit par le numérique
Concept de gratuité comme étant la plusvalue que l'on introduit dans le numérique. L'hypothèse est le libre accès mais comment réfléchir à avoir une approche institutionnel autre où la valeur est repensée
ce n'est pas nécessairement celle que nous avons à rallier
 
Le travail n'est pas complété -- parce que le problème en est un de GOUVERNANCE. 
Les GAFAM ont compris comment récupérer des milliards du libre accès
 
###{H3} Arnaud Laborderie, « Du livre enrichi au livre augmenté : les enjeux d’une clôture numérique » 

============================
Récupéré à la sauvegarde 6577 de l'historique dynamique
===============================
Enseignant chercheur et éditeur à la BNF
 
VIsuel sur YouTube (voir « corpus »)
 
(changement au programme)
Edition enrichie de Candide, une coédition de BNF avec Orange et fondatin voltaire. Une rehcerche sur les mutations du livre, de la lecture. 
 
Recherche théorique sur les implications de la lecture...
Propose 3 entrées : le livre, le monde, le jardin. Application Ipad. 
Du point de vue de l a conception, nous avons voulu modeler des usgaes
codex
carte
arbre
Modélsier des usages et proposer au lecteur : lire, explorer, éditorialiser. 
Ces différents formes se combinent pour proposer une nouvelle app

La carte se présente comme un métaphore du web. Multimodal et multimédias : herméneutique du jardin [?]. Possibilité de paramétrer les fonctions de l'appli pour choisir son mode de lecture. Paramétrable et personnalisé. Les expérimentation montrent un changement de posture, la mise en oeuvre des pratiques de recherche comme sur le web, montrer que l'entrée privilégiée se fait sur un mode sonore.

L'entrée privilégiée se fait par l'audio, le conte lu par Denis Podalydes

Média qui a forgé la lecture et les lecteurssur des siècles

Fonction communicationnelle de l'écriture
L'information en soi n'offre aucune prise, aucune réalité en contexte  umérique si elle n'est pas socialisée
C'est ce que les enseignants ont constaté en observant leurs élèves


L'app. interroge forntière : linéarité et cloture
C'est la cloture que je voudrais discuter aujourd'hui

E Souchier  : livre numérique est un oxymore

« Le livre est un objet clos » ... Pas toujours -- il n'est clos que dans le moment et l'espace de l'imprimé qui n'est pas l'unique forme du livre. 

Média qui a forgé la lecture et les lecteurssur des siècles

Fonction communicationnelle de l'écriture
L'information en soi n'offre aucune prise, aucune réalité en contexte  umérique si elle n'est pas socialiséee n'est pas toujours le cas : uniquement dans un certain temps et espace : l'imprim
S'affranchir du modèle de l'imprimé pour penser le livre numérique comme un objet nouveau. 
DE quoi parle ''on lorsqu'on parle de cloture ?
Cloture : peut-on distinguer plusieurs types/formes de clotures ? 

La cloture du livre n'implique pas nécessairement la cloture du texte, et la cloture du texte pas forcément celle de l'oeuvre. 

Cloture ?
Du livre, du texte, de l'oeuvre...

La cloture du livre n'est pas cloture du texte et non plus cloture de l'oeuvre. Avec le volumen, pas de cloture, aucune percpetion des limites et des extrêmités... Un flux du texte à l'image de la parole qui le consigne. 

Ni cloture physique, ni cloture symbolique, mais un flux du texte. 


Avec le volumen, c'est un défilement (une performance du texte)
Le codex vient définir des clôtures, une spatialisation de la parole


Distinguer deux typesDistinguer 2 types de codex : 

   * Manuscrit (matériel, intellectuel, spirituel) / - manuscrit [cloture = condition de l'Avènement du texte]
   * Le livre médiéval est un espace ouvert par la glose, etc.
   * Texte qui est toujours inachevé, qui échappe à la cloture matérielle du livre
   * Le texte toujours inachevé (anthologie, corpora)
   * L'oeuvre déborde l'espace du livre et vice-versa... L'imprimerie isole le texte -- organise l'unité discursive et fusionne livre et appareil textuel : une clôture auparavant étrangère au texte. Plusieurs textes cohabitent

L'imprimerie clot le texte


C'Est l'imprimerie qui clot le texte et le renfoerme sur lui-même (le débarassant des gloses, images, en fusionnant l'unité textuelle et le livre.). Elle normalise et renferme le texte sur lui-même en lui imposant une fermeture qu'il ne possédait pas. 

Avec des exempliares tous identiques
La cloture deu texte impose  t'ele une cloture de l'oeuvre
 ?
La notion de clôture narrative en littérature

   * La norme : la clôture comme fermeture. L'oeuvre répond à une intentionnalité, conclusive et cloturante. Le livre appelle un dénouement. Pour Aristote, la clôture donne unité au récit... Mortimer : « la clôture est un fait de la lecture »
En litt. la norme est la cloture comme fermeture d'abord parce que l'oeuvre répond à une intention de l'auteur qui est conclusive et cloturante. 
Intention / intentionnalité de l'auteur = concluante, dénouement. 
Pour Aristote = cloture donne unité au récit

Le récit appelle un dénouement, la fin d'un récit. La cloture est créatrice d'ordre, de sens. Le sujet peut-il exiter sans la fin ?

La cloture interroge la fin mais aussi les fins. Parler de cloture = parler de sens
Une histoire peut-être exister sans sa fin ? Même si certains textes ouvrent le texte en guise de clôture.

Cette norme de la cloture comme fermeture n'est pas exclusive. 
Mortimer : il y a toujours cloture car c'est un fait de la lecture. 
L'absence de cloture peut être délibérée
Le Nouveau Roman nous a familiarisé avec la non-cloture, on s'est familiarisé avec les non-fins. 
Principe de morcellement et de fragment : volonté d'échapper à un mode clôt. 

La clôture contemporaine (problématique) du point de vue du lecteur

   * 
Aujourd'hui, lecteur tiraillé entre attente et refus de la clôture - désir d'une certaine mise en ordre, mais refus d'une résolution du monde trop prescriptive. 
Le livre, la litt. sont tiraillés entre une volonté d'ouverture et ??

Cloture numérique : matérielle et symbolique. 
- Matériel
- Symbolique

L'objet livre est clos mais pas fermé : une dialectique entre clôture/fermeture matériel/symbolique
Double fonction d'ouverture et de fermeture 
C'est dans cette dialectique que se pose la question du livre de demain 

Quels sont les conditions et les enjeux de la cloture numérique ?

Application Hopper : bon exemple de cloture matériel de l'objet livre numérique et de l'absence d'une cloture symbolique car l'appli propose de multiple parcours sur un arbre arborescent. 
Les livres transmédia où la narration est inépuisable, infinie. Dans ces dialectique, ce négocient les forntière du livre dans l'espace numérique. 
Sans clôture, on est plus dans le livre -- c'est la condition du livre, d'un champ de connaissance et de signification. Il faut que ce soit un ESPACE que le lecteur arpente.
La cloture définit un espace que le lecteur peut arpenter
Dualité dans laquelle se négocient les frontières et les limites du livre numérique. 
La cloture est la condition entre signification et interprétation, voire d'un champ de connaissances. 


Clôture ne signifie pas fermeture : permet l'enrichissement et l'augmentation

remarque : est ce que la clôture n'est pas une question de structure et de format. Dans ce cas, l'éditorialisation et la circulation du "livre" reste permise (et est

**Distinction entre livre enrichi et livre augmenté au regard de cette cloture : **

   * Enrichissement : pratique issue du livre et de l'édition critique  texte, contenu et données. 
   * Avec le numérique, nouvelle dimension : hypertextes, médias, etc. Enrichissement du texte par le lecteur. L'enrichissement clot le texte sur sa propre histoire. 
   * Enrichissement est issu de la  lecture que l'augmentation ouvre vers d'autre pratique. L'augmentation est du coté des usages et des technologies. 
   * L'augmentation ouvre
   * 

   * Augmentation : se situe du côté des usages technologiques 
   * technologie permettant d'augmenter les capacités mentales et psychiques
   * Comprise comme une technologie permettant d'augmenter capacités mentales et pshychiques tels alphabets et algorythmes
   * Augmentation = extension virtuelle du lvire dans le web ou avec appli de réalité virtuelle et augmentée 
   * Doivent se situer en dehors d'une cloture dont la fonction reste de devoir délimiter l'oeuvre
   * Prolongement du livre dans un espace extérieur et avec lequle il s'articule
   * Enrichi à l'intérieur et augmenté à l'extérieur

Conclusion 

Foucault : "Le livre est un noeud dans un réseau"

Le livre est un réseau. Le livre augmenté précise le réseau et propose une interface du noeud, c'est un enjeu de design

interface du noeud, représente l'objet-livre numérique —> une question de Design

Quelques travaux : 

- Au bonheur des dames : reprend cette même idée d'un double livre

*Période de questions*

Q. : MVR Le codex a été un espace public, un espace de production commun.
Cet espace peut devenir un espace marchandisé
 Dans quel mesure la production de cet espace est conditionné par les entreprises dont on parle et à quel point il est possible de penser l'espace de l'expression comme une publishing sphere
R.: ES : en rapport aux communs
Impensé radical des processus d'édition , la mémoire de l'oubli
Mémoire de l'oubli : le fait de prendre un livre sans savoir que c'est un livre (comme commun).
Les livres ne peuvent pas être, conçus pensés en dehors de leur lecteur
Le problème de l'histoire litt., le problème du bibliothécaire c'est de ne pas avoir de lecteur. 
À partir du moment où on appréhende que "ça" est un livre, à aucun moment quand on le prend on va se demander combien de générations, de gestes ont fait que je puisse l'avoir ; ils sont là les communs

Le numérique vient percuter cela
Il faut revenir à ce dont on est héritier : 
Prétention contemporaine redoutable quand on aborde l'affordance que l'on a intégré dans notre pensée infra-ordinaire

Dans le monde universitaire, porosité des lecteurs, des étudiants et des auteurs: les universitaires parlent aux universitaires
Il faut amener ces perspectives là sur ce qu'est le contexte de la bibliothèque. Les éditeurs grand public, les bibliothèque représentent pour eux 15-20% [??] de leur marché

Les éditeurs ne veulent pas vendre leurs livres numériques destinés aux lecteurs non-universitaires aux bibliothèques pour ne pas éroder leur marche
Le problème du livre numérique : comment réfléchir à des contraintes universitaires [??]
Il y a des lecteurs mais ce sont les éditeurs qui ne veulent pas vendre plus de livres numériques. 

AL : Nos usages de lectures sont façonnés par Google, Amazon, par des logiques commerciales. La question des usgaes du ivres, ils sont façonnés par Google, Amazon...
Il faudrait essayer de modeler nos propres outils 
Mettre en forme l'information est un enjeu de design et une responsabilité collective

ES : Idée selon laquelle l'écriture est née en même temps que le cadastrage

L'écriture est née en même temps que le cadastrage à Sumer

Pour sortir de cette dichotomie cloture/ouverture posée entre le codex et internet, on pourrait pas sortir de cette dichotomie fermée en proposant quelque chose qui serait d'une autre nature, qui est ce que les  technologies de l'intellect permettent à l'homme d'être rassuré par rapport au monde
Cadastrer l'espace pour des raisons de droit
Orgnaiser l'espace collectif dans un rapport d'usage
organiser l'espace collectif dans un rapport d'usage, d'usage du territoire — c'est à dire limiter le chaos à quelque chose qui soit raisonnable, vivable pour l'humain

On va limiter le chaos à quelque chose qui soit raisonnable et vivable pour l'humain. Geste toujours en rassurance en tension dynamique, car il ne suffit pas.
Lutter contre le chaos, l'inadmissible

Q de Bertrand Gervais : notion d'écosystème au centre duquel était le livre, maintenant c'est l'écran
Goody raison graphique à Bachimont raison computationnelle
Idée de la cloture s'inscrit dans une raison graphique mais plus du tout du côté d'une raison computationnelle. 
R :    Le droit d'auteur interdit ce que la technologie permet
C'est absurde, on se bat pour des fractions de centimes. 
On perd cette idée d'émergence, de plus pertinent, de plus gros, qui serait le commun. 
Qu'est-ce qu'un livre dans l'espace numérique ? François Bon : la nouvelle forme du livre, c'est le site

Qu'est-ce qui distingue un livre d'un site web ? Pour Laborderie, c'est la cloture. Partage point de vue de François Bon : "la nouvelle forme du livre c’est le site". La notion de cloture est terriblement présente dans l'espace numérique. 

Perspective idélaisée d'un environnement ouvert ; ce n'est as le cas, il faut franchir des péages exorbitants


Q : Renée Bourassa ? : propose de parler de la membrane cellulaire, métaphore organique, lieu de circulation entre monde de l'intérieur et de l'extérieur. Nous amène dans une forme de définition du livre. 

Membrane cellulaire plutôt que cloture. Lieu de circulation ente mondes intérieur et extérieur
On a besoin de points dans le web où il y a une sorte de condensation du savoir, de l'imaginaire mais aussi un mouvement centrifuge qui va vers l'extérieur. 
Besoin aussi d'un mouvement centrifuge

R :
   Lire est quelque chose d'éminemment vivant
    
AL : Métaphore de la cellule et du jardin, espace clos mais en expansion
Espace en expansian dans la cellule, dans le jardin, métaphore assez fructueuse pour penser les nouvelles formes du livre dans l'espace numérique. 

Charbonneau: Critique ces métaphores.  enclosures au R-U, émergence de la propriété individuelle [?]
Mouvement des enclosures au Royaume Uni
Dans la cellule, il y a aussi l'infection
La cellule n'est pertinente que s'il y a une masse critique (plusieurs cellules pour faire un corps)
Masse critique : plusieurs cellules pour faire corps
Idée de la rhétorique des clotures qui va profondément vers le droit propriétaires. 

ES : 
Appréhender l'objet en tant qu'objet. Le livre a un pli
Je ne pense pas que l'écritue augmente, elle permet
Le fait de constater d'un point de vue anthopologique que l'Homme invente le cadastrage et l'écriture, c'ets une vraie question. Elle m'interroge, qu'est ce que veux dire ce geste simultané?



##Séance {I} « Interactions, poétiques en contexte numérique »


###{I1} Arthur Perret, « La typographie du livre numérique, entre fluidité et frictions »  

Continuer la réflexion conceptuelle sur l'écriture. Réponses aux pistes ouvertes par Souchier.
Sémiotique de la typographie. Presque tout ce qui reste du papier dans le livre numérique. Point de passage entre l'imprimé et l'écran. Quel sens produire avec un système de signes changeants?
Point de apssage entre l'imprimé et l'écran

l'énonciation typographique de  laufer 1986 : entre deux ages de l'écriture : écriture fixe, écriture en mouvement

Analyser la typo = activité récente, liée à la sortie progressive de l'imprimé
Calssification Vox est quaisment généalogique

Appoche graphique, dimension visuelle des signes. 

Passage d'une écriture fixe à une écriture en mouvemement -> flux

Flux = cristallise enthousiasme et appréhension. Un terme clivant qui suggère des mutations fortes. 
Notre système de signes est aussi notre système de pensée.

Fluidité supposée de l'information 

1. QU'est-ce qu'implique une fluidification du sémiotique ?
Laufer — plasticité minimale des caractères  : pas malléables, plastiques. Restent figés comme des glaçons dans l'océan de la page"glacés"
LEs écrans sont devenus mobiles, variables, plastiques
Apparition des font variables Open Type
Typographie paramétrique : metafont . Générer des versions intermédiaires -> Le dégel de la typographie(mais pas liquéfaction) des signes typographiques,

Familles de caractères avec grande variété de styles, diffère de la recherche formelle de la création graphique

Invention d'une identité visuelle forte. 

2. QU'est-ce qu'on apprend en examinant la manière dont la fluidité se manifeste par des signes?
Le flux est un effet d'optique. Redimensionner la fenêtre, etc : succession d'images fixes du texte. Qu'est-ce qui est dessous, qui permet sa fluidité?
Matérialisation de la fluidité

L'image du texte a été analysée, qu'est-ce que l'architecture du texte ?
Laufer : raison typographique // Goody et sa raison graphique

Nouvelles inventions typographiques : Code, balisage -> du texte qui exprime du texte
Ce qui est fascinant avec le code, c'est du texte ->cf.  architexte de Soucheir


pas de ponctuation, plutôt de la scripturation (Laufer) 
ex : manicule (préfigure l'indexation), tiret (interlocution), balisage ( information), Curseur (intersaction)
L'indexation n'est pas possible sans écriture

Compensation par rapport à l'écrit.
Restituer l'interlocution

Sémiotique (le vu) sémiotique (le lu)

Le cursuer : signe passeur (cf. Soucheir) Curseur, signe passeur -> plasticité inhérente, indiquent qqchose sur l'état de leur environnement, par exemple, l'écran diffère du papier. Valeur d'interaction

AVec le numérique, l'écriture gagne une valeur d'interaction très forte. 

Paramétrage des constituants de l'écriture, fait partie du contrat de lecture. Expérience utilisateur.
Asymétrie par rapport au contrat de lecture. 

Savoir-faire et érudition combinés, incontournable


###{I3} Elsa Tadier, « Corps, livre et design. Quand le numérique invite à revisiter la place du corps dans les dispositifs de lecture » 

Citation de Merleau-Ponty
Placer le corps humain entre la façon dont il est au monde et la façon dont il se l'approprie
Donner une place au faire 

Interroger les dispositifs d'écriture et de lecture de ce qui est appelé le livre numérique -> qualifie des objets très divers, qui n'ont pas toujours une forte composante écrite.
Inscrit dans une histoire symbolique et culturelle entre les supports de l'écrit et les créations graphiques. 
Proximité avec le support qui informe, le corps de l'artisan (designer) et le corps du lecteur. Corps participe des fondements du livre interrogé par le numérique. 

Point historique : Place du corps dans l'histoire matérielle du livre. Pas seulement une intellectualité mobilisée par le livre, mais tout le corps. Affordance, ergonomie

Comment le livre et le corps se répondent ? 
Comment se mouvoir avec le livre?
Un objet que l'homme a façonné au cours des siècles pour le faire sien.  (Par tâtonnements)
Dynamique anthropologique = dynamique d'ajustement du corps et du milieu. 

Corps confronté au livre, pluriel, complexe, émotif, normé, sensoriel
Corps émotif, en mouvement, sensoriel. 

Pour s'inscrire dans la culture des supports de l'écrit, la place du corps est centrale pour une approche en dseign. 
On a l'impression de découvrir l'importance de la mobilisation du corps avec le numérique. Mais la question n'est pas neuve, elle se pose autrement

Référence au mime du geste de tourner la page - la question n'est pas neuve mais se pose autrement avec le numérique. 
Performativité du livre. 

Accomplir une pratique eucharistique domestique à la limite de l'ingestion. Au 15e siècle, les femmes ne peuvent participer à l'Eucharistie. La croyante partage les douleurs du christ grâce au livre. Livre construit autour de l'épisode de la passion. du coeur du livre jaillit le sang. le livre saigne. Entrée dans la chair du Christ, symbolique. Proximité de la croyante avec son ouvrage.
Toutes la pages du livre "saignent" - le livre devient un corps symbolique. 
Un livre qui suscite le corps à corps : Le livre a été frotté, gratté - la vie du livre marque les gestes performatifs. 
Relation d'intimité avec le livre // relation d'intimité avec le christ

Le corps du christ laisse aussi ses marques colorées sur le corps de la croyante. Relation entre l'intensité des pratiques corporelles et le retour de ...

Oeuvres historiques représentant des livres touchés -> inscription dans la culture du rapport au livre du corps.
Le dispositif livresque nous renvoie à la nécessité anthropologique du corps communiquant. Nous inscrit dans notre culture et nous renvoie à notre condition d'humains

rupture sémiotique (Souchier)
Perception du volume à requestionner avec les tablettes. 

Désengagement d'une économie de marché : le lecteur ne doit plus être considéré comme un consommateur de ses objets. 
Interroger d'un pt de vue phénoméno la matérialité propre de ces nouveaux dispositifs de lecture. 

Le design doit dépasser la conception d'un corps stimulé, renouveler le dispositif d'un pdv phénoménologique

Le design a un rôle essentiel car il peut inaugurer un mouvement qui va inscrire les dispo lecture / écritre dans la dimension humaine 
Nous remettre au coeur de nos outils


###{I4} Dominique Raymond, « Potentiel2. Numérique et contrainte pour Savage (*Nathalie*), Dickner et Fortier (*Révolutions*) » 

Potentialité : ouverture, champ des possibles. 

Queneau : ouverture de champ des possibles. 

Considérer la contrainte comme structure portant en germe une potentialité en attente de réalisation
structure impliquant en germe l'idée de potentialité 
Potentiel au carré 

Effets sur la lecture 

Révolution: Nicolas Dickner et Dominique Fortier 

Quotidiennement le site des éditions ALtier (Alto)? a publié des passages de Révolutions
Calendrier 366 jours, trame de l'ouvrage (calendrier révolutionnaire). Plante, animal, etc.
Contrainte d'écriture : aspect thématique (chaque jour les auteurs devaient Composer un commentaire à paritr du thème du jour
2à aspect de contrainte implicite : 366 jours et 4 mains
3. Défi quotidien d'écriture. User de moyens numériques : recevoir par courriel, des minuit et grace à une application : effet de surprise lors de la découverte du mot en question. Allier la routine à la découverte, rejoue la lecture d'un feuilleton. L'éditeur propose de reproduire l'expérience des auteurs ?  pas vraiment. différence écriture et lecture.

Comment se manifeste l'articulation contrainte et numérique : 
Analyse des allusions metatextuelles sur le calendrier etc. 2/3 des entrées conservent soit la contrainte, soit le numérique. Quelques allusions à Perec, Gorey. Les auteurs se demandent ce qu'ils sont en train d'écrire. Provoque l'envie d'écrire d'autres textes à contrainte.

Nathalie, Steve Savage, Quartanier

Publié la même année que l'ouvrage de Dickner

Histoire d'un nom, pas celle d'une personne. 
Compilation de résultats Googles
// Saemmer = récolter une matière essentiellement numérique et l'adapter au format livresque. 
Chaque phrase est resserrée pour commencer par Nathalie est / sera / était (// je me souviens de Perec)
L'ordre de succession des énoncés est fourni par le moteur de recherche
C'est Google qui donne l'ordre de succession. 
4e partie plus meta textuelle : questionne le projet en général

Plusieurs vies de Nathalie : Savage, John Smith (pendant anglo de Nathalie)

// Bootz // Potentiel : déterminé et infini, il ne manque que l'existence



BEAU TRAVAIL LES BÉNÉVOLES DU FRAMAPAD !!! Merci. Luc



*Période de questions*

Q: Polices sens empattements pour le web, avec empattement en imprimé
R: reproduire la ressemblance la plus fidèle avec queneau



##Stands {J} [SÉANCE PARALLÈLE]


###{J1} Jimmy Gagné, « Présentation Album jeunesse animé et narré » 


###{J2} Nicolas Tilly, « Impression postnumérique et remix éditorial »


###{J3} Gisèle Henniges, « Co-création auteur et designer : ergonomie de l’œuvre augmentée et expérience utilisateur » 

###{J4} Marcello Vitali-Rosati, Arthur Juchereau, Servanne Monjour, Nicolas Sauret et Joana Casenave, « Stylo : un éditeur sémantique WYSYWYM pour l'édition savante de demain »


##Atelier d'idéation collective {K} [SÉANCE PARALLÈLE]


##Séance {L} « Interfaces, écritures et design éditorial »


###{L1} Christine Develotte et Mabrouka El Hachani, « L’alliance du papier et de la tablette numérique : les spécificités d’une nouvelle écriture de fiction jeunesse »  
Univers narratif complexe mis en  contexte éducatif.
Grilles de résultats : spécificités d'écriture du livre, de l'application. Sur le niveau de la multiodalité, mais aussi rapport corps et écran
Susciter des interactions adulte enfant. 
multimodialité, rapports corps-écran
articulation du livre et de l'application (temporelle, spatiale, narrative)
spécificité d'exploration des lectures (postures corporelles, gestes prescrits)
articulation du livre et de l'application telle que voulu par l'auteur


Spécificités du livre : 
    Livre 1 : Raconte moi le vent
    texte au lexique un peu recherche, monde de l'enfance, codage visuel, discours des personnages

Livre 2 :
La grande histoire du petit trait : très épuré, pr.dominance de l'espace blanc

Rapport de type transmédia
Pour passer du livre physique au livre numérique : indicateur
Plonger dans d'autres espaces

Structure narrative  Raconte moi le vent : récit de voyage, psges désignés par il ou elle, Raconté et dessiné
La grande histoire du petit trait : enfant amené à vivre une expérience

Jeu renforce l'implication de l'enfant dans l'histoire

Spécificités d'écriture de l'appli ; 
Raconte moi le vent : on a l'audio + image qui s'anime
Signes passeurs, mais le geste à effectuer est le même

La grande histoire du petit trait : audio, animation de dessin, incitation à dessiner ou à gommer (l'enfant fait quelaue chose sur la tablette)
progression de la difficulté. 

Raconte moi le vent : contrainte technique : il faut que l'enfant tende les bras pour enclencher l'animation de l'image
Grande histoire du petit trait : beaucoup plus flexible. Le corps se rapproche de l'écran à chaque action.

4 tableaux interactifs pour Raconte moi le vent

Deux supports indépendants

Spécificités du mode de lecture : raconte oi le vent : besoin de l'aide de l'adulte
Pour Grande histoire du petit trait : progression, gestes simples, l'enfant peut lire seul

Appropriation de l'offre éditoriale - partenariat entre les éditeurs et les bibliothèques de Lyon.
Inviter l'enfant à interagir, puis Temps libre laissé à l'enfant pour qu'il puisse explorer de lui-même

Discussions : 
    - qu'est-ce qu'un livre numérique ? Comment est-il amené à se transformer ?
    - lien entre narratologie et formes ludiques

être plus ou moins ouvert du côté de l'usager - différents types d'ouverture. 
(Raconte moi le vent : mode clos) - (Grande histoire : monde ouvert)

Emergence de nouveaux metiers et compétences. 
Transmission, médiatoin.
Réfléchir sur la question de l'offre destinée au public.
Epistémologie pluraliste : inté

Question d'une épistémologie pluraliste qui intègre plusieurs acteurs dans une réflexion sur la compréhension du monde


###{L2} Florence Rio, Bertrand Meslier et Eric Kergosien, « Le "KIT de rédaction" pour livres interactifs numériques ou la reconfiguration de l’acte d’écriture »  

Le design est envisagé non en terme de réception -> de pencher plutôt sur la production : cocréation.
Design éditorial. Forme d'accompagnement de l'écriture. Promesse d'un outil clef en main de l'écriture. 

La production editoriale peut être une forme d’accompagnement de l’écriture

Objet de la présentation : 2 aspects : le design en termes de production et l’impact du design sur la production

Projet recherche et développement (entreprise «Adrénaline») : livres interactifs numériques
Création de fiction sur un forme sériel/court

Un projet R et D, mené avec une entreprise : Adrénalivre. 
Mission : créer des livres dits interactifs numériques.  Travailler sur le storytelling interactif. 
Créer de la fiction inspirée des livres dont vous êtes le héros. 
Mise en scène : Format court et sériel, saisons et épisodes -> autre univers que la littérature. 

Mettre en scène un autre univers que de la littérature
Mise en visualisation de données complexes, non orienté multi facettes.

Répondre à une commande de l'etnreprise : optimisation
Il fallait le rendre plus ergonique + gagner en performance
Idée = créer un outil qui va permettre d'aider les auteurs à gommer la linéarité de l'écriture, guider l'écriture ench

D'autre part : aide qui permet aux éditeurs de structurer des schémas interactifs fournis. 

Contraintes : l'entreprise demande de demander sur des séries (récurrence dans l'écriture, temps de lecture qui doit être anticipé - traduit par un nombre de signes)

Interface de l'existant : outil se présente en plusieurs carrés représentant les différents modules de l'histoire

Quelle est la commande de l’entreprise ?
Adapter et optimiser un kit de rédaction pour augmener la performance dans le temps
***outil qui aide les auterus à gommer la linéarité de l’écriture et de structurer des schémas interactifs 
******contraintes à cette Criture : travailler sur des séries (récurrence dans l’écriture); temps de lecture à anticiper et rythmé par les choix, donnant cours au récit; 
 

                          
Interface : les auteurs écrivent directement dedans
Pb typographique : typo pas modifiable

interface qui est un écran où l’on peut écrire (intégration de balise pour les choix)

condition du récit enchâssé : arborescence 
logique programmatique qui se rajoute à la logique créative de la littérature

Récit enchâssé → il faut donner des conditions 

Logique programmatique qui vient se surajouter à la logique d’écriture

principe de négociation : il ne s’agit pas juste de transformer (maléabilité d’une interface), mais comprendre le fonctionne d’une écriture interactive (non connu de tous, dont une habitude au logiciel)
interface qui commande/oriente le texte

L’éditeur voulait rendre cela plus joli et plus efficace. 
Pour eux, ils ne s’agissait pas seulement d’une maniabilité d’une interface mais qu’il fallait comprendre le potentiel changement du paradigme d’écriture (+ acculturation au logiciel)

Le logiciel considéré comme un archi texte qui allait commander la structure du texte

Il y avait derrière cela une reconfiguration d’une pratique sociale : l’écriture

reconfiguations d’une pratique sociale, l’écriture, dans ses formes

il fallait interroger les pratiques d’écritures des auteurs (écriture simple + écriture sur logiciel)

protocole : il ne s’agit pas que de modifier des cases graphiques, mais de définir les phases d’écritures

auteurs confirmés et à confirmés

Phase d’entretien + phase d’observation (d’auteurs confirmés et non confirmés)

But : voir si les auteurs peuvent se plier à ce type de rédaction (c'est bien un modèle littéraire)

notion de gouvernementalité numérique : injonction à produire du contenu non littéraire 

régime d’écriture, ou modèle littéraire en soi : gouvernementalité numérique 

incitation du cadre (interface) et de la contrainte (technique : seulement accessible en ligne)
statégie de contournement ou d’évitement face à cette objet/interface

moyens qui permettent de structurer l’écriture : matérialité textuelle et iconique 

Mise en place d'un cadre + d'une forme de contrainte

exemples de matérialités : schématisation avec des cartes; arborescence pour représenter la continuité ou la discontinuité du récit (visualisation graphique)

Est-ce que les auteurs se plient à cette gouvernementalité : plutôt non. 
Les auteurs mettent en place des stratégies de contournement et même de renoncement à cette interface.

organise l’écriture : travail intellectuel esxigent pour cette ériture enchanssée  (de l’écrit à la plateforme)

Ils reviennent quasiment tous à une forme de matérialité (schématisation du récit au travers de cartes par ex, recours à des cartes heuristiques ou arbres de connaissance)
Ils organisent leur écriture : écriture qui demande beaucoup de travail (du fait des récits enchâssés et mise en place de conditions) ->  recours à des grilles 
Utilisation de logiciels de mise en scène de l'enchâssement
Ils extraient parfois des éléments et les copient-collent sur word pour se souvenir de là où ils en sont.

dépossession de l’écriture dans le modèle graphique
instrumentalisation de l’écriture/espace évolutif (gabarit porteur des structures narratives)

Forme de distanciation très forte pour les auteurs entre la production et l'écriture (relation auteurs-éditeurs)

espace à réécrire en permanence 
 
potentialiser une forme de création de l’écriture
les auteurs s’ajustent 
 
modification de l’écriture : processus d’écriture 

Pistes à venir : 
    On est face à une logique programmatique (instrumentalisation de l'écriture) : 
    - kit de rédaction comme  un espace d'assignation évolutive de rôle dans l'écriture
    - kit de rédaction comme espace de réénonciaion
    - kit de rédaction comme espace de négociation des rôles
    - kit de rédaction comme espace à réécrire
    - kit de rédaction comme un business plan (architexte qui nécessite que les auteurs s'ajustent)
    
    Volonté d'automatiser cette forme d'écriture 


###{L3} Nathalie Lacelle, Marie-Christine Beaudry et Prune Lieutier, « Projet de soutien au développement de démarches d’édition numérique jeunesse au Québec »

projet à ses débuts, porté par 9 chercheurs
suite à un appel à projet sur la culture et le numérique
définir un référentiel de compétence de littératie en contexte numérique

Travaillent en termes de compétences + recherches de terrain

disponibilité des oeuvres numériques pour enfant
les enseignants ne savent pas comment remplir leur biblio (offrir des propositions d'oeuvres)

Offrir des oeuvres qui correspondent à l'ensemble des compétences

didactique, art, littérature et écriture + processus cognitifs --> recherche
perspective pluri-sémiotique + processus sémiotiques

le livre entame une mutation irréversible, alors faut aller de l'avant : S'intéressent à l'émergence de nouvelles formes de littérature

critères de littérarité fondés sur l'expérience du lecteur
processus spécifiques au numérique comme critère de littérarité

Spécificités de a litt num reconfigurent rôles éditoriaux

faire vivre une expérience exigeant des compétences de lecture numérique

Particpent avec des équipes de producteurs de livres num pour développer une offre culturelle numérique transmédiale au Québec en vue de production québécoise pour les écoles

Problématique du lectorat : difficultés à naviguer + comprendre lien sémiotique entre geste et lecture
difficulté dans la manipulation du support (tablette) avec la lecture
développement d'une expertise d'édition jeunesse au Québec : proposer des contenus de qualité; découvrabilité des oeuvres 

Comment soutenir la transformation du livre num dans le milieu de l'édition jeunesse québécoise

critères
documenté les enjeux de production, diffusion et réception : y comprendre les points de rupture, càd de la conception jusqu'à l'élève
 
co-construire un outil de référence et élaborer un plan de développement de projet
Idée : comprendre dans la chaîne de prod et diff les points de rupture

Partenaires : acteurs et experts 
Chercheurs associés à des milieux pratiques. Toutes la conception de la recherche est fondée sur cette collaboration

Partenaires consultants : entretiens en cours pour réaliser le livre blanc

avoir des professionnels issues des milieux de l'édition et du numérique qui souhaitent développer la littérature jeunesse

Outils de collecte de données : 
méthodologie : deux collectes de données visant à soutenir le développement d'une expertise dans la littérature jeunesse numérique
d'autres visant au type de recherche (action-formation) visant à accompagner et soutenir les éditeurs dans les changements du numérique pour la production de livres jeunesses
Trois volets : production, diffusion, réception

1er objectif : documenter les pratiques de lectures : 

enquête en ligne auprès d'éditeur, d'enseignant et de bibliothécaire; des entrevues de groupes
entrevues individuels + entrevues de groupes

livre blanc évolutif (co-construction, notamment évolutive)

Résultats préliminaires

élaborer un plan d'action pour orienter les professionnels reliés au milieu du livre jeunesse

50% des répondants ont déjà produit un projet numérique
et autant en n'ont fait

Llivre blanc : pas forcément un guide de bonnes pratiques, mais objectifs = poser les cadres, donner des outils

possibilités de rentabilité ? question avant d'entamer un projet numérique
mode graphique (question de marché dans la réception des oeuvres)
technologie à surveiller

Besoins ont émergés : accompagnement de la chaîne de production

les acteurs doivent apprendre à travailler ensemble 
Changement de culture 
besoin de soutient technique : pour ouvrir les potentiels, estimations $$, afin d'accompagner dans les projets

financements peu adaptés

au Québec particulièrement, demande un indice de rentabilité, ce qui complique l'apparition de tels projets numériques

critères d'évaluation qui soutiennent peu les expérimentations pédagogiques (frein au financement)

beaucoup de projets vont aller pour le grand public, alors qu'ils sont destinés à l'éducation : besoin de clarification du positionnement

temps de travail : temps de recherche est un temps long. Comment travailler avec les startups qui sont sur des temps courts 

Besoin d'un traducteur entre monde de recherche et monde de production techno
comprendre les enjeux de la recherche et de traduction 

*Période de questions*

MVR : compétence technique des enfants et littératie à développer = problématique : formatage de la pensée 
*mode d'écriture sous Word, et recours au papiers, au moins pour structurer (pré-écriture, plan)
le principe de l'écriture appelle à une appropriation du moyen et de l'interface

Compétences techniques à acquérir pour les récits enchâssés

délinéarisation posant problème : du mal à cheminer dans le récit (à penser dans la production littéraire pour enfant, ainsi qu'à l'expérience de manière à ce qu'elle soit enrichissante)

Il faut penser à l'accompagnement en termes de réhaussement de lecture

producteur voulant beaucoup d'interactivité, ne venant pas servir l'histoire, perd les enfants

1) pas un créateur : tout une équipe
2) la production artistique jeunesse doit être précisément destinée au jeune (problèmes de dérive et d'oubli du public cible) : appropriation parfois difficile des supports sans parents, car l'objet n'a pas forcément été pensé pour la jeunesse

Accompagnement nécessaire pour les petits (0-6ans), pour les aider à faire le geste
Pour ceux plus âgés : pas forcément plus simple : ils ont déjà une pratique  et ce qu'on leur propose ne correspond pas à leurs usages habituels

Kit de rédaction : auteurs formés (2h par l'entreprise Adrenalivre)  + tutoriel en ligne + système de suivi avec Slack

Auteurs généralement non professionnels (ne vivent pas de leur plume) variés ; 
- des gens déjà publiés
- des gens non publiés
- des gens qui ont déjà fait de l'interactif
- des gens qui n'en ont encore jamais fait

contrat : pas d'obligation d'utiliser le logiciel d'écriture, mais obligation de rendre son livre sous une forme logicielle (incitation à ne pas utiliser Word) 


##Table ronde {M} « Imaginer des structures d’accompagnement éditorial en contexte numérique » 
Participants : Bertrand Gervais, Benoit Bordeleau, Nathalie Lacelle, Prune Lieutier, Annabelle Moreau

présentation des enjeux liés à leur pratique + proposition de stratégies 

plusieurs organismes québécois contribuent au développement de littérature numérique


1. Opuscule 2 - littérature québécoise mobile : présenter la diversité des productions littéraires au Québec -----» accès, notion importante !
Production Rhizome

Calendrier d'événements littéraires
Idée : présenter ce qui se passe en dehors du Québec

version audio des textes proposés, ce qui rapproche des pratiques auctoriales (souffles du poète, mieux sentir les enjambements) Mise en tension entre texte et voix de l'auteur

une audiothèque dans le cadre de lancement et d'entrevue(conf, tables rondes)

Présenter pas seulement des auteurs consacrés
présent avec les auteurs, auditeurs

agrégateur de blogs inscrits dans la sphère littéraire du Québec

téléchargement gratuit de l'interface pour le cell ou la tablette : développer des outils numériques qui servent la communauté littéraire (le monde du livre numérique peut se servir d'outils simples = motivation à utiliser les nouveaux dispositifs)

Technologie : dorsale Drupal (CMS), crée des JSONS qui sont repris par Angular JS

2. magasine «lettres québécoise» (critique littéraire) : envie de se projeter dans l'avenir, alors qu'il était vieux et dépassé, pour tout changer et tout recentré la stratégie papier (marketing) : nouveaux collaborateurs, nouvelles maquettes, afin de s'inscrire dans le milieu littéraire (chez les lecteurs et les chercheurs)
publiés 4 fois par annees

Ont procédé à des États généraux : été 2016 (une quinzaine de personnes à qui ils ont demandés quelles sont leurs attentes)

succès de la stratégie : développer d'autres projets ailleurs que dans le papier (càd vers le numérique), quelque chose de propre à eux et absent ailleurs

Continuer à faire de la critique littéraire
produire des débats enflammés dans le milieu littéraire
projet pour l'automne prochain
réunion de gens du milieu; invitation de gens de l'extérieur (journalistes, etc)

3. le livre blanc
vise à documenter les aspects prod, diff, réception
hackaton de 2 jours 
enjeu de l'accompagnement de l'éditeur

B.Epron question : le numérique autorise une forme de modulation d'échantillons : est-ce utilisé dans la promotion,?
problématique éthique en jeunesse : accès à certains jeux

outils numériques : alimenter un intérêt pour des formes brèves qui cherchent encore leur place 
faire un portrait de la littérature québécoise actuelle;  essaient de tendre vers l'exhaustivité représentif des pratiques actuelles 

intérêt pour amener les gens à s'interroger sur la littérature (offrir des textes inédits, des extraits)

MERCI A TOUS 

THE END



       * \_\_\_o 
       *  \_<\_\,\_ 
       * (\_)|'(\_)                                     
                                     Prise de note parallèle : 
##Séance {L} « Interfaces, écritures et design éditorial »


###{L1} Christine Develotte et Mabrouka El Hachani, « L’alliance du papier et de la tablette numérique : les spécificités d’une nouvelle écriture de fiction jeunesse »  
Univers narratif complexe mis en  contexte éducatif.
Grilles de résultats : spécificités d'écriture du livre, de l'application. Sur le niveau de la multiodalité, mais aussi rapport corps et écran
Susciter des interactions adulte enfant. 
multimodialité, rapports corps-écran
articulation du livre et de l'application (temporelle, spatiale, narrative)
spécificité d'exploration des lectures (postures corporelles, gestes prescrits)
articulation du livre et de l'application telle que voulu par l'auteur


Spécificités du livre : 
    Livre 1 : Raconte moi le vent
    texte au lexique un peu recherche, monde de l'enfance, codage visuel, discours des personnages

Livre 2 :
La grande histoire du petit trait : très épuré, pr.dominance de l'espace blanc

Rapport de type transmédia
Pour passer du livre physique au livre numérique : indicateur
Plonger dans d'autres espaces

Structure narrative  Raconte moi le vent : récit de voyage, psges désignés par il ou elle, Raconté et dessiné
La grande histoire du petit trait : enfant amené à vivre une expérience

Jeu renforce l'implication de l'enfant dans l'histoire

Spécificités d'écriture de l'appli ; 
Raconte moi le vent : on a l'audio + image qui s'anime
Signes passeurs, mais le geste à effectuer est le même

La grande histoire du petit trait : audio, animation de dessin, incitation à dessiner ou à gommer (l'enfant fait quelaue chose sur la tablette)
progression de la difficulté. 

Raconte moi le vent : contrainte technique : il faut que l'enfant tende les bras pour enclencher l'animation de l'image
Grande histoire du petit trait : beaucoup plus flexible. Le corps se rapproche de l'écran à chaque action.

4 tableaux interactifs pour Raconte moi le vent

Deux supports indépendants

Spécificités du mode de lecture : raconte oi le vent : besoin de l'aide de l'adulte
Pour Grande histoire du petit trait : progression, gestes simples, l'enfant peut lire seul

Appropriation de l'offre éditoriale - partenariat entre les éditeurs et les bibliothèques de Lyon.
Inviter l'enfant à interagir, puis Temps libre laissé à l'enfant pour qu'il puisse explorer de lui-même

Discussions : 
    - qu'est-ce qu'un livre numérique ? Comment est-il amené à se transformer ?
    - lien entre narratologie et formes ludiques

être plus ou moins ouvert du côté de l'usager - différents types d'ouverture. 
(Raconte moi le vent : mode clos) - (Grande histoire : monde ouvert)

Emergence de nouveaux metiers et compétences. 
Transmission, médiatoin.
Réfléchir sur la question de l'offre destinée au public.
Epistémologie pluraliste : inté

Question d'une épistémologie pluraliste qui intègre plusieurs acteurs dans une réflexion sur la compréhension du monde


###{L2} Florence Rio, Bertrand Meslier et Eric Kergosien, « Le "KIT de rédaction" pour livres interactifs numériques ou la reconfiguration de l’acte d’écriture »  

Le design est envisagé non en terme de réception -> de pencher plutôt sur la production : cocréation.
Design éditorial. Forme d'accompagnement de l'écriture. Promesse d'un outil clef en main de l'écriture. 

Un projet R et D, mené avec une entreprise Adrénalivre. 
Mission : créer des livres dits interactifs numériques.  Travailler sur le storytelling interactif. 
Créer de la fiction inspirée des livres dont vous êtes le héros. 
Mise en scène : Format court et sériel, saisons et épisodes -> autre univers que la littérature. 

Mise en visualisation de données complexes, non orienté multi facettes.

Répondre à une commande de l'entreprise : optimisation
Il fallait le rendre plus ergonique + gagner en performance
Idée = créer un outil qui va permettre d'aider les auteurs à gommer la linéarité de l'écriture, guider l'écriture enchâssée. Une aide à structurer des schémas interactifs. 
Contraintes. Adrénalivre demande à ses auteurs de travailler sur des séries : récurrence dans l'écriture et suivi dans l'action. Temps de lecture rythmé par le choix. Plus il y a des choix, plus l'entreprise est satisfaite. 
Mettent à disposition une interface adaptée à leur contexte d'édition. Chaque module est représenté par des carrés. Logique de code se met en place pour l'auteur puisqu'il doit intégrer des balises pour mettre ses choix à disposition. La typographie n'est pas modifiable. Induire des conditions. Si, alors. Logique programmatique qui s'ajoute à la littérature. Arborescence. 

Négociation : travailler sur l'idée de maniabilité de l'interface, et comprendre le potentiel changement de l'écriture. Tous les auteurs n'ont pas forcément pratiqué l'écirutre interactive auparavant. 
L'interface commande le texte. L'idée n'est pas seulement de donner des codes, mais de reconfigurer la pratique sociale de l'écriture. 

L'écriture peut elle être en kit? 
L'idée est qu'ils s'agit d'un modèle d'écriture en luim-même. Gouvenementalité numérique. Mettre en place une gouvernantalité éditoriale numérique. Opposition entre l'écriture pour adrénalivres et l'écriture normale. 
Pour l'entreprise, ne surtout pas utiliser word. Contrainte en matérialité et en technique. Temps de chargement extrêmement lent et bug- sauvegarder le travail en ligne. Stratégies alternatives, les auteurs ne se plient pas tellement à la gouvernementalité numérique. Tous mettent en place des stratégies de contournement, d'évitement, de renoncement. Moyens extrérieurs à l'interface, à l'objet numérique, leur permettant d'organiser leur pratique d'écriture et de se rassurer. Retour à la matérialité textuelle, et ensuite mettent en forme leur texte avec l'interface proposée. Se représenter sur papier la continuité ou discontinuité de leur récit. 
Organisent leur écriture — demande beaucoup de travail. Tous ne vivent pas de leur plume et l'écriture enchassée demande un grand souvenir de ce qu'on a écrit avant, s'ils n'écrivent pas tous les jours toute la journée.
Second architexte : Word. Intègrent le codage dans word pour pouvoir ensuite le coller dans l'interface. 
On est face à une préécriture, et l'interface intervient avec une préécriture obligatoire. Certains ont renoncé aux choix et les ont rejetés sur l'éditeur. 
Ce type de rédaction serait un espace d'assignation évolutive. réénonciation à entrée dans le kit. 
Vision capitaliste de l'écriture à laquelle les auteurs doivent s'ajuster. Volonté d'automatiser une forme d'écriture


###{L3} Nathalie Lacelle, Marie-Christine Beaudry et Prune Lieutier, « Projet de soutien au développement de démarches d’édition numérique jeunesse au Québec »
Disponibilité des oeuvres numériques pour enfant. Offrir des oeuvres de qualité correspondant aux compétences qu'on demande de mobiliser. Cherchers didactique, art, lecture, écriture, Perspective plurisémiotique et processus cognitifs mis en oeuvre avec les nouvelles formes de textualités issues du numérique.

S'intéresser à l'émergence de nouvelles formes. Le livren umérique exige une utilisation pertinente des particularités du numérique. Penser l'expérience spécifique au numérique comme faisant partie des critères de littérarité pouvant qualifier les oeuvres. Reconfiguration de la place des acteurs du monde éditorial. 
Faire vivre une vraie expérience : pourtant, décus par l'offre. Développer une offre culturelle numérique transmédiale au québec. Reste marginal. 
Difficultés des enfants à comprendre le lien entre l'histoire et le geste de navigation.
Comment soutenir la transmission dans le milieu de l'edition jeunesse qc. S'assurer de sa découvrabilité. 

Documenter les enjeux, les besoins, pour construire un outil de référence pour développer un plan pour le projet et chacun de ses acteurs. 
Se rendre de la conception jusqu'à l'élève. 
Collaboration entre le milieu pratique et les chercheurs. 
Hybridité et multimodalité. 
Mettre sur pied le livre blanc de l'édition numérique jeunesse au québec.
Accompagner les intervenants dans le développement de leurs projets. Les guider dans leur manière de penser le projet.

Outils de collecte de données. 
Soutenir le dévelopement de l'expertise 

   * documenter la production, la diffusion, la réception. Recherche Action/formation imposé par le frqsc
Enquêtes en ligne auprès des éditeurs, des enseignants, des bibliothécaires. Entrevues individuelles et de groupe. Recension écrite aussi. 
Le livre blanc sera évolutif et numérique.
Plan d'action avec les éditeurs. Entrevues individuelles. Séances de travail. 

Donner les bons réflexes, poser un cadre. 
Les éditeurs veulent avoir des données sur le marché. Peu de données sur les modes d'achats des livres numériques. 
Outils pratiques, listes de contacts, de concours. 
Informations sur la technologue à surveiller. Comprendre aujourd'hui avant d'explorer demain. 

Vrai besoin d'accompagnement des acteurs de la chaine de production. S'accoutumer au numérique. Changement de culture. besoin de soutien technique. besoin d'un chef technique. Anticiper les usages futurs. 
Financement peu accessible et peu adapté. Rentre dans peu de cases. Soutiennent peu l'expérimentation à l'usage pédagogique. Mettre en place des mécaniques de revenus, qui ne seraient pas là si le financement avait été adapté. 
Défi de compreéhension et d'arrimage entre les secteurs. Chronologie de travail : respecter les temps d'itération de chacun. Manque des « traducteurs » entre les maillons. Comment on traduit ça en séquence numérique. Transformer le didacticte en numérique. 
Grande variété des équipements et intérêt et formation des enseignants.

Q.MVR : cométences techniques des enfants et littératie digitale à développer. Les outils formatent ce que les enfants apprennent — gérer l'apprentissage du numérique et formatage par les plateformes. Workflow produit par les acteurs commerciaux. 
R. Deux temps : aussi recours au papier. principe de l'écriure appelle une appropriation du moyen. Compétence technique à acquérir pour l'écriture enchâssée. Recours externe pour structurer, une préécriture. 
MVR : on ne réfléchit même pas à pourquoi on utilise word
R: c'est le seul éditeur dans l'oeil des auteurs.
R: délinéarisation semble poser problème. Multimodalité difficile, les enfants sont privés d'une partie de l'hisoire et ont du mal à cheminer dans le récit. Qu'est-ce qui peut être une expérience enrichissante? 
Certains producteurs veulent mettre beaucoup d'interactivité même si ça ne sert pas l'histoire. Ex : le souffle, les enfants crachent. Méconnaissance de l'usage des enfants des ipads. Regard d'adulte, décalage avec les enfants. les tout petits doivent être accompagnés. Ados, développent des pratiques formatées ne correspondant pas à un usage adapté. 

Q. Utilisation du kit, intégrée dans le format? auteurs formés à l'interface, qui sont les auteurs?
R. Ils sont formés, formation de prise en main et tutoriel en ligne par adrénalivres. formation 2h, sur place ou par skype. Suivi avec slack. Jeu de complémentarité et d'accompagnement. Limites : ne vivent pas de l'écriture, écrivent le soir alors que personne n'est à l'entreprise pour leur répondre. Tous les profils d'auteurs. Recherche de l'autoédition. l'application peut donner lieu à une publication hors adrénalivre. le contenu peut être créé sans être éditorialisé. Ces auteurs-là ne sont pas formés. Obligation de rendre son livre sur une forme logicielle. On les incite à ne pas utiliser word et à tout de suite travailler.




##Table ronde {M} « Imaginer des structures d’accompagnement éditorial en contexte numérique » 

Participants : Bertrand Gervais, Benoit Bordeleau, Nathalie Lacelle, Prune Lieutier, Annabelle Moreau
 Stratégies éditoriales, mise de l'avant de répertoire, aide à la prise de décision. Livre numérique ou livre augmenté.
Opuscules 2 : développement partenariat littérature québécoise mobile. Présenter ce qui se passe à la grandeur du territoire. Evenements, finalistes de prix littéraires, version audio par les écrivains et les écrivaines. Rapprochement de la pratique des auteurs ; rythme, enjambement des poètes. 
Pratiques naissent sur les plateformes, encore en rodage parfois. Spectacles, mises en lecture, mettent à l'essai des textes devant public. Réflexe d'enregistement se crée à l'extérieur du projet. 
Agrégateur regroupe 175 blogues littéraires. présenter ce qui se passe sur la scène littéraire actuelle. Site web et web app. Interface directe, sans passer par itunes. outils numériques qui servent la communauté littéraire. Aider plutôt que compliquer la vie d'une communauté d'emblée plutôt résistante. Drupal. 

Annabelle Moreau : Magazine lettres québécoises, (1976) , porté longtemps par une même équipe. Renouvellement de l'équipe en 2017. Mission de critique littéraire. Exploration et diffusion de la litt qc et franco can. 
Recentrer la stratégie papier, nouvelle maquette. nouveaux collaborateurs. se réinscrire dans le milieu littéraire et répondre aux attentes et aux besoin des gens du milieu. chercheurs, lecteurs, auteurs, éditeurs. 
2015 : états généraux : vouloir un magazine plus attirant . 
à l'avenir, des balados critiques. peu présente dans les médias. produire des débats enflammés et intéressants. 45 min, se laisser le temps de débattre. ça va se retrouver sur opuscules. 

Prune: accompagnement des producteurs, des éditeurs. recommandations aux pouvoirs publics. Mise en place de programmes d'éducation au numérique.

BG : avoir à la table à la fois revue papier litt, qui sent le besoin de l'appel d'air numérique, 
La fabrique du livre répond à des enjeux numériques. mettre en vis a vis des gens préoccupés par l'avenir du livre et ceux qui le renouvellent. Proposer des livres numériques, partager le territoire. 
LQM interface entre différents acteurs, dans un milieu en pleine transformation. 

Q. Reader's digest, échantillons à lire. Le numérique autorise une forme de modulation, d'échantillons des chapitres. Acheter la suite. Est-ce que cette forme d'échantillon est envisageable pour le numérique? 
R: freemium. Mécanique d'achat. laisser es enfants être hookés : éthique. 
R: lectures lors des lancements. stratégies de vente. alimenter un intérêt pour les formes brèves qui vendent moins. outil intéressant pour ça. Auteurs arrêtent leurs lectures avant la fin.  offrir une diversité de pratiques. piquer la curiosité.
Conserver des traces. 
AM: donner le plus d'informations possibles pour favoriser la réflexion. textes inédits? 

