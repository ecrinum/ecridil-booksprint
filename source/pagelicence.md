# pagelicence {.frontmatter .pagelicence}
<!-- ce titre ne doit pas apparaitre (display:none) -->

Ce livre est sous licence Creative Commons CC0 Public Domain

Source : https://framagit.org/ecrinum/ecridil-booksprint

&nbsp;

ISBN 978-2-924446-11-9 (imprimé)

ISBN 978-2-924446-12-6 (PDF)


![logoCRCEN](img/logo-crcen.jpg) ![logoexsitu](img/logo-exsitu.jpg) ![logoCRSH](img/logo-crsh.jpg)
