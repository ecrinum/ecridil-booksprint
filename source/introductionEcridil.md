# ÉCRIDIL2018 à Montréal {.frontmatter}

Après plus d’une décennie d’expérimentations sur le livre numérique (depuis
l’introduction des premières liseuses et l’adoption du standard epub), les
discours soutiennent soudain l’idée d’une certaine faillite de la révolution
annoncée pour le livre dans un contexte numérique, à grands renforts d’études
sur la préférence des jeunes pour les livres sur support papier et de
statistiques sur les ventes directes de livres numériques. C’est pourtant là
faire l’impasse de transformations profondes dans les pratiques de production et
d’usage du livre aujourd’hui – pensons au succès de la plateforme Kindle
d’Amazon, aux nouveaux modes de circulation du savoir scientifique (qu’ils
soient liés aux enjeux du libre accès, aux empires éditoriaux ou aux modèles
discursifs réinventés), aux formes numériques de diffusion de la culture, ainsi
qu’aux statistiques impressionnantes d’adhésion au prêt numérique d’ouvrages
dans les bibliothèques publiques. Ce sont là des traces avérées d’un brassage
certain dans le monde éditorial.

S’il y a certainement une conception conventionnelle du livre qui persiste dans
les esprits et les préférences des lecteurs (rattachée à son acquisition, à sa
manipulation, à sa possession physique sous forme de codex), un examen du
spectre large des manifestations du livre aujourd’hui ne peut écarter une
pénétration avérée du numérique dans ses modalités d’existence. Ainsi
sommes-nous encouragés à (re)poser des questions en apparence bien triviales,
mais en réalité fondamentales. Qu’est-ce qu’un livre&nbsp;? Comment a-t-il été appelé
à se transformer en contexte numérique&nbsp;? Qui en sont les acteurs et les auteurs&nbsp;? Mais aussi, de façon lucide et prospective, quels défis persistent, malgré les
avancées récentes et à la faveur d’un environnement en constante évolution&nbsp;?
L’ouverture du regard aux réalités (encore plus) complexes du livre aujourd’hui
s’impose, de sorte d’irriguer ce secteur de perspectives critiques éclairantes.

Afin de s’engager dans un examen collectif de ces problématiques transversales,
l’événement ÉCRIDIL a été créé à l’Université de Nîmes en 2016 à l’initiative de
Stéphane Vial. Dans les «&nbsp;Catalactes&nbsp;» publiés dans la foulée du premier
événement, ce dernier précise ainsi le projet&nbsp;: «&nbsp;[ÉCRiDil] souhaite interroger
la chaîne du livre à partir de la culture de l’innovation sociale et numérique
(*Digital social innovation*). Il s’agit d’appréhender sans les séparer les
trois usages fondamentaux de la chaîne du livre (ÉCRire, éDIter, Lire) dans une
perspective systémique et intégrative qui s’inspire du modèle émergent des
innovations globales&nbsp;» (Vial et Catoir-Brisson, 2017). C’est dans le même esprit
que s’est tenue à Montréal la deuxième édition d’ÉCRIDIL. Le dialogue
interdisciplinaire favorisé à cette occasion se reflète dans la pluralité des
approches représentées par les membres du comité d’organisation de l’édition
2018&nbsp;: René Audet (études littéraires), Renée Bourassa (design), Oriane
Deseilligny (sciences de l’information et de la communication), Michael
Eberle-Sinatra (études anglaises et édition), Bertrand Gervais (études
littéraires et pratiques numériques), Stéphane Vial (philosophie et design),
Josée Vincent (littérature et édition) et Marcello Vitali-Rosati (littérature et
philosophie). Suscitant les rencontres disciplinaires, ÉCRIDIL fait aussi une
place de choix aux praticiens – designeurs, éditeurs, etc. – afin d’enrichir les
échanges entre théoriciens et praticiens de l’édition et du livre. C’est ainsi
l’écosystème du livre dans son ensemble qui pendant deux jours s’est trouvé au
cœur de nos discussions, sur un substrat commun de culture numérique, une
complexité que cet ouvrage cherche à synthétiser à travers une proposition
éditoriale originale.

## ecridil.ex-situ.info/
