#!/bin/sh

echo "wget pageTitre.md"

wget https://mypads.framapad.org/p/pagetitre-7atz17fv/export/markdown?auth_token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJsb2dpbiI6Imxha29uaXMiLCJrZXkiOiJjamdncHQxdG4wMDczMW1yN2Z6bGpza3A1IiwiaWF0IjoxNTI0NzU4NDM5fQ.1YEkyC_F6byg2gt51aftEPsG-bsGQb9QazedSh3qZBk -O source/pageTitre.md

echo "wget chapitre1.md"

wget https://mypads.framapad.org/p/chapitre1-43tx1780/export/markdown?auth_token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJsb2dpbiI6Imxha29uaXMiLCJrZXkiOiJjamdncHQxdG4wMDczMW1yN2Z6bGpza3A1IiwiaWF0IjoxNTI0NzU4NDM5fQ.1YEkyC_F6byg2gt51aftEPsG-bsGQb9QazedSh3qZBk -O source/chapitre1.md

echo "wget chapitre2.md"

wget https://mypads.framapad.org/p/chapitre2-aqty17ey/export/markdown?auth_token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJsb2dpbiI6Imxha29uaXMiLCJrZXkiOiJjamdncHQxdG4wMDczMW1yN2Z6bGpza3A1IiwiaWF0IjoxNTI0NzU4NDM5fQ.1YEkyC_F6byg2gt51aftEPsG-bsGQb9QazedSh3qZBk -O source/chapitre2.md

echo "pandoc html"

pandoc --standalone --ascii --template=templates/templatebook.html5 --section-divs -f markdown -t html5 ./source/pageTitre.md ./source/chapitre1.md ./source/chapitre2.md book.yaml -o book.html
