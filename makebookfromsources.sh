#!/bin/sh

echo "pandoc html"

# pandoc --standalone --ascii --template=templates/templatebook.html5 --section-divs -f markdown -t html5  ./source/chapitre2.md book.yaml -o chapter2.html

pandoc --standalone --ascii --template=templates/templatebook.html5 --section-divs -f markdown -t html5 \
  ./source/couv.md \
  ./source/pagetitre.md \
  ./source/pagelicence.md \
  ./source/introductionEcridil.md \
  ./source/makingoff.md \
  ./contenus_key/architecture.md \
  ./contenus_key/cloture.md \
  ./contenus_key/collectif.md \
  ./contenus_key/espacepublic.md \
  ./contenus_key/forme.md \
  ./contenus_key/gestedecriture.md \
  ./contenus_key/jeu.md \
  ./contenus_key/humanites_numeriques.md \
  ./contenus_key/materialite.md \
  ./contenus_key/mouvement.md \
  ./contenus_key/parcours.md \
  ./contenus_key/semiotique.md \
  ./contenus_key/sensorialite.md \
  ./contenus_key/transferts.md \
  ./contenus_key/typographie.md \
  ./source/intervenantsliste.md \
  ./source/contributeurs.md \
  ./source/tablematiere.md \
  book.yaml \
  -o book.html

node index.js


  # ./contenus_key/fragment.md \
  # ./contenus_key/immersion.md \
  # ./contenus_key/litteratie.md \
  # ./contenus_key/pluridisciplinarite.md \
